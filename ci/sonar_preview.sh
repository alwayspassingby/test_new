#!/bin/bash

	sonar-scanner \
    -Dsonar.host.url=http://127.0.0.1:9000 \
    -Dsonar.login=admin \
    -Dsonar.password=admin \
    -Dsonar.analysis.mode=preview \
    -Dsonar.gitlab.project_id=$CI_PROJECT_ID \
    -Dsonar.gitlab.commit_sha=$CI_COMMIT_SHA \
    -Dsonar.gitlab.ref_name=$CI_COMMIT_REF_NAME \
    -Dsonar.projectKey=test \
    -Dsonar.sources=/Users/shen/test

if [ $? -eq 0 ]; then
    echo "sonarqube code-analyze-preview over."
fi
