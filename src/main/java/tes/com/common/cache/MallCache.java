package tes.com.common.cache;

import tes.com.common.entity.user.AutoHomeUser;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by liuzhongdu on 2017/12/24.
 */
public class MallCache {

    public static Map<String, Object> turboInterfaces = new ConcurrentHashMap<String, Object>();

    public static Map<String, AutoHomeUser> autoHomeUserLogin = new ConcurrentHashMap<String, AutoHomeUser>();

}
