package tes.com.common.entity.apicase;

import java.util.Date;

/**
 * Created by liuzhongdu on 2017/7/9.
 */
public class BaseAPICase {
    private Integer aId ;

    //    API URL
    private String httpURL;

    //    api 类型 0:为http接口 1:为turbo接口
    private Integer type;

    //    RPC 接口的服务
    private String rpcService;

    //    RPC 接口的调用方法
    private String rpcMethod;

    //    RPC 接口版本
    private String rpcVersion;

    //    RPC 分组
    private String rpcGroup;

    private String httpHeaders;

    //    请求的方法类型，目前支持 0：get和 1：post
    private Integer httpRequestType;

    //    Http连接类型,如果是 RPC 接口不需要
    private String httpContentType;

    //    cookies设置, 如果是 RPC 接口不需要
    private String httpCookies;

    //    1：form类型 用于http表单请求和 RPC 参数 2：raw-json类型，内容为JSON字符串 3：raw-text类型，内容为文本
    private Integer parameterType = 1;

    //    请求参数
    private String requestParameter;

    //    请求说明
    private String description;

    //    断言类型 :0：Http状态码断言 1：json内容断言  2：文本内容断言 3、调通即成功
    private Integer expectedType;

    //    断言的字段（key）
    private String expectedKey;

    //    断言表达式类型，0:isNotNull, 1:isNull, 2:isEqualTo, 3:isNotEqualTo, 4:contains, 5:doesNotContain
    private Integer expression;

    //    断言的值
    private String expectedValue;

    //   用例状态 0：有效用例 1 删除用例
    private Integer status ;

    //    用例创建时间
    private Date createTime;

    //    用例修改时间
    private Date modifyTime;

    public BaseAPICase() {
    }

    public String getHttpHeaders() {
        return httpHeaders;
    }

    public void setHttpHeaders(String httpHeaders) {
        this.httpHeaders = httpHeaders;
    }

    public Integer getaId() {
        return aId;
    }

    public void setaId(Integer aId) {
        this.aId = aId;
    }

    public String getHttpURL() {
        return httpURL;
    }

    public void setHttpURL(String httpURL) {
        this.httpURL = httpURL;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getRpcService() {
        return rpcService;
    }

    public void setRpcService(String rpcService) {
        this.rpcService = rpcService;
    }

    public String getRpcMethod() {
        return rpcMethod;
    }

    public void setRpcMethod(String rpcMethod) {
        this.rpcMethod = rpcMethod;
    }

    public String getRpcVersion() {
        return rpcVersion;
    }

    public void setRpcVersion(String rpcVersion) {
        this.rpcVersion = rpcVersion;
    }

    public String getRpcGroup() {
        return rpcGroup;
    }

    public void setRpcGroup(String rpcGroup) {
        this.rpcGroup = rpcGroup;
    }

    public Integer getHttpRequestType() {
        return httpRequestType;
    }

    public void setHttpRequestType(Integer httpRequestType) {
        this.httpRequestType = httpRequestType;
    }

    public String getHttpContentType() {
        return httpContentType;
    }

    public void setHttpContentType(String httpContentType) {
        this.httpContentType = httpContentType;
    }

    public String getHttpCookies() {
        return httpCookies;
    }

    public void setHttpCookies(String httpCookies) {
        this.httpCookies = httpCookies;
    }

    public Integer getParameterType() {
        return parameterType;
    }

    public void setParameterType(Integer parameterType) {
        this.parameterType = parameterType;
    }

    public String getRequestParameter() {
        return requestParameter;
    }

    public void setRequestParameter(String requestParameter) {
        this.requestParameter = requestParameter;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getExpectedType() {
        return expectedType;
    }

    public void setExpectedType(Integer expectedType) {
        this.expectedType = expectedType;
    }

    public String getExpectedKey() {
        return expectedKey;
    }

    public void setExpectedKey(String expectedKey) {
        this.expectedKey = expectedKey;
    }

    public Integer getExpression() {
        return expression;
    }

    public void setExpression(Integer expression) {
        this.expression = expression;
    }

    public String getExpectedValue() {
        return expectedValue;
    }

    public void setExpectedValue(String expectedValue) {
        this.expectedValue = expectedValue;
    }


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}
