package tes.com.common.entity.apicase;

import java.util.Map;

/**
 * Created by liuzhongdu on 2017/7/9.
 */
public class TestCase extends BaseAPICase {

    private Boolean isMotan = true;

    private int groupId;

    private Integer taskId;

    //所属组
    private String groupName;

    private Integer gcId;

    private Integer priority;

    // 请求参数
    private Map<String,String> parameterMap;

    private String requestBody;

    //http cookies
    private Map<String,String> cookiesMap;

    private Map<String,String> httpHeaderMap;


    private String host;

    private Integer tuserId;

    public Map<String, String> hostsMap;

    //    全局变量的key
    private String staticKey;

    public String getStaticKey() {
        return staticKey;
    }

    public void setStaticKey(String staticKey) {
        this.staticKey = staticKey;
    }

    public Boolean getMotan() {
        return isMotan;
    }

    public void setMotan(Boolean motan) {
        isMotan = motan;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public Map<String, String> getHttpHeaderMap() {
        return httpHeaderMap;
    }

    public void setHttpHeaderMap(Map<String, String> httpHeaderMap) {
        this.httpHeaderMap = httpHeaderMap;
    }

    public Integer getTuserId() {
        return tuserId;
    }

    public void setTuserId(Integer tuserId) {
        this.tuserId = tuserId;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public Map<String, String> getParameterMap() {
        return parameterMap;
    }

    public void setParameterMap(Map<String, String> parameterMap) {
        this.parameterMap = parameterMap;
    }

    public Map<String, String> getCookiesMap() {
        return cookiesMap;
    }

    public void setCookiesMap(Map<String, String> cookiesMap) {
        this.cookiesMap = cookiesMap;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public Map<String, String> getHostsMap() {
        return hostsMap;
    }

    public void setHostsMap(Map<String, String> hosts) {
        this.hostsMap = hosts;
    }

    public Integer getGcId() {
        return gcId;
    }

    public void setGcId(Integer gcId) {
        this.gcId = gcId;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
