package tes.com.common.entity.capture;

import java.util.Date;

/**
 * Created by liuzhongdu on 2018/9/8.
 */
public class APICapture {

    private Integer id;

    /**
     * 抓取的url地址
     */
    private String capture_url;

    /**
     * 抓取的类型，0 目前为swagger-v2版本
     */
    private Integer capture_type;

    /**
     * 描述
     */
    private String description;

    /**
     * 状态，0：有效，1：无效
     */
    private Integer status;

    /**
     * 消息类型，0为不通知
     */
    private Integer capture_msg_type;

    private Date createTime;

    private Date modifyTime;

    private String crontab_time;

    private Date last_run_time;


    public String getCrontab_time() {
        return crontab_time;
    }

    public void setCrontab_time(String crontab_time) {
        this.crontab_time = crontab_time;
    }

    public Date getLast_run_time() {
        return last_run_time;
    }

    public void setLast_run_time(Date last_run_time) {
        this.last_run_time = last_run_time;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCapture_url() {
        return capture_url;
    }

    public void setCapture_url(String capture_url) {
        this.capture_url = capture_url;
    }

    public Integer getCapture_type() {
        return capture_type;
    }

    public void setCapture_type(Integer capture_type) {
        this.capture_type = capture_type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCapture_msg_type() {
        return capture_msg_type;
    }

    public void setCapture_msg_type(Integer capture_msg_type) {
        this.capture_msg_type = capture_msg_type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}
