package tes.com.common.entity.group;

/**
 * @Description: 分组实体类
 * @date 2018/01/09 14:05
 */
public class APIGroup {
    private Integer groupId;
    private String  groupName;
    private String hosts;
    private String description;


    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getHosts() {
        return hosts;
    }

    public void setHosts(String hosts) {
        this.hosts = hosts;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
