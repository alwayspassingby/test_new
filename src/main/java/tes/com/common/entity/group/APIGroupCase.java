package tes.com.common.entity.group;

/**
 * Created by liuzhongdu on 2017/7/1.
 */
public class APIGroupCase {
    private Integer Id;
    private Integer groupId;
    private Integer caseId;
    /**
     * 优先级：3，2，1，0 默认最低优先级为0
     */
    private Integer priority = 0;
    private Integer before_caseID;
    private Integer isRun;
    private Integer status;

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getCaseId() {
        return caseId;
    }

    public void setCaseId(Integer caseId) {
        this.caseId = caseId;
    }

    public Integer getBefore_caseID() {
        return before_caseID;
    }

    public void setBefore_caseID(Integer before_caseID) {
        this.before_caseID = before_caseID;
    }

    public Integer getIsRun() {
        return isRun;
    }

    public void setIsRun(Integer isRun) {
        this.isRun = isRun;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
