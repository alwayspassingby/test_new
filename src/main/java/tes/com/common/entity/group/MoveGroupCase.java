package tes.com.common.entity.group;

/**
 * Created by baoan on 2018/4/13.
 */
public class MoveGroupCase {
    private Integer fromGroupId;
    private Integer toGroupId;
    private Integer caseId;

    public Integer getFromGroupId() {
        return fromGroupId;
    }

    public void setFromGroupId(Integer fromGroupId) {
        this.fromGroupId = fromGroupId;
    }

    public Integer getToGroupId() {
        return toGroupId;
    }

    public void setToGroupId(Integer toGroupId) {
        this.toGroupId = toGroupId;
    }

    public Integer getCaseId() {
        return caseId;
    }

    public void setCaseId(Integer caseId) {
        this.caseId = caseId;
    }
}
