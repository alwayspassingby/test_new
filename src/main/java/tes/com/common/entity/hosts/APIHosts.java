package tes.com.common.entity.hosts;

import java.util.Date;

/**
 * Created by liuzhongdu on 2017/12/30.
 */
public class APIHosts {
    //    ID
    private Integer hId;
    //    域名
    private String host;
    //    IP地址
    private String ip;

    //    描述信息
    private String description;

    //    状态 0代表有效，1代表无效
    private Integer status;

    //    用例创建时间
    private Date createTime;

    //    用例修改时间
    private Date modifyTime;


    public Integer gethId() {
        return hId;
    }

    public void sethId(Integer hId) {
        this.hId = hId;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}
