package tes.com.common.entity.query;

/**
 * Created by liuzhongdu on 2018/1/7.
 */
public class BaseQueryParam {
    /**
     * 每页几条
     */
    private Integer limit = 10;

    /**
     * 请求的数据的位置
     */
    private Integer offset = 0;

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }
}
