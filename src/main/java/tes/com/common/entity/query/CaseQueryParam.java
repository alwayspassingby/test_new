package tes.com.common.entity.query;

public class CaseQueryParam extends BaseQueryParam {

    private Integer caseId;
    private String urlId;
    private String descriptionId;
    private String rpcGroup;
    private String rpcMethodId;
    private String serviceId;
    private Integer typeId;

    public String getRpcMethodId() {
        return rpcMethodId;
    }

    public void setRpcMethodId(String rpcMethodId) {
        this.rpcMethodId = rpcMethodId;
    }

    public String getRpcGroup() {
        return rpcGroup;
    }

    public void setRpcGroup(String rpcGroup) {
        this.rpcGroup = rpcGroup;
    }

    public String getDescriptionId() {
        return descriptionId;
    }

    public void setDescriptionId(String descriptionId) {
        this.descriptionId = descriptionId;
    }

    public Integer getCaseId() {
        return caseId;
    }

    public void setCaseId(Integer caseId) {
        this.caseId = caseId;
    }

    public String getUrlId() {
        return urlId;
    }

    public void setUrlId(String urlId) {
        this.urlId = urlId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }
}
