package tes.com.common.entity.query;

/**
 * Created by liuzhongdu on 2018/3/12.
 */
public class GroupQueryParam extends BaseQueryParam {
    private Integer query_gId;
    private String query_gName;
    private String query_description;
    private Integer isSelected;
    private String searchText;

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public Integer getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(Integer isSelected) {
        this.isSelected = isSelected;
    }

    public Integer getQuery_gId() {
        return query_gId;
    }

    public void setQuery_gId(Integer query_gId) {
        this.query_gId = query_gId;
    }

    public String getQuery_gName() {
        return query_gName;
    }

    public void setQuery_gName(String query_gName) {
        this.query_gName = query_gName;
    }

    public String getQuery_description() {
        return query_description;
    }

    public void setQuery_description(String query_description) {
        this.query_description = query_description;
    }
}
