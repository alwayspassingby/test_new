package tes.com.common.entity.query;

/**
 * Created by liuzhongdu on 2018/1/7.
 */
public class HostQueryParam extends BaseQueryParam {

    private Integer queryId;

    private String queryIp;

    private String queryHost;


    public Integer getQueryId() {
        return queryId;
    }

    public void setQueryId(Integer queryId) {
        this.queryId = queryId;
    }

    public String getQueryIp() {
        return queryIp;
    }

    public void setQueryIp(String queryIp) {
        this.queryIp = queryIp;
    }

    public String getQueryHost() {
        return queryHost;
    }

    public void setQueryHost(String queryHost) {
        this.queryHost = queryHost;
    }
}
