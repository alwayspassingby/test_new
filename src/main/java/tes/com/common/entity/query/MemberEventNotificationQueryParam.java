package tes.com.common.entity.query;

/**
 * Created by baoan on 2018/3/25.
 */
public class MemberEventNotificationQueryParam extends BaseQueryParam {

    private String source;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
