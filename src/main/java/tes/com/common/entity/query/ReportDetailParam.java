package tes.com.common.entity.query;

/**
 * Created by baoan on 2018/3/25.
 */
public class ReportDetailParam extends BaseQueryParam {
    private String taskId;
    private String version;
    private Integer isSuccess;


    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(Integer isSuccess) {
        this.isSuccess = isSuccess;
    }
}
