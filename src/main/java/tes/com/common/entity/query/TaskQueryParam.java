package tes.com.common.entity.query;

/**
 * Created by liuzhongdu on 2018/1/7.
 */
public class TaskQueryParam extends BaseQueryParam {
    private Integer taskId;
    private String taskName;
    private String description;

    /**
     * 是否选择群组
     */
    private Integer isSelected;

    private String searchText;

    public Integer getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(Integer isSelected) {
        this.isSelected = isSelected;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
