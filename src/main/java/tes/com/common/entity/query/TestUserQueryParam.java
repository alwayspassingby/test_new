package tes.com.common.entity.query;

/**
 * Created by liuzhongdu on 2018/1/7.
 */
public class TestUserQueryParam extends BaseQueryParam {

    private Integer query_tuId;

    private String query_description;

    private String query_tuName;

    private Integer query_type;

    public Integer getQuery_tuId() {
        return query_tuId;
    }

    public void setQuery_tuId(Integer query_tuId) {
        this.query_tuId = query_tuId;
    }

    public String getQuery_description() {
        return query_description;
    }

    public void setQuery_description(String query_description) {
        this.query_description = query_description;
    }

    public String getQuery_tuName() {
        return query_tuName;
    }

    public void setQuery_tuName(String query_tuName) {
        this.query_tuName = query_tuName;
    }

    public Integer getQuery_type() {
        return query_type;
    }

    public void setQuery_type(Integer query_type) {
        this.query_type = query_type;
    }
}
