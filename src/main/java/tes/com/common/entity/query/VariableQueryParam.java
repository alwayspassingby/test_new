package tes.com.common.entity.query;

public class VariableQueryParam extends BaseQueryParam {

    private Integer caseId;
    private Integer groupId;
    private Integer taskId;
    private Integer vId;
    private Integer isSelected = 1;

//    /**
//     * 用例变量名称
//     */
//    private String vname;
//
//    /**
//     * 用例变量值
//     */
//    private String vkey;

    /**
     * 变量的名称
     */
    private String v_name;

    /**
     * 变量的值
     */
    private String v_value;

    /**
     * 描述
     */
    private String description;

    /**
     * 全局变量类型
     * 0:系统变量  1：为群组变量  2：任务变量 4:case变量
     */
    private Integer variable_type;

    private String searchText;

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getV_name() {
        return v_name;
    }

    public void setV_name(String v_name) {
        this.v_name = v_name;
    }

    public String getV_value() {
        return v_value;
    }

    public void setV_value(String v_value) {
        this.v_value = v_value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getVariable_type() {
        return variable_type;
    }

    public void setVariable_type(Integer variable_type) {
        this.variable_type = variable_type;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public Integer getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(Integer isSelected) {
        this.isSelected = isSelected;
    }

    public Integer getvId() {
        return vId;
    }

    public void setvId(Integer vId) {
        this.vId = vId;
    }

    public Integer getCaseId() {
        return caseId;
    }

    public void setCaseId(Integer caseId) {
        this.caseId = caseId;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

//    public String getVname() {
//        return vname;
//    }
//
//    public void setVname(String vname) {
//        this.vname = vname;
//    }
//
//    public String getVkey() {
//        return vkey;
//    }
//
//    public void setVkey(String vkey) {
//        this.vkey = vkey;
//    }
}
