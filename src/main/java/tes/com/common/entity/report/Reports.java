package tes.com.common.entity.report;

import java.util.Date;

public class Reports {

    //    ID
    private Integer taskId;
    //    域名
    private Integer version;

    //    执行时间
    private Date excuteTime;

    private Integer totalCase;

    private Integer successCase;

    private Integer failCase;

    private Integer status;

    private String taskPerson;




    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Date getExcuteTime() {
        return excuteTime;
    }

    public void setExcuteTime(Date excuteTime) {
        this.excuteTime = excuteTime;
    }

    public Integer getTotalCase() {
        return totalCase;
    }

    public void setTotalCase(Integer totalCase) {
        this.totalCase = totalCase;
    }

    public Integer getSuccessCase() {
        return successCase;
    }

    public void setSuccessCase(Integer successCase) {
        this.successCase = successCase;
    }

    public Integer getFailCase() {
        return failCase;
    }

    public void setFailCase(Integer failCase) {
        this.failCase = failCase;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTaskPerson() {
        return taskPerson;
    }

    public void setTaskPerson(String taskPerson) {
        this.taskPerson = taskPerson;
    }



}
