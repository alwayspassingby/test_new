package tes.com.common.entity.result;

/**
 * Created by liuzhongdu on 2017/7/9.
 */
public class APIBaseResult {
    private Integer code;

    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
