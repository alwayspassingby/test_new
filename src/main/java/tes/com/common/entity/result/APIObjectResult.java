package tes.com.common.entity.result;

/**
 * Created by liuzhongdu on 2018/1/8.
 */
public class APIObjectResult<T> extends APIBaseResult {

    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


}
