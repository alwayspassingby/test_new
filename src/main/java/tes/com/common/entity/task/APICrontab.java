package tes.com.common.entity.task;

import java.util.Date;

/**
 * Created by liuzhongdu on 2018/2/9.
 * 定时任务
 */
public class APICrontab {
    /**
     * id 自增
     */
    private Integer id;
    /**
     * 任务ID
     */
    private Integer taskId;

//    /**
//     * 0：为按每天固定时间执行
//     * 1：为小时递增执行
//     * 2：为分钟递增执行
//     */
//    private Integer crontab_type;

    /**
     * 定时任务表达式
     * 0：为按每天固定时间执行，例如：每天8：15 执行一次，对应： 0 15 8
     * 1：为小时递增执行，例如：每3个小时执行一次，对应：0 0 3
     * 2：为分钟递增执行，例如：每3分钟执行一次，对应：0 3 0
     */
    private String crontab_time;

    /**
     * 测试负责人
     */
    private String taskPerson;
    /**
     * 最后执行时间
     */
    private Date last_run_time;
    /**
     * 状态，0 有效，1无效
     */
    private Integer status;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改实际
     */
    private Date modifyTime;

    public String getTaskPerson() {
        return taskPerson;
    }

    public void setTaskPerson(String taskPerson) {
        this.taskPerson = taskPerson;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getCrontab_time() {
        return crontab_time;
    }

    public void setCrontab_time(String crontab_time) {
        this.crontab_time = crontab_time;
    }

    public Date getLast_run_time() {
        return last_run_time;
    }

    public void setLast_run_time(Date last_run_time) {
        this.last_run_time = last_run_time;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

//    public Integer getCrontab_type() {
//        return crontab_type;
//    }
//
//    public void setCrontab_type(Integer crontab_type) {
//        this.crontab_type = crontab_type;
//    }
}
