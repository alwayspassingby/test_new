package tes.com.common.entity.task;

import tes.com.common.entity.group.APIGroup;

import java.util.Date;
import java.util.List;

/**
 * Created by baoan on 2017/12/23.
 */
public class APITask {

    private Integer taskId;
    private String taskName;
    private String description;
    private List<APIGroup> groupList;
    private Date createTime;
    private Integer status;
    private String reporter;
    private String taskPerson;
    private Integer sendType;

    public String getTaskPerson() {
        return taskPerson;
    }

    public void setTaskPerson(String taskPerson) {
        this.taskPerson = taskPerson;
    }


    public String getReporter() {
        return reporter;
    }

    public void setReporter(String reporter) {
        this.reporter = reporter;
    }



    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<APIGroup> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<APIGroup> groupList) {
        this.groupList = groupList;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSendType() {
        return sendType;
    }

    public void setSendType(Integer sendType) {
        this.sendType = sendType;
    }
}
