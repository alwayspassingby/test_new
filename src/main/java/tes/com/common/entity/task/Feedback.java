package tes.com.common.entity.task;

/**
 * Created by baoan on 2018/4/7.
 */
public class Feedback {
    private Integer feedbackId;
    private String feedbackContent;

    public Integer getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(Integer feedbackId) {
        this.feedbackId = feedbackId;
    }

    public String getFeedbackContent() {
        return feedbackContent;
    }

    public void setFeedbackContent(String feedbackContent) {
        this.feedbackContent = feedbackContent;
    }
}
