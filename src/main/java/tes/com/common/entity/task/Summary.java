package tes.com.common.entity.task;

/**
 * Created by baoan on 2018/5/2.
 */
public class Summary {
    /*
    *   数据类型：1、今日数据。2、昨日数据。3、一周数据
    * */
    private Integer type;
    private Integer TotalTask;
    private Integer FailTask;
    private Integer TotalCase;
    private Integer SuccessCase;
    private Integer FailCase;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getTotalTask() {
        return TotalTask;
    }

    public void setTotalTask(Integer totalTask) {
        TotalTask = totalTask;
    }

    public Integer getFailTask() {
        return FailTask;
    }

    public void setFailTask(Integer failTask) {
        FailTask = failTask;
    }

    public Integer getTotalCase() {
        return TotalCase;
    }

    public void setTotalCase(Integer totalCase) {
        TotalCase = totalCase;
    }

    public Integer getSuccessCase() {
        return SuccessCase;
    }

    public void setSuccessCase(Integer successCase) {
        SuccessCase = successCase;
    }

    public Integer getFailCase() {
        return FailCase;
    }

    public void setFailCase(Integer failCase) {
        FailCase = failCase;
    }
}
