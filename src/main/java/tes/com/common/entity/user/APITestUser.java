package tes.com.common.entity.user;

import java.util.Date;

/**
 * Created by liuzhongdu on 2018/1/21.
 */
public class APITestUser {
    //    用户ID
    private Integer tuId;
    //    用户名称
    private String tname;
    //    用户密码
    private String tpwd;
    //    用户秒杀
    private String description;
    //    用户类型 1：安卓端 2，IOS端  3，web端'
    private Integer type;
    //    用户状态
    private Integer t_status;

    //    用例创建时间
    private Date createTime;

    //    用例修改时间
    private Date modifyTime;


    public Integer getTuId() {
        return tuId;
    }

    public void setTuId(Integer tuId) {
        this.tuId = tuId;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getTpwd() {
        return tpwd;
    }

    public void setTpwd(String tpwd) {
        this.tpwd = tpwd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Integer getT_status() {
        return t_status;
    }

    public void setT_status(Integer t_status) {
        this.t_status = t_status;
    }
}
