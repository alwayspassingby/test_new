package tes.com.common.entity.user;

import java.util.Map;

/**
 * Created by liuzhongdu on 2018/1/31.
 */
public class AutoHomeUser extends APITestUser {

    private String UserId;
    private String UserPwd;
    private String NickName;
    private String MobilePhone;
    private String Minpic;
    private String SessionLogin;
    private String PcpopClub;

    private Map<String, String> cookieMap;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getUserPwd() {
        return UserPwd;
    }

    public void setUserPwd(String userPwd) {
        UserPwd = userPwd;
    }

    public String getNickName() {
        return NickName;
    }

    public void setNickName(String nickName) {
        NickName = nickName;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        MobilePhone = mobilePhone;
    }


    public String getMinpic() {
        return Minpic;
    }

    public void setMinpic(String minpic) {
        Minpic = minpic;
    }

    public String getSessionLogin() {
        return SessionLogin;
    }

    public void setSessionLogin(String sessionLogin) {
        SessionLogin = sessionLogin;
    }

    public String getPcpopClub() {
        return PcpopClub;
    }

    public void setPcpopClub(String pcpopClub) {
        PcpopClub = pcpopClub;
    }

    public Map<String, String> getCookieMap() {
        return cookieMap;
    }

    public void setCookieMap(Map<String, String> cookieMap) {
        this.cookieMap = cookieMap;
    }
}
