package tes.com.common.entity.user;

public class YZJUserInfo extends APITestUser {

    private String uid;

//    private Date createTime;

    private String phone;

    private String department;

    private String networkSubType;

    private String oauth_token_secret;

    private Integer userType;

    private Integer appClientId;

    private String orgId;

    private String id;

    private String token;

    private String oauth_token;

    private String name;

    private String wbUserId;

    private String userName;

    private String tid;

    private Integer status;

    private String companyName;

    private String orgInfoId;

    private String wbNetworkId;

    private String oId;

    private String openId;


//    private String personId;
    private String eid;

    private String deviceId;

    private String deviceType;

    private String ua;

    private String loginUrl;


    public String getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

    public String getUa() {
        return ua;
    }

    public void setUa(String ua) {
        this.ua = ua;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getNetworkSubType() {
        return networkSubType;
    }

    public void setNetworkSubType(String networkSubType) {
        this.networkSubType = networkSubType;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

//    public Date getCreateTime() {
//        return createTime;
//    }
//
//    public void setCreateTime(Date createTime) {
//        this.createTime = createTime;
//    }

    public String getOauth_token_secret() {
        return oauth_token_secret;
    }

    public void setOauth_token_secret(String oauth_token_secret) {
        this.oauth_token_secret = oauth_token_secret;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public Integer getAppClientId() {
        return appClientId;
    }

    public void setAppClientId(Integer appClientId) {
        this.appClientId = appClientId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getOauth_token() {
        return oauth_token;
    }

    public void setOauth_token(String oauth_token) {
        this.oauth_token = oauth_token;
    }

    public String getWbUserId() {
        return wbUserId;
    }

    public void setWbUserId(String wbUserId) {
        this.wbUserId = wbUserId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOrgInfoId() {
        return orgInfoId;
    }

    public void setOrgInfoId(String orgInfoId) {
        this.orgInfoId = orgInfoId;
    }

    public String getWbNetworkId() {
        return wbNetworkId;
    }

    public void setWbNetworkId(String wbNetworkId) {
        this.wbNetworkId = wbNetworkId;
    }

    public String getoId() {
        return oId;
    }

    public void setoId(String oId) {
        this.oId = oId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

//    public String getPersonId() {
//        return personId;
//    }
//
//    public void setPersonId(String personId) {
//        this.personId = personId;
//    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
}
