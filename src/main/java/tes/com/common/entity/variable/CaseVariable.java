package tes.com.common.entity.variable;

public class CaseVariable extends Variable {

    private Integer caseId;

//    private Integer gc_vId;
//
//
//    public Integer getGc_vId() {
//        return gc_vId;
//    }
//
//    public void setGc_vId(Integer gc_vId) {
//        this.gc_vId = gc_vId;
//    }

    public Integer getCaseId() {
        return caseId;
    }

    public void setCaseId(Integer caseId) {
        this.caseId = caseId;
    }

}
