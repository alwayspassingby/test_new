package tes.com.controller;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import tes.com.common.entity.capture.APICapture;
import tes.com.common.entity.result.APIBaseResult;
import tes.com.service.apiService.CaptureService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

@Controller
@RequestMapping("/capture")
public class CaptureController {

    @Resource
    CaptureService captureService;

    /**
     * @return
     */
    @RequestMapping(value = "/captureManager", method = RequestMethod.GET)
    public String captureManager() {
        return "captureCaseManager";
    }


    /**
     * 添加抓取配置
     *
     * @param apiCapture
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/addCaptureConfig", method = RequestMethod.POST)
    @ResponseBody
    public Object addCaptureConfig(@ModelAttribute("apiCapture") APICapture apiCapture,
                                   HttpServletRequest request, HttpServletResponse response) throws Exception {

        APIBaseResult apiBaseResult = new APIBaseResult();

        if (StringUtils.isEmpty(apiCapture.getCapture_url())) {
            apiBaseResult.setCode(10);
            apiBaseResult.setMessage("URL不能为空！");
            return apiBaseResult;
        }

        if (!apiCapture.getCapture_url().matches("^(http|https)://[^\\s]*")) {
            apiBaseResult.setCode(10);
            apiBaseResult.setMessage("URL格式不正确（支持HTTPS和HTTP）！");
            return apiBaseResult;
        }

        if (apiCapture.getCapture_type() != 0) {
            apiBaseResult.setCode(10);
            apiBaseResult.setMessage("目前只支持swagger2.0版本");
            return apiBaseResult;
        }


        apiCapture.setStatus(0);
        apiCapture.setCapture_msg_type(0);
        apiCapture.setCreateTime(new Date());
        apiCapture.setCrontab_time("* 0/1 * * * ? *");
        apiCapture.setLast_run_time(new Date());

        return captureService.addCaptureConfig(apiCapture);

    }


}
