package tes.com.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import tes.com.common.entity.apicase.TestCase;
import tes.com.common.entity.query.CaseQueryParam;
import tes.com.common.entity.result.APIBaseResult;
import tes.com.common.entity.result.APIObjectResult;
import tes.com.service.apiServiceImp.APICaseService;
import tes.com.util.JSONUtil;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by liuzhongdu on 2016/11/14.
 */
@Controller
@RequestMapping("/main")
public class CaseController {

    @Resource
    private APICaseService apiCaseService;


    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(HttpServletRequest request, HttpServletResponse response) {

        return "index";
    }

    @RequestMapping(value = "/addHttpCase")
    public ModelAndView httpCase(@ModelAttribute("testCase") TestCase testCase) {
        Map<String,Object> data = new HashMap<String,Object>();
        data.put("apiCase",JSONUtil.beanToString(testCase));
        return new ModelAndView("addHttpCase",data);
    }

    @RequestMapping(value = "/addRpcCase")
    public ModelAndView addRpcCase(@ModelAttribute("testCase") TestCase testCase) {
        Map<String,Object> data = new HashMap<String,Object>();
        data.put("apiCase",JSONUtil.toJSON(testCase));
        return new ModelAndView("addRpcCase",data);
    }

    @RequestMapping(value = "/searchCase", method = RequestMethod.GET)
    public String searchCase() {
        return "searchCase";
    }


    @RequestMapping(value = "/modifyCase", method = RequestMethod.GET)
    public ModelAndView modifyCase(@RequestParam(value = "id", required = false) Integer id, @RequestParam(value = "isModify", required = false) Boolean isModify) {
        Object returnAPICase = getAPICaseByID(id);
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("caseId", id);
        if (isModify == null || isModify) {
            data.put("isModify", true);
        } else {
            data.put("isModify", false);
        }
        data.put("apiCase", JSONUtil.toJSON(returnAPICase));
        return new ModelAndView("modifyCase", data);
    }


    /**
     * 添加用例
     *
     * @param testCase
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/addCase", method = RequestMethod.POST)
    @ResponseBody
    public Object addCase(@ModelAttribute("mallAPICase") TestCase testCase,
                          HttpServletRequest request, HttpServletResponse response) throws Exception {
        APIBaseResult returnResult = new APIBaseResult();
        if (testCase.getType() != 0 && testCase.getType() != 1) {
            returnResult.setCode(10);
            returnResult.setMessage("type为0代表是http,为1代表 rpc");
            return returnResult;
        }

        if (testCase.getType() == 0) {
//            if (!testCase.getHttpURL().matches("^(http|https)://[^\\s]*")) {
//                returnResult.setCode(10);
//                returnResult.setMessage("请求的URL格式不正确（支持HTTPS和HTTP）！");
//                return returnResult;
//            }

            if (testCase.getHttpRequestType() != 0 && testCase.getHttpRequestType() != 1) {
                returnResult.setCode(10);
                returnResult.setMessage("未知请求方式(支持POST和GET请求)");
                return returnResult;
            }

            if (testCase.getParameterType() < 1 || testCase.getParameterType() > 3) {
                returnResult.setCode(10);
                returnResult.setMessage("未知参数类型！");
                return returnResult;
            }


        } else if (testCase.getType() == 1) {
            if (testCase.getRpcService() == null || testCase.getRpcService().isEmpty()) {
                returnResult.setCode(10);
                returnResult.setMessage("RPC接口服务名不能为空！");
                return returnResult;
            }

            if (testCase.getRpcMethod() == null || testCase.getRpcMethod().isEmpty()) {
                returnResult.setCode(10);
                returnResult.setMessage("RPC接口方法不能为空！！");
                return returnResult;
            }

            testCase.setHttpRequestType(0);
        } else {
            returnResult.setCode(10);
            returnResult.setMessage("用例类型错误！");
            return returnResult;
        }

//        判断断言的类型
        if (testCase.getExpectedType() < 0 || testCase.getExpectedType() >3) {
            returnResult.setCode(10);
            returnResult.setMessage("请选择检查结果类型！");
            return returnResult;
        }

        if (testCase.getExpectedType() == 0 || testCase.getExpectedType() ==3){
            if (testCase.getExpression() == null) {
                returnResult.setCode(10);
                returnResult.setMessage("请选择表达式！");
                return returnResult;
            }

            if (testCase.getExpectedValue() == null || testCase.getExpectedValue().trim().length() == 0) {
                returnResult.setCode(10);
                returnResult.setMessage("请输入期望结果！");
                return returnResult;
            }
        }else if(testCase.getExpectedType() ==1 || testCase.getExpectedType() != 2){
            if (testCase.getExpression() == null) {
                returnResult.setCode(10);
                returnResult.setMessage("请选择表达式！");
                return returnResult;
            }

            if (testCase.getExpression() < 0 || testCase.getExpression()>5) {
                returnResult.setCode(10);
                returnResult.setMessage("未知的表达式类型！");
            }

            if (testCase.getExpectedKey() == null || testCase.getExpectedKey().trim().length() == 0) {
                returnResult.setCode(10);
                returnResult.setMessage("请输入期望结果！");
                return returnResult;
            }

            if (testCase.getExpectedValue() == null || testCase.getExpectedValue().trim().length() == 0) {
                returnResult.setCode(10);
                returnResult.setMessage("请输入期望结果！");
                return returnResult;
            }

        }else {}

        //设置用例的状态为有效状态
        testCase.setStatus(0);

        if (testCase.getaId() == null || testCase.getaId() == 0) {
            testCase.setCreateTime(new Date());
            int count = apiCaseService.addCase(testCase);
//            int countGroup = apiCaseService.insertCaseGroup(testCase.getGroupId(), testCase.getaId(),  0);
//            if (count == 1 && countGroup == 1) {
            if (count == 1) {
                returnResult.setCode(0);
                returnResult.setMessage("插入成功，ID：" + testCase.getaId());
                return returnResult;
            } else {
                returnResult.setCode(101);
                returnResult.setMessage("插入失败");
                return returnResult;
            }
        } else {
            testCase.setModifyTime(new Date());
            int count = apiCaseService.updateCase(testCase);
            if (count == 1) {
                returnResult.setCode(0);
                returnResult.setMessage("更新成功，ID：" + testCase.getaId());
                return returnResult;
            } else {
                returnResult.setCode(101);
                returnResult.setMessage("更新失败！");
                return returnResult;
            }

        }
    }

    /**
     * 删除用例
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/deleteCase", method = RequestMethod.GET)
    @ResponseBody
    public Object deleteCase(@RequestParam(value = "id", required = false) Integer id) {

        APIBaseResult returnResult = new APIBaseResult();
        if (id == null || id == 0) {
            returnResult.setCode(10);
            returnResult.setMessage("用例ID错误！");
            return returnResult;
        }
        if (apiCaseService.deleteCaseById(id) == 0) {
            returnResult.setCode(101);
            returnResult.setMessage("删除失败");
            return returnResult;
        } else {
            returnResult.setCode(0);
            returnResult.setMessage("删除成功");
            return returnResult;
        }

    }

    /**
     * 查询用例
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/queryAPICaseList", method = RequestMethod.GET)
    @ResponseBody
    public Object queryAPICase(@ModelAttribute("caseQueryParam") CaseQueryParam caseQueryParam,
                               HttpServletRequest request, HttpServletResponse response) throws Exception {
        return apiCaseService.queryAPICases(caseQueryParam);

    }


    /**
     * 通过ID 获取APICase
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/getAPICaseByID", method = RequestMethod.GET)
    @ResponseBody
    public Object getAPICaseByID(@RequestParam(value = "id", required = false) Integer id) {
        APIObjectResult objectResult = new APIObjectResult();
        if (id == null || id == 0) {
            objectResult.setCode(101);
            objectResult.setMessage("id没有指定");
            return objectResult;
        }
        TestCase testCase = apiCaseService.getCaseById(id);
        if (testCase == null) {
            objectResult.setCode(201);
            objectResult.setMessage("没有找到该记录！");
        }
        objectResult.setData(testCase);
        objectResult.setCode(0);
        objectResult.setMessage("");
        return objectResult;
    }


//    /**
//     * 调用调试接口，用户调试页面
//     * @param testCase
//     * @return
//     */
//    @RequestMapping(value = "/deBugCase", method = RequestMethod.POST)
//    @ResponseBody
//    public Object runCase(@ModelAttribute("mallAPICase") TestCase testCase) {
//        return null;
//
//    }


    /**
     * @param id
     * @return
     */
    @RequestMapping(value = "/executeCase", method = RequestMethod.GET)
    public ModelAndView executeCase(@RequestParam(value = "id", required = false) Integer id) {

        APIObjectResult<TestCase> returnAPICase = (APIObjectResult<TestCase>) getAPICaseByID(id);

        Map<String,Object> data = new HashMap<String,Object>();
        if (returnAPICase.getCode() == 0 && returnAPICase.getData()!=null){
            data.put("caseId",id);
            if (id != null && returnAPICase.getData().getType() == 1){
                return new ModelAndView("executeRpcCase", data);
            }else {
                return new ModelAndView("executeHttpCase", data);
            }
        }else {
            return new ModelAndView("executeHttpCase", data);
        }
    }


    @RequestMapping(value = "/executeRpcCase", method = RequestMethod.GET)
    public String executeRpcCase() {
        return "executeRpcCase";
    }

    @RequestMapping(value = "/executeHttpCase", method = RequestMethod.GET)
    public String executeHttpCase() {
        return "executeHttpCase";
    }


}
