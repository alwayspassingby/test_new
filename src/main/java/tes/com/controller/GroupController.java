package tes.com.controller;


import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import tes.com.common.entity.apicase.TestCase;
import tes.com.common.entity.group.APIGroup;
import tes.com.common.entity.group.APIGroupCase;
import tes.com.common.entity.group.MoveGroupCase;
import tes.com.common.entity.query.GroupQueryParam;
import tes.com.common.entity.result.APIBaseResult;
import tes.com.common.entity.result.APIObjectResult;
import tes.com.common.entity.task.APITask;
import tes.com.service.apiService.GroupService;
import tes.com.service.apiService.TaskService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author wenxiaolong
 * @Description: 分组controller
 * @date 2018/01/09 14:05
 */
@Controller
@RequestMapping("/group")
public class GroupController {
    //    private final static Logger logger = LoggerFactory.getLogger(GroupController .class);
    @Resource
    private GroupService groupService;

    @Resource
    private TaskService taskService;


    /**
     * 请求访问群组列表
     *
     * @return
     */
    @RequestMapping(value = "/groupManager", method = RequestMethod.GET)
    public String groupManager() {
        return "groupManager";
    }


    /**
     * 请求 指定任务下的群组列表（场景列表）
     *
     * @return
     */
    @RequestMapping(value = "/groupListByTask", method = RequestMethod.GET)
    public Object groupListByTaskId(String taskId) {
        int task_id = 0;

        if (StringUtils.isEmpty(taskId)) {
            task_id = 0;
        } else {
            try {
                task_id = Integer.parseInt(taskId);
            } catch (Exception e) {
                task_id = 0;
            }
        }
        Map<String, Object> data = new HashMap<String, Object>();
        APITask task = taskService.getTaskById(task_id);
        if (task == null) {
            data.put("taskId", 0);
            data.put("taskName", "");
        } else {
            data.put("taskId", task.getTaskId());
            data.put("taskName", task.getTaskName());
        }
        return new ModelAndView("groupListByTask", data);
    }


    /**
     * 获取group列表
     */
    @RequestMapping(value = "getGroupList", method = RequestMethod.POST)
    @ResponseBody
    public Object getGroupList(GroupQueryParam groupListQueryParam) {
        return groupService.queryGroupList(groupListQueryParam);
    }

    @ResponseBody
    @RequestMapping(value = "/addGroup")
    public APIObjectResult<Object> addGroup(APIGroup apigroup) {
        APIObjectResult<Object> mallObjectResult = new APIObjectResult<Object>();
        Integer result;
        try {
            result=groupService.addGroup(apigroup);
            if (1==result) {
                mallObjectResult.setCode(0);
                mallObjectResult.setMessage("添加成功，ID：" + apigroup.getGroupId());
            } else {
                mallObjectResult.setCode(101);
                mallObjectResult.setMessage("添加失败！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mallObjectResult;
    }
    @ResponseBody
    @RequestMapping(value = "/updateGroup")
    public APIObjectResult<Object> updateGroup(APIGroup apigroup, HttpServletRequest request) {
        APIObjectResult<Object> objectResult = new APIObjectResult<Object>();
        Integer result;
        try {
            result = groupService.updateGroup(apigroup);
            if (1==result) {
                objectResult.setCode(0);
                objectResult.setMessage("更新成功，ID：" + apigroup.getGroupId());
            } else {
                objectResult.setCode(101);
                objectResult.setMessage("更新失败！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return objectResult;
    }

    @RequestMapping(value = "/deletegroup")
    @ResponseBody
    public Object deleteCase(@RequestParam(value = "id", required = false) Integer id) {

        APIBaseResult returnResult = new APIBaseResult();
        if (id == null || id == 0) {
            returnResult.setCode(10);
            returnResult.setMessage("分组ID错误！");
            return returnResult;
        }
        if (groupService.deleteGroupById(id) == 0) {
            returnResult.setCode(101);
            returnResult.setMessage("删除失败");
            return returnResult;
        } else {
            returnResult.setCode(0);
            returnResult.setMessage("删除成功");
            return returnResult;
        }

    }
    /**
     * 通过ID 获取APICase
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/getGroupByID")
    @ResponseBody
    public Object getGroupByID(@RequestParam(value = "id", required = false) Integer id) {
        APIObjectResult<APIGroup> groupresult = new APIObjectResult<APIGroup>();
        if (id == null || id == 0) {
            groupresult.setCode(101);
            groupresult.setMessage("id没有指定");
            return groupresult;
        }
        APIGroup apiGroup=groupService.getGroupById(id);
        if (apiGroup== null) {
            groupresult.setCode(201);
            groupresult.setMessage("没有找到该记录！");
            return groupresult;
        }
        groupresult.setData(apiGroup);
        groupresult.setCode(0);
        groupresult.setMessage("");
        return groupresult;
    }

    @RequestMapping(value = "/getCaseListByGroup", method = RequestMethod.GET)
    @ResponseBody
    public Object getCaseListByGroup(GroupQueryParam groupListQueryParam) {

        APIObjectResult<TestCase> objectResult = new APIObjectResult<TestCase>();

        if (groupListQueryParam.getQuery_gId() == null || groupListQueryParam.getQuery_gId() == 0) {
            objectResult.setCode(101);
            objectResult.setMessage("分组ID不正确");
            return objectResult;
        }
        if (!StringUtils.isEmpty(groupListQueryParam.getSearchText())){
            groupListQueryParam.setSearchText(groupListQueryParam.getSearchText().trim());
        }

        return groupService.getCaseListByGroupId(groupListQueryParam);
    }


    @RequestMapping(value = "/addGroupCase", method = RequestMethod.GET)
    @ResponseBody
    public Object addGroupCase(APIGroupCase groupCase) {

        APIBaseResult objectResult = new APIBaseResult();

        if (groupCase.getGroupId() == null || groupCase.getGroupId() == 0) {
            objectResult.setCode(101);
            objectResult.setMessage("分组ID不正确");
            return objectResult;
        }

        if (groupCase.getCaseId() == null || groupCase.getCaseId() == 0) {
            objectResult.setCode(101);
            objectResult.setMessage("分组ID不正确");
            return objectResult;
        }
        groupCase.setBefore_caseID(0);
        groupCase.setIsRun(0);
        groupCase.setStatus(0);
        return groupService.addGroupCase(groupCase);
    }


    @RequestMapping(value = "/deleteGroupCase", method = RequestMethod.GET)
    @ResponseBody
    public Object deleteGroupCase(APIGroupCase groupCase) {

        APIBaseResult objectResult = new APIBaseResult();

        if (groupCase.getGroupId() == null || groupCase.getGroupId() == 0) {
            objectResult.setCode(101);
            objectResult.setMessage("分组ID不正确");
            return objectResult;
        }

        if (groupCase.getCaseId() == null || groupCase.getCaseId() == 0) {
            objectResult.setCode(101);
            objectResult.setMessage("分组ID不正确");
            return objectResult;
        }
        return groupService.deleteGroupCase(groupCase);
    }

    @RequestMapping(value = "/isExist",method = RequestMethod.GET)
    @ResponseBody
    public Object isExist(@RequestParam(value = "groupId", required = false) Integer groupId){

        return groupService.isExist(groupId);
    }

    @RequestMapping(value = "/moveGroupCase",method = RequestMethod.GET)
    @ResponseBody
    public Object moveGroupCase(MoveGroupCase moveGroupCase){
        APIBaseResult objectResult = new APIBaseResult();

        if (moveGroupCase.getCaseId() == null || moveGroupCase.getFromGroupId() == null || moveGroupCase.getToGroupId() == null) {
            objectResult.setCode(101);
            objectResult.setMessage("迁移数据不能为空");
            return objectResult;
        }
        return groupService.moveGroupCase(moveGroupCase);
    }


    /**
     * 设置优先级
     * @param id
     * @param priority
     * @return
     */
    @RequestMapping(value = "/setGroupCasePriority",method = RequestMethod.GET)
    @ResponseBody
    public Object setGroupCasePriority(String  id,String priority ){
        APIBaseResult baseResult = new APIBaseResult();
        if (StringUtils.isEmpty(id)){
            baseResult.setCode(101);
            baseResult.setMessage("id 不能为空！");
            return baseResult;
        }
        if (StringUtils.isEmpty(priority)){
            baseResult.setCode(101);
            baseResult.setMessage("priority 不能为空！");
            return baseResult;
        }
        Integer gcId = null;
        Integer gcPriority = null;
        try {
            gcId = Integer.parseInt(id);
            if (gcId < 1 ){
                baseResult.setCode(101);
                baseResult.setMessage("没有找到相应的id!");
                return baseResult;
            }
        }catch (Exception e){
            baseResult.setCode(101);
            baseResult.setMessage("id格式不正确，必须为数字");
            return baseResult;
        }

        try {
            gcPriority = Integer.parseInt(priority);
            if (gcPriority < 0 || gcPriority > 3 ){
                baseResult.setCode(101);
                baseResult.setMessage("输入的priority不支持！");
                return baseResult;
            }
        }catch (Exception e){
            baseResult.setCode(101);
            baseResult.setMessage("priority格式不正确，必须为数字(0-3)");
            return baseResult;
        }

        return groupService.updateGroupCasePriority(gcId,gcPriority);
    }


}
