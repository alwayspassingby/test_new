package tes.com.controller;

import tes.com.common.entity.hosts.APIHosts;
import tes.com.common.entity.query.HostQueryParam;
import tes.com.common.entity.result.APIBaseResult;
import tes.com.common.entity.result.APIObjectResult;
import tes.com.service.apiService.HostService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by liuzhongdu on 2017/12/30.
 */
@Controller
@RequestMapping("/host")
public class HostsController {

    @Resource
    private HostService hostService;


    @RequestMapping(value = "/hostList", method = RequestMethod.GET)
    public String hostList() {
        return "hostsSearchList";
    }


    /**
     * 添加hosts
     *
     * @param apiHosts
     * @return
     */
    @RequestMapping(value = "/addHosts", method = RequestMethod.POST)
    @ResponseBody
    public Object addHosts(APIHosts apiHosts) {
        APIBaseResult returnResult = new APIBaseResult();
        if (apiHosts.getHost() == null || apiHosts.getHost().isEmpty()) {
            returnResult.setCode(107);
            returnResult.setMessage("host为空！");
            return returnResult;
        }

        if (apiHosts.getIp() == null || apiHosts.getIp().isEmpty()) {
            returnResult.setCode(107);
            returnResult.setMessage("ip地址为空！");
            return returnResult;
        }

        Object object = hostService.insertHosts(apiHosts);

        return object;

    }


    /**
     * 更新host
     *
     * @param apiHosts
     * @return
     */
    @RequestMapping(value = "/updateHosts", method = RequestMethod.POST)
    @ResponseBody
    public Object updateHosts(APIHosts apiHosts) {
        APIBaseResult returnResult = new APIBaseResult();
        if (apiHosts.gethId() == null || apiHosts.gethId() == 0) {
            returnResult.setCode(107);
            returnResult.setMessage("host ID 不存在！");
            return returnResult;
        }

        if (apiHosts.getHost() == null || apiHosts.getHost().isEmpty()) {
            returnResult.setCode(107);
            returnResult.setMessage("host为空！");
            return returnResult;
        }

        if (apiHosts.getIp() == null || apiHosts.getIp().isEmpty()) {
            returnResult.setCode(107);
            returnResult.setMessage("ip地址为空！");
            return returnResult;
        }

        APIHosts hostById = hostService.getHostById(apiHosts.gethId());

        if (hostById == null) {
            returnResult.setCode(101);
            returnResult.setMessage("没有找到该记录！！");
            return returnResult;
        }

        return hostService.updateHosts(apiHosts);
    }

    /**
     * 通ID获取host
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/getHostById", method = RequestMethod.GET)
    @ResponseBody
    public Object getHostById(String id) {
        APIObjectResult<APIHosts> returnResult = new APIObjectResult<APIHosts>();

        if (id == null || id.isEmpty()) {
            returnResult.setCode(107);
            returnResult.setMessage("ID 不存在！");
            return returnResult;
        }
        Integer hId = null;

        try {
            hId = Integer.parseInt(id);
            if (hId == 0) {
                returnResult.setCode(107);
                returnResult.setMessage("ID 不存在！");
                return returnResult;
            }
        } catch (Exception e) {
            returnResult.setCode(107);
            returnResult.setMessage("非法的host ID！");
            return returnResult;
        }

        APIHosts apiHosts = hostService.getHostById(hId);
        if (apiHosts == null) {
            returnResult.setCode(101);
            returnResult.setMessage("没有找到该记录！");
            return returnResult;
        }
        returnResult.setCode(0);
        returnResult.setMessage("成功");
        returnResult.setData(apiHosts);
        return returnResult;
    }


    /**
     * 根据指定的IP地址，获取hosts列表
     *
     * @param ip
     * @return
     */
    @RequestMapping(value = "/getHostByIp", method = RequestMethod.GET)
    @ResponseBody
    public Object getHostByIp(String ip) {

        APIBaseResult returnResult = new APIBaseResult();
        if (ip == null || ip.isEmpty()) {
            returnResult.setCode(107);
            returnResult.setMessage("ip地址 不能为空！");
            return returnResult;
        }
        return hostService.getHostByIp(ip);

    }

    /**
     * 根据指定的host 获取Host列表
     *
     * @param host
     * @return
     */
    @RequestMapping(value = "/getHostByHost", method = RequestMethod.GET)
    @ResponseBody
    public Object getHostByHost(String host) {
        APIBaseResult returnResult = new APIBaseResult();
        if (host == null || host.isEmpty()) {
            return hostService.getHostByHost("");
        } else {
            return hostService.getHostByHost(host);

        }


    }


    /**
     * 根据ID删除
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/deleteHosts", method = RequestMethod.GET)
    @ResponseBody
    public Object deleteHosts(String id) {
        APIBaseResult returnResult = new APIBaseResult();

        if (id == null || id.isEmpty()) {
            returnResult.setCode(107);
            returnResult.setMessage("ID 不存在！");
            return returnResult;
        }
        Integer hId = null;

        try {
            hId = Integer.parseInt(id);
            if (hId == 0) {
                returnResult.setCode(107);
                returnResult.setMessage("ID 不存在！");
                return returnResult;
            }
        } catch (Exception e) {
            returnResult.setCode(107);
            returnResult.setMessage("非法的host ID！");
            return returnResult;
        }

        return hostService.deleteHosts(hId);
    }


    /**
     * 获取host列表
     */
    @RequestMapping(value = "queryHostsList", method = RequestMethod.GET)
    @ResponseBody
    public Object queryHostsList(HostQueryParam hostQueryParam) {

        return hostService.queryHosts(hostQueryParam);
    }
}