package tes.com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import tes.com.common.entity.query.MemberEventNotificationQueryParam;
import tes.com.service.apiServiceImp.MemberEventPointNotificationServiceImp;

import javax.annotation.Resource;

@Controller
@RequestMapping("/event")
public class MemberEventController {

    @Resource
    private MemberEventPointNotificationServiceImp memberEventPointNotificationService;

    @RequestMapping(value = "/memberEventList", method = RequestMethod.GET)
    public String memberEventList() {
        return "memberEventListManager";
    }

    @RequestMapping(value = "/memberEventListProduct", method = RequestMethod.GET)
    public String memberEventListProduct() {
        return "memberEventListManagerProduct";
    }

    /**
     * 查询会员事件通知列表
     *
     * @param memberEventNotificationQueryParam
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/queryMemberEventPointNotificationList", method = RequestMethod.GET)
    @ResponseBody
    public Object queryMemberEventPointNotificationList(@ModelAttribute("memberEventNotificationQueryParam") MemberEventNotificationQueryParam memberEventNotificationQueryParam) throws Exception {
        return memberEventPointNotificationService.queryMemberEventPointNotificationList(memberEventNotificationQueryParam);
    }


    @RequestMapping(value = "/queryMemberEventPointNotificationListProduct", method = RequestMethod.GET)
    @ResponseBody
    public Object queryMemberEventPointNotificationListProduct(@ModelAttribute("memberEventNotificationQueryParam") MemberEventNotificationQueryParam memberEventNotificationQueryParam) throws Exception {
        return memberEventPointNotificationService.queryMemberEventPointNotificationListProduct(memberEventNotificationQueryParam);
    }


}
