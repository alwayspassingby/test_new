package tes.com.controller;

import tes.com.common.entity.query.ReportDetailParam;
import tes.com.common.entity.query.ReportQueryParam;
import tes.com.common.entity.task.Feedback;
import tes.com.service.apiService.ReportService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by baoan on 2018/3/25.
 */
@Controller
@RequestMapping("/report")
public class ReportController {

    @Resource
    private ReportService reportService;

    @RequestMapping(value = "/reportList", method = RequestMethod.GET)
    public String reportList() {
        return "reportList";
    }

    @RequestMapping(value = "/reportDetail", method = RequestMethod.GET)
    public Object reportDetail(Integer taskId,Integer version) {
        Map<String,Object> data = new HashMap<String,Object>();
        data.put("taskId",taskId);
        data.put("version",version);
        return new ModelAndView("reportDetail",data);
    }

    @RequestMapping(value = "/queryReportList", method = RequestMethod.GET)
    @ResponseBody
    public Object queryReportList(@ModelAttribute("ReportQueryParam") ReportQueryParam reportQueryParam,
                                  HttpServletRequest request, HttpServletResponse response) throws Exception {
        return reportService.queryReportList(reportQueryParam);
    }

    @RequestMapping(value = "/queryReportDetail", method = RequestMethod.GET)
    @ResponseBody
    public Object queryReportDetail(@ModelAttribute("ReportDetailParam") ReportDetailParam reportDetailParam,
                                    HttpServletRequest request, HttpServletResponse response) throws Exception {
        return reportService.queryReportDetail(reportDetailParam);
    }
    @RequestMapping(value = "/queryReportCount", method = RequestMethod.GET)
    @ResponseBody
    public Object queryReportCount(@ModelAttribute("ReportDetailParam") ReportDetailParam reportDetailParam,
                                    HttpServletRequest request, HttpServletResponse response) throws Exception {
        return reportService.queryReportCount(reportDetailParam);
    }

    @RequestMapping(value = "/addFeedback", method = RequestMethod.GET)
    @ResponseBody
    public int addFeedback(@ModelAttribute("AddFeedback") Feedback addFeedback,
                           HttpServletRequest request, HttpServletResponse response) throws Exception {
        return reportService.addFeedback(addFeedback);
    }

    @RequestMapping(value = "/getChartData", method = RequestMethod.GET)
    @ResponseBody
    public Object getChartData(@ModelAttribute("ReportQueryParam") ReportQueryParam reportQueryParam,
                                  HttpServletRequest request, HttpServletResponse response) throws Exception {
        return reportService.getChartData(reportQueryParam);
    }

    @RequestMapping(value = "/autoWiki", method = RequestMethod.GET)
    public String autoWiki() {
        return "autoWiki";
    }

    @RequestMapping(value = "/summary", method = RequestMethod.GET)
    public String summary() {
        return "summary";
    }

    @RequestMapping(value = "/summaryData", method = RequestMethod.GET)
    @ResponseBody
    public Object summaryData(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return reportService.summaryData();
    }
}
