package tes.com.controller;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tes.com.common.entity.group.APIGroup;
import tes.com.common.entity.hosts.APIHosts;
import tes.com.common.entity.query.TaskQueryParam;
import tes.com.common.entity.result.APIBaseResult;
import tes.com.common.entity.result.APIObjectResult;
import tes.com.common.entity.result.BaseQueryResult;
import tes.com.common.entity.task.APICrontab;
import tes.com.common.entity.task.APITask;
import tes.com.common.entity.task.APITaskHost;
import tes.com.common.entity.task.APITaskUser;
import tes.com.common.entity.user.APITestUser;
import tes.com.service.apiService.TaskService;
import tes.com.service.apiServiceImp.APICaseService;
import tes.com.service.apiServiceImp.APIGroupService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by baoan on 2017/12/23.
 */

@Controller
@RequestMapping("/task")
public class TaskController {

    @Resource
    private APICaseService apiCaseService;
    @Resource
    private TaskService taskService;
    @Resource
    private APIGroupService apigroupservice;

    private HttpServletRequest request;

    @Autowired
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @RequestMapping(value = "/taskManager", method = RequestMethod.GET)
    public String taskPage() {
        return "taskManager";
    }

//    @RequestMapping(value = "/taskManager1", method = RequestMethod.GET)
//    public String taskPage1() {
//        return "taskManager1";
//    }


    @RequestMapping(value = "/queryTaskList", method = RequestMethod.GET)
    @ResponseBody
    public Object queryTaskList(@ModelAttribute("TaskQueryParam") TaskQueryParam taskQueryParam,
                                HttpServletRequest request, HttpServletResponse response) throws Exception {
        return taskService.queryTasks(taskQueryParam);
    }


    /**
     * 通ID获取task
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/getTaskById", method = RequestMethod.GET)
    @ResponseBody
    public Object getTaskById(String id) {
        APIObjectResult<APITask> returnResult = new APIObjectResult<APITask>();

        if (id == null || id.isEmpty()) {
            returnResult.setCode(107);
            returnResult.setMessage("ID 不存在！");
            return returnResult;
        }
        Integer hId = null;

        try {
            hId = Integer.parseInt(id);
            if (hId == 0) {
                returnResult.setCode(107);
                returnResult.setMessage("ID 不存在！");
                return returnResult;
            }
        } catch (Exception e) {
            returnResult.setCode(107);
            returnResult.setMessage("非法的任务 ID！");
            return returnResult;
        }

        APITask mallHosts = taskService.getTaskById(hId);
        if (mallHosts == null) {
            returnResult.setCode(101);
            returnResult.setMessage("没有找到该记录！");
            return returnResult;
        }
        returnResult.setCode(0);
        returnResult.setMessage("成功");
        returnResult.setData(mallHosts);
        return returnResult;
    }



    /**
     * 更新task
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/updateTask", method = RequestMethod.POST)
    @ResponseBody
    public Object updateTask(APITask task) {
        APIBaseResult returnResult = new APIBaseResult();
        if (task.getTaskId() == 0) {
            returnResult.setCode(107);
            returnResult.setMessage("task ID 不存在！");
            return returnResult;
        }

        if (task.getDescription() == null || task.getDescription().isEmpty()) {
            returnResult.setCode(107);
            returnResult.setMessage("描述信息为空！");
            return returnResult;
        }

        APITask taskById = taskService.getTaskById(task.getTaskId());

        if (taskById == null) {
            returnResult.setCode(101);
            returnResult.setMessage("没有找到该记录！！");
            return returnResult;
        }

        return taskService.updateTasks(task);
    }



    /**
     * 添加task
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/addTasks", method = RequestMethod.POST)
    @ResponseBody
    public Object addTasks(APITask task) {
        APIBaseResult returnResult = new APIBaseResult();
        if (task.getTaskName() == null || task.getTaskName().isEmpty()) {
            returnResult.setCode(107);
            returnResult.setMessage("任务名称 为空！");
            return returnResult;
        }

        if (task.getDescription() == null || task.getDescription().isEmpty()) {
            returnResult.setCode(107);
            returnResult.setMessage("任务描述 为空！");
            return returnResult;
        }

        Object object = taskService.insertTasks(task);

        return object;

    }



    /**
     * 在 task 中添加Group
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/addGroupByTask", method = RequestMethod.GET)
    @ResponseBody
    public Object addGroupByTask(@RequestParam(name="groupId") Integer groupId,@RequestParam(name="taskId") Integer taskId ) {
        APIBaseResult returnResult = new APIBaseResult();
        if (groupId == null || groupId == 0) {
            returnResult.setCode(107);
            returnResult.setMessage("您选择的组 为空！");
            return returnResult;
        }

        if (taskId == null || taskId == 0) {
            returnResult.setCode(107);
            returnResult.setMessage("您选择的任务 为空！");
            return returnResult;
        }

        Object object = taskService.addGroupByTask(groupId,taskId);

        return object;

    }

    /**
     * 在 task 中删除Group
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/deleteGroupByTask", method = RequestMethod.GET)
    @ResponseBody
    public Object deleteGroupByTask(@RequestParam(name="groupId") Integer groupId,@RequestParam(name="taskId") Integer taskId ) {
        APIBaseResult returnResult = new APIBaseResult();
        if (groupId == null || groupId == 0) {
            returnResult.setCode(107);
            returnResult.setMessage("您选择的组 为空！");
            return returnResult;
        }

        if (taskId == null || taskId == 0) {
            returnResult.setCode(107);
            returnResult.setMessage("您选择的任务 为空！");
            return returnResult;
        }

        Object object = taskService.deleteGroupByTask(groupId,taskId);

        return object;

    }


    /**
     * 根据ID删除
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/deleteTasks", method = RequestMethod.GET)
    @ResponseBody
    public Object deleteTasks(String id) {
        APIBaseResult returnResult = new APIBaseResult();

        if (id == null || id.isEmpty()) {
//            returnResult.setCode(107);
            returnResult.setMessage("ID 不存在！");
            return returnResult;
        }
        Integer hId = null;

        try {
            hId = Integer.parseInt(id);
            if (hId == 0) {
//                returnResult.setCode(107);
                returnResult.setMessage("ID 不存在！");
                return returnResult;
            }
        } catch (Exception e) {
//            returnResult.setCode(107);
            returnResult.setMessage("非法的host ID！");
            return returnResult;
        }

        return taskService.deleteTasks(hId);
    }


    /**
     * 查询组
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/getGroupListFromTaskId", method = RequestMethod.GET)
    @ResponseBody
    public Object queryGroup(TaskQueryParam taskQueryParam, HttpServletRequest request, HttpServletResponse response) {
        BaseQueryResult<APIGroup> apigroupquery = null;
        apigroupquery = taskService.getGroupListByTaskId(taskQueryParam);
        return apigroupquery;
    }



    /**
     * 根据插入任务task
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/runTask", method = RequestMethod.GET)
    @ResponseBody
    public Object runTask(@RequestParam(name="taskId") Integer id) {
        APIBaseResult returnResult = new APIBaseResult();

        if (id == null || id == 0) {
            returnResult.setCode(107);
            returnResult.setMessage("ID 不存在！");
            return returnResult;
        }
        if (taskService.runTask(id) != 0){
            returnResult.setCode(200);
        }
        return returnResult;
    }

    /**
     * 根据插入任务task
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/setCrontab", method = RequestMethod.GET)
    @ResponseBody
    public Object setCrontab(@RequestParam(name="taskId") Integer taskId,@RequestParam(name="crontabTime") String crontabTime) {
        APIBaseResult returnResult = new APIBaseResult();
        if (taskId == null || taskId == 0) {
            returnResult.setCode(107);
            returnResult.setMessage("task ID不存在！");
            return returnResult;
        }

        if (StringUtils.isEmpty(crontabTime)) {
            returnResult.setCode(107);
            returnResult.setMessage("定时任务时间不能为空！");
            return returnResult;
        }

        APICrontab apiCrontab = new APICrontab();
        apiCrontab.setTaskId(taskId);
        apiCrontab.setCrontab_time(crontabTime);
        return taskService.setCrontab(apiCrontab);
    }

    @RequestMapping(value = "/getHostsListByTask", method = RequestMethod.GET)
    @ResponseBody
    public Object getHostsListByTask(TaskQueryParam hostQueryParamByTask) {

        BaseQueryResult<APIHosts> baseQueryResult = new BaseQueryResult<APIHosts>();

        if (hostQueryParamByTask.getTaskId() == null || hostQueryParamByTask.getTaskId() == 0) {
            baseQueryResult.setTotal(0);
            return baseQueryResult;
        }
        if (!StringUtils.isEmpty(hostQueryParamByTask.getSearchText())){
            hostQueryParamByTask.setSearchText(hostQueryParamByTask.getSearchText().trim());
        }

        return taskService.queryHostByTask(hostQueryParamByTask);
    }



    @RequestMapping(value = "/queryHostByTask", method = RequestMethod.GET)
    @ResponseBody
    public Object queryHostByTask(TaskQueryParam hostQueryParamByTask) {
            return taskService.queryHostByTask(hostQueryParamByTask);
    }

    /**
     * 添加关系表
     * @param apiTaskHost
     * @return
     */
    @RequestMapping(value = "/addSetHost", method = RequestMethod.GET)
    @ResponseBody
    public APIBaseResult addSetHost(APITaskHost apiTaskHost) {
        APIBaseResult mallBaseResult = new APIBaseResult();
        if (apiTaskHost.getHost_id() == null){
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("hostId 为空！");
            return mallBaseResult;
        }
        if(apiTaskHost.getTask_id() == null){
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("taskId 为空！");
            return mallBaseResult;
        }

        return taskService.addSetHostByTask(apiTaskHost);
    }

    @RequestMapping(value = "/deleteSetHost", method = RequestMethod.GET)
    @ResponseBody
    public APIBaseResult deleteSetHost(APITaskHost apiTaskHost) {
        APIBaseResult mallBaseResult = new APIBaseResult();
        if (apiTaskHost.getHost_id() == null){
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("hostId 为空！");
            return mallBaseResult;
        }
        if(apiTaskHost.getTask_id() == null){
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("taskId 为空！");
            return mallBaseResult;
        }
        return taskService.deleteSetHostByTask(apiTaskHost);
    }

    @RequestMapping(value = "/getExistingCrontab",method = RequestMethod.GET)
    @ResponseBody
    public Object getExistingCrontab(@RequestParam(name="taskId") Integer taskId){
        return taskService.getExistingCrontab(taskId);
    }



    @RequestMapping(value = "/getTUserListByTask", method = RequestMethod.GET)
    @ResponseBody
    public Object getTUserListByTask(TaskQueryParam userQueryParamByTask) {

        BaseQueryResult<APITestUser> baseQueryResult = new BaseQueryResult<APITestUser>();

        if (userQueryParamByTask.getTaskId() == null || userQueryParamByTask.getTaskId() == 0) {
            baseQueryResult.setTotal(0);
            return baseQueryResult;
        }
        if (!StringUtils.isEmpty(userQueryParamByTask.getSearchText())){
            userQueryParamByTask.setSearchText(userQueryParamByTask.getSearchText().trim());
        }

        return taskService.queryTUserListByTask(userQueryParamByTask);
    }


    @RequestMapping(value = "/addTaskTUser", method = RequestMethod.GET)
    @ResponseBody
    public APIBaseResult addTaskTUser(APITaskUser apiTaskUser) {
        APIBaseResult mallBaseResult = new APIBaseResult();
        if (apiTaskUser.getTuId() == null){
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("用户ID 为空！");
            return mallBaseResult;
        }
        if(apiTaskUser.getTaskId() == null){
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("taskId 为空！");
            return mallBaseResult;
        }

        return taskService.addTaskTUser(apiTaskUser);
    }

    @RequestMapping(value = "/deleteTaskTUser", method = RequestMethod.GET)
    @ResponseBody
    public APIBaseResult deleteTaskTUser(APITaskUser apiTaskUser) {
        APIBaseResult mallBaseResult = new APIBaseResult();
        if (apiTaskUser.getTuId() == null){
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("用户ID 为空！");
            return mallBaseResult;
        }
        if(apiTaskUser.getTaskId() == null){
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("taskId 为空！");
            return mallBaseResult;
        }
        return taskService.deleteTaskTUser(apiTaskUser);
    }

}
