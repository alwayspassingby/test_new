package tes.com.controller;

import com.fasterxml.jackson.databind.JsonNode;
import tes.com.common.entity.query.TestUserQueryParam;
import tes.com.common.entity.result.APIBaseResult;
import tes.com.common.entity.result.APIObjectResult;
import tes.com.common.entity.user.YZJUserInfo;
import tes.com.controller.http.HttpRequestController;
import tes.com.service.apiService.APITestUserService;
import tes.com.util.JSONUtil;
import tes.com.util.encrypt.EncryptPassword;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Connection;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by liuzhongdu on 2018/1/21.
 */
@Controller
@RequestMapping("/tuser")
public class TestUserController {

    @Resource
    private APITestUserService apiTestUserService;

    /**
     * @return
     */
    @RequestMapping(value = "/userManager", method = RequestMethod.GET)
    public String userManager() {
        return "testUserManager";
    }


    /**
     * 添加User
     *
     * @param yzjUserInfo
     * @return
     */
    @RequestMapping(value = "/addTUser", method = RequestMethod.POST)
    @ResponseBody
    public Object addUser(YZJUserInfo yzjUserInfo, String validCode, HttpServletRequest request) {
        APIBaseResult returnResult = new APIBaseResult();

        if (StringUtils.isEmpty(yzjUserInfo.getTname())) {
            returnResult.setCode(101);
            returnResult.setMessage("用户名为空！");
            return returnResult;
        }
        if (StringUtils.isEmpty(yzjUserInfo.getTpwd())) {
            returnResult.setCode(101);
            returnResult.setMessage("密码为空！");
            return returnResult;
        }
        if (yzjUserInfo.getType() == null || yzjUserInfo.getType() > 3 || yzjUserInfo.getType() < 1) {
            returnResult.setCode(101);
            returnResult.setMessage("类型没有定义！");
            return returnResult;
        }

        if (yzjUserInfo.getType() == 1 || yzjUserInfo.getType() ==2){
            //        加密密码
            yzjUserInfo.setTpwd(EncryptPassword.encryptPassword(yzjUserInfo.getTname(),yzjUserInfo.getTpwd()));

        }

        Map<String,String> dataMap = new HashedMap<String,String>();

        dataMap.put("tname",yzjUserInfo.getTname());
        dataMap.put("tpwd",yzjUserInfo.getTpwd());
        dataMap.put("type",yzjUserInfo.getType().toString());

        if (yzjUserInfo.getType() != 3){
            dataMap.put("loginUrl",yzjUserInfo.getLoginUrl());
            dataMap.put("eid",yzjUserInfo.getEid());
            dataMap.put("deviceType",yzjUserInfo.getDeviceType());
            dataMap.put("deviceId",yzjUserInfo.getDeviceId());
            dataMap.put("ua",yzjUserInfo.getUa());

        }

        String url = "http://" + request.getServerName() + ":8580" + "/tuser/testUserLogin";


        Connection.Response res = HttpRequestController.httpAPIRequest(url,null,dataMap,null,true,null,null,
                Connection.Method.POST,yzjUserInfo.getDescription());
//        System.out.println(res.body());
        if (res == null){
            returnResult.setCode(101);
            returnResult.setMessage("服务器异常！");
            return returnResult;
        }
        try {
            JsonNode jsonNode = JSONUtil.parseJsonNode(res.body());
            if (jsonNode.get("code").asInt() != 0){
                returnResult.setCode(101);
                returnResult.setMessage("该用户登陆失败，请求检查该用户名和密码！！");
                return returnResult;
            }else {
                if (jsonNode.get("data").get("success").asBoolean()){
                    return apiTestUserService.insertTestUser(yzjUserInfo, validCode);
                }else {
                    returnResult.setCode(101);
                    returnResult.setMessage(jsonNode.get("data").get("error").asText());
                    return returnResult;
                }
            }

        }catch (Exception e){
            returnResult.setCode(101);
            returnResult.setMessage("该用户登陆失败，请求检查该用户名和密码！！");
            return returnResult;
        }
    }


    /**
     * 删除用户
     *
     * @param tuId
     * @return
     */
    @RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
    @ResponseBody
    public Object deleteUser(String tuId) {
        APIBaseResult returnResult = new APIBaseResult();

        if (tuId == null || tuId.isEmpty()) {
            returnResult.setCode(107);
            returnResult.setMessage("没有提供用户ID！");
            return returnResult;
        }
        Integer id = null;

        try {
            id = Integer.parseInt(tuId);
            if (id == 0) {
                returnResult.setCode(107);
                returnResult.setMessage("用户ID 不存在！");
                return returnResult;
            }
        } catch (Exception e) {
            returnResult.setCode(107);
            returnResult.setMessage("非法的用户ID！");
            return returnResult;
        }

        return apiTestUserService.deleteUser(id);

    }


    /**
     * 请求列表
     *
     * @param mallTestUserQueryParam
     * @return
     */
    @RequestMapping(value = "/queryTUserList", method = RequestMethod.GET)
    @ResponseBody
    public Object queryTUserList(TestUserQueryParam mallTestUserQueryParam) {

        return apiTestUserService.queryUserList(mallTestUserQueryParam);
    }


    @RequestMapping(value = "/getUserById", method = RequestMethod.GET)
    @ResponseBody
    public Object getUserById(String id) {

        APIBaseResult returnResult = new APIBaseResult();

        if (id == null || id.isEmpty() || id.trim().isEmpty()) {
            returnResult.setCode(107);
            returnResult.setMessage("没有提供用户ID！");
            return returnResult;
        }
        Integer tuId = null;

        try {
            tuId = Integer.parseInt(id);
            if (tuId == 0) {
                returnResult.setCode(107);
                returnResult.setMessage("用户ID 不存在！");
                return returnResult;
            }
        } catch (Exception e) {
            returnResult.setCode(107);
            returnResult.setMessage("非法的用户ID！");
            return returnResult;
        }

        return apiTestUserService.getUserById(tuId);
    }


    /**
     * 更新用户信息
     *
     * @param yzjUserInfo
     * @return
     */
    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    @ResponseBody
    public Object updateUser(YZJUserInfo yzjUserInfo, String validCode) {
        APIBaseResult returnResult = new APIBaseResult();
        if (yzjUserInfo.getTuId() == null || yzjUserInfo.getTuId() == 0) {
            returnResult.setCode(107);
            returnResult.setMessage("用户 ID 不存在！");
            return returnResult;
        }

        if (yzjUserInfo.getTname() == null || yzjUserInfo.getTname().isEmpty()) {
            returnResult.setCode(107);
            returnResult.setMessage("用户名不能为空！");
            return returnResult;
        }

        if (yzjUserInfo.getTpwd() == null || yzjUserInfo.getTpwd().isEmpty()) {
            returnResult.setCode(107);
            returnResult.setMessage("密码不能为空！");
            return returnResult;
        }

        if (yzjUserInfo.getType() == null || yzjUserInfo.getType() > 3 || yzjUserInfo.getType() < 1) {
            returnResult.setCode(107);
            returnResult.setMessage("类型为空或没有定义类型！");
            return returnResult;
        }
        return apiTestUserService.updateTestUser(yzjUserInfo, validCode);
    }


    /**
     * 获取所有账号列表
     *
     * @return
     */
    @RequestMapping(value = "/getTUsers", method = RequestMethod.GET)
    @ResponseBody
    public Object getTUsers() {
        return apiTestUserService.getTUsers();
    }


    @RequestMapping(value = "/getAutoHomeUserById", method = RequestMethod.GET)
    @ResponseBody
    public Object getAutoHomeUserById(String id) {

        return null;
    }


    @RequestMapping(value = "/getCookieByUserID", method = RequestMethod.GET)
    @ResponseBody
    public Object getCookieByUser(String id, String validCode) {
        APIObjectResult<YZJUserInfo> mallObjectResult = (APIObjectResult<YZJUserInfo>) getUserById(id);
        if (mallObjectResult.getCode() != 0) {
            return mallObjectResult;
        }
        return apiTestUserService.getCookiesByUserId(mallObjectResult.getData(), validCode);

    }

}
