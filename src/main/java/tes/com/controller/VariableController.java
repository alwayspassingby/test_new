package tes.com.controller;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tes.com.common.entity.apicase.GC_Variable;
import tes.com.common.entity.apicase.TestCase;
import tes.com.common.entity.group.APIGroup;
import tes.com.common.entity.query.VariableQueryParam;
import tes.com.common.entity.result.APIBaseResult;
import tes.com.common.entity.result.APIObjectResult;
import tes.com.common.entity.result.BaseQueryResult;
import tes.com.common.entity.variable.CaseVariable;
import tes.com.common.entity.variable.GroupVariable;
import tes.com.common.entity.variable.TaskVariable;
import tes.com.common.entity.variable.Variable;
import tes.com.service.apiService.GroupService;
import tes.com.service.apiService.VariableService;
import tes.com.service.apiServiceImp.APICaseService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

@Controller
@RequestMapping("/variable")
public class VariableController {

    @Resource
    private VariableService variableService;

    @Resource
    private APICaseService apiCaseService;

    @Resource
    private GroupService groupService;



    /**
     * @return
     */
    @RequestMapping(value = "/variableManager", method = RequestMethod.GET)
    public String userManager() {
        return "variableManager";
    }


    /**
     * 设置用例的变量
     *
     * @param caseVariable
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/addCaseVariable", method = RequestMethod.POST)
    @ResponseBody
    public Object addCaseVariable(@ModelAttribute("caseVariable") CaseVariable caseVariable,
                                  HttpServletRequest request, HttpServletResponse response) throws Exception {

        APIBaseResult baseResult = new APIBaseResult();
        if (caseVariable.getCaseId() == null || caseVariable.getCaseId() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("caseId 不能为空！");
            return baseResult;
        }

        TestCase testCase = apiCaseService.getCaseById(caseVariable.getCaseId());
        if (testCase == null) {
            baseResult.setCode(201);
            baseResult.setMessage("没有找到相应的用例!");
            return baseResult;
        }


        if (caseVariable.getV_name() == null || caseVariable.getV_name().trim().length() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("变量名称不能为空！");
            return baseResult;
        }

        if (caseVariable.getV_value() == null || caseVariable.getV_value().trim().length() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("变量Key不能为空！");
            return baseResult;
        }
        caseVariable.setStatus(0);
        caseVariable.setCreateTime(new Date());
        return variableService.addCaseVariable(caseVariable);
    }


    /**
     * @param caseVariable
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/modifyCaseVariable", method = RequestMethod.POST)
    @ResponseBody
    public Object modifyCaseVariable(@ModelAttribute("caseVariable") CaseVariable caseVariable,
                                     HttpServletRequest request, HttpServletResponse response) throws Exception {

        APIBaseResult baseResult = new APIBaseResult();
        if (caseVariable.getvId() == null || caseVariable.getvId() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("vId 不能为空！");
            return baseResult;
        }

        if (caseVariable.getCaseId() == null || caseVariable.getCaseId() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("caseId 不能为空！");
            return baseResult;
        }
        APIObjectResult<CaseVariable> tmp = variableService.getCaseVariableById(caseVariable.getvId());
        if (tmp.getCode() != 0) {
            baseResult.setCode(101);
            baseResult.setMessage("没有找到记录，vId:" + caseVariable.getvId());
            return baseResult;
        } else {
            if (caseVariable.getCaseId() != tmp.getData().getCaseId()) {
                baseResult.setCode(101);
                baseResult.setMessage("caseId 不匹配！");
                return baseResult;
            }
        }

        if (caseVariable.getV_name() == null || caseVariable.getV_name().trim().length() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("变量名称不能为空！");
            return baseResult;
        }

        if (caseVariable.getV_value() == null || caseVariable.getV_value().trim().length() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("变量Key不能为空！");
            return baseResult;
        }
        return variableService.updateCaseVariable(caseVariable);
    }

    /**
     * 根据Id 获取变量对象
     *
     * @param vId
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getCaseVariableById", method = RequestMethod.GET)
    @ResponseBody
    public Object getCaseVariableById(@RequestParam("vId") String vId,
                                      HttpServletRequest request, HttpServletResponse response) throws Exception {

        APIBaseResult baseResult = new APIBaseResult();
        if (vId == null || vId.trim().length() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("变量ID不能为空！");
            return baseResult;
        }

        Integer id = null;

        try {
            id = Integer.parseInt(vId);
        } catch (Exception e) {
            baseResult.setCode(101);
            baseResult.setMessage("vId 必须是整数！");
            return baseResult;
        }

        if (id == null || id == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("vID不能为空！");
            return baseResult;
        }

        return variableService.getCaseVariableById(id);
    }


    /**
     * 获取变量列表
     *
     * @param variableQueryParam
     * @return
     */
    @RequestMapping(value = "/getCaseVariableList", method = RequestMethod.POST)
    @ResponseBody
    public Object getCaseVariableList(@RequestBody VariableQueryParam variableQueryParam) {
        BaseQueryResult<CaseVariable> baseQueryResult = new BaseQueryResult<CaseVariable>();
        if (variableQueryParam.getCaseId() == null || variableQueryParam.getCaseId() == 0) {
            baseQueryResult.setTotal(0);
            baseQueryResult.setRows(null);
            return baseQueryResult;
        }
        return variableService.queryCaseVariableList(variableQueryParam);
    }


    /**
     * @param vId
     * @return
     */
    @RequestMapping(value = "/delCaseVariableById", method = RequestMethod.GET)
    @ResponseBody
    public Object delCaseVariableById(@RequestParam("vId") String vId) {

        APIBaseResult baseResult = new APIBaseResult();
        if (vId == null || vId.trim().length() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("vId不能为空！");
            return baseResult;
        }
        Integer id = null;
        try {
            id = Integer.parseInt(vId);
        } catch (Exception e) {
            baseResult.setCode(102);
            baseResult.setMessage("vId 必须是整数！");
            return baseResult;
        }

        if (id == null || id == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("vId不能为空！");
            return baseResult;
        }
        return variableService.delCaseVariableById(id);
    }


    /**
     * 根据 groupId和caseId，获取变量列表
     *
     * @param variableQueryParam
     * @return
     */
    @RequestMapping(value = "/getCaseVariableListFromGroupId", method = RequestMethod.POST)
    @ResponseBody
    public Object getCaseVariableListFromGroupId(@ModelAttribute("variableQueryParam") VariableQueryParam variableQueryParam) {

        APIBaseResult baseResult = new APIBaseResult();
        if (variableQueryParam.getCaseId() == null || variableQueryParam.getCaseId() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("没用指定用例ID！");
            return baseResult;
        }

        if (variableQueryParam.getGroupId() == null || variableQueryParam.getGroupId() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("没用指定用例用例ID");
            return baseResult;
        }

        TestCase testCase = apiCaseService.getCaseById(variableQueryParam.getCaseId());
        if (testCase == null) {
            baseResult.setCode(201);
            baseResult.setMessage("没有找到相应的用例!");
            return baseResult;
        }

        APIGroup apiGroup = groupService.getGroupById(variableQueryParam.getGroupId());
        if (apiGroup == null) {
            baseResult.setCode(201);
            baseResult.setMessage("指定的群组不存在！");
            return baseResult;
        }

        return variableService.getCaseVariableListFromGroupId(variableQueryParam);
    }


    /**
     * @param variableQueryParam
     * @return
     */
    @RequestMapping(value = "/getTaskVariableList", method = RequestMethod.POST)
    @ResponseBody
    public Object getTaskVariableList(@ModelAttribute("variableQueryParam") VariableQueryParam variableQueryParam) {

        APIBaseResult baseResult = new APIBaseResult();
        if (variableQueryParam.getTaskId() == null || variableQueryParam.getTaskId() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("没用指定任务ID！");
            return baseResult;
        }
        if (variableQueryParam.getIsSelected() == null) {
            variableQueryParam.setIsSelected(1);
        }

        return variableService.getTaskVariableList(variableQueryParam);
    }



    /**
     * 添加变量到群组中
     *
     * @param groupVariable
     * @return
     */
    @RequestMapping(value = "/addGroupCaseVariable", method = RequestMethod.POST)
    @ResponseBody
    public Object addGroupCaseVariable(@ModelAttribute("groupVariable") GC_Variable groupVariable) {

        APIBaseResult apiBaseResult = new APIBaseResult();

        if (groupVariable.getGroupId() == null || groupVariable.getGroupId() == 0){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("群组UD不能为空！");
            return apiBaseResult;
        }

        if (groupVariable.getvId() == null || groupVariable.getvId() == 0){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("vId 不能为空！");
            return apiBaseResult;
        }

        groupVariable.setStatus(0);
        groupVariable.setCreateTime(new Date());

        return variableService.addGroupCaseVariable(groupVariable);

    }

    /**
     * 添加变量到群组中
     *
     * @param gId
     * @param vId
     * @return
     */

    @RequestMapping(value = "/delGroupCaseVariable", method = RequestMethod.GET)
    @ResponseBody
    public Object delGroupCaseVariable(@RequestParam("gId") Integer gId, @RequestParam("vId") Integer vId) {

        APIBaseResult baseResult = new APIBaseResult();


        if (gId == null || gId == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("id 不能为空！");
            return baseResult;
        }

        if (vId == null || vId == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("id 不能为空！");
            return baseResult;
        }
        return variableService.delGroupCaseVariableById(gId, vId);

    }

    @RequestMapping(value = "/moveTaskVariable", method = RequestMethod.GET)
    @ResponseBody
    public Object moveTaskVariable(@RequestParam("taskId") Integer taskId, @RequestParam("vId") Integer vId) {

        APIBaseResult baseResult = new APIBaseResult();


        if (taskId == null || taskId == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("taskId 不能为空！");
            return baseResult;
        }

        if (vId == null || vId == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("vId 不能为空！");
            return baseResult;
        }
        return variableService.moveTaskVariable(taskId, vId);

    }


    @RequestMapping(value = "/addToTaskVariableList", method = RequestMethod.POST)
    @ResponseBody
    public Object addToTaskVariableList(TaskVariable taskVariable) {

        APIBaseResult baseResult = new APIBaseResult();

        if (taskVariable.getTaskId() == null || taskVariable.getTaskId() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("taskId 不能为空！");
            return baseResult;
        }

        if (taskVariable.getvId() == null || taskVariable.getvId() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("vId 不能为空！");
            return baseResult;
        }
        return variableService.addToTaskVariableList(taskVariable);

    }





    @RequestMapping(value = "/addGlobalVariable", method = RequestMethod.POST)
    @ResponseBody
    public Object addGlobalVariable(Variable globalVariable, HttpServletRequest request) {

        APIBaseResult apiBaseResult  = new APIBaseResult();
        if (StringUtils.isEmpty(globalVariable.getV_name())){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量名称不能为空！");
            return apiBaseResult;
        }
        if (StringUtils.isEmpty(globalVariable.getV_value())){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量的值不能为空！");
            return apiBaseResult;
        }
        if(globalVariable.getType() == null || globalVariable.getType() < 0 || globalVariable.getType() > 3){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量的类型不正确！0:系统变量  1：为群组变量  2：任务变量");
            return apiBaseResult;
        }
        return variableService.addGlobalVariable(globalVariable);

    }



    @RequestMapping(value = "/updateGlobalVariable", method = RequestMethod.POST)
    @ResponseBody
    public Object updateGlobalVariable(Variable globalVariable, HttpServletRequest request) {

        APIBaseResult apiBaseResult  = new APIBaseResult();

        if (globalVariable.getvId() == 0){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("没有指定变量ID！");
            return apiBaseResult;
        }

        if (StringUtils.isEmpty(globalVariable.getV_name())){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量名称不能为空！");
            return apiBaseResult;
        }
        if (StringUtils.isEmpty(globalVariable.getV_value())){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量的值不能为空！");
            return apiBaseResult;
        }
        if(globalVariable.getType() == null || globalVariable.getType() < 0 || globalVariable.getType() > 3){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量的类型不正确！0:系统变量  1：为群组变量  2：任务变量");
            return apiBaseResult;
        }
        return variableService.updateGlobalVariable(globalVariable);

    }

    /**
     * @param variableQueryParam
     * @return
     */
    @RequestMapping(value = "/getGlobalVariableList", method = RequestMethod.POST)
    @ResponseBody
    public Object getGlobalVariableList(@ModelAttribute("variableQueryParam") VariableQueryParam variableQueryParam) {

        if (variableQueryParam.getVariable_type() == null) {
            variableQueryParam.setVariable_type(0);
        }

        return variableService.getGlobalVariableList(variableQueryParam);
    }


    /**
     * 根据id 获取变量对象
     *
     * @param vId
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getGlobalVariableById", method = RequestMethod.GET)
    @ResponseBody
    public Object getGlobalVariableById(@RequestParam("vId") String vId,
                                        HttpServletRequest request, HttpServletResponse response) throws Exception {

        APIBaseResult baseResult = new APIBaseResult();
        if (vId == null || vId.trim().length() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("变量ID不能为空！");
            return baseResult;
        }

        Integer id = null;

        try {
            id = Integer.parseInt(vId);
        } catch (Exception e) {
            baseResult.setCode(101);
            baseResult.setMessage("vId 必须是整数！");
            return baseResult;
        }

        if (id == null || id == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("vId 不能为空！");
            return baseResult;
        }

        return variableService.getGlobalVariableById(id);
    }


    /**
     * 根据 vid 删除对应的记录
     *
     * @param vId
     * @return
     */
    @RequestMapping(value = "/delGlobalVariableById", method = RequestMethod.GET)
    @ResponseBody
    public Object delGlobalVariableById(@RequestParam("vId") String vId) {

        APIBaseResult baseResult = new APIBaseResult();
        if (vId == null || vId.trim().length() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("vId 不能为空！");
            return baseResult;
        }
        Integer id = null;
        try {
            id = Integer.parseInt(vId);
        } catch (Exception e) {
            baseResult.setCode(102);
            baseResult.setMessage("vId 必须是整数！");
            return baseResult;
        }

        if (id == null || id == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("vId 不能为空！");
            return baseResult;
        }
        return variableService.delGlobalVariableById(id);
    }


    @RequestMapping(value = "/addTaskVariable", method = RequestMethod.POST)
    @ResponseBody
    public Object addTaskVariable(TaskVariable taskVariable, HttpServletRequest request) {

        APIBaseResult apiBaseResult  = new APIBaseResult();
        if (StringUtils.isEmpty(taskVariable.getV_name())){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量名称不能为空！");
            return apiBaseResult;
        }
        if (StringUtils.isEmpty(taskVariable.getV_value())){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量的值不能为空！");
            return apiBaseResult;
        }

        if(taskVariable.getTaskId() == null || taskVariable.getTaskId() ==0){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("taskId 不能为空！");
            return apiBaseResult;
        }

        if(taskVariable.getType() == null || taskVariable.getType() < 0 || taskVariable.getType() > 3){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量的类型不正确！0:系统变量  1：为群组变量  2：任务变量 3: 用例变量");
            return apiBaseResult;
        }
        if(taskVariable.getType() !=  2){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量的类型不正确！");
            return apiBaseResult;
        }
        return variableService.addTaskVariable(taskVariable);

    }


    @RequestMapping(value = "/updateTaskVariable", method = RequestMethod.POST)
    @ResponseBody
    public Object updateTaskVariable(TaskVariable taskVariable, HttpServletRequest request) {

        APIBaseResult apiBaseResult  = new APIBaseResult();

        if (taskVariable.getTaskId() == null || taskVariable.getTaskId() == 0){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("没有指定taskId！");
            return apiBaseResult;
        }

        if (taskVariable.getvId() == 0){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("没有指定变量ID！");
            return apiBaseResult;
        }

        if (StringUtils.isEmpty(taskVariable.getV_name())){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量名称不能为空！");
            return apiBaseResult;
        }
        if (StringUtils.isEmpty(taskVariable.getV_value())){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量的值不能为空！");
            return apiBaseResult;
        }

        if(taskVariable.getType() != 2){
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量的类型不正确！");
            return apiBaseResult;
        }
        return variableService.updateGlobalVariable(taskVariable);

    }




    /**
     * 群组的自定义变量列表
     * @param variableQueryParam
     * @return
     */
    @RequestMapping(value = "/queryGroupVariableList", method = RequestMethod.POST)
    @ResponseBody
    public Object queryGroupVariableList(@ModelAttribute("variableQueryParam") VariableQueryParam variableQueryParam) {

        APIBaseResult baseResult = new APIBaseResult();
        if (variableQueryParam.getGroupId() == null || variableQueryParam.getGroupId() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("没用指定群组ID！");
            return baseResult;
        }
        if (variableQueryParam.getIsSelected() == null) {
            variableQueryParam.setIsSelected(1);
        }

        return variableService.getGroupVariableList(variableQueryParam);
    }


    /**
     * 创建群组变量接口
     *
     * @param groupVariable
     * @param request
     * @return
     */
    @RequestMapping(value = "/addGroupVariable", method = RequestMethod.POST)
    @ResponseBody
    public Object addGroupVariable(GroupVariable groupVariable, HttpServletRequest request) {

        APIBaseResult apiBaseResult = new APIBaseResult();
        if (StringUtils.isEmpty(groupVariable.getV_name())) {
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量名称不能为空！");
            return apiBaseResult;
        }
        if (StringUtils.isEmpty(groupVariable.getV_value())) {
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量的值不能为空！");
            return apiBaseResult;
        }

        if (groupVariable.getGroupId() == null || groupVariable.getGroupId() == 0) {
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("groupId 不能为空！");
            return apiBaseResult;
        }

        if (groupVariable.getType() == null || groupVariable.getType() < 0 || groupVariable.getType() > 3) {
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量的类型不正确！0:系统变量  1：为群组变量  2：任务变量 3: 用例变量");
            return apiBaseResult;
        }
        if (groupVariable.getType() != 1) {
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量的类型不正确！, 1：为群组变量");
            return apiBaseResult;
        }
        return variableService.addGroupTypeVariable(groupVariable);
    }


    @RequestMapping(value = "/addToGroupVariableList", method = RequestMethod.POST)
    @ResponseBody
    public Object addToGroupVariableList(GroupVariable groupVariable) {

        APIBaseResult baseResult = new APIBaseResult();

        if (groupVariable.getGroupId() == null || groupVariable.getGroupId() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("group 不能为空！");
            return baseResult;
        }

        if (groupVariable.getvId() == null || groupVariable.getvId() == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("vId 不能为空！");
            return baseResult;
        }
        return variableService.addGroupVariableToVariableList(groupVariable);
    }


    @RequestMapping(value = "/moveGroupVariable", method = RequestMethod.GET)
    @ResponseBody
    public Object moveGroupVariable(@RequestParam("groupId") Integer groupId, @RequestParam("vId") Integer vId) {

        APIBaseResult baseResult = new APIBaseResult();


        if (groupId == null || groupId == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("groupId 不能为空！");
            return baseResult;
        }

        if (vId == null || vId == 0) {
            baseResult.setCode(101);
            baseResult.setMessage("vId 不能为空！");
            return baseResult;
        }
        return variableService.moveGroupVariable(groupId, vId);
    }


    @RequestMapping(value = "/updateGroupVariable", method = RequestMethod.POST)
    @ResponseBody
    public Object updateGroupVariable(GroupVariable groupVariable, HttpServletRequest request) {

        APIBaseResult apiBaseResult = new APIBaseResult();

        if (groupVariable.getGroupId() == null || groupVariable.getGroupId() == 0) {
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("没有指定 groupId ！");
            return apiBaseResult;
        }

        if (groupVariable.getvId() == 0) {
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("没有指定变量ID！");
            return apiBaseResult;
        }

        if (StringUtils.isEmpty(groupVariable.getV_name())) {
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量名称不能为空！");
            return apiBaseResult;
        }
        if (StringUtils.isEmpty(groupVariable.getV_value())) {
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量的值不能为空！");
            return apiBaseResult;
        }

        if (groupVariable.getType() != 1) {
            apiBaseResult.setCode(101);
            apiBaseResult.setMessage("变量的类型不正确！");
            return apiBaseResult;
        }
        return variableService.updateGlobalVariable(groupVariable);

    }

}
