package tes.com.controller.http;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import javax.net.ssl.*;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Map;

/**
 * Created by liuzhongdu on 16/6/21.
 */
public class HttpRequestController {

    /**
     *
     * @param apiURL 请求URL
     * @param map 数据map
     * @param isFrom 是否为表单提交
     * @param cookiesMap cookies数据map
     * @param contentType
     * @param method
     * @param description
     * @return
     * @throws Exception
     */
    public static Connection.Response httpAPIRequest(String apiURL,String userAgent,Map<String, String> map,String requestBody,Boolean isFrom,Map<String, String> cookiesMap,String contentType, Connection.Method method,String description) {
        Connection con = Jsoup.connect(apiURL);
        if (StringUtils.isEmpty(userAgent)){
            userAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36";
        }
        if(isFrom){
            if (!map.isEmpty()){
                con.data(map);
            }
        }else {
            con.data("_","");
            con.requestBody(requestBody);
        }
        if (cookiesMap != null && !cookiesMap.isEmpty()){
            con.cookies(cookiesMap);
        }
        con.method(method);
        if (StringUtils.isEmpty(contentType)){
            contentType  = "application/x-www-form-urlencoded";
        }
        con.header("Content-Type", contentType);
//        con.header("Connection", "Keep-Alive");
        con.ignoreContentType(true);
//        con.proxy("127.0.0.1",8888);
        con.userAgent(userAgent);
        Connection.Response res = HttpRequestController.sendRequest(con,description);
        return res;
    }


    /**
     * 发送请求
     * @param con
     * @return
     * @throws Exception
     */
    public static Connection.Response sendRequest(Connection con,String description){
        Connection.Response res = null;

        //请求的开始时间
        long data1 = new Date().getTime();
//        Log4jUtil.log("============ 开始请求接口 : " + description + " ============");
//        outReqInfo(con.request());
        try {
            //如果请求的是https，将支持https
            if (con.request().url().getProtocol().equals("https")){
                trustEveryone();
            }
            res = con.execute();
            long data2 = new Date().getTime();
//            Log4jUtil.log(res.url().toString());
//            Log4jUtil.log("请求时间 ：" + (data2 - data1));
//            Log4jUtil.log("响应返回结果 ：");
//            Log4jUtil.log(res.body());
//            Log4jUtil.log("============ 请求接口: " + description + " 结束 =============== \n");
//            Assertions.assertThat(res.statusCode()).as("请求返回回不成功" + "(" + description + ")" + "，状态码为：" + res.statusCode()).isEqualTo(200);
            return res;
        } catch (Exception e) {
//            Log4jUtil.logError(Thread.currentThread(), e);
           return null;
        }
    }

//    /**
//     * 请求信息输出
//     * @param req
//     */
//    public static void outReqInfo(Connection.Request req) {
//        //输出请求的方式
//        Log4jUtil.log("method : " + req.method().toString());
//        //输出url
//        Log4jUtil.log("url : " + req.url());
//        //输出请求头的内容
//        for (String str : req.headers().keySet()) {
//            Log4jUtil.log(str + ":" + req.headers().get(str));
//        }
//        //输出请求数据
//        if (req.method() == Connection.Method.GET) {
//            Log4jUtil.log("data :" + req.data().toString());
//        }
//        if (req.method() == Connection.Method.POST) {
//            Log4jUtil.log("data :" + req.data().toString());
//            Log4jUtil.log("requestBody :" + req.requestBody());
//
//        }
//        //输出cookies
//        Log4jUtil.log("cookies : ");
//        for (String str : req.cookies().keySet()) {
//            Log4jUtil.log(str + ":" + req.cookies().get(str));
//        }
//    }


    /**
     * 支持https
     */
    public static void trustEveryone() {
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[] { new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            } }, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }


}
