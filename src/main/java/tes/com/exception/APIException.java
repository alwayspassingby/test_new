package tes.com.exception;

/**
 * Created by liuzhongdu on 2017/11/18.
 */
public class APIException extends Exception {
    private Integer errorCode = 20171213;
    private String errorMessage = "处理异常";

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public APIException(String message, Integer errorCode) {
        this(message);
        setErrorCode(errorCode);
        setErrorMessage(message);
    }

    public APIException(String message, Integer errorCode, Throwable cause) {
        super(message, cause);
        setErrorCode(errorCode);
        setErrorMessage(message);
    }

    public APIException(String message) {
        super(message);
        setErrorMessage(message);
    }


    public APIException(String message, Throwable cause) {
        super(message, cause);
        setErrorMessage(message);

    }

}
