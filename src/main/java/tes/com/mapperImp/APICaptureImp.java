package tes.com.mapperImp;

import org.springframework.stereotype.Repository;
import tes.com.common.entity.capture.APICapture;

/**
 * Created by liuzhongdu on 2017/7/9.
 */
@Repository
public interface APICaptureImp {

    /**
     * 添加抓取服务配置
     *
     * @param apiCapture
     * @return
     */
    public int insertCaptureConfig(APICapture apiCapture);


}
