package tes.com.mapperImp;

import tes.com.common.entity.apicase.TestCase;
import tes.com.common.entity.group.APIGroup;
import tes.com.common.entity.query.CaseQueryParam;
import tes.com.common.entity.task.APITask;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by liuzhongdu on 2017/7/9.
 */
@Repository
public interface APICaseImp {

    public TestCase getAPICaseById(@Param("id") Integer id);

    /**
     * 添加用例
     * @param testCase
     * @return
     */
    public int insertAPICase(TestCase testCase);

    public int deleteCaseById(@Param("id") Integer id);

    public int updateCase(TestCase testCase);

    public List<TestCase> queryAPICaseList(CaseQueryParam caseQueryParam);

    public int getAPICaseListTotal(CaseQueryParam caseQueryParam);

    public List<APIGroup> queryGroupList(@Param("id") Integer id);


    @Deprecated
    public List<APITask> getTask();

    public int insertCaseGroup(@Param("groupId") int groupId,@Param("caseId") int caseId,@Param("isRun") int isRun);

    public int getGroupIdByCaseId(@Param("caseId") Integer integer);

    public int updateGroupIdByCaseId(TestCase testCase);
}
