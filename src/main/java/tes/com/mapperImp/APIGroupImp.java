package tes.com.mapperImp;


import tes.com.common.entity.apicase.TestCase;
import tes.com.common.entity.group.APIGroup;
import tes.com.common.entity.group.APIGroupCase;
import tes.com.common.entity.group.MoveGroupCase;
import tes.com.common.entity.query.GroupQueryParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author wenxiaolong
 * @Description: 分组Imp类
 * @date 2018/01/09 14:05
 */
@Repository
public interface APIGroupImp {


    /**
     * 获取群组列表
     *
     * @param groupListQueryParam
     * @return
     */
    public List<APIGroup> queryGroupList(GroupQueryParam groupListQueryParam);


    /**
     * 通过指定的群组ID，获取群组，
     *
     * @param id
     * @return
     */
    public APIGroup getGroupById(Integer id);

    /**
     * 获取群组总数
     *
     * @param groupQueryParam
     * @return
     */
    public int getGroupTotal(GroupQueryParam groupQueryParam);

    public int deleteGroupById(Integer id);
    public int updateGroup(APIGroup apiGroup);
    public int insertGroup(APIGroup apiGroup);


    /**
     * 查询群组中的用例列表
     *
     * @param groupListQueryParam
     * @return
     */
    public List<TestCase> queryCaseListFromGroup(GroupQueryParam groupListQueryParam);

    /**
     * @param groupListQueryParam
     * @return
     */
    public int getCaseTotal(GroupQueryParam groupListQueryParam);

    public int insertGroupCase(APIGroupCase groupCase);

    public int deleteGroupCase(APIGroupCase groupCase);

    public int getGroupCaseTotalById(APIGroupCase groupCase);


    public List<APIGroup> isExist(Integer groupId);

    public Integer moveCaseFromGroup(MoveGroupCase moveGroupCase);

    public int updateGroupCasePriority(APIGroupCase apiGroupCase);

    public APIGroupCase queryGroupCaseById(@Param("id") Integer id);




}
