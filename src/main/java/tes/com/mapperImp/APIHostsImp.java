package tes.com.mapperImp;

import tes.com.common.entity.hosts.APIHosts;
import tes.com.common.entity.query.HostQueryParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by liuzhongdu on 2017/12/30.
 */
@Repository
public interface APIHostsImp {

    public int insertHosts(APIHosts hosts);

    public int updateHosts(APIHosts hosts);

    public int deleteHosts(@Param("id") Integer id);

    public APIHosts getHostById(@Param("id") Integer id);

    public List<APIHosts> getHostByIp(@Param("ip") String ip);

    public List<APIHosts> getHostByHost(@Param("host") String host);

    public List<APIHosts> queryHosts(HostQueryParam hostQueryParam);

    public Integer getHostTotal(HostQueryParam hostQueryParam);

}
