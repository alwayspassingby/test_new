package tes.com.mapperImp;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tes.com.common.entity.group.APIGroup;
import tes.com.common.entity.hosts.APIHosts;
import tes.com.common.entity.query.TaskQueryParam;
import tes.com.common.entity.task.*;
import tes.com.common.entity.user.APITestUser;

import java.util.List;

/**
 * Created by liuzhongdu on 2017/12/30.querySelectedGroup
 */
@Repository
public interface APITaskImp {

    public int insertTask(APITask task);

    public int updateTask(APITask task);

    public int deleteTask(@Param("id") Integer id);

    public APITask getTaskById(@Param("id") Integer id);

    public List<APITask> queryTaskList(TaskQueryParam taskQueryParam);

    public Integer getTaskTotal(TaskQueryParam taskQueryParam);

    public Integer getTaskTotal();


    public Integer addGroupByTask(APITaskGroup apiTaskGroup);
    public Integer deleteGroupByTask(@Param("taskId") Integer taskId, @Param("groupId") Integer groupId);

    public Integer getGroupTotalByTaskId(TaskQueryParam taskQueryParam);

    public List<APIGroup> getGroupListByTaskId(TaskQueryParam taskQueryParam);
//
//    List<APIGroup> querySelectedGroup(GroupQueryParam groupQueryParam);
//
//    List<APIGroup> queryNoSelectedGroup(GroupQueryParam groupQueryParam);

    public Integer runTask(@Param("taskId") Integer id);

    public Integer setCrontab(APICrontab apiCrontab);

    public Integer updateCrontab(APICrontab apiCrontab);

    public List<APICrontab> getCrontabListByTaskId(@Param("taskId") Integer taskId);


    public int addSetHostByTask(APITaskHost apiTaskHost);

    public int deleteSetHostByTask(APITaskHost apiTaskHost);
    public int updateSetHostByTask(APITaskHost apiTaskHost);

    public int getHostTotal(APITaskHost apiTaskHost);

    public String getExistingCrontab(Integer taskId);

    public List<APIHosts> queryHostsListFromTaskId(TaskQueryParam hostQueryParamByTask);

    public Integer getHostsTotalByTaskId(TaskQueryParam hostQueryParamByTask);

    public List<APITestUser> queryTUserListFromTaskId(TaskQueryParam userQueryParamByTask);

    public Integer getTUserListTotalByTaskId(TaskQueryParam userQueryParamByTask);

    public int getTUserTotalFromTask(APITaskUser apiTaskUser);


    public int addTaskTUser(APITaskUser apiTaskUser);

    public int deleteTaskTUser(APITaskUser apiTaskUser);
    public int updateTaskTUser(APITaskUser apiTaskUser);

    public Integer deleteCrontab(@Param("taskId")Integer taskId);
}
