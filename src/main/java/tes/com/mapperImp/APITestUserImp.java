package tes.com.mapperImp;

import tes.com.common.entity.query.TestUserQueryParam;
import tes.com.common.entity.user.YZJUserInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by liuzhongdu on 2017/12/30.
 */
@Repository
public interface APITestUserImp {

    public int insertTestUser(YZJUserInfo yzjUserInfo);

    public int updateTestUser(YZJUserInfo yzjUserInfo);

    public int deleteUser(@Param("id") Integer id);

    public YZJUserInfo getUserById(@Param("id") Integer id);

    public List<YZJUserInfo> getUserByName(@Param("name") String name);

    public int getTestUserTotal(TestUserQueryParam mallTestUserQueryParam);

    public List<YZJUserInfo> queryUserList(TestUserQueryParam mallTestUserQueryParam);

    public List<YZJUserInfo> getTUserList();




}
