package tes.com.mapperImp;

import tes.com.common.entity.MemberEventPointNotification;
import tes.com.common.entity.query.MemberEventNotificationQueryParam;

import java.util.List;

public interface MemberEventPointNotificationImp {

    public List<MemberEventPointNotification> queryMemberEventPointNotificationList(MemberEventNotificationQueryParam memberEventNotificationQueryParam);

    public List<MemberEventPointNotification> queryMemberEventPointNotificationListProduct(MemberEventNotificationQueryParam memberEventNotificationQueryParam);

    public int queryMemberEventPointNotificationListCount(MemberEventNotificationQueryParam memberEventNotificationQueryParam);

    public int queryMemberEventPointNotificationListCountProduct(MemberEventNotificationQueryParam memberEventNotificationQueryParam);
}
