package tes.com.mapperImp;

import tes.com.common.entity.query.ReportDetailParam;
import tes.com.common.entity.query.ReportQueryParam;
import tes.com.common.entity.report.ReportDetail;
import tes.com.common.entity.report.Reports;
import tes.com.common.entity.task.Feedback;
import tes.com.common.entity.task.Summary;

import java.util.Date;
import java.util.List;

/**
 * Created by baoan on 2018/3/25.
 */

public interface ReportMapper {

    public List<Reports> queryReportList(ReportQueryParam reportQueryParam);

    public List<ReportDetail> queryReportDetail(ReportDetailParam reportDetailParam);

    public List<ReportDetail> queryReportCount(ReportDetailParam reportDetailParam);

    public int addFeedback(Feedback addFeedback);

    public Object getChartData(ReportQueryParam reportQueryParam);

    int queryReportDetailCount(ReportDetailParam reportDetailParam);

    public int queryReportListCount(ReportQueryParam reportQueryParam);

    public Summary summaryData1(Date date);

    public Summary summaryData2(Date today);

    public Summary summaryData3(Date today);

    public Integer summaryData4(Date today);
    public Integer summaryData5(Date today);
    public Integer summaryData6(Date today);
}
