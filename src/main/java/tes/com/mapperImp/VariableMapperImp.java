package tes.com.mapperImp;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tes.com.common.entity.apicase.GC_Variable;
import tes.com.common.entity.query.VariableQueryParam;
import tes.com.common.entity.variable.CaseVariable;
import tes.com.common.entity.variable.GroupVariable;
import tes.com.common.entity.variable.TaskVariable;
import tes.com.common.entity.variable.Variable;

import java.util.List;


@Repository
public interface VariableMapperImp {

    /**
     * 添加用例变量
     *
     * @param caseVariable
     * @return
     */
    public int insertCaseVariable(CaseVariable caseVariable);

    /**
     * 通过vId获取 用例变量
     *
     * @param vId
     * @return
     */
    public CaseVariable getCaseVariableById(@Param("vId") Integer vId);


    /**
     * 获取变量列表
     *
     * @param variableQueryParam
     * @return
     */
    public List<CaseVariable> queryCaseVariableList(VariableQueryParam variableQueryParam);

    /**
     * 获取变量列表总数
     *
     * @param variableQueryParam
     * @return
     */
    public int getCaseVariableListTotal(VariableQueryParam variableQueryParam);


    /**
     * 删除变量，根据指定的Id
     *
     * @param vId
     * @return
     */
    public int deleteCaseVariableById(@Param("vId") Integer vId);


    public int deleteGroupCaseVariableById(@Param("gId") Integer gId, @Param("vId") Integer vId);

    /**
     * 获取gc_variable中 id的数
     *
     * @param vId
     * @return
     */
    public int get_gc_VariableTotalByVId(@Param("vId") Integer vId);

    public int updateCaseVariable(CaseVariable caseVariable);


    /**
     * 根据指定群组，指定用例ID的变量列表
     *
     * @param variableQueryParam
     * @return
     */
    public List<CaseVariable> getCaseVariableListFromGroupId(VariableQueryParam variableQueryParam);

    public List<TaskVariable> getTaskVariableList(VariableQueryParam variableQueryParam);

    public int addVariableToTaskVariableList(TaskVariable taskVariable);


    public List<TaskVariable> getTaskVariableWithTaskIdAndVId(@Param("taskId") Integer taskId,
                                                              @Param("vId") Integer vId,
                                                              @Param("status") Integer status);




    public int getCaseVariableTotalFromGroupId(VariableQueryParam variableQueryParam);

    public int getTaskVariableListTotal(VariableQueryParam variableQueryParam);

    /**
     * 在群组中添加关联的变量
     * @param groupVariable
     * @return
     */
    public int insertGroupCaseVariable(GC_Variable groupVariable);


    /**
     * 添加全局变量
     * @param globalVariable
     * @return
     */
    public int insertGlobalVariable(Variable globalVariable);

    /**
     * 更新全局变量
     *
     * @param globalVariable
     * @return
     */
    public int updateGlobalVariable(Variable globalVariable);

    /**
     * 根据id,删除变量
     *
     * @param vId
     * @return
     */
    public int deleteGlobalVariableById(@Param("vId") Integer vId);


    /**
     * 根据ID获取全局变量对象
     *
     * @param vId
     * @return
     */
    public Variable getGlobalVariableById(@Param("vId") Integer vId);


    /**
     * 获取全局变量列表
     *
     * @param variableQueryParam
     * @return
     */
    public List<Variable> queryGlobalVariableList(VariableQueryParam variableQueryParam);

    /**
     * 获取全局变量列表总数
     *
     * @param variableQueryParam
     * @return
     */
    public int getGlobalVariableListTotal(VariableQueryParam variableQueryParam);

    public int updateTaskVariableStatus(@Param("taskId") Integer taskId, @Param("vId") Integer vId, @Param("status") Integer status);

    public int updateTaskVariable(TaskVariable taskVariable);


    /**
     * 插入 task 类型的 变量
     * @param taskVariable
     * @return
     */
    public int insertTaskTypeVariable(TaskVariable taskVariable);


    /**
     * 获取该群组自定义变量列表的总数
     *
     * @param variableQueryParam
     * @return
     */
    public int getGroupVariableListTotal(VariableQueryParam variableQueryParam);


    /**
     * 获取群组自定义变量列表
     *
     * @param variableQueryParam
     * @return
     */
    public List<GroupVariable> getGroupVariableList(VariableQueryParam variableQueryParam);

    //    插入group类型变量
    public int insertGroupTypeVariable(GroupVariable groupVariable);


    /**
     * 根据群组ID，vID 和 status 获取变量
     *
     * @param groupId
     * @param vId
     * @param status
     * @return
     */
    public List<GroupVariable> getGroupVariableWithTaskIdAndVId(@Param("groupId") Integer groupId,
                                                                @Param("vId") Integer vId,
                                                                @Param("status") Integer status);


    public int addVariableToGroupVariableList(GroupVariable groupVariable);


    public int updateGroupVariableStatus(@Param("groupId") Integer groupId, @Param("vId") Integer vId, @Param("status") Integer status);

    public int updateGroupVariableStatusById(GroupVariable groupVariable);

}
