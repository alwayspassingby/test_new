package tes.com.service;

import tes.com.exception.APIException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by liuzhongdu on 2017/12/9.
 */
public class ParameterContentFilterService {

    /**
     * 字符内容过滤
     *
     * @param content
     * @return
     * @throws Exception
     */
    public String contentFilter(String content) throws APIException {
        if (content == null || content.trim().isEmpty()) {
            return null;
        }

        //自动生成时间戳
        if (content.contains("${AGT}")) {
            String dateStr = String.valueOf(new Date().getTime());
            content = content.replace("${AGT}", dateStr);
        }
        //自动生成手机号
        if (content.contains("${AGP}")) {
            content = content.replace("${AGP}", autoPN());
        }
        //用当前系统的毫秒，生成唯一的字符串
        if (content.contains("${AGUS}")) {
            content = content.replace("${AGUS}", Long.toHexString(System.currentTimeMillis()));
        }

        if (content.matches("\\$\\{RD\\d+\\}")) {
            String randomNumber = content.trim().replace("$", "").replace("{RD", "").replace("}", "").trim();
            int number = Integer.parseInt(randomNumber);
            StringBuffer sb = new StringBuffer();
            int index = 10;
            Random r = new Random();
            for (int i = 0; i < number; i++) {
                if (index == 0) {
                    int num = r.nextInt(10);
                    sb.append(num);
                } else {
                    int num = r.nextInt(index);
                    sb.append(num);
                    index--;
                }
            }
            content = content.replace("${RD" + randomNumber + "}", sb.toString());
        }

        //生成Http header中的date
        if (content.contains("${AGT_GMT}")) {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            String dateStr = dateFormat.format(calendar.getTime());
            content = content.replace("${AGT_GMT}", dateStr);
        }

        if (content.matches("^\\$\\{img\\d\\}$")) {
            String key = content.trim().replace("$", "").replace("{", "").replace("}", "").trim();
            key = key.replace("img", "");
            content = autoBase64Img(key);
        }

//        if (content.trim().matches("^\\$\\{\\w+\\.\\w+\\}$")) {
//            Pattern pattern = Pattern.compile("^\\$\\{\\w+\\.\\w+\\}$");
//            Matcher matcher = pattern.matcher(content.trim());
//            List<String> tus = new ArrayList<String>();
//            Boolean result = matcher.find();
//            while (result) {
//                String group = matcher.group();
//                String str = group.trim().replace("$", "").replace("{", "").replace("}", "").trim();
//                String[] tms = str.split("\\.");
//                if (tms.length == 2) {
//                    Map<String, String> map = APICache.userLoginInfoMap.get(tms[0]);
//                    if (map == null || map.isEmpty()) {
//                        content = content.trim().replace(group, "");
//                    } else {
//                        String value = map.get(tms[1]);
//                        if (value == null || value.isEmpty()) {
//                            content = content.trim().replace(group, "");
//                        } else {
//                            content = content.trim().replace(group, value);
//                        }
//                    }
//                } else {
//                    content = content.trim().replace(group, "");
//                }
//                result = matcher.find();
//            }
//        }

//        if (content.trim().matches("^\\$\\{\\w+\\}$")){
//            String key = content.trim().replace("$", "").replace("{", "").replace("}", "").trim();
//            content = APICache.staticVariableMap.get(key) == null ? "" : APICache.staticVariableMap.get(key);
//        }

        return content;
    }

    /**
     * 自动生成手机号
     *
     * @return
     */
    public String autoPN() {
        String[] prefix = new String[]{"137", "158", "188", "189", "133"};
        int p = new Random().nextInt(4);
        int n = (int) (Math.random() * 100000000);
        return prefix[p] + n;
    }

    /**
     * 自动生成图片的base 64编码
     *
     * @param num
     * @return
     * @throws Exception
     */
    public String autoBase64Img(String num) throws APIException {
        int index = 0;
        byte[] bytes = null;

        try {

            if (num.matches("^\\d$")) {
                index = Integer.parseInt(num);
            }
            String path = null;
            if (this.getClass().getResource("/img/img" + index + ".jpeg") == null) {
                path = this.getClass().getResource("/img/img" + 0 + ".jpeg").toURI().getPath();
            } else {
                path = this.getClass().getResource("/img/img" + index + ".jpeg").toURI().getPath();
            }

            FileInputStream fis = null;
            ByteArrayOutputStream bos = null;
            File file = new File(path);
            fis = new FileInputStream(file);
            bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            fis.close();
            bos.close();
            bytes = bos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            new APIException(e.getMessage(), e.getCause());

        }
        String base64 = new String(Base64.getEncoder().encode(bytes));

        return base64;
    }

}
