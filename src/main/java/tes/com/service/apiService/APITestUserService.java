package tes.com.service.apiService;

import tes.com.common.entity.query.TestUserQueryParam;
import tes.com.common.entity.result.APIObjectResult;
import tes.com.common.entity.result.BaseQueryResult;
import tes.com.common.entity.user.YZJUserInfo;

import java.util.List;

/**
 * Created by liuzhongdu on 2018/1/21.
 */
public interface APITestUserService {


    public Object insertTestUser(YZJUserInfo yzjUserInfo, String validCode);

    public Object updateTestUser(YZJUserInfo yzjUserInfo, String validCode);

    public Object deleteUser(Integer id);

    public Object getUserById(Integer id);

    public BaseQueryResult<YZJUserInfo> queryUserList(TestUserQueryParam mallTestUserQueryParam);

    public APIObjectResult<List<YZJUserInfo>> getTUsers();

    public APIObjectResult<String> getCookiesByUserId(YZJUserInfo yzjUserInfo, String validCode);


}
