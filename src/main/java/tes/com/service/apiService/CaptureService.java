package tes.com.service.apiService;

import tes.com.common.entity.capture.APICapture;
import tes.com.common.entity.result.APIBaseResult;

public interface CaptureService {

    /**
     * 添加群组
     *
     * @param apiCapture
     * @return
     */
    public APIBaseResult addCaptureConfig(APICapture apiCapture);

}
