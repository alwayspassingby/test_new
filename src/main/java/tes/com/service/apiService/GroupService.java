package tes.com.service.apiService;

import tes.com.common.entity.apicase.TestCase;
import tes.com.common.entity.group.APIGroup;
import tes.com.common.entity.group.APIGroupCase;
import tes.com.common.entity.group.MoveGroupCase;
import tes.com.common.entity.query.GroupQueryParam;
import tes.com.common.entity.result.APIBaseResult;
import tes.com.common.entity.result.BaseQueryResult;
import org.apache.ibatis.annotations.Param;

public interface GroupService {
    /**
     * 获取群组列表
     * @param groupListQueryParam
     * @return
     */
    public BaseQueryResult<APIGroup> queryGroupList(GroupQueryParam groupListQueryParam);

    /**
     * 添加群组
     * @param apigroup
     * @return
     */
    public int addGroup( APIGroup apigroup);

    /**
     * 删除群组
     * @param id
     * @return
     */
    public int deleteGroupById(Integer id);

    /**
     * 更新群组
     * @param apigroup
     * @return
     */
    public int updateGroup(APIGroup apigroup);


    /**
     * 获取群组，根据群组ID
     * @param id
     * @return
     */
    public APIGroup getGroupById(Integer id);

    /**
     * 获取用例列表
     * @param groupListQueryParam
     * @return
     */
    public BaseQueryResult<TestCase> getCaseListByGroupId(GroupQueryParam groupListQueryParam);

    /**
     * 添加群组用例
     * @param groupCase
     * @return
     */
    public APIBaseResult addGroupCase(APIGroupCase groupCase);

    /**
     * 删除群组用例
     * @param groupCase
     * @return
     */
    public APIBaseResult deleteGroupCase(APIGroupCase groupCase);

    /**
     * 根据群组ID，判断是否存在
     * @param groupId
     * @return
     */
    public Object isExist(Integer groupId);


    /**
     * 迁移群组中的用例
     * @param moveGroupCase
     * @return
     */
    public Object moveGroupCase(MoveGroupCase moveGroupCase);

    /**
     * 更新群组用例的优先级
     * @param id groupCase ID
     * @param priority 优先级
     * @return
     */
    public APIBaseResult updateGroupCasePriority(Integer id, Integer priority);

    /**
     * 根据ID去获取APIGroupCase
     * @param id
     * @return
     */
    public APIGroupCase queryGroupCaseById(@Param("id") Integer id);
}
