package tes.com.service.apiService;

import tes.com.common.entity.hosts.APIHosts;
import tes.com.common.entity.query.HostQueryParam;
import tes.com.common.entity.result.BaseQueryResult;

/**
 * Created by liuzhongdu on 2017/12/30.
 */
public interface HostService {

    public Object insertHosts(APIHosts hosts);

    public Object updateHosts(APIHosts hosts);

    public Object deleteHosts(Integer hId);

    public APIHosts getHostById(Integer hId);

    public Object getHostByIp(String ip);

    public Object getHostByHost(String host);

    public BaseQueryResult<APIHosts> queryHosts(HostQueryParam hostQueryParam);


}
