package tes.com.service.apiService;

import tes.com.common.entity.MemberEventPointNotification;
import tes.com.common.entity.query.MemberEventNotificationQueryParam;
import tes.com.common.entity.result.BaseQueryResult;

public interface MemberEventPointNotificationService {

    public BaseQueryResult<MemberEventPointNotification> queryMemberEventPointNotificationList(MemberEventNotificationQueryParam memberEventNotificationQueryParam);

    public BaseQueryResult<MemberEventPointNotification> queryMemberEventPointNotificationListProduct(MemberEventNotificationQueryParam memberEventNotificationQueryParam);
}
