package tes.com.service.apiService;

import tes.com.common.entity.query.ReportDetailParam;
import tes.com.common.entity.query.ReportQueryParam;
import tes.com.common.entity.report.ReportDetail;
import tes.com.common.entity.report.Reports;
import tes.com.common.entity.result.BaseQueryResult;
import tes.com.common.entity.task.Feedback;

/**
 * Created by baoan on 2018/3/25.
 */
public interface ReportService {

    public BaseQueryResult<Reports> queryReportList(ReportQueryParam reportQueryParam);

    public BaseQueryResult<ReportDetail> queryReportDetail(ReportDetailParam reportDetailParam);

    public Object queryReportCount(ReportDetailParam reportDetailParam);

    public int addFeedback(Feedback addFeedback);

    public Object getChartData(ReportQueryParam reportQueryParam);

    public Object summaryData();
}
