package tes.com.service.apiService;

import tes.com.common.entity.group.APIGroup;
import tes.com.common.entity.hosts.APIHosts;
import tes.com.common.entity.query.TaskQueryParam;
import tes.com.common.entity.result.APIBaseResult;
import tes.com.common.entity.result.BaseQueryResult;
import tes.com.common.entity.task.APICrontab;
import tes.com.common.entity.task.APITask;
import tes.com.common.entity.task.APITaskHost;
import tes.com.common.entity.task.APITaskUser;
import tes.com.common.entity.user.APITestUser;
import org.apache.ibatis.annotations.Param;

/**
 * Created by baoan on 2018/1/11.
 */
public interface TaskService {

    public Object insertTasks(APITask tasks);

    public Object updateTasks(APITask tasks);

    public Object deleteTasks(Integer tId);

    public APITask getTaskById(Integer tId);

    public BaseQueryResult<APITask> queryTasks(TaskQueryParam taskQueryParam);

    public int getTaskTotal();

    public Object addGroupByTask(@Param("groupId") Integer groupId, @Param("taskId") Integer taskId);
    public Object deleteGroupByTask(@Param("groupId") Integer groupId, @Param("taskId") Integer taskId);


    public BaseQueryResult<APIGroup> getGroupListByTaskId(TaskQueryParam taskQueryParam);

//    BaseQueryResult<APIGroup> querySelectedGroup(GroupQueryParam groupQueryParam);
//
//    BaseQueryResult<APIGroup> queryNoSelectedGroup(GroupQueryParam groupQueryParam);





    public int runTask(@Param("id") Integer id);

    public APIBaseResult setCrontab(APICrontab apiCrontab);

    public APIBaseResult addSetHostByTask(APITaskHost apiTaskHost);

    public APIBaseResult deleteSetHostByTask(APITaskHost apiTaskHost);

    public Object getExistingCrontab(Integer taskId);

    public BaseQueryResult<APIHosts> queryHostByTask(TaskQueryParam hostQueryParamByTask);

    public BaseQueryResult<APITestUser> queryTUserListByTask(TaskQueryParam userQueryParamByTask);

    public APIBaseResult addTaskTUser(APITaskUser apiTaskUser);

    public APIBaseResult deleteTaskTUser(APITaskUser apiTaskUser);

}
