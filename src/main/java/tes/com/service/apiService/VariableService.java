package tes.com.service.apiService;

import tes.com.common.entity.apicase.GC_Variable;
import tes.com.common.entity.query.VariableQueryParam;
import tes.com.common.entity.result.APIBaseResult;
import tes.com.common.entity.result.APIObjectResult;
import tes.com.common.entity.result.BaseQueryResult;
import tes.com.common.entity.variable.CaseVariable;
import tes.com.common.entity.variable.GroupVariable;
import tes.com.common.entity.variable.TaskVariable;
import tes.com.common.entity.variable.Variable;

public interface VariableService {

    public APIBaseResult addCaseVariable(CaseVariable caseVariable);

    public APIObjectResult<CaseVariable> getCaseVariableById(Integer vId);

    public BaseQueryResult<CaseVariable> queryCaseVariableList(VariableQueryParam variableQueryParam);

    public APIBaseResult delCaseVariableById(Integer vId);

    public APIBaseResult updateCaseVariable(CaseVariable caseVariable);


    public APIBaseResult moveTaskVariable(Integer taskId, Integer vId);

    public APIBaseResult addToTaskVariableList(TaskVariable taskVariable);


    public APIBaseResult delGroupCaseVariableById(Integer gId, Integer vId);
    /**
     * 根据指定的groupID和caseID获取 变量列表
     *
     * @param variableQueryParam
     * @return
     */
    public BaseQueryResult<CaseVariable> getCaseVariableListFromGroupId(VariableQueryParam variableQueryParam);

    public BaseQueryResult<TaskVariable> getTaskVariableList(VariableQueryParam variableQueryParam);


    public APIBaseResult addGroupCaseVariable(GC_Variable groupVariable);

    public APIBaseResult addGlobalVariable(Variable globalVariable);


    public BaseQueryResult<Variable> getGlobalVariableList(VariableQueryParam variableQueryParam);

    public APIObjectResult<Variable> getGlobalVariableById(Integer vId);

    public APIBaseResult updateGlobalVariable(Variable globalVariable);

    public APIBaseResult delGlobalVariableById(Integer vId);

    public APIBaseResult addTaskVariable(TaskVariable taskVariable);


    /**
     * 获取群组变量列表
     *
     * @param variableQueryParam
     * @return
     */
    public BaseQueryResult<GroupVariable> getGroupVariableList(VariableQueryParam variableQueryParam);


    /**
     * 添加群组变量,这个群组变量列表是在 variable表中，还没有关联到群组变量列表中
     *
     * @param groupVariable
     * @return
     */
    public APIBaseResult addGroupTypeVariable(GroupVariable groupVariable);

    /**
     * 把群组变量添加到群组的变量列表中（已经添加的变量列表 group_variable表中）
     *
     * @param groupVariable
     * @return
     */
    public APIBaseResult addGroupVariableToVariableList(GroupVariable groupVariable);

    /**
     * 工具groupId 和 vId,把改变量从已经添加变量列表中删除
     *
     * @param group
     * @param vId
     * @return
     */
    public APIBaseResult moveGroupVariable(Integer group, Integer vId);


}
