package tes.com.service.apiServiceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tes.com.common.entity.capture.APICapture;
import tes.com.common.entity.result.APIBaseResult;
import tes.com.mapperImp.APICaptureImp;
import tes.com.service.apiService.CaptureService;

/**
 * Created by liuzhongdu on 2017/7/9.
 */
@Service("captureService")
public class APICaptureServiceImp implements CaptureService {

    @Autowired
    private APICaptureImp apiCaptureImp;

    /**
     * 添加服务配置
     *
     * @param apiCapture
     * @return
     */
    @Override
    public APIBaseResult addCaptureConfig(APICapture apiCapture) {
        APIBaseResult baseResult = new APIBaseResult();
        int res = apiCaptureImp.insertCaptureConfig(apiCapture);
        if (res == 1) {
            baseResult.setCode(0);
            baseResult.setMessage("添加服务配置成功!");
        } else {
            baseResult.setCode(101);
            baseResult.setMessage("添加失败！");
        }
        return baseResult;
    }
}
