package tes.com.service.apiServiceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tes.com.common.entity.apicase.TestCase;
import tes.com.common.entity.query.CaseQueryParam;
import tes.com.common.entity.result.BaseQueryResult;
import tes.com.mapperImp.APICaseImp;

import java.util.List;

/**
 * Created by liuzhongdu on 2017/7/9.
 */
@Service
public class APICaseService {

    @Autowired
    private APICaseImp apiCaseImp;


    /**
     * 添加用例
     * @param testCase
     * @return
     */
    public int addCase( TestCase testCase){
        return apiCaseImp.insertAPICase(testCase);
    }

    /**
     * 根据用例ID，获取用例
     *
     * @param id
     * @return
     */
    public TestCase getCaseById(Integer id){
        return apiCaseImp.getAPICaseById(id);
    }

    /**
     * 根据用例ID，删除用例，
     * @param id
     * @return
     */
    public int deleteCaseById(Integer id){
        return apiCaseImp.deleteCaseById(id);
    }

    /**
     * @param testCase
     * @return
     */
    public int updateCase(TestCase testCase){
//        if (apiCaseImp.getGroupIdByCaseId(testCase.getaId()) == 0){
//            apiCaseImp.insertCaseGroup(testCase.getGroupId(), testCase.getaId(),0);
//        }else {
//            apiCaseImp.updateGroupIdByCaseId(testCase);
//        }
        return apiCaseImp.updateCase(testCase);
    }

    /**
     * 获取用例列表
     *
     * @param caseQueryParam
     * @return
     */
    public BaseQueryResult<TestCase> queryAPICases(CaseQueryParam caseQueryParam) {

        BaseQueryResult<TestCase> apiCaseAPICaseQuery = new BaseQueryResult<TestCase>();
        int total = getCasetotal(caseQueryParam);
        List<TestCase> testCaseList = apiCaseImp.queryAPICaseList(caseQueryParam);
       if (total>0){
           apiCaseAPICaseQuery.setTotal(total);
           apiCaseAPICaseQuery.setRows(testCaseList);
           System.out.println(testCaseList.toString());
       }else {
           apiCaseAPICaseQuery.setTotal(0);
           apiCaseAPICaseQuery.setRows(testCaseList);
       }
       return apiCaseAPICaseQuery;
    }

    /**
     * 获取用例总数
     *
     * @param caseQueryParam
     * @return
     */
    public int getCasetotal(CaseQueryParam caseQueryParam) {
        return apiCaseImp.getAPICaseListTotal(caseQueryParam);
    }


}
