package tes.com.service.apiServiceImp;

import tes.com.common.entity.apicase.TestCase;
import tes.com.common.entity.group.APIGroup;
import tes.com.common.entity.group.APIGroupCase;
import tes.com.common.entity.group.MoveGroupCase;
import tes.com.common.entity.query.GroupQueryParam;
import tes.com.common.entity.result.APIBaseResult;
import tes.com.common.entity.result.BaseQueryResult;
import tes.com.mapperImp.APIGroupImp;
import tes.com.service.apiService.GroupService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wenxiaolong
 * @Description: 分组服务类
 * @date 2018/01/09 14:05
 */
@Service("groupService")
public class APIGroupService implements GroupService {

    @Autowired
    private APIGroupImp apigroupimp;

    /**
     * 获取群组列表
     *
     * @param groupListQueryParam
     * @return
     */
    @Override
    public BaseQueryResult<APIGroup> queryGroupList(GroupQueryParam groupListQueryParam) {
        BaseQueryResult<APIGroup> apigroupquery = new BaseQueryResult<APIGroup>();
        int total = getAPIGrouptotal(groupListQueryParam);
        List<APIGroup> apigroupList = apigroupimp.queryGroupList(groupListQueryParam);
        if (total > 0) {
            apigroupquery.setTotal(total);
            apigroupquery.setRows(apigroupList);
        } else {
            apigroupquery.setTotal(0);
            apigroupquery.setRows(apigroupList);
        }
        return apigroupquery;
    }

    @Override
    public int addGroup( APIGroup apigroup){
        return apigroupimp.insertGroup(apigroup);
    }

    @Override
    public int deleteGroupById(Integer id){
        return apigroupimp.deleteGroupById(id);
    }

    @Override
    public int updateGroup(APIGroup apigroup){
        return apigroupimp.updateGroup(apigroup);
    }

    public int getAPIGrouptotal(GroupQueryParam groupQueryParam) {
        return apigroupimp.getGroupTotal(groupQueryParam);
    }

    @Override
    public APIGroup getGroupById(Integer id){
        return apigroupimp.getGroupById(id);
    }


    /**
     * 获取用例列表
     *
     * @param groupListQueryParam
     * @return
     */
    @Override
    public BaseQueryResult<TestCase> getCaseListByGroupId(GroupQueryParam groupListQueryParam) {

        BaseQueryResult<TestCase> groupQueryList = new BaseQueryResult<TestCase>();

        Integer total = apigroupimp.getCaseTotal(groupListQueryParam);

        List<TestCase> testCases = new ArrayList<TestCase>();

        if (total > 0) {
            testCases = apigroupimp.queryCaseListFromGroup(groupListQueryParam);
            groupQueryList.setTotal(total);
            groupQueryList.setRows(testCases);

        } else {
            groupQueryList.setTotal(0);
            groupQueryList.setRows(testCases);
        }
        return groupQueryList;
    }


    /**
     * 添加群组用例
     *
     * @param groupCase
     * @return
     */
    @Override
    public APIBaseResult addGroupCase(APIGroupCase groupCase) {
        APIBaseResult mallBaseResult = new APIBaseResult();
        Integer total = apigroupimp.getGroupCaseTotalById(groupCase);
        if (total == 0) {
            int result = apigroupimp.insertGroupCase(groupCase);
            if (result == 1) {
                mallBaseResult.setCode(0);
                mallBaseResult.setMessage("添加成功!");
            } else {
                mallBaseResult.setCode(101);
                mallBaseResult.setMessage("添加失败！");
            }
        } else {
            mallBaseResult.setCode(102);
            mallBaseResult.setMessage("已经存在该用例！");
        }

        return mallBaseResult;
    }

    /**
     * 删除群组用例
     *
     * @param groupCase
     * @return
     */
    @Override
    public APIBaseResult deleteGroupCase(APIGroupCase groupCase) {
        APIBaseResult mallBaseResult = new APIBaseResult();
        Integer result = apigroupimp.deleteGroupCase(groupCase);
        if (result > 0) {
            mallBaseResult.setCode(0);
            mallBaseResult.setMessage("删除成功!");
        } else {
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("删除失败！");
        }

        return mallBaseResult;
    }

    @Override
    public Object isExist(Integer groupId) {
        APIBaseResult mallBaseResult = new APIBaseResult();
        Integer result = apigroupimp.isExist(groupId).size();
        if (result > 0) {
            mallBaseResult.setCode(200);
            mallBaseResult.setMessage("分组id存在!");
        } else {
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("分组id 不存在！");
        }

        return mallBaseResult;
    }

    @Override
    public Object moveGroupCase(MoveGroupCase moveGroupCase) {
        APIBaseResult mallBaseResult = new APIBaseResult();
        Integer resultFrom = apigroupimp.moveCaseFromGroup(moveGroupCase);
        if (resultFrom == 1 ){
            mallBaseResult.setCode(200);
            mallBaseResult.setMessage("迁移成功");
        }else {
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("迁移失败");
        }
        return mallBaseResult;
    }

    /**
     * 更新群组用例的优先级
     * @param id
     * @param priority
     * @return
     */
    @Override
    public APIBaseResult updateGroupCasePriority(Integer id, Integer priority) {
        APIBaseResult mallBaseResult = new APIBaseResult();
        APIGroupCase apiGroupCase = queryGroupCaseById(id);
        if (apiGroupCase == null){
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("没有找到该Id记录！");
            return mallBaseResult;
        }
        apiGroupCase.setPriority(priority);
        int flag = apigroupimp.updateGroupCasePriority(apiGroupCase);
        if (flag == 1){
            mallBaseResult.setCode(0);
            mallBaseResult.setMessage("更新成功！");
            return mallBaseResult;
        }else {
            mallBaseResult.setCode(102);
            mallBaseResult.setMessage("更新失败！");
            return mallBaseResult;
        }
    }

    /**
     * 根据ID去获取APIGroupCase
     * @param id
     * @return
     */
    @Override
    public APIGroupCase queryGroupCaseById(@Param("id") Integer id){
        return apigroupimp.queryGroupCaseById(id);
    }

}
