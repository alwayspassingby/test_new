package tes.com.service.apiServiceImp;

import tes.com.common.entity.hosts.APIHosts;
import tes.com.common.entity.query.HostQueryParam;
import tes.com.common.entity.result.APIBaseResult;
import tes.com.common.entity.result.BaseQueryResult;
import tes.com.mapperImp.APIHostsImp;
import tes.com.service.apiService.HostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by liuzhongdu on 2017/12/30.
 */
@Service("hostService")
public class APIHostServiceImp implements HostService {


    @Autowired
    private APIHostsImp mallHostsImp;


    /**
     * 插入host
     *
     * @param hosts
     * @return
     */
    @Override
    public Object insertHosts(APIHosts hosts) {
        APIBaseResult mallBaseResult = new APIBaseResult();
        if (!hosts.getIp().matches("^((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)$")) {
            mallBaseResult.setCode(107);
            mallBaseResult.setMessage("ip格式不正确！");
            return mallBaseResult;
        }

        if (!hosts.getHost().matches("^\\S(\\S+\\.)+\\S+$")) {
            mallBaseResult.setCode(107);
            mallBaseResult.setMessage("host格式不正确！");
            return mallBaseResult;
        }

        hosts.setIp(hosts.getIp().trim());
        hosts.setHost(hosts.getHost().trim());
        hosts.setStatus(0);
        hosts.setCreateTime(new Date());
        int flag = mallHostsImp.insertHosts(hosts);
        if (flag == 1) {
            mallBaseResult.setCode(0);
            mallBaseResult.setMessage("插入成功，ID：" + hosts.gethId());
            return mallBaseResult;
        } else {
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("插入失败！");
            return mallBaseResult;
        }
    }

    /**
     * @param hosts
     * @return
     */
    @Override
    public Object updateHosts(APIHosts hosts) {
        APIBaseResult returnResult = new APIBaseResult();
        int flag = mallHostsImp.updateHosts(hosts);

        if (flag == 1) {
            returnResult.setCode(0);
            returnResult.setMessage("更新成功！");
            return returnResult;
        } else {
            returnResult.setCode(101);
            returnResult.setMessage("更新失败！");
            return returnResult;
        }
    }

    /**
     * 根据ID删除host
     *
     * @param hId
     * @return
     */
    @Override
    public Object deleteHosts(Integer hId) {
        APIBaseResult returnResult = new APIBaseResult();
        int flag = mallHostsImp.deleteHosts(hId);
        if (flag == 1) {
            returnResult.setCode(0);
            returnResult.setMessage("删除成功！！");
            return returnResult;
        } else {
            returnResult.setCode(101);
            returnResult.setMessage("删除失败！");
            return returnResult;
        }
    }

    /**
     * @param hId
     * @return
     */
    @Override
    public APIHosts getHostById(Integer hId) {
        APIHosts apiHosts = mallHostsImp.getHostById(hId);
        return apiHosts;
    }

    /**
     * @param ip
     * @return
     */
    @Override
    public Object getHostByIp(String ip) {
        APIBaseResult mallBaseResult = new APIBaseResult();
        if (!ip.matches("^^((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)$")) {
            mallBaseResult.setCode(107);
            mallBaseResult.setMessage("ip格式不正确！");
            return mallBaseResult;
        }
        List<APIHosts> mallHostsList = mallHostsImp.getHostByIp(ip);

        return mallHostsList;
    }

    /**
     * @param host
     * @return
     */
    @Override
    public Object getHostByHost(String host) {
//        APIBaseResult mallBaseResult = new APIBaseResult();
//
//        if (!host.matches("^\\S(\\S+\\.)+\\S+$")) {
//            mallBaseResult.setCode(107);
//            mallBaseResult.setMessage("host格式不正确！");
//            return mallBaseResult;
//        }
        List<APIHosts> mallHostsList = mallHostsImp.getHostByHost(host);


        return mallHostsList;
    }

    /**
     * 请求查询host
     *
     * @param hostQueryParam
     * @return
     */
    @Override
    public BaseQueryResult<APIHosts> queryHosts(HostQueryParam hostQueryParam) {

        BaseQueryResult<APIHosts> baseQueryResult = new BaseQueryResult<APIHosts>();

        int total = mallHostsImp.getHostTotal(hostQueryParam);
        baseQueryResult.setTotal(total);

        baseQueryResult.setRows(mallHostsImp.queryHosts(hostQueryParam));

        return baseQueryResult;
    }
}
