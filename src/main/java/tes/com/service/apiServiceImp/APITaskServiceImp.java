package tes.com.service.apiServiceImp;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tes.com.common.entity.group.APIGroup;
import tes.com.common.entity.hosts.APIHosts;
import tes.com.common.entity.query.TaskQueryParam;
import tes.com.common.entity.result.APIBaseResult;
import tes.com.common.entity.result.BaseQueryResult;
import tes.com.common.entity.task.*;
import tes.com.common.entity.user.APITestUser;
import tes.com.mapperImp.APITaskImp;
import tes.com.service.apiService.TaskService;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by baoan on 2018/1/11.
 */
@Service("taskService")
public class APITaskServiceImp implements TaskService {

    @Autowired
    private APITaskImp mallTaskImp;

    @Override
    public Object insertTasks(APITask tasks) {
        tasks.setStatus(0);
        tasks.setCreateTime(new Date());
        int taskResult = mallTaskImp.insertTask(tasks);
        return taskResult;
    }

    @Override
    public Object updateTasks(APITask tasks) {
        int taskResult = mallTaskImp.updateTask(tasks);
        return taskResult;
    }

    @Override
    public Object deleteTasks(Integer tId) {
        int taskResult = mallTaskImp.deleteTask(tId);
        return taskResult;
    }

    @Override
    public APITask getTaskById(Integer tId) {
        APITask taskResult = mallTaskImp.getTaskById(tId);
        return taskResult;
    }

    @Override
    public BaseQueryResult<APITask> queryTasks(TaskQueryParam taskQueryParam) {
        BaseQueryResult<APITask> baseQueryResult = new BaseQueryResult<APITask>();
        int total = mallTaskImp.getTaskTotal();
        List<APITask> taskList = mallTaskImp.queryTaskList(taskQueryParam);
        if (total>0){
            baseQueryResult.setTotal(total);
            baseQueryResult.setRows(taskList);
        }else {
            baseQueryResult.setTotal(0);
            baseQueryResult.setRows(taskList);
        }

        return baseQueryResult;
    }

    @Override
    public int getTaskTotal() {
        return 0;
    }

    @Override
    public Object addGroupByTask(@Param("groupId") Integer groupId, @Param("taskId") Integer taskId) {

        APITaskGroup taskGroup = new APITaskGroup();
        taskGroup.setCreateTime(new Date());
        taskGroup.setModifyTime(new Date());
        taskGroup.setGroupId(groupId);
        taskGroup.setTaskId(taskId);
        taskGroup.setStatus(0);
        return mallTaskImp.addGroupByTask(taskGroup);
    }

    @Override
    public Object deleteGroupByTask(@Param("groupId") Integer groupId, @Param("taskId") Integer taskId) {
        return  mallTaskImp.deleteGroupByTask(taskId,groupId);
    }


    @Override
    public BaseQueryResult<APIGroup> getGroupListByTaskId(TaskQueryParam taskQueryParam) {
        BaseQueryResult<APIGroup> groupBaseQueryResult = new BaseQueryResult<APIGroup>();

        Integer total = mallTaskImp.getGroupTotalByTaskId(taskQueryParam);

        List<APIGroup> groupList = new ArrayList<APIGroup>();

        if (total > 0) {
            groupList = mallTaskImp.getGroupListByTaskId(taskQueryParam);
            groupBaseQueryResult.setTotal(total);
            groupBaseQueryResult.setRows(groupList);

        } else {
            groupBaseQueryResult.setTotal(0);
            groupBaseQueryResult.setRows(groupList);
        }
        return groupBaseQueryResult;
    }

//    @Override
//    public BaseQueryResult<APIGroup> querySelectedGroup(GroupQueryParam groupQueryParam) {
//        BaseQueryResult<APIGroup> apiGroupQuery = new BaseQueryResult<APIGroup>();
//        apiGroupQuery.setRows(mallTaskImp.querySelectedGroup(groupQueryParam));
//        apiGroupQuery.setTotal(apiGroupQuery.getRows() == null? 0:apiGroupQuery.getRows().size());
//        return apiGroupQuery;
//    }
//
//    @Override
//    public BaseQueryResult<APIGroup> queryNoSelectedGroup(GroupQueryParam groupQueryParam) {
//        BaseQueryResult<APIGroup> apiGroupQuery = new BaseQueryResult<APIGroup>();
//        apiGroupQuery.setRows(mallTaskImp.queryNoSelectedGroup(groupQueryParam));
//        apiGroupQuery.setTotal(apiGroupQuery.getRows() == null? 0:apiGroupQuery.getRows().size());
//        return apiGroupQuery;
//    }

    @Override
    public int runTask(@Param("id") Integer id) {
        return mallTaskImp.runTask(id);
    }

    @Override
    public synchronized APIBaseResult setCrontab(APICrontab apiCrontab) {
        APIBaseResult mallBaseResult = new APIBaseResult();

        List<APICrontab> mallCrontabList = mallTaskImp.getCrontabListByTaskId(apiCrontab.getTaskId());

        boolean value = apiCrontab.getCrontab_time().equals("0");

        if (mallCrontabList != null && mallCrontabList.size() > 1){
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("设置定时任务失败，存在多条纪录，赶紧找保安哥哥看看数据");
            return mallBaseResult;
        }
        else if (mallCrontabList != null && mallCrontabList.size() ==1){
//            如果为true,❓说明getCrontab_time = 0，认为就是关闭定时任务
            if(value){
                if (mallTaskImp.deleteCrontab(apiCrontab.getTaskId()) == 1) {
                     mallBaseResult.setCode(0);
                     mallBaseResult.setMessage("定时任务设置成功，已经为你关闭定时任务！");
                     return mallBaseResult;
                 }else {
                     mallBaseResult.setCode(101);
                     mallBaseResult.setMessage("关闭定时任务失败！");
                     return mallBaseResult;
                 }
            }
//            否则就是更新它
            else {
                mallCrontabList.get(0).setCrontab_time(apiCrontab.getCrontab_time());
                mallCrontabList.get(0).setStatus(0);
                if (mallTaskImp.updateCrontab(mallCrontabList.get(0)) ==1){
                    mallBaseResult.setCode(0);
                    mallBaseResult.setMessage("设置更新定时任务成功！");
                    return mallBaseResult;
                }else {
                    mallBaseResult.setCode(101);
                    mallBaseResult.setMessage("设置定时任务失败，更新操作异常！");
                    return mallBaseResult;
                }
            }
        }else {
            if(value){
                mallBaseResult.setCode(0);
                mallBaseResult.setMessage("设置成功！与为你关闭定时任务");
                return mallBaseResult;
            }else {
                apiCrontab.setLast_run_time(new Date());
                apiCrontab.setCreateTime(new Date());
                apiCrontab.setModifyTime(new Date());
                apiCrontab.setStatus(0);
                if (mallTaskImp.setCrontab(apiCrontab) == 1) {
                    mallBaseResult.setCode(0);
                    mallBaseResult.setMessage("设置定时任务成功！");
                    return mallBaseResult;
                }else {
                    mallBaseResult.setCode(101);
                    mallBaseResult.setMessage("设置定时任务失败！插入异常!");
                    return mallBaseResult;
                }
            }
        }
    }



    @Override
    public synchronized APIBaseResult addSetHostByTask(APITaskHost apiTaskHost) {
        APIBaseResult mallBaseResult = new APIBaseResult();

        int total = mallTaskImp.getHostTotal(apiTaskHost);
        if ( total > 1){
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("数据异常，存在多条记录！");
            return mallBaseResult;
        }else if (total == 1){
            if (mallTaskImp.deleteSetHostByTask(apiTaskHost) == 0){
                mallBaseResult.setCode(102);
                mallBaseResult.setMessage("添加操作失败！");
                return mallBaseResult;
            }
            if (mallTaskImp.updateSetHostByTask(apiTaskHost) == 0){
                mallBaseResult.setCode(102);
                mallBaseResult.setMessage("添加失败！");
                return mallBaseResult;
            }
            mallBaseResult.setCode(0);
            mallBaseResult.setMessage("添加成功！");
            return mallBaseResult;
        }else {
            apiTaskHost.setStatus(0);
            apiTaskHost.setCreateTime(new Date());
            apiTaskHost.setModifyTime(new Date());
            int flag =  mallTaskImp.addSetHostByTask(apiTaskHost);

            if (flag ==1){
                mallBaseResult.setCode(0);
                mallBaseResult.setMessage("添加成功！");
            }else {
                mallBaseResult.setCode(101);
                mallBaseResult.setMessage("添加失败！");
            }
            return mallBaseResult;
        }

    }

    @Override
    public synchronized APIBaseResult deleteSetHostByTask(APITaskHost apiTaskHost) {
        APIBaseResult mallBaseResult = new APIBaseResult();
        int total = mallTaskImp.getHostTotal(apiTaskHost);
        if (total <= 0){
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("没有找到该记录，不能进行删除操作！");
            return mallBaseResult;
        }
        else if (total > 1){
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("数据异常，存在多条记录！");
            return mallBaseResult;
        }else {
            int flag = mallTaskImp.deleteSetHostByTask(apiTaskHost);
            if (flag ==1){
                mallBaseResult.setCode(0);
                mallBaseResult.setMessage("删除成功！");
            }else {
                mallBaseResult.setCode(101);
                mallBaseResult.setMessage("删除失败！");
            }
            return mallBaseResult;
        }
    }

    @Override
    public Object getExistingCrontab(Integer taskId) {
        HashMap<String,String> crontab = new HashMap<String,String>();
        crontab.put("taskId",taskId.toString());
        crontab.put("crontab",mallTaskImp.getExistingCrontab(taskId));
        return crontab;
    }


    @Override
    public BaseQueryResult<APIHosts> queryHostByTask(TaskQueryParam hostQueryParamByTask) {

        BaseQueryResult<APIHosts> hostsBaseQueryResult = new BaseQueryResult<APIHosts>();

        Integer total = mallTaskImp.getHostsTotalByTaskId(hostQueryParamByTask);

        List<APIHosts> hostsList = new ArrayList<APIHosts>();

        if (total > 0) {
            hostsList = mallTaskImp.queryHostsListFromTaskId(hostQueryParamByTask);
            hostsBaseQueryResult.setTotal(total);
            hostsBaseQueryResult.setRows(hostsList);

        } else {
            hostsBaseQueryResult.setTotal(0);
            hostsBaseQueryResult.setRows(hostsList);
        }
        return hostsBaseQueryResult;
    }


    /**
     * 通过taskID 去获取用户列表
     * @param userQueryParamByTask
     * @return
     */
    @Override
    public BaseQueryResult<APITestUser> queryTUserListByTask(TaskQueryParam userQueryParamByTask) {

        BaseQueryResult<APITestUser> testUserBaseQueryResult = new BaseQueryResult<APITestUser>();

        Integer total = mallTaskImp.getTUserListTotalByTaskId(userQueryParamByTask);

        List<APITestUser> userList = new ArrayList<APITestUser>();

        if (total > 0) {
            userList = mallTaskImp.queryTUserListFromTaskId(userQueryParamByTask);
            testUserBaseQueryResult.setTotal(total);
            testUserBaseQueryResult.setRows(userList);

        } else {
            testUserBaseQueryResult.setTotal(0);
            testUserBaseQueryResult.setRows(userList);
        }
        return testUserBaseQueryResult;
    }


    /**
     * 添加task_TUser关系表
     * @param apiTaskUser
     * @return
     */
    @Override
    public APIBaseResult addTaskTUser(APITaskUser apiTaskUser) {
        APIBaseResult mallBaseResult = new APIBaseResult();

        int total = mallTaskImp.getTUserTotalFromTask(apiTaskUser);
        if ( total > 1){
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("数据异常，存在多条记录！");
            return mallBaseResult;
        }else if (total == 1){
            if (mallTaskImp.deleteTaskTUser(apiTaskUser) == 0){
                mallBaseResult.setCode(102);
                mallBaseResult.setMessage("添加操作失败！");
                return mallBaseResult;
            }
            if (mallTaskImp.updateTaskTUser(apiTaskUser) == 0){
                mallBaseResult.setCode(102);
                mallBaseResult.setMessage("添加失败！");
                return mallBaseResult;
            }
            mallBaseResult.setCode(0);
            mallBaseResult.setMessage("添加成功！");
            return mallBaseResult;
        }else {
            apiTaskUser.setStatus(0);
            apiTaskUser.setCreateTime(new Date());
            apiTaskUser.setModifyTime(new Date());
            int flag =  mallTaskImp.addTaskTUser(apiTaskUser);

            if (flag ==1){
                mallBaseResult.setCode(0);
                mallBaseResult.setMessage("添加成功！");
            }else {
                mallBaseResult.setCode(101);
                mallBaseResult.setMessage("添加失败！");
            }
            return mallBaseResult;
        }
    }

    /**
     * 删除task_TUser关系表
     * @param apiTaskUser
     * @return
     */
    @Override
    public APIBaseResult deleteTaskTUser(APITaskUser apiTaskUser) {
        APIBaseResult mallBaseResult = new APIBaseResult();
        int total = mallTaskImp.getTUserTotalFromTask(apiTaskUser);
        if (total <= 0){
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("没有找到该记录，不能进行删除操作！");
            return mallBaseResult;
        }
        else if (total > 1){
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("数据异常，存在多条记录！");
            return mallBaseResult;
        }else {
            int flag = mallTaskImp.deleteTaskTUser(apiTaskUser);
            if (flag ==1){
                mallBaseResult.setCode(0);
                mallBaseResult.setMessage("删除成功！");
            }else {
                mallBaseResult.setCode(101);
                mallBaseResult.setMessage("删除失败！");
            }
            return mallBaseResult;
        }
    }
}
