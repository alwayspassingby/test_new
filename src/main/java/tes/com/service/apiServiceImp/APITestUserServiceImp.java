package tes.com.service.apiServiceImp;

import tes.com.common.cache.MallCache;
import tes.com.common.entity.query.TestUserQueryParam;
import tes.com.common.entity.result.APIBaseResult;
import tes.com.common.entity.result.APIObjectResult;
import tes.com.common.entity.result.BaseQueryResult;
import tes.com.common.entity.user.AutoHomeUser;
import tes.com.common.entity.user.APITestUser;
import tes.com.common.entity.user.YZJUserInfo;
import tes.com.mapperImp.APITestUserImp;
import tes.com.service.apiService.APITestUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by liuzhongdu on 2018/1/21.
 */
@Service("apiTestUserService")
public class APITestUserServiceImp implements APITestUserService {

    @Autowired
    private APITestUserImp apiTestUserImp;

    @Override
    public synchronized Object insertTestUser(YZJUserInfo yzjUserInfo, String validCode) {
        APIBaseResult mallBaseResult = new APIBaseResult();
        yzjUserInfo.setCreateTime(new Date());
        yzjUserInfo.setT_status(0);
        int code = apiTestUserImp.insertTestUser(yzjUserInfo);
        if (code == 1) {
            mallBaseResult.setCode(0);
            mallBaseResult.setMessage("添加用户成功！");
            return mallBaseResult;
        } else {
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("添加用户失败！");
            return mallBaseResult;
        }
    }

    /**
     * 更新用户
     *
     * @param yzjUserInfo
     * @return
     */
    @Override
    public synchronized Object updateTestUser(YZJUserInfo yzjUserInfo, String validCode) {
        APITestUser APITestUser = apiTestUserImp.getUserById(yzjUserInfo.getTuId());
        APIBaseResult mallBaseResult = new APIBaseResult();
        if (APITestUser == null) {
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("没有找到该用户！");
            return mallBaseResult;
        }
        int code = apiTestUserImp.updateTestUser(yzjUserInfo);
        if (code == 1) {
            mallBaseResult.setCode(0);
            mallBaseResult.setMessage("更新成功！");
            return mallBaseResult;
        } else {
            mallBaseResult.setCode(101);
            mallBaseResult.setMessage("更新失败！");
            return mallBaseResult;
        }

    }

    @Override
    public Object deleteUser(Integer id) {
        APIBaseResult returnResult = new APIBaseResult();
        int flag = apiTestUserImp.deleteUser(id);
        if (flag == 1) {
            returnResult.setCode(0);
            returnResult.setMessage("删除用户成功！！");
            return returnResult;
        } else {
            returnResult.setCode(101);
            returnResult.setMessage("删除用户失败！");
            return returnResult;
        }
    }

    @Override
    public Object getUserById(Integer id) {
        YZJUserInfo yzjUserInfo = apiTestUserImp.getUserById(id);
        APIObjectResult<YZJUserInfo> mallObjectResult = new APIObjectResult<YZJUserInfo>();
        if (yzjUserInfo == null) {
            mallObjectResult.setCode(101);
            mallObjectResult.setMessage("没有找到该用户！");
            return mallObjectResult;
        }
        mallObjectResult.setData(yzjUserInfo);
        mallObjectResult.setCode(0);
        mallObjectResult.setMessage("成功");
        return mallObjectResult;
    }


    public List<YZJUserInfo> getUserByName(String name) {
        return apiTestUserImp.getUserByName(name);
    }

    @Override
    public BaseQueryResult<YZJUserInfo> queryUserList(TestUserQueryParam mallTestUserQueryParam) {


        BaseQueryResult<YZJUserInfo> baseQueryResult = new BaseQueryResult<YZJUserInfo>();

        int total = apiTestUserImp.getTestUserTotal(mallTestUserQueryParam);
        baseQueryResult.setTotal(total);

        baseQueryResult.setRows(apiTestUserImp.queryUserList(mallTestUserQueryParam));

        return baseQueryResult;
    }

    @Override
    public APIObjectResult<List<YZJUserInfo>> getTUsers() {
        APIObjectResult<List<YZJUserInfo>> mallObjectResult = new APIObjectResult<List<YZJUserInfo>>();
        List<YZJUserInfo> yzjUserInfoList = apiTestUserImp.getTUserList();
        if (yzjUserInfoList == null || yzjUserInfoList.isEmpty()) {
            mallObjectResult.setCode(101);
            mallObjectResult.setMessage("数据为空！");
            return mallObjectResult;
        } else {
            mallObjectResult.setData(yzjUserInfoList);
            mallObjectResult.setCode(0);
            mallObjectResult.setMessage("成功");
            return mallObjectResult;
        }
    }

    @Override
    public APIObjectResult<String> getCookiesByUserId(YZJUserInfo yzjUserInfo, String validCode) {
        APIObjectResult<String> mallObjectResult = new APIObjectResult<String>();
        AutoHomeUser autoHomeUser = MallCache.autoHomeUserLogin.get(yzjUserInfo.getTname());
            mallObjectResult.setCode(0);
            mallObjectResult.setMessage("成功");
            String cookiesStr = getCookiesStr(autoHomeUser.getCookieMap());
            mallObjectResult.setData(cookiesStr);
            return mallObjectResult;
    }


    /**
     * @param cookieMap
     * @return
     */
    public String getCookiesStr(Map<String, String> cookieMap) {
        if (cookieMap.isEmpty()) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (String key : cookieMap.keySet()) {
            stringBuffer.append(key);
            stringBuffer.append("  ->  ");
            stringBuffer.append(cookieMap.get(key));
            stringBuffer.append(" & \n");
        }
        return stringBuffer.toString();
    }

}
