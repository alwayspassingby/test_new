package tes.com.service.apiServiceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tes.com.common.entity.MemberEventPointNotification;
import tes.com.common.entity.query.MemberEventNotificationQueryParam;
import tes.com.common.entity.result.BaseQueryResult;
import tes.com.mapperImp.MemberEventPointNotificationImp;
import tes.com.service.apiService.MemberEventPointNotificationService;

import java.util.ArrayList;
import java.util.List;

@Service("memberEventPointNotificationService")
public class MemberEventPointNotificationServiceImp implements MemberEventPointNotificationService {

    @Autowired
    private MemberEventPointNotificationImp memberEventPointNotificationImp;

    /**
     * 查询会员积分事件列表
     *
     * @param memberEventNotificationQueryParam
     * @return
     */
    @Override
    public BaseQueryResult<MemberEventPointNotification> queryMemberEventPointNotificationList(MemberEventNotificationQueryParam memberEventNotificationQueryParam) {

        BaseQueryResult<MemberEventPointNotification> baseQueryResult = new BaseQueryResult<MemberEventPointNotification>();

        List<MemberEventPointNotification> eventList = new ArrayList<MemberEventPointNotification>();

        int total = memberEventPointNotificationImp.queryMemberEventPointNotificationListCount(memberEventNotificationQueryParam);
        ;
        if (total > 0) {
            eventList = memberEventPointNotificationImp.queryMemberEventPointNotificationList(memberEventNotificationQueryParam);
            baseQueryResult.setTotal(total);
            baseQueryResult.setRows(eventList);
        } else {
            baseQueryResult.setTotal(0);
            baseQueryResult.setRows(eventList);
        }

        return baseQueryResult;
    }


    @Override
    public BaseQueryResult<MemberEventPointNotification> queryMemberEventPointNotificationListProduct(MemberEventNotificationQueryParam memberEventNotificationQueryParam) {

        BaseQueryResult<MemberEventPointNotification> baseQueryResult = new BaseQueryResult<MemberEventPointNotification>();

        List<MemberEventPointNotification> eventList = new ArrayList<MemberEventPointNotification>();

        int total = memberEventPointNotificationImp.queryMemberEventPointNotificationListCountProduct(memberEventNotificationQueryParam);
        ;
        if (total > 0) {
            eventList = memberEventPointNotificationImp.queryMemberEventPointNotificationListProduct(memberEventNotificationQueryParam);
            baseQueryResult.setTotal(total);
            baseQueryResult.setRows(eventList);
        } else {
            baseQueryResult.setTotal(0);
            baseQueryResult.setRows(eventList);
        }

        return baseQueryResult;
    }


}
