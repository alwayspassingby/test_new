package tes.com.service.apiServiceImp;

import tes.com.common.entity.query.ReportDetailParam;
import tes.com.common.entity.query.ReportQueryParam;
import tes.com.common.entity.report.ReportDetail;
import tes.com.common.entity.report.Reports;
import tes.com.common.entity.result.BaseQueryResult;
import tes.com.common.entity.task.Feedback;
import tes.com.common.entity.task.Summary;
import tes.com.mapperImp.ReportMapper;
import tes.com.service.apiService.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by baoan on 2018/3/25.
 */
@Service("reportService")
public class ReportServiceImp implements ReportService {

    @Autowired
    private ReportMapper reportMapper;

    @Override
    public BaseQueryResult<Reports> queryReportList(ReportQueryParam reportQueryParam) {
        BaseQueryResult<Reports> baseQueryResult = new BaseQueryResult<Reports>();
        List<Reports> result = reportMapper.queryReportList(reportQueryParam);

        int total = reportMapper.queryReportListCount(reportQueryParam);;
//        int total = result.size();
        baseQueryResult.setTotal(total);
        baseQueryResult.setRows(result);

        return baseQueryResult;
    }

    public BaseQueryResult<ReportDetail> queryReportDetail(ReportDetailParam reportDetailParam) {
        BaseQueryResult<ReportDetail> baseQueryResult = new BaseQueryResult<ReportDetail>();
        List<ReportDetail> result = reportMapper.queryReportDetail(reportDetailParam);
        int total = reportMapper.queryReportDetailCount(reportDetailParam);;
        baseQueryResult.setTotal(total);
        baseQueryResult.setRows(result);

        return baseQueryResult;
    }

    @Override
    public Object queryReportCount(ReportDetailParam reportDetailParam) {
        BaseQueryResult<ReportDetail> baseQueryResult = new BaseQueryResult<ReportDetail>();
        List<ReportDetail> result = reportMapper.queryReportCount(reportDetailParam);
        int total = result.size();
        baseQueryResult.setTotal(total);
        baseQueryResult.setRows(result);

        return baseQueryResult;
    }

    @Override
    public int addFeedback(Feedback addFeedback) {
        return reportMapper.addFeedback(addFeedback);
    }

    @Override
    public Object getChartData(ReportQueryParam reportQueryParam) {
        Reports data = (Reports) reportMapper.getChartData(reportQueryParam);
        Map resultData = new HashMap();
        resultData.put("successNum",data.getSuccessCase());
        resultData.put("failNum",data.getFailCase());
        return resultData;
    }

    @Override
    public Object summaryData() {
        List<Summary> summaryDataList = new ArrayList<Summary>();
        Summary summaryData = new Summary();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date today = calendar.getTime();
        summaryData = reportMapper.summaryData1(today);
        summaryData.setType(1);
        summaryData.setFailTask(reportMapper.summaryData4(today));
        summaryDataList.add(summaryData);

        summaryData = reportMapper.summaryData2(today);
        summaryData.setType(2);
        summaryData.setFailTask(reportMapper.summaryData5(today));
        summaryDataList.add(summaryData);

        summaryData = reportMapper.summaryData3(today);
        summaryData.setType(3);
        summaryData.setFailTask(reportMapper.summaryData6(today));
        summaryDataList.add(summaryData);

        return summaryDataList;
    }
}
