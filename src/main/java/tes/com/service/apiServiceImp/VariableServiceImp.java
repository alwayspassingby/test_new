package tes.com.service.apiServiceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tes.com.common.entity.apicase.GC_Variable;
import tes.com.common.entity.query.VariableQueryParam;
import tes.com.common.entity.result.APIBaseResult;
import tes.com.common.entity.result.APIObjectResult;
import tes.com.common.entity.result.BaseQueryResult;
import tes.com.common.entity.variable.CaseVariable;
import tes.com.common.entity.variable.GroupVariable;
import tes.com.common.entity.variable.TaskVariable;
import tes.com.common.entity.variable.Variable;
import tes.com.mapperImp.VariableMapperImp;
import tes.com.service.apiService.VariableService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("variableService")
public class VariableServiceImp implements VariableService {

    @Autowired
    private VariableMapperImp variableMapperImp;

    @Override
    public APIBaseResult addCaseVariable(CaseVariable caseVariable) {

        APIBaseResult baseResult = new APIBaseResult();
        int res = variableMapperImp.insertCaseVariable(caseVariable);
        if (res == 1) {
            baseResult.setCode(0);
            baseResult.setMessage("添加成功!");
        } else {
            baseResult.setCode(101);
            baseResult.setMessage("添加失败！");
        }
        return baseResult;
    }

    @Override
    public APIObjectResult<CaseVariable> getCaseVariableById(Integer vId) {

        APIObjectResult<CaseVariable> apiObjectResult = new APIObjectResult<CaseVariable>();
        CaseVariable caseVariable = variableMapperImp.getCaseVariableById(vId);
        if (caseVariable == null) {
            apiObjectResult.setCode(101);
            apiObjectResult.setMessage("没有找到相应记录！");
        } else {
            apiObjectResult.setData(caseVariable);
            apiObjectResult.setCode(0);
            apiObjectResult.setMessage("");
        }
        return apiObjectResult;
    }

    /**
     * 获取变量列表
     *
     * @param variableQueryParam
     * @return
     */
    @Override
    public BaseQueryResult<CaseVariable> queryCaseVariableList(VariableQueryParam variableQueryParam) {
        BaseQueryResult<CaseVariable> baseQueryResult = new BaseQueryResult<CaseVariable>();
        int total = getCaseVariableListTotal(variableQueryParam);
        if (total == 0) {
            baseQueryResult.setTotal(0);
            baseQueryResult.setRows(new ArrayList<CaseVariable>());
        } else {
            List<CaseVariable> caseVariableList = variableMapperImp.queryCaseVariableList(variableQueryParam);
            baseQueryResult.setTotal(total);
            baseQueryResult.setRows(caseVariableList);
        }
        return baseQueryResult;
    }


    @Override
    public APIBaseResult delGroupCaseVariableById(Integer gId, Integer vId) {
        APIBaseResult baseResult = new APIBaseResult();
        int res = variableMapperImp.deleteGroupCaseVariableById(gId, vId);
        if (res == 1) {
            baseResult.setCode(0);
            baseResult.setMessage("依次成功!");
            return baseResult;
        } else {
            baseResult.setCode(101);
            baseResult.setMessage("移除失败!");
            return baseResult;
        }
    }


    /**
     * 添加task变量到我已经添加的变量列表
     * 对应task_variable
     *
     * @param taskVariable
     * @return
     */
    @Override
    public APIBaseResult addToTaskVariableList(TaskVariable taskVariable) {
        APIBaseResult baseResult = new APIBaseResult();
        List<TaskVariable> enableTaskVariableList = variableMapperImp.getTaskVariableWithTaskIdAndVId(taskVariable.getTaskId(), taskVariable.getvId(), 0);
        if (enableTaskVariableList != null && enableTaskVariableList.size() > 0) {
            baseResult.setCode(101);
            baseResult.setMessage("已经添加过了，请忽重复添加！！");
            return baseResult;
        } else {
            List<TaskVariable> disableTaskVariableList = variableMapperImp.getTaskVariableWithTaskIdAndVId(taskVariable.getTaskId(), taskVariable.getvId(), 1);
            if (disableTaskVariableList == null || disableTaskVariableList.size() == 0) {
                taskVariable.setStatus(0);
                taskVariable.setCreateTime(new Date());
                int res = variableMapperImp.addVariableToTaskVariableList(taskVariable);
                if (res == 1) {
                    baseResult.setCode(0);
                    baseResult.setMessage("添加成功!");
                    return baseResult;
                } else {
                    baseResult.setCode(101);
                    baseResult.setMessage("添加失败！");
                    return baseResult;
                }
            } else {
                TaskVariable tv = disableTaskVariableList.get(disableTaskVariableList.size() - 1);
                tv.setStatus(0);
                int res = variableMapperImp.updateTaskVariable(tv);
                if (res == 1) {
                    baseResult.setCode(0);
                    baseResult.setMessage("添加成功!");
                    return baseResult;
                } else {
                    baseResult.setCode(101);
                    baseResult.setMessage("添加失败！");
                    return baseResult;
                }
            }
        }
    }

    /**
     * 获取 变量列表的总数
     *
     * @param variableQueryParam
     * @return
     */
    public Integer getCaseVariableListTotal(VariableQueryParam variableQueryParam) {
        return variableMapperImp.getCaseVariableListTotal(variableQueryParam);
    }


    /**
     * @param vId
     * @return
     */
    @Override
    public APIBaseResult delCaseVariableById(Integer vId) {

        APIBaseResult baseResult = new APIBaseResult();
//        先看看gc_variable关联表中是否存在该变量数据
        int count = variableMapperImp.get_gc_VariableTotalByVId(vId);
        if (count > 0) {
            baseResult.setCode(201);
            baseResult.setMessage("该变量已经关联到其他群组，请删除关联的群组在删除操作！");
            return baseResult;
        }
        int res = variableMapperImp.deleteCaseVariableById(vId);

        if (res == 1) {
            baseResult.setCode(0);
            baseResult.setMessage("删除成功!");
            return baseResult;
        } else {
            baseResult.setCode(101);
            baseResult.setMessage("删除失败!");
            return baseResult;
        }
    }

    /**
     * 更新变量消息
     *
     * @param caseVariable
     * @return
     */
    @Override
    public APIBaseResult updateCaseVariable(CaseVariable caseVariable) {
        APIBaseResult baseResult = new APIBaseResult();
        int res = variableMapperImp.updateCaseVariable(caseVariable);
        if (res == 1) {
            baseResult.setCode(0);
            baseResult.setMessage("更新成功!");
            return baseResult;
        } else {
            baseResult.setCode(101);
            baseResult.setMessage("更新失败!");
            return baseResult;
        }
    }


    /**
     * 根据指定的groupID和caseID获取变量列表
     *
     * @param variableQueryParam
     * @return
     */
    @Override
    public BaseQueryResult<CaseVariable> getCaseVariableListFromGroupId(VariableQueryParam variableQueryParam) {
        BaseQueryResult<CaseVariable> baseQueryResult = new BaseQueryResult<CaseVariable>();
        int total = variableMapperImp.getCaseVariableTotalFromGroupId(variableQueryParam);
        if (total == 0) {
            baseQueryResult.setTotal(0);
            baseQueryResult.setRows(new ArrayList<CaseVariable>());
        } else {
            List<CaseVariable> caseVariableList = variableMapperImp.getCaseVariableListFromGroupId(variableQueryParam);
            baseQueryResult.setTotal(total);
            baseQueryResult.setRows(caseVariableList);
        }
        return baseQueryResult;
    }


    /**
     * 获取任务变量列表
     *
     * @param variableQueryParam
     * @return
     */
    @Override
    public BaseQueryResult<TaskVariable> getTaskVariableList(VariableQueryParam variableQueryParam) {
        BaseQueryResult<TaskVariable> baseQueryResult = new BaseQueryResult<TaskVariable>();
        int total = variableMapperImp.getTaskVariableListTotal(variableQueryParam);
        if (total == 0) {
            baseQueryResult.setTotal(0);
            baseQueryResult.setRows(new ArrayList<TaskVariable>());
        } else {
            List<TaskVariable> taskVariableList = variableMapperImp.getTaskVariableList(variableQueryParam);
            baseQueryResult.setTotal(total);
            baseQueryResult.setRows(taskVariableList);
        }
        return baseQueryResult;
    }

    @Override
    public APIBaseResult addGroupCaseVariable(GC_Variable groupVariable) {

        APIBaseResult baseResult = new APIBaseResult();
        int res = variableMapperImp.insertGroupCaseVariable(groupVariable);
        if (res == 1) {
            baseResult.setCode(0);
            baseResult.setMessage("添加成功!");
        } else {
            baseResult.setCode(101);
            baseResult.setMessage("添加失败！");
        }
        return baseResult;

    }


    @Override
    public APIBaseResult moveTaskVariable(Integer taskId, Integer vId) {
        APIBaseResult baseResult = new APIBaseResult();

        int res = variableMapperImp.updateTaskVariableStatus(taskId, vId, 1);
        if (res >= 1) {
            baseResult.setCode(0);
            baseResult.setMessage("删除成功!");
            return baseResult;
        } else {
            baseResult.setCode(101);
            baseResult.setMessage("删除失败!");
            return baseResult;
        }
    }

    /**
     * 添加全局变量
     * @param globalVariable
     * @return
     */
    @Override
    public APIBaseResult addGlobalVariable(Variable globalVariable) {

        APIBaseResult baseResult = new APIBaseResult();
        globalVariable.setCreateTime(new Date());
        globalVariable.setStatus(0);

        int res = variableMapperImp.insertGlobalVariable(globalVariable);
        if (res == 1) {
            baseResult.setCode(0);
            baseResult.setMessage("添加成功!");
        } else {
            baseResult.setCode(101);
            baseResult.setMessage("添加失败！");
        }
        return baseResult;
    }


    /**
     * 根据条件参数去查询变量列表
     *
     * @param variableQueryParam
     * @return
     */
    @Override
    public BaseQueryResult<Variable> getGlobalVariableList(VariableQueryParam variableQueryParam) {

        BaseQueryResult<Variable> baseQueryResult = new BaseQueryResult<Variable>();
        int total = variableMapperImp.getGlobalVariableListTotal(variableQueryParam);
        if (total == 0) {
            baseQueryResult.setTotal(0);
            baseQueryResult.setRows(new ArrayList<Variable>());
        } else {
            List<Variable> globalVariables = variableMapperImp.queryGlobalVariableList(variableQueryParam);
            baseQueryResult.setTotal(total);
            baseQueryResult.setRows(globalVariables);
        }
        return baseQueryResult;
    }

    /**
     * 根据vId 获取全局变量
     *
     * @param vId
     * @return
     */
    @Override
    public APIObjectResult<Variable> getGlobalVariableById(Integer vId) {
        APIObjectResult<Variable> apiObjectResult = new APIObjectResult<Variable>();

        Variable globalVariable = variableMapperImp.getGlobalVariableById(vId);

        if (globalVariable == null) {
            apiObjectResult.setCode(101);
            apiObjectResult.setMessage("没有找到相应记录！");
        } else {
            apiObjectResult.setData(globalVariable);
            apiObjectResult.setCode(0);
            apiObjectResult.setMessage("");
        }

        return apiObjectResult;
    }

    @Override
    public APIBaseResult updateGlobalVariable(Variable globalVariable) {
        APIBaseResult baseResult = new APIBaseResult();
        int res = variableMapperImp.updateGlobalVariable(globalVariable);
        if (res == 1) {
            baseResult.setCode(0);
            baseResult.setMessage("更新成功!");
            return baseResult;
        } else {
            baseResult.setCode(101);
            baseResult.setMessage("更新失败!");
            return baseResult;
        }
    }


    @Override
    public APIBaseResult delGlobalVariableById(Integer vId) {
        APIBaseResult baseResult = new APIBaseResult();
        int res = variableMapperImp.deleteGlobalVariableById(vId);
        if (res == 1) {
            baseResult.setCode(0);
            baseResult.setMessage("删除成功!");
            return baseResult;
        } else {
            baseResult.setCode(101);
            baseResult.setMessage("删除失败!");
            return baseResult;
        }
    }


    /**
     * 添加 task variable
     * @param taskVariable
     * @return
     */
    @Override
    public APIBaseResult addTaskVariable(TaskVariable taskVariable) {
        APIBaseResult baseResult = new APIBaseResult();
        taskVariable.setCreateTime(new Date());
        taskVariable.setStatus(0);

        int res = variableMapperImp.insertTaskTypeVariable(taskVariable);
        if (res == 1) {
            baseResult.setCode(0);
            baseResult.setMessage("添加成功!");
        } else {
            baseResult.setCode(101);
            baseResult.setMessage("添加失败！");
        }
        return baseResult;
    }


    /**
     * 获取群组列表
     *
     * @param variableQueryParam
     * @return
     */
    @Override
    public BaseQueryResult<GroupVariable> getGroupVariableList(VariableQueryParam variableQueryParam) {
        BaseQueryResult<GroupVariable> baseQueryResult = new BaseQueryResult<GroupVariable>();
        int total = variableMapperImp.getGroupVariableListTotal(variableQueryParam);
        if (total == 0) {
            baseQueryResult.setTotal(0);
            baseQueryResult.setRows(new ArrayList<GroupVariable>());
        } else {
            List<GroupVariable> groupVariableList = variableMapperImp.getGroupVariableList(variableQueryParam);
            baseQueryResult.setTotal(total);
            baseQueryResult.setRows(groupVariableList);
        }
        return baseQueryResult;
    }

    /**
     * 添加群组变量列表，把variable表中的变量，添加关联到group_variable表中
     *
     * @param groupVariable
     * @return
     */
    @Override
    public APIBaseResult addGroupTypeVariable(GroupVariable groupVariable) {
        APIBaseResult baseResult = new APIBaseResult();
        groupVariable.setCreateTime(new Date());
        groupVariable.setStatus(0);

        int res = variableMapperImp.insertGroupTypeVariable(groupVariable);
        if (res == 1) {
            baseResult.setCode(0);
            baseResult.setMessage("添加成功!");
        } else {
            baseResult.setCode(101);
            baseResult.setMessage("添加失败！");
        }
        return baseResult;
    }


    /**
     * 添加变量到我的已经添加的变量列表
     *
     * @param groupVariable
     * @return
     */
    @Override
    public APIBaseResult addGroupVariableToVariableList(GroupVariable groupVariable) {
        APIBaseResult baseResult = new APIBaseResult();
        List<GroupVariable> enableTaskVariableList = variableMapperImp.getGroupVariableWithTaskIdAndVId(groupVariable.getGroupId(), groupVariable.getvId(), 0);
        if (enableTaskVariableList != null && enableTaskVariableList.size() > 0) {
            baseResult.setCode(101);
            baseResult.setMessage("已经添加过了，请忽重复添加！！");
            return baseResult;
        } else {
            List<GroupVariable> disableTaskVariableList = variableMapperImp.getGroupVariableWithTaskIdAndVId(groupVariable.getGroupId(), groupVariable.getvId(), 1);
            if (disableTaskVariableList == null || disableTaskVariableList.size() == 0) {
                groupVariable.setStatus(0);
                groupVariable.setCreateTime(new Date());
                int res = variableMapperImp.addVariableToGroupVariableList(groupVariable);
                if (res == 1) {
                    baseResult.setCode(0);
                    baseResult.setMessage("添加成功!");
                    return baseResult;
                } else {
                    baseResult.setCode(101);
                    baseResult.setMessage("添加失败！");
                    return baseResult;
                }
            } else {
                GroupVariable gv = disableTaskVariableList.get(disableTaskVariableList.size() - 1);
                gv.setStatus(0);
                int res = variableMapperImp.updateGroupVariableStatusById(gv);
                if (res == 1) {
                    baseResult.setCode(0);
                    baseResult.setMessage("添加成功!");
                    return baseResult;
                } else {
                    baseResult.setCode(101);
                    baseResult.setMessage("添加失败！");
                    return baseResult;
                }
            }
        }
    }


    /**
     * 从我的已添加变量列表中，移除变量
     *
     * @param groupId
     * @param vId
     * @return
     */
    @Override
    public APIBaseResult moveGroupVariable(Integer groupId, Integer vId) {
        APIBaseResult baseResult = new APIBaseResult();

        int res = variableMapperImp.updateGroupVariableStatus(groupId, vId, 1);
        if (res >= 1) {
            baseResult.setCode(0);
            baseResult.setMessage("删除成功!");
            return baseResult;
        } else {
            baseResult.setCode(101);
            baseResult.setMessage("删除失败!");
            return baseResult;
        }
    }
}
