package tes.com.util;

import tes.com.exception.APIException;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by liuzhongdu on 2017/1/19.
 * 反射机制工具类
 */
public class ClassRefService {

    /**
     * 通过全类名去创建类的实例
     *
     * @param objectName 类的全名称，必须是要名称,例：tes.com.util.dingding.MessageInfo
     * @return
     */
    public Object newObjectForObjectName(String objectName) {
        try {
            Class<?> clazz = Class.forName(objectName);
            Constructor<?> constructor = clazz.getDeclaredConstructor();
            constructor.setAccessible(true);
            Object object = constructor.newInstance();
            if (object != null) {
                return object;
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 从beanObject 查找 methodStr 方法是否存在
     *
     * @param objectBean
     * @param methodStr
     * @return
     */
    public Boolean isExistMethodForBean(Object objectBean, String methodStr) {
        //获取方法列表
        for (Class<?> clazz = objectBean.getClass(); clazz != Object.class; clazz = clazz.getSuperclass()) {
            Method[] methods = clazz.getDeclaredMethods();
            if (methods.length != 0) {
                for (Method m : methods) {
                    if (m.getName().equals(methodStr)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    public Boolean isExistMethodForClass(Class cl, String methodStr) {
        //获取方法列表
        for (Class<?> clazz = cl; clazz != Object.class; clazz = clazz.getSuperclass()) {
            Method[] methods = clazz.getDeclaredMethods();
            if (methods.length != 0) {
                for (Method m : methods) {
                    if (m.getName().equals(methodStr)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    /**
     * 从Bean对象中，查看指定的属性是否存在
     *
     * @return
     */
    public Boolean isExistFieldForBean(Object object, String fieldName) {
        for (Class<?> clazz = object.getClass(); clazz != Object.class; clazz = clazz.getSuperclass()) {
            Field[] fields = clazz.getDeclaredFields();
            if (fields.length != 0) {
                for (Field field : fields) {
                    if (field.getName().equals(fieldName)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * 获取字段
     *
     * @param object
     * @param fieldName
     * @return
     */
    public Field getFieldForBean(Object object, String fieldName) {
        Field field = null;
        try {
            for (Class<?> clazz = object.getClass(); clazz != Object.class; clazz = clazz.getSuperclass()) {
                Field[] fields = clazz.getDeclaredFields();
                if (fields.length != 0) {
                    for (Field f : fields) {
                        if (f.getName().equals(fieldName)) {
                            field = f;
                            break;
                        }
                    }
                }
            }

            if (field == null) {
                return null;
            }
            return field;
        } catch (Exception e) {
            return null;
        }
    }


    /**
     * 指定方法名，在给定的Bean对象获取改方法列表
     *
     * @param objectBean
     * @param methodStr
     * @return
     * @throws Exception
     */
    public List<Method> getMethodListWithMethodNameFromBean(Object objectBean, String methodStr) throws APIException {
        List<Method> beanMethodList = new ArrayList<Method>();
        for (Class<?> clazz = objectBean.getClass(); clazz != Object.class; clazz = clazz.getSuperclass()) {
            //获取该 bean中的方法列表
            Method[] methods = clazz.getDeclaredMethods();
            if (methods.length != 0) {
                for (Method m : methods) {
                    if (m.getName().equals(methodStr)) {
                        //如果方法列表中有指定的方法，添加到列表中
                        beanMethodList.add(m);
                    }
                }
            }
        }
        return beanMethodList;
    }

    public List<Method> getMethodListWithMethodNameFromClass(Class<?> cl, String methodStr) throws APIException {
        List<Method> beanMethodList = new ArrayList<Method>();
        for (Class<?> clazz = cl; clazz != Object.class; clazz = clazz.getSuperclass()) {
            //获取该 bean中的方法列表
            Method[] methods = clazz.getDeclaredMethods();
            if (methods.length != 0) {
                for (Method m : methods) {
                    if (m.getName().equals(methodStr)) {
                        //如果方法列表中有指定的方法，添加到列表中
                        beanMethodList.add(m);
                    }
                }
            }
        }
        return beanMethodList;
    }


    /**
     * 获取字段的get或set方法
     *
     * @param object      类对象
     * @param fieldName   字段属性名称
     * @param isGetMethod ture 为获取get方法，false 为获取set方法
     * @return
     * @throws Exception
     */
    public String getFieldGetSetMethod(Object object, String fieldName, Boolean isGetMethod) throws APIException {
        if (!isExistFieldForBean(object, fieldName)) {
            throw new APIException("【" + fieldName + "】没有这个字段属性，请检查！");
        }
        StringBuffer methodName = new StringBuffer();
        if (isGetMethod) {
            methodName.append("get");
        } else {
            methodName.append("set");
        }
        //首字母变大小
        String toUpMethod = fieldName.replaceAll("^\\w", String.valueOf(fieldName.charAt(0)).toUpperCase());
        methodName.append(toUpMethod);

        if (!isExistMethodForBean(object, methodName.toString())) {
            throw new APIException("【" + methodName.toString() + "】没有找到这个方法！");
        }

        return methodName.toString();

    }

    /**
     * 设置字段属性
     *
     * @param object
     * @param method
     * @param fieldValues
     * @throws Exception
     */

    public void setAttrributeValue(Object object, Method method, List<Object> fieldValues) throws APIException {
        Object value = null;
        try {
            if (method != null) {
                value = method.invoke(object, fieldValues.toArray());
            }
            if (value != null) {
                throw new Exception("掉用方法异常！");
            }
        } catch (Exception e) {
            throw new APIException(e.getMessage());
        }
    }





}
