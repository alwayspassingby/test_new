package tes.com.util;


import tes.com.exception.APIException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by liuzhongdu on 16/6/21.
 */
public class JSONUtil {
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private static ObjectMapper mapper = new ObjectMapper();
    private static JsonParser jsonParser = new JsonParser();

    public JSONUtil() {
    }



    /**
     * JsonObject 解析
     * @param json
     * @return
     */
    public static JsonObject parseJSONObject(String json){
        if (isBlank(json)){
            return null;
        }else {
            try{
                return (JsonObject) jsonParser.parse(json);
            }catch (Exception e){
                throw new JsonSyntaxException("");
            }
        }
    }

    /**
     * jackson 解析
     * @param json
     * @return
     */
    public static JsonNode parseJsonNode(String json) throws APIException {
        if (isBlank(json)){
            return null;
        }else {
            try{
                return mapper.readTree(json);
            }catch (Exception e){
                throw new APIException("json数据格式错误！\n" + json);
            }
        }
    }

    /**
     * jackson 解析 ，javaBean转Json字符串
     *
     * @param object
     * @return
     */
    public static String beanToString(Object object) {
        if (mapper == null) {
            mapper = new ObjectMapper();
        }
        try {
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);

            return mapper.writeValueAsString(object);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }



    public static <T> T parseJSON(String json, Class<T> cls) throws JsonSyntaxException {
        if(isBlank(json)) {
            return null;
        } else {
            try {
                return gson.fromJson(json, cls);
            } catch (Exception var3) {
//                var3.printStackTrace();
                throw new JsonSyntaxException(var3);
            }
        }
    }

    public static <T> T parseJSON(String json, Type type) throws JsonSyntaxException {
        if(isBlank(json)) {
            return null;
        } else {
            try {
                return gson.fromJson(json, type);
            } catch (Exception var3) {
                var3.printStackTrace();
                throw new JsonSyntaxException("解析JSON异常，JSon格式不正确");
            }
        }
    }

    public static String toJSON(Object entity) throws JsonSyntaxException {
        if(entity == null) {
            return null;
        } else {
            try {
                return gson.toJson(entity);
            } catch (Exception var2) {
                var2.printStackTrace();
                throw new JsonSyntaxException("转换异常！");
            }
        }
    }

    public static <T> T mapToBean(Map<String, Object> param, Class<T> cls) throws JsonSyntaxException {
        if(param == null) {
            return null;
        } else {
            try {
                return parseJSON(toJSON(param), cls);
            } catch (Exception var3) {
                throw new JsonSyntaxException("");
            }
        }
    }

    public static Map<String, String> BeanToMap(Object entity) throws JsonSyntaxException {
        if(entity == null) {
            return null;
        } else {
            try {
                Type e = (new TypeToken() {
                }).getType();
                return (Map)gson.fromJson(toJSON(entity), e);
            } catch (Exception var2) {
                throw new JsonSyntaxException("");
            }
        }
    }

    public static List<Map<String, String>> BeanToList(Object entity) throws JsonSyntaxException {
        if(entity == null) {
            return null;
        } else {
            try {
                Type e = (new TypeToken() {
                }).getType();
                return (List)gson.fromJson(toJSON(entity), e);
            } catch (Exception var2) {
                throw new JsonSyntaxException("");
            }
        }
    }

    public static <T> List<T> ListToBean(List<Map<String, Object>> obj, Class<T> cls) throws JsonSyntaxException {
        if(obj == null) {
            return null;
        } else {
            try {
                ArrayList e = new ArrayList();
                Iterator var4 = obj.iterator();

                while(var4.hasNext()) {
                    Map p = (Map)var4.next();
                    e.add(mapToBean(p, cls));
                }

                return e;
            } catch (Exception var5) {
                throw new JsonSyntaxException("");
            }
        }
    }

    public static boolean isBlank(String str) {
        if(str == null || str.length() == 0 || "null".equalsIgnoreCase(str)) {
            return true;
        } else {
            for(int i = 0; i < str.length(); ++i) {
                if(!Character.isWhitespace(str.charAt(i))) {
                    return false;
                }
            }

            return true;
        }
    }

}
