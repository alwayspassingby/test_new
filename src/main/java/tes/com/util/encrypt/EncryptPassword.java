package tes.com.util.encrypt;

import org.apache.commons.codec.binary.Base64;

/**
 * Created by liuzhongdu on 2018/7/18.
 */
public class EncryptPassword {

    /**
     * 用户账号密码加密
     * @param username
     * @param password
     * @return
     */
    public static String encryptPassword(String username, String password) {
        try {
            if (password == null) {
                password = "";
            }
            byte[] bytes = password.getBytes("UTF-8");
            byte[] decrypt = DESUtils.encrypt(bytes, username);
            byte[] encodeBase64 = Base64.encodeBase64(decrypt);
            String authPassword = new String(encodeBase64, "UTF-8");
            return authPassword;
        } catch (Exception ex) {
            return null;
        }
    }
}
