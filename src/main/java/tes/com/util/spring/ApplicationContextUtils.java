package tes.com.util.spring;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Created by liuzhongdu on 2017/1/18.
 */
public class ApplicationContextUtils implements ApplicationContextAware {

    private static ApplicationContext applicationContext;


    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        applicationContext = context;

    }

//    public static ApplicationContext getApplicationContext() throws BeansException {
//        return applicationContext;
//
//    }

    /**
     * 从当前Spring加载中获取bean
     * @param beanId
     * @return
     */
    public static Object getBeanForBeanId(String beanId) {
        Object object = null;
        object = applicationContext.getBean(beanId);
        return object;
    }


    /**
     * 从当前的Spring加载中，确定BeanID对于的对象是否存在并加载
     * @param beanId
     * @return
     */
    public static Boolean isExistBeanForBeanId(String beanId){
        String [] beanNames = applicationContext.getBeanDefinitionNames();
        if (beanNames.length != 0){
            for (int i = 0 ; i< beanNames.length; i++){
                if (beanNames[i].equals(beanId)){
                    return true;
                }
            }
        }
        return false;
    }





}
