/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50716
 Source Host           : localhost:3306
 Source Schema         : yzj_api

 Target Server Type    : MySQL
 Target Server Version : 50716
 File Encoding         : 65001

 Date: 28/08/2018 16:54:14
*/
drop database if Exists tes_api;
create Database If Not Exists `tes_api` Character Set utf8mb4 COLLATE utf8mb4_unicode_ci;

use `tes_api`;

-- ----------------------------
-- Table structure for apicase
-- ----------------------------
DROP TABLE IF EXISTS `apicase`;
CREATE TABLE `apicase` (
  `aId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'api ID',
  `httpURL` varchar(255) DEFAULT NULL COMMENT 'http 接口的url',
  `type` int(11) NOT NULL COMMENT 'api 类型 0:为http接口 1:为RPC接口',
  `rpcService` varchar(255) DEFAULT NULL COMMENT 'RPC接口的服务',
  `rpcMethod` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT 'RPC接口的调用方法',
  `rpcGroup` varchar(255) DEFAULT NULL COMMENT 'RPC分组',
  `rpcVersion` varchar(255) DEFAULT NULL COMMENT 'RPC接口版本号',
  `httpRequestType` int(11) DEFAULT NULL COMMENT '请求的方法类型，目前支持 0：get和 1：post 2:为trubo',
  `httpHeaders` varchar(1000) DEFAULT NULL COMMENT 'http请求的头信息',
  `httpContentType` varchar(255) DEFAULT NULL COMMENT 'Http连接类型,如果是turbo接口不需要',
  `httpCookies` text COMMENT 'cookies设置, 如果是RPC接口不需要',
  `requestParameter` text COMMENT '请求参数',
  `parameterType` int(3) NOT NULL COMMENT '1：form类型 用于http表单请求和 RPC 参数 2：raw-json类型，内容为JSON字符串 3：raw-text类型，内容为文本',
  `description` varchar(255) DEFAULT NULL COMMENT '请求说明',
  `expectedType` int(11) NOT NULL COMMENT '断言类型 http:0是需要code，1是json  turbo：只有1',
  `expression` int(11) DEFAULT NULL COMMENT '断言表达式类型，0:isNotNull, 1:isNull, 2:isEqualTo, 3:isNotEqualTo, 4:contains, 5:doesNotContain',
  `expectedKey` varchar(255) DEFAULT NULL COMMENT '断言的字段（key）',
  `expectedValue` varchar(255) DEFAULT NULL COMMENT '断言的值',
  `staticName` varchar(255) DEFAULT NULL COMMENT '全局变量的名字',
  `staticKey` varchar(2000) DEFAULT NULL COMMENT '全局变量的key',
  `status` int(3) NOT NULL COMMENT '用例状态 0：有效用例 1 删除用例',
  `createTime` datetime NOT NULL COMMENT '用例创建时间',
  `modifyTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '用例修改实际',
  PRIMARY KEY (`aId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- ----------------------------
-- Table structure for gc_variable
-- ----------------------------
DROP TABLE IF EXISTS `gc_variable`;
CREATE TABLE `gc_variable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL COMMENT '关联的群组ID',
  `vId` int(11) NOT NULL COMMENT '变量ID',
  `status` int(3) NOT NULL COMMENT '状态 0：有效   ，1 ：无效',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `modifyTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- ----------------------------
-- Table structure for group
-- ----------------------------
DROP TABLE IF EXISTS `group`;
CREATE TABLE `group` (
  `groupId` int(11) NOT NULL AUTO_INCREMENT COMMENT '组ID',
  `groupName` varchar(255) NOT NULL COMMENT '组名',
  `hosts` varchar(255) DEFAULT NULL COMMENT '指定host',
  `description` varchar(255) DEFAULT NULL COMMENT '组描述',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- ----------------------------
-- Table structure for group_case
-- ----------------------------
DROP TABLE IF EXISTS `group_case`;
CREATE TABLE `group_case` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL COMMENT '组ID',
  `caseId` int(11) NOT NULL COMMENT '用例ID',
  `priority` int(11) NOT NULL COMMENT '优先级：3，2，1，0依次优先级',
  `isRun` int(11) NOT NULL COMMENT '是否允许执行',
  `status` int(11) NOT NULL COMMENT '状态：0：有效。1：删除',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- ----------------------------
-- Table structure for hosts
-- ----------------------------
DROP TABLE IF EXISTS `hosts`;
CREATE TABLE `hosts` (
  `hId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'host ID 自增',
  `ip` varchar(255) NOT NULL COMMENT 'host IP ',
  `host` varchar(255) NOT NULL COMMENT 'host name',
  `description` varchar(255) DEFAULT NULL COMMENT 'host 描述信息',
  `status` varchar(255) DEFAULT NULL COMMENT 'host状态，0有效，1无效',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `modifyTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改实际',
  PRIMARY KEY (`hId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for hosts_group
-- ----------------------------
DROP TABLE IF EXISTS `hosts_group`;
CREATE TABLE `hosts_group` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '关联表id，自增',
  `hostId` int(11) NOT NULL COMMENT 'host id',
  `groupId` varchar(255) NOT NULL COMMENT 'group id',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for task
-- ----------------------------
DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
  `taskId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'task id,自增',
  `taskName` varchar(255) NOT NULL COMMENT 'task任务名字',
  `description` varchar(255) DEFAULT NULL COMMENT '描述信息',
  `status` int(3) DEFAULT NULL COMMENT '用例状态 0：有效用例 1 删除用例',
  `reporter` varchar(255) DEFAULT NULL,
  `sendType` int(3) DEFAULT '0' COMMENT '发送报告类型，1：只失败发钉钉  2：只成功发钉钉  3:成功失败都发  其他：不发',
  `taskPerson` varchar(255) DEFAULT NULL,
  `modifyTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '用例修改实际',
  `createTime` timestamp NULL DEFAULT NULL COMMENT '用例创建时间',
  PRIMARY KEY (`taskId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



-- ----------------------------
-- Table structure for task_crontab
-- ----------------------------
DROP TABLE IF EXISTS `task_crontab`;
CREATE TABLE `task_crontab` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID 自增',
  `taskId` int(11) DEFAULT NULL COMMENT 'task ID',
  `crontab_time` varchar(255) DEFAULT NULL COMMENT '定时时间表达式',
  `last_run_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最后运行时间',
  `status` int(11) NOT NULL COMMENT '0：有效  1：删除',
  `createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifyTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改实际',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



-- ----------------------------
-- Table structure for task_execute_result
-- ----------------------------
DROP TABLE IF EXISTS `task_execute_result`;
CREATE TABLE `task_execute_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskId` int(11) DEFAULT NULL COMMENT '任务ID',
  `groupId` int(11) DEFAULT NULL COMMENT '群组ID',
  `caseId` int(11) DEFAULT NULL COMMENT 'case ID',
  `version` int(11) DEFAULT NULL COMMENT '任务版本号',
  `caseInfo` text COMMENT 'case的信息，包含：接口类型、接口参数……',
  `isSuccess` int(3) DEFAULT NULL COMMENT '是否执行成功 0:成功，1:失败',
  `caseResponse` mediumtext COMMENT '返回信息',
  `failInfo` mediumtext COMMENT '断言信息',
  `isFeedback` int(4) DEFAULT NULL COMMENT '是否反馈',
  `failCause` varchar(255) DEFAULT NULL COMMENT '反馈信息',
  `executeStartTime` datetime DEFAULT NULL COMMENT '接口调用开始时间',
  `executeEndTime` datetime DEFAULT NULL COMMENT '接口调用结束时间',
  `durationTime` int(11) DEFAULT NULL COMMENT '接口调用耗时，单位：毫秒',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- ----------------------------
-- Table structure for task_group
-- ----------------------------
DROP TABLE IF EXISTS `task_group`;
CREATE TABLE `task_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskId` int(11) NOT NULL COMMENT '任务ID',
  `groupId` int(11) NOT NULL COMMENT '组ID',
  `status` int(11) NOT NULL COMMENT '任务和组关系的状态：0：有效。1：删除',
  `createTime` datetime NOT NULL,
  `modifyTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



-- ----------------------------
-- Table structure for task_hosts
-- ----------------------------
DROP TABLE IF EXISTS `task_hosts`;
CREATE TABLE `task_hosts` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '关联表id，自增',
  `host_id` int(11) NOT NULL COMMENT 'host id',
  `task_id` int(11) NOT NULL COMMENT 'task id',
  `status` int(11) NOT NULL COMMENT '0：有效  1：删除',
  `createTime` datetime NOT NULL COMMENT '创建的时间',
  `modifyTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改的实际时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- ----------------------------
-- Table structure for task_job
-- ----------------------------
DROP TABLE IF EXISTS `task_job`;
CREATE TABLE `task_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID 自增',
  `taskId` int(11) NOT NULL COMMENT 'task ID',
  `version` int(11) NOT NULL COMMENT 'task运行的版本号',
  `status` int(11) NOT NULL COMMENT '0：未执行，1：执行中，2：执行完成',
  `totalCase` int(11) DEFAULT NULL,
  `successCase` int(11) DEFAULT NULL,
  `failCase` int(11) DEFAULT NULL,
  `taskPerson` varchar(255) DEFAULT NULL,
  `excuteTime` datetime DEFAULT NULL,
  `modifyTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改实际',
  `createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- ----------------------------
-- Table structure for task_user
-- ----------------------------
DROP TABLE IF EXISTS `task_user`;
CREATE TABLE `task_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '关联表id，自增',
  `tuId` int(11) NOT NULL COMMENT 'host id',
  `taskId` int(11) NOT NULL COMMENT 'task id',
  `status` int(3) NOT NULL COMMENT '0：有效  1：删除',
  `createTime` datetime NOT NULL COMMENT '创建的时间',
  `modifyTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改的实际时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for task_variable
-- ----------------------------
DROP TABLE IF EXISTS `task_variable`;
CREATE TABLE `task_variable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskId` int(11) NOT NULL COMMENT 'taskId',
  `vId` int(11) NOT NULL COMMENT '变量Id',
  `status` int(3) NOT NULL COMMENT '''状态 0：有效   ，1 ：无效'',',
  `createTime` datetime NOT NULL,
  `modifyTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



-- ----------------------------
-- Table structure for tuser
-- ----------------------------
DROP TABLE IF EXISTS `tuser`;
CREATE TABLE `tuser` (
  `tuId` int(11) NOT NULL AUTO_INCREMENT COMMENT '测试用户ID ID 自增',
  `tname` varchar(255) NOT NULL COMMENT '测试用户名',
  `tpwd` varchar(255) NOT NULL COMMENT '测试用户账号密码',
  `description` varchar(255) DEFAULT NULL COMMENT '测试用户 描述信息',
  `type` int(11) NOT NULL COMMENT '测试用户类型 1：安卓端登陆 2，IOS端登陆  3，Web端登陆',
  `t_status` int(11) NOT NULL DEFAULT '0' COMMENT '状态，0有效，1无效',
  `eid` varchar(255) DEFAULT NULL,
  `deviceId` varchar(255) DEFAULT NULL COMMENT '设备ID',
  `deviceType` varchar(255) DEFAULT NULL COMMENT '设备类型，设备的型号',
  `ua` varchar(255) DEFAULT NULL,
  `loginUrl` varchar(255) DEFAULT NULL COMMENT '用户登录接口url',
  `modifyTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改实际',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`tuId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for variable
-- ----------------------------
DROP TABLE IF EXISTS `variable`;
CREATE TABLE `variable` (
  `vId` int(11) NOT NULL AUTO_INCREMENT COMMENT '变量ID',
  `caseId` int(11) NOT NULL DEFAULT '0' COMMENT '对应的用例ID',
  `taskId` int(11) NOT NULL DEFAULT '0',
  `v_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '变量名称',
  `v_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(3) NOT NULL COMMENT '''0:系统变量  1：为群组变量  2：任务变量'' 3：用例的变量',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(2) NOT NULL COMMENT '变量状态 0：有效用例 1 删除用例',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  `modifyTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`vId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for capture
-- ----------------------------
DROP TABLE IF EXISTS `capture`;
CREATE TABLE `capture` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `capture_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '抓取URL地址',
  `capture_type` int(3) NOT NULL COMMENT '0 为Swagger',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '描述',
  `status` int(11) NOT NULL COMMENT '0：有效，1：无效',
  `capture_msg_type` int(3) NOT NULL DEFAULT '0' COMMENT '0:抓取到新的用例不通知',
  `crontab_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '定时任务抓取的时间',
  `last_run_time` datetime NOT NULL COMMENT '最后抓取的时间',
  `createTime` datetime NOT NULL,
  `modifyTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`modifyTime`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


alter table variable add groupId int(11) not Null default 0 COMMENT '群组ID，指定后说明该变量属于该群组';

DROP TABLE IF EXISTS `group_variable`;
CREATE TABLE `group_variable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL COMMENT '群组ID',
  `vId` int(11) NOT NULL COMMENT '变量Id',
  `status` int(3) NOT NULL COMMENT '''状态 0：有效   ，1 ：无效'',',
  `createTime` datetime NOT NULL,
  `modifyTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;