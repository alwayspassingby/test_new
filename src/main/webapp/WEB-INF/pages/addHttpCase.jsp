
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>添加Http接口</title>
    <link rel="stylesheet" href="/bootstrap3/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/tes_front.css">
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap3/js/bootstrap.min.js"></script>
    <script src="/js/tes_front_case.js"></script>
</head>
<body>

<div class="panel-body">

    <div class="panel panel-default">
        <div class="panel-heading"><strong><span class="glyphicon glyphicon-pencil"></span> 添加Http接口</strong></div>
        <div class="panel-body">
            <form method="POST" class="form-horizontal" role="form" action="#" id="addHttpCase">
                <%--<div class="container">--%>

                <%--<div class="form-group" style="margin-top: 20px">--%>
                    <%--<label class="control-label col-lg-1">用例所属组:</label>--%>
                    <%--<div class="col-lg-6">--%>
                        <%--<select type="text" class="form-control" id="groupId" name="groupId">--%>
                            <%--<option selected></option>--%>
                        <%--</select>--%>
                    <%--</div>--%>
                <%--</div>--%>

                <div class="form-group" style="margin-top: 20px">
                    <label class="control-label col-lg-1">URL:</label>
                    <div class="col-lg-6 col-md-6">
                        <input class="form-control" id="httpURL" name="httpURL" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-6 col-md-6">
                        <input class="form-control" id="type" name="type" type="hidden" value="0">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-1">请求方式:</label>
                    <div class="col-lg-6 col-md-6">
                        <select name="httpRequestType" id="httpRequestType" class="form-control">
                            <option value="">请选择请求方式</option>
                            <option value="0">GET</option>
                            <option value="1">POST</option>
                        </select>
                    </div>
                </div>

                <%--<div class="form-group">--%>
                    <%--<label class="control-label col-lg-1">Content-Type:</label>--%>
                    <%--<div class="col-lg-6 col-md-6">--%>
                        <%--<input type="text" class="form-control" id="httpContentType" name="httpContentType" value=""/>--%>

                    <%--</div>--%>
                <%--</div>--%>

                    <div class="form-group">
                        <label class="control-label col-lg-1">HttpHeaders:</label>
                        <div class="col-lg-5 col-md-5">
                            <button class="btn btn-primary btn-mini" onclick="addInputs('httpHeaders')" type="button"><span
                                    class="glyphicon glyphicon-plus"></span></button>
                            <span class="glyphicon glyphicon-question-sign" style="padding-left: 20px;color: #2e6da4"></span>
                        </div>

                    </div>

                    <div class="form-group" id="httpHeaders">
                    </div>



                    <div class="form-group">
                        <label class="control-label col-lg-1">Cookies:</label>
                        <div class="col-lg-6 col-md-6">
                            <button class="btn btn-primary btn-mini" onclick="addInputs('httpCookies')" type="button"><span
                                    class="glyphicon glyphicon-plus"></span></button>
                        </div>
                    </div>

                    <div class="form-group" id="httpCookies">
                    </div>

                <div class="form-group">
                    <label class="control-label col-lg-1">参数类型:</label>
                    <div class="col-lg-6 col-md-6">
                        <select name="parameterType" id="parameterType" class="form-control" onchange="selectParameterChange(this,'parameter')">
                            <option value="1" selected>form类型</option>
                            <option value="2">raw-json类型</option>
                            <option value="3">raw-text类型</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-1">请求参数:</label>
                    <div class="col-lg-6 col-md-6">
                        <button id="addParameterInput" class="btn btn-primary btn-mini" onclick="addInputs('parameter')" type="button">
                            <span class="glyphicon glyphicon-plus"></span></button>
                    </div>
                </div>

                <div class="form-group" id="parameter">

                    <%--<div class="col-lg-6 col-md-6 add-div" >--%>
                    <%--<textarea class="form-control" id=""  name="httpCookies" rows="5"></textarea>--%>
                    <%--</div>--%>

                    <%--<div class="input-group col-lg-6 col-md-offset-1 add-div">--%>
                        <%--<input type="text" class="col-lg-4 add-input" name="description" value=""/>--%>
                        <%--<input type="text" class="col-lg-7 add-input" name="description" value=""/>--%>
                        <%--<div class="btn-group-sm">--%>
                            <%--<button class="btn btn-danger btn-mini"--%>
                                    <%--style=" margin-left:2px;padding: 4px;font-size: 9px;line-height: 1;" type="button">--%>
                                <%--删除--%>
                            <%--</button>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                </div>


                <div class="form-group">
                    <label class="control-label col-lg-1">请求说明:</label>
                    <div class="col-lg-6 col-md-6">
                        <input type="text" class="form-control" id="description" name="description" value=""/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-1">检查结果类型:</label>
                    <div class="col-lg-6 col-md-6">
                        <select type="text" class="form-control" id="expectedType" name="expectedType"
                                onchange="selectChange(this)">
                            <option value="">请选择结果检查类型：</option>
                            <option value="0">Http状态码断言</option>
                            <option value="1">json内容断言</option>
                            <option value="3">文本内容断言</option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-lg-1">表达式:</label>
                    <div class="col-lg-6 col-md-6">
                        <select type="text" class="form-control" id="expression" name="expression">
                            <option value="">请选择表达式：</option>
                            <option value="0">isNotNull</option>
                            <option value="1">isNull</option>
                            <option value="2">isEqualTo</option>
                            <option value="3">isNotEqualTo</option>
                            <option value="4">contains</option>
                            <option value="5">doesNotContain</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-1">检查点:</label>
                    <%--<div class="col-lg-6 col-md-6">--%>
                        <%--<input type="text" class="form-control" id="expectedKey" name="expectedKey" value=""/>--%>
                    <%--</div>--%>

                    <div class="col-lg-6 col-md-6">
                        <input type="text" class="form-control" id="expectedKey" name="expectedKey" value=""/>
                    </div>

                </div>

                <div class="form-group">
                    <label class="control-label col-lg-1">预期结果:</label>
                    <div class="col-lg-6 col-md-6">
                        <input type="text" class="form-control" id="expectedValue" name="expectedValue" value=""/>
                    </div>
                </div>

                <%--<div class="form-group">--%>
                    <%--<label class="control-label col-lg-1">设置变量名称:</label>--%>
                    <%--<div class="col-lg-6 col-md-6">--%>
                        <%--<input type="text" class="form-control" id="staticName" name="staticName" value=""/>--%>
                    <%--</div>--%>

                <%--</div>--%>

                <%--<div class="form-group">--%>
                    <%--<label class="control-label col-lg-1">表达式:</label>--%>
                    <%--<div class="col-lg-6 col-md-6">--%>
                        <%--<input type="text" class="form-control" id="staticKey" name="staticKey" value=""/>--%>
                    <%--</div>--%>

                <%--</div>--%>

                <div class="form-group">
                    <div class="col-sm-offset-1 col-sm-10">
                        <button type="button" class="btn btn-primary" onclick="insertTestCase(true,'addHttpCase')">添加用例</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>

</body>

<script language="javascript">

    $(document).ready(function () {
        var caseData = ${apiCase};
        if (!(caseData == null || caseData == undefined || caseData == "")){
            var dataJson = JSON.stringify(caseData);
            var date = JSON.parse(dataJson);
            initPageContentFromCaseData(date);
        }
    });


</script>
</html>
