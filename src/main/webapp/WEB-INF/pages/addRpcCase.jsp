<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>添加RPC接口</title>
    <link rel="stylesheet" href="/bootstrap3/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/tes_front.css">
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap3/js/bootstrap.min.js"></script>
    <script src="/js/tes_front_case.js"></script>
</head>
<body>


<div class="panel-body">

    <div class="panel panel-default">
        <div class="panel-heading"><strong><span class="glyphicon glyphicon-pencil"></span> 添加RPC接口</strong></div>
        <div class="panel-body">
            <%--<form method="post" class="form-horizontal" role="form" action="./addCase" onsubmit="return check(false)">--%>
            <form method="POST" class="form-horizontal" role="form" action="#" id="addHttpCase">

                <%--<div class="form-group" style="margin-top: 20px">--%>
                    <%--<label class="control-label col-lg-1">用例所属组:</label>--%>
                    <%--<div class="col-lg-6">--%>
                        <%--<select type="text" class="form-control" id="groupId" name="groupId">--%>
                            <%--<option selected></option>--%>
                        <%--</select>--%>
                    <%--</div>--%>
                <%--</div>--%>


                <div class="form-group">
                    <div class="col-lg-6 col-md-6">
                        <input class="form-control" id="type" name="type" type="hidden" value="1">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-1">RPC接口服务:</label>
                    <div class="col-lg-6 col-md-6">
                        <input class="form-control" id="rpcService" name="rpcService" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-1">RPC接口方法:</label>
                    <div class="col-lg-6 col-md-6">
                        <input class="form-control" id="rpcMethod" name="rpcMethod" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-1">RPC接口版本:</label>
                    <div class="col-lg-6 col-md-6">
                        <input class="form-control" id="rpcVersion" name="rpcVersion" type="text">
                    </div>
                </div>


                <div class="form-group" style="margin-top: 20px">
                    <label class="control-label col-lg-1">RPC接口分组:</label>
                    <div class="col-lg-6 col-md-6">
                        <input class="form-control" id="rpcGroup" name="rpcGroup" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-1">请求参数:</label>
                    <div class="col-lg-6 col-md-6">
                        <button class="btn btn-primary btn-mini" onclick="addInputs('parameter')" type="button"><span
                                class="glyphicon glyphicon-plus"></span></button>
                    </div>
                </div>

                <div class="form-group" id="parameter"></div>


                <div class="form-group">
                    <label class="control-label col-lg-1">请求说明:</label>
                    <div class="col-lg-6 col-md-6">
                        <input type="text" class="form-control" id="description" name="description" value=""/>

                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-1">检查结果类型:</label>
                    <div class="col-lg-6 col-md-6">
                        <select type="text" class="form-control" id="expectedType" name="expectedType"
                                onchange="selectChange(this)">
                            <option value="">请选择结果检查类型：</option>
                            <option value="1">json内容断言</option>
                            <option value="2">调通即成功</option>
                            <option value="3">文本内容断言</option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-lg-1">表达式:</label>
                    <div class="col-lg-6 col-md-6">
                        <select type="text" class="form-control" id="expression" name="expression">
                            <option value="">请选择表达式：</option>
                            <option value="0">isNotNull</option>
                            <option value="1">isNull</option>
                            <option value="2">isEqualTo</option>
                            <option value="3">isNotEqualTo</option>
                            <option value="4">contains</option>
                            <option value="5">doesNotContain</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-1">检查点:</label>
                    <div class="col-lg-6 col-md-6">
                        <input type="text" class="form-control" id="expectedKey" name="expectedKey" value=""/>
                    </div>

                </div>

                <div class="form-group">
                    <label class="control-label col-lg-1">预期结果:</label>
                    <div class="col-lg-6 col-md-6">
                        <input type="text" class="form-control" id="expectedValue" name="expectedValue" value=""/>
                    </div>
                </div>

                <div class="form-group">s
                    <div class="col-sm-offset-1 col-sm-10">
                        <button type="button" class="btn btn-primary" onclick="insertTestCase(false,'addHttpCase')">添加
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>

</body>
<script language="javascript">

    $(document).ready(function () {
        var caseData = ${apiCase};
        if (!(caseData == null || caseData == undefined || caseData == "")){
            var dataJson = JSON.stringify(caseData);
            var date = JSON.parse(dataJson);
            initPageContentFromCaseData(date);
        }


        <%--$.ajax({--%>
            <%--type: "get",--%>
            <%--url: "/main/getGroup",--%>
            <%--dataType: "json",--%>
            <%--data: {},--%>
            <%--success: function (data) {--%>
                <%--var dataJson = JSON.stringify(data);--%>
                <%--var result = JSON.parse(dataJson);--%>
                <%--var size = result.length;--%>
                <%--var count = 0;--%>
                <%--var obj1 = document.all.groupId;--%>
                <%--obj1.options[0] = new Option("请选择", '0');--%>
                <%--for (var i = 0; i < size; i++) {--%>
                    <%--obj1.options[i + 1] = new Option(result[i].groupName, result[i].groupId);--%>
                <%--}--%>
            <%--},--%>
            <%--error: function () {--%>
<%--//                    alert("~~~~~~~~~~~~~~~~~");--%>
            <%--}--%>
        <%--});--%>

    });

</script>
</html>
