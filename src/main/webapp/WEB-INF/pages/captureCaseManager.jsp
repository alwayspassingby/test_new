<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <title>用例抓取管理管理</title>

    <script src="/js/jquery.js"></script>

    <script src="/bootstrap3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/bootstrap3/css/bootstrap.min.css">

    <script src="/bootstrap-table/dist/bootstrap-table.js"></script>
    <link rel="stylesheet" href="/bootstrap-table/dist/bootstrap-table.css">
    <script src="/bootstrap-table/dist/bootstrap-table-zh-CN.js"></script>

</head>

<body>

<div class="modal fade" id="captureConfigModal" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                <h4 class="modal-title" id="captureConfigModalLabel"></h4>
            </div>
            <form id="captureConfigForm" action="#" method="post">

                <input name="vId" id="vId" type="hidden">

                <div class="modal-body">
                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none;width: 90px">URL：
                            </button>
                        </div>
                        <input name="capture_url" id="capture_url" type="text" class="form-control"
                               style="border-radius: 4px">
                        <label id="url_Info" style="display: block;color: red;font-size: 10px"></label>
                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default"
                                    style="border: none;width: 90px">类型：
                            </button>
                        </div>
                        <select type="text" class="form-control" name="capture_type" id="capture_type"
                                style="border-radius: 4px">
                            <option value="0" selected>swagger-v2</option>
                        </select>
                        <label id="type_Info" style="display: block;color: red;font-size: 10px"></label>
                    </div>

                    <%--<div class="input-group" style="margin-right: 50px;margin-bottom: 20px">--%>
                    <%--<div class="input-group-btn">--%>
                    <%--<button type="button" disabled="disabled" class="btn btn-default"--%>
                    <%--style="border: none;width: 90px">消息通知：--%>
                    <%--</button>--%>
                    <%--</div>--%>
                    <%--<select type="text" class="form-control" name="capture_msg_type" id="capture_msg_type" style="border-radius: 4px">--%>
                    <%--<option value="0" selected>不通知</option>--%>
                    <%--</select>--%>
                    <%--</div>--%>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default"
                                    style="border: none;width: 90px">描述：
                            </button>
                        </div>
                        <input name="description" id="description" type="text" class="form-control"
                               style="border-radius: 4px">
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            onclick="clearFormData('captureConfigForm')"
                            style="float: right;margin-left: 10px;margin-right: 20px">关闭
                    </button>

                    <button id="btn_add" style="float: right;display: none" type="button" class="btn btn-primary"
                            onclick="insertCaptureConfig()">添加
                    </button>
                    <button id="btn_modify" style="float:right" type="button" class="btn btn-primary"
                            onclick="updateGlobalVariable()">修改
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="panel-body" style="padding-bottom:0px;">

    <div id="toolbar">

        <div class="input-group" style="float: left">
            <div class="input-group-btn" style="width: 0%">
                <input type="text" class="form-control col-sm-1" id="query_content" placeholder="输入关键字搜索">
                <span class="btn-group" style="padding-left: 10px">
                    <input type="text" class="form-control col-sm-1" id="query_content" placeholder="输入关键字搜索">
                    <button id="btn_clean" class="btn btn-warning" type="button">清除</button>
                    <button id="btn_query" class="btn btn-primary" type="button"
                            style="margin-left: 10px">搜索</button>
                    <button id="btn_add_item" class="btn btn-success" type="button" style="margin-left: 10px"
                            onclick="showAddVariableModal()">新增</button>
                    </span>
            </div>

        </div>
    </div>

    <table id="captureOverviewTable"></table>

</div>

<script type="application/javascript">
    var table_options_capture_overview = {
        url: '/variable/getGlobalVariableList',
        method: 'post',                      //请求方式（*）
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: false,                     //是否启用排序
        toolbar: '#toolbar',                 //工具按钮使用日期
        sortOrder: "asc",                   //排序方式
        // sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        showColumns: false,                  //是否显示所有的列
        height: 700,
        pageNumber: 1,                       //初始化加载第一页，默认第一页
        pageSize: 10,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
        clickToSelect: true,                // 是否启用点击选中行
//        showExport: true,                     //是否显示导出
        showRefresh: true,
//        exportDataType: "all",              //basic', 'all', 'selected'.
//        silent: true,
        queryParams: queryParams, //参数
        columns: [
            {
                field: 'vId',
                title: 'ID',
                align: "center",
                width: "20",
            },
            {
                field: 'v_name',
                title: '变量名称',
                align: "center",
                width: "100",
            },
            {
                field: 'v_value',
                title: '设置的值',
                align: "center",
                width: "100",
            },
            {
                field: 'type',
                title: '变量类型',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if (row.type == 0) {
                        return "系统变量";
                    } else if (row.type == 1) {
                        return "群组变量";
                    } else if (row.type == 2) {
                        return "任务变量";
                    } else {
                        return "未知类型";
                    }
                }
            }, {
                field: 'description',
                title: '描述',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if (row.description == null) {
                        return " ";
                    } else {
                        return row.description;
                    }
                }

            }, {
                field: '操作',
                title: '操作',
                align: "center",
                width: "90",
                formatter: function (value, row, index) {
                    if (row.type == 0) {
                        var group_begin = '<div class="btn-group"> <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">更多 <span class="caret"></span> </button>' +
                                '<ul class="dropdown-menu" style="min-width:100px;text-align:center">';
                        var a1 = '<li><a href="javascript:void(0);" onclick="showVariableModifyModal(\'' + row.vId + '\' )">修改</a></li>';
                        var a2 = '<li><a href="javascript:void(0);" onclick="delGlobalVariable( \'' + row.vId + '\' )">删除</a></li>';
                        var group_end = '</ul></div>';
                        return group_begin + a1 + a2 + group_end;
                    } else if (row.type == 2) {
                        var button = '<div class="btn-group"> ' +
                                '<button type="button" class="btn btn-info" onclick="showVariableModifyModal(\'' + row.vId + '\' )" >查看引用</button></div>';
                        return button
                    }
                }
            }],

    };


    /**
     * 删除记录
     */
    function delGlobalVariable(vId) {
        function del() {
            if (confirm("真的要删除该条记录吗?")) {
                return true;
            } else {
                return false;
            }
        }

        if (del()) {
            $.ajax({
                type: "get",
                url: "/variable/delGlobalVariableById",
                dataType: "json",
                data: {
                    vId: vId
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("删除成功！");
                        $('#captureOverviewTable').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        result;
                    }
                },
                error: function () {
                    alert("删除异常");

                }
            });
        } else {
            return;
        }
    }


    /**
     * 添加用户窗口
     */
    function showAddVariableModal() {
        $('#captureConfigModalLabel').text("添加抓取服务配置");
        $('#btn_modify').css("display", "none");
        $('#btn_add').css('display', 'block');
        $('#captureConfigModal').modal('show');
    }

    /**
     * 显示修改窗口
     */
    function showVariableModifyModal(id) {
        $('#variableModalLabel').text("修改变量信息");
        $("#btn_add").css("display", "none");
        $('#btn_modify').css('display', 'block');
        $.ajax({
            type: "get",
            url: "/variable/getGlobalVariableById",
            dataType: "json",
            data: {
                vId: id
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    clearFormData("variableForm");
                    $('#vId').val(result.data.vId);
                    $('#v_name').val(result.data.v_name);
                    $('#v_value').val(result.data.v_value);
                    $('#description').val(result.data.description);
                    var el = document.getElementById("type");
                    $('#type').empty();
                    if (result.data.type == 0) {
                        el.options[0] = new Option("系统变量", '0');
                    } else if (result.data.type == 1) {
                        el.options[0] = new Option("群组变量", '1');
                    } else if (result.data.type == 2) {
                        el.options[0] = new Option("任务变量", '2');
                    } else {
                        el.options[0] = new Option("类型异常", '999');
                    }
                    $('#variableModal').modal('show');
                    return;
                } else {
                    alert(result.message);
                    result;
                }
            },
            error: function () {
                alert("添加异常！");
            }
        });

    }

    //    请求参数
    function queryParams(params) {
        return {
            limit: params.limit,
            offset: params.offset,
            vId: $('#query_id').val(),
            v_name: $('#query_name').val(),
            description: $('#query_description').val(),
            variable_type: $('#query_type').val(),
        };
    }

    //初始化table
    $('#captureOverviewTable').bootstrapTable(table_options_capture_overview);


    /**
     * 清空表单内容
     * @param formID
     */
    function clearFormData(formID) {
        $("#" + formID)[0].reset();
        $("#" + formID).find("label").css("display", "none");
//        $('#baseImg').attr('src', 'data:image/gif;base64,');
//        $("#validCodeGroup").css("display", "none");


    }

    /**
     * 添加账号
     */
    function insertCaptureConfig() {
        if (check()) {
            $.ajax({
                type: "POST",
                url: "/capture/addCaptureConfig",
                dataType: "json",
                data: $("#captureConfigForm").serialize(),
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("添加成功！");
                        $('#captureConfigModal').modal('hide');
                        clearFormData("captureConfigForm");
                        $('#captureOverviewTable').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        return;
                    }
                },
                error: function () {
                    alert("添加异常！");
                }
            });
        }

    }

    /**
     * 更新host
     */
    function updateGlobalVariable() {
        if (check()) {
            $.ajax({
                type: "POST",
                url: "/variable/updateGlobalVariable",
                dataType: "json",
                data: $("#variableForm").serialize(),
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("修改成功！");
                        $('#variableModal').modal('hide');
                        clearFormData("variableForm");
                        $('#captureOverviewTable').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        return;
                    }
                },
                error: function () {
                    alert("添加异常！");

                }
            });

        }

    }


    /**
     * 检查输入内容是否正确
     * @returns {boolean}
     */
    function check() {
        if ($("#capture_url").val() == "" || $("#capture_url").val().length == 0) {
            $("#url_Info").text("抓取的服务器地址不能为空！");
            $("#url_Info").css("display", "block");
            return false;
        }
        else {
            $("#captureConfigForm").find("label").css("display", "none");
            return true;
        }
    }


    //    /**
    //     * 获取验证码
    //     */
    //    function getImgLoginCode() {
    //
    //        $.ajax({
    //            type: "GET",
    //            url: "/tuser/getImgLoginCode",
    //            dataType: "json",
    //            success: function (data) {
    //                var dataJson = JSON.stringify(data);
    //                var result = JSON.parse(dataJson);
    //                if (result.code == 0) {
    //                    $('#baseImg').attr('src', 'data:image/gif;base64,' + result.data);
    //                    return;
    //                } else {
    //                    alert(result.message);
    //                    result;
    //                }
    //            },
    //            error: function () {
    //                alert("添加异常！");
    //            }
    //        });
    //    }

    /**
     * 设置点击事件
     */
    $(function () {
        $('#btn_clean').click(function () {
            $('#query_content').val("");
            $('#captureOverviewTable').bootstrapTable('refresh');
        });
        $('#btn_query').click(function () {
            $('#captureOverviewTable').bootstrapTable('destroy');
            $('#captureOverviewTable').bootstrapTable(table_options_capture_overview);
        });
    });

</script>


</body>
</html>
