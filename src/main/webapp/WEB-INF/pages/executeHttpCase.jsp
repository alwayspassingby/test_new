
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="renderer" content="webkit">
    <title>添加Http接口用例</title>

    <%--<script src="/js/jquery.js"></script>--%>
    <%--<script src="/bootstrap3/js/bootstrap.min.js"></script>--%>
    <%--<link rel="stylesheet" href="/bootstrap3/css/bootstrap.min.css">--%>


    <link rel="stylesheet" href="/bootstrap3/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/tes_front.css">
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap3/js/bootstrap.min.js"></script>
    <script src="/js/tes_front_case.js"></script>

    <link rel="stylesheet" href="/css/c-json.css">
    <script src="/js/c-json.js"></script>

    <style>
        body {
            min-height: 300px;
            padding-top: 10px;
        }

        label.col-lg-2 {
            padding-right: 1px;
        }

        .container {
            width: 100%;
            padding-right: 8px;
            padding-left: 8px;
            /*margin-right:auto; */
            /*margin-left:auto*/
        }

        .add-div {
            width: 85%;
        }
    </style>
</head>
<body>

<div class="modal fade" id="userInfoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">使用用户信息说明</h4>
            </div>

            <div class="modal-body">
                <p class="text-primary">如果使用测试账号去调试，你可以使用以下该全局变量字段去调试。</p>
                <p class="text-primary">请填写请求参数中使用，格式：&#36;{field} field为以下全局变量：</p>
                <p class="text-warning">uid;</p>
                <p class="text-warning">phone;</p>
                <p class="text-warning">department;</p>
                <p class="text-warning">networkSubType;</p>
                <p class="text-warning">oauth_token_secret; </p>
                <p class="text-warning">oauth_token;</p>
                <p class="text-warning">appClientId;</p>
                <p class="text-warning">orgId;</p>
                <p class="text-warning">userType;  </p>
                <p class="text-warning">name;</p>
                <p class="text-warning">wbUserId;</p>
                <p class="text-warning">userName;</p>
                <p class="text-warning">tid;</p>
                <p class="text-warning">token; </p>
                <p class="text-warning">companyName;</p>
                <p class="text-warning">id; </p>
                <p class="text-warning">orgInfoId;</p>
                <p class="text-warning">oId; </p>
                <p class="text-warning">openId; </p>
                <p class="text-warning">eid;</p>
                <p class="text-warning">wbNetworkId;</p>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>

        </div>
    </div>
</div>


<div class="container" style="overflow: hidden">
    <div class="row" style="padding-right:2px">
        <div class="col-sm-6" style="padding-right:2px">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <button type="button" class="btn btn-success" onclick="excuteAPICase()" id="excuteCase">点击运行
                    </button>
                    <button type="button" class="btn btn-info" onclick="saveCase()" id="saveCase">另存为用例</button>
                </div>
                <div class="panel-body" style="height: 700px;">

                    <%--滚动条控件--%>
                    <div data-spy="scroll" style="height:700px;overflow:auto; position: relative;">

                        <form class="form-horizontal" style="margin-top: 20px" role="form" id="excuteAPICaseForm">

                            <div class="form-group">
                                <label class="control-label col-lg-2">URL:</label>
                                <div class="col-lg-8 col-md-8">
                                    <input class="form-control" id="httpURL" name="httpURL" type="text">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-7 col-md-6">
                                    <input class="form-control" id="type" name="type" type="hidden" value="0">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">选择host:</label>
                                <div class="col-lg-6 col-md-6">
                                    <select name="host" id="host" class="form-control" placeholder="请选择host">
                                        <OPTION selected></OPTION>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">选择测试账号:</label>
                                <div class="col-lg-6 col-md-6">
                                    <select name="tuserId" id="tuserId" class="form-control" onchange="setAuthPanelShow(this)">
                                    </select>
                                </div>
                                <span class="glyphicon glyphicon-question-sign"
                                      style="padding-top: 10px;padding-left: 20px;color: #2e6da4" onclick="showUserStaticInfo()"></span>
                            </div>

                            <div id="authCollapse" class="panel-collapse collapse">

                                <div class="form-group">
                                    <label class="control-label col-lg-2">Auth认证:</label>
                                    <div class="col-lg-8 col-md-8" style="padding-top: 5px;">
                                        <input type="radio" class="radio-inline" name="auth" id="auth_false" value="0" checked> 否
                                        <input type="radio" class="radio-inline" name="auth" id="auth_true" value="1" > 是
                                    </div>
                                </div>

                            </div>


                            <div class="form-group">
                                <label class="control-label col-lg-2">请求方式:</label>
                                <div class="col-lg-3 col-md-3">
                                    <select name="httpRequestType" id="httpRequestType" class="form-control">
                                        <option value="">请选择请求方式</option>
                                        <option value="0">GET</option>
                                        <option value="1">POST</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-lg-2">HttpHeaders:</label>
                                <div class="col-lg-5 col-md-5">
                                    <button class="btn btn-primary btn-mini" onclick="addInputs('httpHeaders')"
                                            type="button"><span
                                            class="glyphicon glyphicon-plus"></span></button>
                                    <span class="glyphicon glyphicon-question-sign"
                                          style="padding-top: 10px;padding-left: 10px;color: #2e6da4"></span>
                                </div>

                            </div>

                            <div class="form-group" id="httpHeaders">
                            </div>


                            <div class="form-group">
                                <label class="control-label col-lg-2">Cookies:</label>
                                <div class="col-lg-6 col-md-6">
                                    <button class="btn btn-primary btn-mini" onclick="addInputs('httpCookies')"
                                            type="button"><span
                                            class="glyphicon glyphicon-plus"></span></button>
                                </div>
                            </div>

                            <div class="form-group" id="httpCookies">
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">参数类型:</label>
                                <div class="col-lg-3 col-md-3">
                                    <select name="parameterType" id="parameterType" class="form-control"
                                            onchange="selectParameterChange(this,'parameter')">
                                        <option value="1" selected>form类型</option>
                                        <option value="2">raw-json类型</option>
                                        <option value="3">raw-text类型</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">请求参数:</label>
                                <div class="col-lg-6 col-md-6">
                                    <button id="addParameterInput" class="btn btn-primary btn-mini"
                                            onclick="addInputs('parameter')" type="button"><span
                                            class="glyphicon glyphicon-plus"></span></button>
                                </div>
                            </div>

                            <div class="form-group" id="parameter">

                            </div>


                            <div class="form-group">
                                <label class="control-label col-lg-2">请求说明:</label>
                                <div class="col-lg-6 col-md-6">
                                    <input type="text" class="form-control" id="description" name="description"
                                           value=""/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">检查结果类型:</label>
                                <div class="col-lg-6 col-md-6">
                                    <select type="text" class="form-control" id="expectedType" name="expectedType"
                                            onchange="selectChange(this)">
                                        <option value="">请选择结果检查类型：</option>
                                        <option value="0">Http状态码断言</option>
                                        <option value="1">json内容断言</option>
                                        <option value="3">文本内容断言</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-lg-2">表达式:</label>
                                <div class="col-lg-6 col-md-6">
                                    <select type="text" class="form-control" id="expression" name="expression">
                                        <option value="">请选择表达式：</option>
                                        <option value="0">isNotNull</option>
                                        <option value="1">isNull</option>
                                        <option value="2">isEqualTo</option>
                                        <option value="3">isNotEqualTo</option>
                                        <option value="4">contains</option>
                                        <option value="5">doesNotContain</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">检查点:</label>
                                <%--<div class="col-lg-6 col-md-6">--%>
                                <%--<input type="text" class="form-control" id="expectedKey" name="expectedKey" value=""/>--%>
                                <%--</div>--%>

                                <div class="col-lg-6 col-md-6">
                                    <input type="text" class="form-control" id="expectedKey" name="expectedKey"
                                           value=""/>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">预期结果:</label>
                                <div class="col-lg-6 col-md-6">
                                    <input type="text" class="form-control" id="expectedValue" name="expectedValue"
                                           value=""/>
                                </div>
                            </div>


                        </form>
                    </div>

                </div>
            </div>
        </div>


        <div class="col-sm-6" style="padding-left:10px">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <button type="button" class="btn btn-default" disabled style="border:none">运行结果</button>
                </div>
                <div class="panel-body" style="height: 700px;">
                    <ul id="resTab" class="nav nav-tabs">
                        <li class="active"><a href="#res_result" data-toggle="tab">Result</a></li>
                        <li><a href="#res_preview" data-toggle="tab">Preview</a></li>
                        <li><a href="#res_info" data-toggle="tab">Info</a></li>
                    </ul>


                    <div id="tabContent" class="tab-content">
                        <div class="tab-pane fade" id="res_result">
                            <div class="form-group">
                                <textarea  id="show-div5" readonly="readonly" class="form-control"  style="height:656px;resize: none; background-color: white;border: none;outline: none" ></textarea>
                            </div>
                        </div>
                        <div class="tab-pane fade in active" id="res_preview">
                            <div id="show-div3" style="height:656px;width: 100%;overflow-y: auto"></div>
                            <div id="show-div4" style="height:656px;margin-left:10px;width: 100%"></div>
                        </div>
                        <div class="tab-pane fade" id="res_info">
                            <div class="form-group">
                                <textarea id="show-div2" readonly="readonly" class="form-control" style="height:656px;resize: none; background-color: white;border: none;outline: none"></textarea>
                            </div>
                        </div>
                    </div>
                    <div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>


</body>
<script>

    //    初始化页面数据
    $(document).ready(function () {
        var caseId = "${caseId}";
        if (caseId == null || caseId == undefined || caseId == '') {
            getHostsByDoMain(null);
        } else {
            $.ajax({
                type: "get",
                url: "/main/getAPICaseByID",
                dataType: "json",
                data: {
                    id: caseId
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        initPageContentFromCaseData(result.data);
                        getHostsByDoMain(result.data.httpURL);
                        return;
                    } else {
                        $(".form-group").remove();
                        alert(result.message);
                        result;
                    }
                },
            });
        }

        setUserList();

    });


    function showUserStaticInfo() {
        $('#userInfoModal').modal('show');
    }

    /**
     * 获取host
     * @param host
     */
    function getHostsByDoMain(host) {
        var domain = null;

        if (host != null && host.length != 0) {

            domain = host.split("?")[0];
            domain = domain.split("//")[1];
            domain = domain.split("/")[0];
        } else {
            domain = "";
        }

        $.ajax({
            type: "get",
            url: "/host/getHostByHost",
            dataType: "json",
            data: {
                host: domain
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                var size = result.length;
                var obj1 = document.all.host;
                for (var i = 0; i < size; i++) {
                    obj1.options[i] = new Option(result[i].ip + " " + result[i].host, result[i].ip);
                }
                obj1.options[size] = new Option("默认host地址", '');

            },
//                error: function () {
//                }
        });


    }


    function checkHttpDebug() {
        if (!f_check_IP()) {
            return false;
        }
        if (!check(true)) {
            return false;
        }

        return true;
    }


    //    执行http用例
    function excuteAPICase() {

        if (checkHttpDebug()) {
            $("#show-div2").val("");
            $("#show-div4").val("");
            $("#show-div3").empty();
            $("#show-div5").val("");

            var datalist = $("#excuteAPICaseForm").serializeArray();

            // 获取headers的值
            var obj_h = new Object();
            obj_h.name = "httpHeaders";
            obj_h.value = get_InputOrTextarea_Value("httpHeaders", true);
            datalist.push(obj_h);

            // 获取请求参数的值
            var obj_p = new Object();
            obj_p.name = "requestParameter";
            var select = $("#parameterType").val();
            if (select != 2 && select != 3) {
                obj_p.value = get_InputOrTextarea_Value("parameter", true);
            } else {
                obj_p.value = get_InputOrTextarea_Value("parameter", false);
            }
            datalist.push(obj_p);

            // 获取httpCookies的值
            var obj_c = new Object();
            obj_c.name = "httpCookies";
            obj_c.value = get_InputOrTextarea_Value("httpCookies", true);
            datalist.push(obj_c);
            $.ajax({
                type: "post", //请求方式
                // url: "http://test-admin.dd01.fun/case/excuteAPICase", //请求路径
                url: "http://localhost:8580/case/excuteAPICase", //请求路径
                dataType: "json",
//                jsonpCallback: 'handleResponse', //设置回调函数名
//            contentType:"application/json;charset=UTF-8",
                data: $.param(datalist),  //传参

                success: function (result) {
                    var dataJson = JSON.stringify(result);
                    var data = JSON.parse(dataJson);
                    if (data.data == null || data.data == undefined || data.data.length == 0 ||
                            data.data.body == null || data.data.body == undefined || data.data.body.length == 0) {
                        $("#show-div4").val(dataJson);
                    }
                    else {
                        $("#show-div4").val(data.data.body);
                    }
                    $("#show-div2").val(data.content);
                    var trueDiv = document.getElementById('show-div5');
                    if (data.success) {
                        trueDiv.style.color = "#0BC31C";
                    } else {
                        trueDiv.style.color = "#FF421C";
                    }
                    ;
                    if (data.failInfo == null && data.success) {
                        $("#show-div5").val("用例执行成功。");
                    } else {
                        $("#show-div5").val(data.failInfo);
                    }
                    Process();
                    return;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                    return false;
                }
            });


        }

    }

    function saveCase() {

        var datalist = $("#excuteAPICaseForm").serializeArray();

        // 获取headers的值
        var obj_h = new Object();
        obj_h.name = "httpHeaders";
        obj_h.value = get_InputOrTextarea_Value("httpHeaders", true);
        datalist.push(obj_h);

        // 获取请求参数的值
        var obj_p = new Object();
        obj_p.name = "requestParameter";
        var select = $("#parameterType").val();
        if (select != 2 && select != 3) {
            obj_p.value = get_InputOrTextarea_Value("parameter", true);
        } else {
            obj_p.value = get_InputOrTextarea_Value("parameter", false);
        }
        datalist.push(obj_p);

        // 获取httpCookies的值
        var obj_c = new Object();
        obj_c.name = "httpCookies";
        obj_c.value = get_InputOrTextarea_Value("httpCookies", true);
        datalist.push(obj_c);

        //获取当前网址，如： http://localhost:8083/xxx/xxx/xxxx.jsp
//        var curWwwPath=window.document.location.href;
        //获取主机地址之后的目录，如： xxx/xxx/xxx.jsp
//        var pathName=window.document.location.pathname;
//        var pos=curWwwPath.indexOf(pathName);
        //获取主机地址，如： http://localhost:8083
//        var localhostPaht=curWwwPath.substring(0,pos);
        var localhostPaht = "http://" + window.document.location.host;
//        alert(localhostPaht);
        window.location.href = localhostPaht + "/main/addHttpCase?" + $.param(datalist);
    }

    function f_check_IP() {
        var ip = document.getElementById('host').value;
        if (!(ip == null || ip == undefined || ip == "")) {
            var re = /^(\d+)\.(\d+)\.(\d+)\.(\d+)$/;//正则表达式
            if (re.test(ip)) {
                if (RegExp.$1 < 256 && RegExp.$2 < 256 && RegExp.$3 < 256 && RegExp.$4 < 256) {
                    return true;
                } else {
                    alert("host格式不正确");
                    return false;
                }
            } else {
                alert("host格式不正确");
                return false;
            }
        }
        return true;
    }

    /**
     * 选择用户cookie
     */
    function selectUser() {
        var userId = $("#tuserId").val();
        var validCode = $("#validCode").val();
        if (userId == 999) {
            $("#httpCookies").val('');
            $("#validCode").css('display', 'none');
            $("#baseImg").css('display', 'none');
            $("#codeInfo").css('display', 'none');
            return;
        }
        $.ajax({
            type: "get", //请求方式
            url: "/tuser/getCookieByUserID", //请求路径
            data: {
                id: userId,
                validCode: validCode
            },  //传参
            success: function (result) {
                if (result.code == 0) {
                    $("#httpCookies").val(result.data);
                    $("#validCode").css('display', 'none');
                    $("#baseImg").css('display', 'none');
                    $("#codeInfo").css('display', 'none');
                    $('#baseImg').attr('src', 'data:image/gif;base64,');
                    $("#validCode").val('');
                    return;
                } else if (result.code == 2010202) {
                    alert(result.message);
                    $("#validCode").css('display', 'block');
                    $("#baseImg").css('display', 'block');
                    $("#codeInfo").css('display', 'block');
                    getImgLoginCode();
                    return;
                } else {
                    alert(result.message);
                    return;
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
                return false;
            }
        });
    }


    /**
     * 获取验证码
     */
    function getImgLoginCode() {

        $.ajax({
            type: "GET",
            url: "/tuser/getImgLoginCode",
            dataType: "json",
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    $('#baseImg').attr('src', 'data:image/gif;base64,' + result.data);
                    return;
                } else {
                    alert(result.message);
                    result;
                }
            },
            error: function () {
                alert("添加异常！");
            }
        });
    }

    //    设置用户列表
    function setUserList() {
        $("#tuserId").empty();
        $.ajax({
            type: "get", //请求方式
            url: "/tuser/getTUsers", //请求路径
            data: {},  //传参
            success: function (result) {
                var objSelect = document.getElementById("tuserId");
                objSelect.options.add(new Option("不使用", "0", true));
                $('#authCollapse').collapse('hide');
                var userList = result.data;
                if(userList == null || userList.length == 0){
                    return;
                }
                for (var i = 0; i < userList.length; i++) {
                    var name = null;
                    if(userList[i].type > 0 && userList[i].type <=3){
                        if (userList[i].type == 1){
                            name = "Android端";
                        }else if (userList[i].type ==2){
                            name = "IOS端";
                        }else {
                            name = "Web端";
                        }
                        objSelect.options.add(new Option(userList[i].tname + "-" + name + "-" +userList[i].description, userList[i].tuId));
                    }
                }
                return;
            },
//            error: function (XMLHttpRequest, textStatus, errorThrown) {
//                alert(errorThrown);
//                return false;
//            }
        });
    }

</script>
</html>
