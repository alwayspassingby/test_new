<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="renderer" content="webkit">
    <title>调试RPC接口</title>
    <link rel="stylesheet" href="/bootstrap3/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/tes_front.css">
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap3/js/bootstrap.min.js"></script>
    <script src="/js/tes_front_case.js"></script>

    <link rel="stylesheet" href="/css/c-json.css">
    <script src="/js/c-json.js"></script>

    <style>
        body {
            min-height: 300px;
            padding-top: 10px;
        }

        label.col-lg-2 {
            padding-right: 1px;
        }

        .container {
            width: 100%;
            padding-right: 8px;
            padding-left: 8px;
            /*margin-right:auto; */
            /*margin-left:auto*/
        }


        #textareaCode {
            min-height: 300px
        }

        .add-div {
            width: 85%;
        }
    </style>

</head>
<body style="overflow-x: hidden;overflow-x:hidden">

<div class="container" style="overflow: hidden">
    <div class="row" style="padding-right:2px">
        <div class="col-sm-6" style="padding-right:2px">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <button type="button" class="btn btn-success" onclick="excuteAPICase()" id="excuteCase">点击运行
                    </button>
                    <button type="button" class="btn btn-info" onclick="saveCase()" id="saveCase">另存为用例</button>
                </div>
                <div class="panel-body" style="height: 700px;">

                    <%--滚动条控件--%>
                    <div data-spy="scroll" style="height:700px;overflow:auto; position: relative;">

                        <form class="form-horizontal" style="margin-top: 20px" role="form" id="excuteAPICaseForm">

                            <div class="form-group">
                                <div class="col-lg-6 col-md-6">
                                    <input class="form-control" id="type" name="type" type="hidden" value="1">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">RPC接口服务:</label>
                                <div class="col-lg-7 col-md-7">
                                    <input class="form-control" id="rpcService" name="rpcService" type="text">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-lg-2">RPC接口方法:</label>
                                <div class="col-lg-7 col-md-7">
                                    <input class="form-control" id="rpcMethod" name="rpcMethod" type="text">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">RPC接口版本:</label>
                                <div class="col-lg-7 col-md-7">
                                <input class="form-control" id="rpcVersion" name="rpcVersion" type="text">
                                </div>
                            </div>


                            <div class="form-group" style="margin-top: 20px">
                                <label class="control-label col-lg-2">RPC接口分组:</label>
                                <div class="col-lg-7 col-md-7">
                                    <input class="form-control" id="rpcGroup" name="rpcGroup" type="text">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">请求参数:</label>
                                <div class="col-lg-6 col-md-6">
                                    <button class="btn btn-primary btn-mini" onclick="addInputs('parameter')" type="button"><span
                                            class="glyphicon glyphicon-plus"></span></button>
                                </div>
                            </div>

                            <div class="form-group" id="parameter"></div>


                            <div class="form-group">
                                <label class="control-label col-lg-2">请求说明:</label>
                                <div class="col-lg-7 col-md-7">
                                    <input type="text" class="form-control" id="description" name="description" value=""/>

                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-lg-2">检查结果类型:</label>
                                <div class="col-lg-3 col-md-3">
                                    <select type="text" class="form-control" id="expectedType" name="expectedType"
                                            onchange="selectChange(this)">
                                        <option value="">请选择结果检查类型：</option>
                                        <option value="1">json内容断言</option>
                                        <option value="2">调通即成功</option>
                                        <option value="3">文本内容断言</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">表达式:</label>
                                <div class="col-lg-3 col-md-3">
                                    <select type="text" class="form-control" id="expression" name="expression">
                                        <option value="">请选择表达式：</option>
                                        <option value="0">isNotNull</option>
                                        <option value="1">isNull</option>
                                        <option value="2">isEqualTo</option>
                                        <option value="3">isNotEqualTo</option>
                                        <option value="4">contains</option>
                                        <option value="5">doesNotContain</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">检查点:</label>
                                <div class="col-lg-7 col-md-7">
                                    <input type="text" class="form-control" id="expectedKey" name="expectedKey"
                                           value=""/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">预期结果:</label>
                                <div class="col-lg-7 col-md-7">
                                    <input type="text" class="form-control" id="expectedValue" name="expectedValue"
                                           value=""/>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>


        <div class="col-sm-6" style="padding-left:10px">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <button type="button" class="btn btn-default" disabled="disabled">运行结果</button>
                </div>
                <div class="panel-body" style="height: 700px;">
                    <ul id="resTab" class="nav nav-tabs">
                        <li class="active"><a href="#res_result" data-toggle="tab">Result</a></li>
                        <li><a href="#res_preview" data-toggle="tab">Preview</a></li>
                        <li><a href="#res_info" data-toggle="tab">Info</a></li>
                    </ul>


                    <div id="tabContent" class="tab-content">
                        <div class="tab-pane fade " id="res_result">
                            <div class="form-group">
                                <textarea  id="show-div5" readonly="readonly"  class="form-control"  style="height:656px;resize: none; background-color: white"></textarea>
                            </div>
                        </div>
                        <div class="tab-pane fade in active" id="res_preview">
                            <div id="show-div3" style="height:656px;width: 100%;overflow-y: auto"></div>
                            <div id="show-div4" style="height:656px;margin-left:10px;width: 100%"></div>
                        </div>
                        <div class="tab-pane fade" id="res_info">
                            <div class="form-group">
                                <textarea id="show-div2" readonly="readonly" class="form-control" style="height:656px;resize: none; background-color: white"></textarea>
                            </div>
                        </div>
                    </div>
                    <div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

</body>
<script>
    $(document).ready(function () {
        var caseId = "${caseId}";
        if (!(caseId == null || caseId == undefined || caseId == '')) {
            $.ajax({
                type: "get",
                url: "/main/getAPICaseByID",
                dataType: "json",
                data: {
                    id: caseId
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        initPageContentFromCaseData(result.data);
                        return;
                    } else {
                        $(".form-group").remove();
                        alert(result.message);
                        result;
                    }
                },
//                error: function () {
//                    $(".form-group").remove();
//                    alert("获取用例异常 ID = " + caseId);
//
//                }
            });
        }

    });

    function excuteAPICase(){

        if (check(false)) {
            $("#show-div2").val("");
            $("#show-div4").val("");
            $("#show-div3").empty();
            $("#show-div5").val("");
            var datalist = $("#excuteAPICaseForm").serializeArray();

            // 获取请求参数的值
            var obj_p = new Object();
            obj_p.name = "requestParameter";
            var select = $("#parameterType").val();
            if (select != 2 && select != 3) {
                obj_p.value = get_InputOrTextarea_Value("parameter", true);
            } else {
                obj_p.value = get_InputOrTextarea_Value("parameter", false);
            }
            datalist.push(obj_p);

            $.ajax({
                type: "post", //请求方式
                url: "http://test-admin.dd01.fun/case/excuteAPICase", //请求路径
                // url: "http://localhost:8580/case/excuteAPICase", //请求路径
                dataType:"json",
//                jsonpCallback: 'handleResponse', //设置回调函数名
//            contentType:"application/json;charset=UTF-8",
                data: $.param(datalist),  //传参
                success: function (result) {
                    var dataJson = JSON.stringify(result);
                    var data = JSON.parse(dataJson);
                    if (data.data == null || data.data == undefined || data.data.length == 0 ||
                            data.data.body == null || data.data.body == undefined || data.data.body.length == 0) {
                        $("#show-div4").val(data.message);
                    }
                    else {
                        $("#show-div4").val(data.data.body);
                    }
                    $("#show-div2").val("");

                    $("#show-div2").val(data.content);
                    var trueDiv=document.getElementById('show-div5');
                    if (data.success){
                        trueDiv.style.color="#0BC31C";
                    }else {
                        trueDiv.style.color="#FF421C";
                    };
                    if (data.failInfo == null && data.success){
                        $("#show-div5").val("断言成功");
                    }else {
                        $("#show-div5").val(data.failInfo);
                    }
                    Process();
                    return;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                    return false;
                }
            });
        }

    }

    function saveCase() {

        var datalist = $("#excuteAPICaseForm").serializeArray();

        // 获取请求参数的值
        var obj_p = new Object();
        obj_p.name = "requestParameter";
        var select = $("#parameterType").val();
        if (select != 2 && select != 3) {
            obj_p.value = get_InputOrTextarea_Value("parameter", true);
        } else {
            obj_p.value = get_InputOrTextarea_Value("parameter", false);
        }
        datalist.push(obj_p);

        //获取当前网址，如： http://localhost:8083/xxx/xxx/xxxx.jsp
//        var curWwwPath=window.document.location.href;
        //获取主机地址之后的目录，如： xxx/xxx/xxx.jsp
//        var pathName=window.document.location.pathname;
//        var pos=curWwwPath.indexOf(pathName);
        //获取主机地址，如： http://localhost:8083
//        var localhostPaht=curWwwPath.substring(0,pos);
        var localhostPaht="http://" + window.document.location.host ;
//        alert(localhostPaht);
        window.location.href= localhostPaht+"/main/addRpcCase?"+$.param(datalist);
    }


    function getTurboParam() {
        $("#paramList").empty();

        if(getTurboParamcheck()){
            $.ajax({
                type: "get", //请求方式
                url: "/main/getTurboParameter", //请求路径
                data: {
                    classPath: $("#rpcService").val(),
                    methodName: $("#rpcMethod").val()
                },  //传参
                success: function (result) {
                    var objSelect = document.getElementById("paramList");
                    var methodList = result.data;
                    objSelect.options.add(new Option("请选择"," ",true));
                    for(var i=0;i < methodList.length;i++){
                        var j = i+1;
                        var text = "参数格式 "+j;
                        objSelect.options.add(new Option(text,methodList[i]));
                    }
                    return;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                    return false;
                }
            });
        }

    }
    
    function getTurboParamcheck() {
        if ($("#rpcService").val() == "" ) {
            alert("请输入接口名称！");
            return false;
        }if ($("#rpcMethod").val() == "" ) {
            alert("请输入接口方法！");
            return false;
        }
        else {
            return true;
        }
    }

</script>



</html>
