<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <title>场景列表</title>

    <script src="/js/jquery.js"></script>

    <script src="/bootstrap3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/bootstrap3/css/bootstrap.min.css">

    <script src="/bootstrap-table/dist/bootstrap-table.js"></script>
    <link rel="stylesheet" href="/bootstrap-table/dist/bootstrap-table.css">
    <script src="/bootstrap-table/dist/bootstrap-table-zh-CN.js"></script>
    <script src="/js/tes_front_variable.js"></script>
    <style type="text/css">
        .popover {
            word-break: break-all;
        }

        .tooltip-inner {
            word-break: break-all;
            max-width: 300px;
            font-size: 12px;
            /*color: #151515;*/
            /*background-color: #7B7B7B;*/
        }
    </style>
</head>

<body>

<div class="modal fade" id="GroupVariableListModal" tabindex="-1" role="dialog" data-backdrop="false"
     data-keyboard="false"
     aria-labelledby="GroupVariableListModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 1000px">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">设置变量列表</h4>
            </div>

            <div id="groupVariableToolbar" class="btn-group" style="display: block">

                <ul id="groupVariableTab" class="nav nav-tabs">
                    <li class="active">
                        <a href="#" data-toggle="tab"
                           onclick="selectedAddGroupVariableList(true,$('#gv_groupId').val())">
                            已添加的变量
                        </a>
                    </li>
                    <li><a href="#" data-toggle="tab"
                           onclick="selectedAddGroupVariableList(false,$('#gv_groupId').val())">未添加的变量</a>
                    </li>
                </ul>

                <div id="addGroupVariableButton" style="padding-top: 10px">
                    <button id="addGroupVeriable" class="btn btn-success btn-sm"
                            onclick="showAddGroupVariableModalByGroupId($('#gv_groupId').val())" type="button"><span
                            class="glyphicon glyphicon-plus"></span></button>
                </div>

            </div>

            <form id="groupVariableList" action="#" hidden>
                <input name="gv_groupId" id="gv_groupId" type="hidden">
                <input name="gv_isSelected" id="gv_isSelected" type="hidden">
            </form>


            <div class="panel-body" style="padding-bottom:0px;">
                <table id="OverviewGroupVariableList"></table>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"
                        onclick="clearModalData('groupVariableList','OverviewGroupVariableList')">关闭
                </button>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="groupVariableModal" tabindex="-1" role="dialog" data-backdrop="false"
     data-keyboard="false" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                <h4 class="modal-title" id="groupVariableModalLabel"></h4>
            </div>
            <form id="groupVariableForm" action="#" method="post">

                <input name="gv_Id" id="gv_Id" type="hidden">
                <input name="gv_type" id="gv_type" value="1" type="hidden">
                <input name="gv_gId" id="gv_gId" type="hidden">

                <div class="modal-body">
                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">名称：
                            </button>
                        </div>
                        <input name="gv_name" id="gv_name" type="text" class="form-control" style="border-radius: 4px">
                        <label id="name_Info" style="display: block;color: red;font-size: 10px"></label>
                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">value：
                            </button>
                        </div>
                        <input name="gv_value" id="gv_value" type="text" class="form-control"
                               style="border-radius: 4px">
                        <label id="value_Info" style="display: block;color: red;font-size: 10px"></label>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            onclick="clearFormData('variableForm')"
                            style="float: right;margin-left: 10px;margin-right: 20px">关闭
                    </button>

                    <button id="gv_but_add" style="float: right;display: none" type="button" class="btn btn-primary"
                            onclick="addGroupVariable()">添加
                    </button>
                    <button id="gv_but_modify" style="float:right" type="button" class="btn btn-primary"
                            onclick="updateGroupVariable()">修改
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>


<script type="application/javascript" title="群组自定义变量列表">

    var table_options_overview_group_getVariableListFromTask = {
        url: '/variable/queryGroupVariableList',
        method: 'post',                      //请求方式（*）
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: false,                     //是否启用排序
        clickToSelect: true,
        toolbar: '#groupVariableToolbar', //工具按钮使用日期
        search: false,
        searchOnEnterKey: false,
        trimOnSearch: true,
        sortOrder: "asc",                   //排序方式
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        showColumns: false,                  //是否显示所有的列
        height: 530,
        pageNumber: 1,                       //初始化加载第一页，默认第一页
        pageSize: 10,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
//        clickToSelect: true,                // 是否启用点击选中行
//        showExport: true,                     //是否显示导出
//        showRefresh: true,
//        exportDataType: "all",              //basic', 'all', 'selected'.
//        silent: true,
        queryParams: queryParamsGetGroupVariableList, //参数
        columns: [{
            field: 'vId',
            title: 'ID',
            align: "center",
            width: "20",
        },
            {
                field: 'v_name',
                title: '变量名',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if (row.v_name == null) {
                        return "-"
                    }
                    if (row.v_name.length > 30) {
                        var text = row.v_name.substr(0, 30) + "...";
                        return '<span data-toggle="tooltip" title="' + row.v_name + '"> ' + text + ' </span>';
                    } else {
                        return row.v_name;
                    }
                }
            },
            {
                field: 'v_value',
                title: '表达式',
                align: "center",
                width: "130",
                formatter: function (value, row, index) {
                    if (row.v_value.length > 25) {
                        var text = row.v_value.substr(0, 25) + "...";
                        return '<span data-toggle="tooltip" title="' + row.v_value + '"> ' + text + ' </span>';
                    } else {
                        return row.v_value;
                    }
                }
            },
            {
                field: '操作',
                title: '操作',
                align: "center",
                width: "90",
                formatter: function (value, row, index) {

                    var group_move = '<div class="btn-group" style="margin-right: 3px">' +
                        ' <button type="button" class="btn-default btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="moveGroupVariable(\'' + row.vId + '\' )">移除 </button></div>';

                    var group_modify = '<div class="btn-group" style="margin-right: 4px">' +
                        ' <button type="button" class="btn-info btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="showUpdateGroupVariableModal(\'' + row.vId + '\' )">修改 </button></div>';

                    var group_add = '<div class="btn-group" style="margin-right: 3px">' +
                        ' <button type="button" class="btn-info btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="addToGroupVariableList(\'' + row.vId + '\' )">添加 </button></div>';

                    var group_delete = '<div class="btn-group">' +
                        ' <button type="button" class="btn-info btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="delVariable(\'' + row.vId + '\' )">删除 </button></div>';

                    if ($('#gv_isSelected').val() == 1) {
                        return group_move + group_modify;
                    } else {
                        return group_add + group_modify + group_delete;
                    }
                }
            }
        ],
//        onSearch: function (text) {
//            $('#OverviewTableCaseVariableList').bootstrapTable(table_options_overview_getVariableListFromGroup);
//        },
        onPostBody: function () {
//            $("[data-toggle='popover']").popover();
            $("[data-toggle='tooltip']").tooltip();
        }

    };

    function queryParamsGetGroupVariableList(params) {
        return {
            limit: params.limit,
            offset: params.offset,
            isSelected: $('#gv_isSelected').val(),
            groupId: $('#gv_groupId').val(),
            // searchText: params.search,
        };
    }

    function setGroupVariable(groupId) {
        $('#addGroupVariableButton').css("display", "none")
        $('#OverviewGroupVariableList').bootstrapTable('destroy');
        $('#gv_groupId').val(groupId);
        $('#gv_isSelected').val(1);
        $('#GroupVariableListModal').modal('show');
        $('#groupVariableTab li:eq(0) a').tab('show');
        $('#OverviewGroupVariableList').bootstrapTable(table_options_overview_group_getVariableListFromTask);
    }

    function selectedAddGroupVariableList(isAddList, groupId) {
        if (isAddList) {
            $('#addGroupVariableButton').css("display", "none")
            $('#gv_isSelected').val(1);
        } else {
            $('#addGroupVariableButton').css("display", "block")
            $('#gv_isSelected').val(0);

        }
        $('#gv_groupId').val(groupId);
        $('#OverviewGroupVariableList').bootstrapTable('destroy');
        $('#OverviewGroupVariableList').bootstrapTable(table_options_overview_group_getVariableListFromTask);

    }

    /**
     * 显示群组任务窗口
     **/
    function showAddGroupVariableModalByGroupId(groupId) {
        $('#gv_gId').val(groupId)
        $('#groupVariableModalLabel').text("设置分组变量");
        $('#gv_but_modify').css("display", "none");
        $('#gv_but_add').css('display', 'block');
        $('#groupVariableModal').modal('show');
    }


    /**
     * 添加任务变量
     */
    function addGroupVariable() {
        if (variableCheck()) {
            $.ajax({
                type: "POST",
                url: "/variable/addGroupVariable",
                dataType: "json",
                data: {
                    type: $("#gv_type").val(),
                    groupId: $("#gv_gId").val(),
                    v_name: $("#gv_name").val(),
                    v_value: $("#gv_value").val()
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("添加成功！");
                        $('#groupVariableModal').modal('hide');
                        clearFormData("groupVariableForm");
                        $('#OverviewGroupVariableList').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        return;
                    }
                },
                error: function () {
                    alert("添加异常！");
                }
            });
        }

    }


    /**
     * 显示修改窗口
     */
    function showUpdateGroupVariableModal(vId) {
        $('#gv_Id').val(vId);
        $('#groupVariableModalLabel').text("修改变量");
        $("#gv_but_add").css("display", "none");
        $('#gv_but_modify').css('display', 'block');
        $.ajax({
            type: "get",
            url: "/variable/getGlobalVariableById",
            dataType: "json",
            data: {
                vId: vId
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    clearFormData("groupVariableForm");
                    $('#gv_Id').val(result.data.vId);
                    $('#gv_name').val(result.data.v_name);
                    $('#gv_value').val(result.data.v_value);
                    $('#gv_type').val(result.data.type);
                    $('#gv_gId').val($("#gv_groupId").val());
                    $('#groupVariableModal').modal('show');
                    return;
                } else {
                    alert(result.message);
                    result;
                }
            },
            error: function () {
                alert("修改异常！");
            }
        });

    }


    /**
     * 从我的变量列表中移除变量
     */
    function moveGroupVariable(vId) {
        function del() {
            if (confirm("真的要移除该条记录吗?")) {
                return true;
            } else {
                return false;
            }
        }

        if (del()) {
            $.ajax({
                type: "get",
                url: "/variable/moveGroupVariable",
//                dataType: "json",
                data: {
                    groupId: $('#gv_groupId').val(),
                    vId: vId
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert(result.message);
                        $('#OverviewGroupVariableList').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        result;
                    }
                },
                error: function () {
                    alert(result.message);

                }
            });
        } else {
            return;
        }
    }

    /**
     * 添加 变量到列表
     * @param vId
     */
    function addToGroupVariableList(vId) {

        $.ajax({
            type: "POST",
            url: "/variable/addToGroupVariableList",
            dataType: "json",
            data: {
                groupId: $('#gv_groupId').val(),
                vId: vId
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    alert("添加成功！");
                    $('#OverviewGroupVariableList').bootstrapTable('refresh');
                    return;
                } else {
                    alert(result.message);
                    return;
                }
            },
            error: function () {
                alert("添加异常！");
            }
        });

    }

    function updateGroupVariable() {
        if (variableCheck()) {
            $.ajax({
                type: "POST",
                url: "/variable/updateGroupVariable",
                dataType: "json",
                data: {
                    groupId: $("#gv_gId").val(),
                    v_name: $("#gv_name").val(),
                    v_value: $("#gv_value").val(),
                    type: $("#gv_type").val(),
                    vId: $("#gv_Id").val()
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("修改成功！");
                        $('#groupVariableModal').modal('hide');
                        clearFormData("groupVariableForm");
                        $('#OverviewGroupVariableList').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        return;
                    }
                },
                error: function () {
                    alert("添加异常！");

                }
            });

        }

    }

    /**
     * 删除变量
     **/
    function delVariable(vId) {

        function upConf() {
            if (confirm("删除可能会影响到目前使用该记录的用例，真的要删除吗??")) {
                return true;
            } else {
                return false;
            }
        }

        if (upConf()) {
            $.ajax({
                type: "get",
                url: "/variable/delCaseVariableById",
                dataType: "json",
                data: {
                    vId: vId
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("删除成功！");
                        $('#OverviewGroupVariableList').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        return;
                    }
                },
                error: function () {
                    alert("添加异常！");
                }
            });
        }
    }


    //    变量检查
    function variableCheck() {
        if ($("#gv_name").val() == "" || $("#gv_name").val().length == 0) {
            $("#name_Info").text("变量名称不能为空！");
            $("#name_Info").css("display", "block");
            return false;
        }
        else if ($("#gv_value").val() == "" || $("#gv_value").val().length == 0) {
            $("#name_Info").css("display", "none");
            $("#value_Info").text("变量的值不能为空！");
            $("#value_Info").css("display", "block");
            return false;
        } else {
            $("#groupVariableForm").find("label").css("display", "none");
            return true;
        }
    }


</script>

<div class="modal fade" id="groupModal" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form id="groupForm" action="#" method="post">

                <input name="groupId" id="groupId" type="hidden">


                <div class="modal-body">
                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">场景名称：
                            </button>
                        </div>
                        <input name="groupName" id="groupName" type="text" class="form-control"
                               style="border-radius: 4px">
                        <label id="nameInfo" style="display: block;color: red;font-size: 10px"></label>
                    </div>

                    <%--<div class="input-group" style="margin-right: 50px;margin-bottom: 20px">--%>
                    <%--<div class="input-group-btn">--%>
                    <%--<button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"--%>
                    <%--style="border: none">密码：--%>
                    <%--</button>--%>
                    <%--</div>--%>
                    <%--<input name="tpwd" id="tpwd" type="text" class="form-control" style="border-radius: 4px">--%>
                    <%--<label id="pwdInfo" style="display: block;color: red;font-size: 10px"></label>--%>

                    <%--</div>--%>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">场景描述：
                            </button>
                        </div>
                        <input name="description" id="description" type="text" class="form-control"
                               style="border-radius: 4px">
                        <label id="desInfo" style="display: block;color: red;font-size: 10px"></label>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            onclick="clearFormData('groupForm')"
                            style="float: right;margin-left: 10px;margin-right: 20px">关闭
                    </button>

                    <button id="btn_add" style="float: right;display: none" type="button" class="btn btn-primary"
                            onclick="insertGroup()">新增
                    </button>
                    <button id="btn_modify" style="float:right" type="button" class="btn btn-primary"
                            onclick="updateGroup()">修改
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>


<div class="modal fade" id="addCaseByGroup" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 1000px">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabelAddCase"></h4>
            </div>

            <div id="toolbar2" class="btn-group" style="display: block">
                <ul id="myTab" class="nav nav-tabs">
                    <li class="active">
                        <a href="#" data-toggle="tab" onclick="selectedAddCaseList($('#group_id').val())">
                            已添加的用例
                        </a>
                    </li>
                    <li><a href="#" data-toggle="tab" onclick="selectedNoAddCaseList($('#group_id').val())">未添加的用例</a>
                    </li>
                </ul>
            </div>

            <form id="addCaseFromGroup" action="#" hidden>
                <input name="group_id" id="group_id" type="hidden">
                <input name="isSelected" id="isSelected" type="hidden">
            </form>
            <div class="panel-body" style="padding-bottom:0px;">
                <table id="taskOverviewTableAddCase"></table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"
                        onclick="clearModalData('addCaseFromGroup','taskOverviewTableAddCase')">关闭
                </button>

                <%--<button id="btn_modify" type="button" class="btn btn-primary" onclick="updateTask()">保存</button>--%>
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="moveCaseByGroup" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 500px">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="">将用例移动到其他场景中</h4>
            </div>

            <input name="moveCaseId" id="moveCaseId" type="hidden">
            <div class="modal-body">
                <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                    <div class="input-group-btn">
                        <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                style="border: none">迁移到场景的 Id：
                        </button>
                    </div>
                    <input name="toGroupId" id="toGroupId" type="text" class="form-control"
                           style="border-radius: 4px; width:90px">
                    <label id="toGroup" style="display: block;color: red;font-size: 10px"></label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"
                        onclick="closeMoveGroup()">关闭
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal"
                        onclick="MoveGroup()">确定
                </button>

                <%--<button id="btn_modify" type="button" class="btn btn-primary" onclick="updateTask()">保存</button>--%>
            </div>

        </div>
    </div>
</div>

<%--变量窗口--%>
<div class="modal fade" id="CaseVeriableListModal" tabindex="-1" role="dialog" data-backdrop="false"
     data-keyboard="false"
     aria-labelledby="CaseVeriableListModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 1000px">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">查看用例的变量列表</h4>
            </div>

            <div id="variableToolbar" class="btn-group" style="display: block">

                <ul id="variableTab" class="nav nav-tabs">
                    <li class="active">
                        <a href="#" data-toggle="tab"
                           onclick="selectedAddVariableListFrom($('#group_id').val(),$('#v_caseId').val())">
                            已添加
                        </a>
                    </li>
                    <li><a href="#" data-toggle="tab"
                           onclick="selectedNotAddVariableListFrom($('#group_id').val(),$('#v_caseId').val())">未添加</a>
                    </li>
                </ul>

                <div id="addVariableButton">
                    <button id="addCaseVeriable" class="btn btn-success btn-sm"
                            onclick="showAddVariableModalByCaseId($('#v_caseId').val())" type="button"><span
                            class="glyphicon glyphicon-plus"></span></button>
                </div>

            </div>

            <form id="variableListFromGroup" action="#" hidden>
                <input name="v_groupId" id="v_groupId" type="hidden">
                <input name="v_isSelected" id="v_isSelected" type="hidden">
                <input name="v_caseId" id="v_caseId" type="hidden">
            </form>

            <div class="panel-body" style="padding-bottom:0px;">
                <table id="OverviewTableCaseVariableList"></table>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"
                        onclick="clearModalData('variableListFromGroup','OverviewTableCaseVariableList')">关闭
                </button>
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="variableModal" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false"
     aria-labelledby="variableModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                <h4 class="modal-title" id="variableModalLabel"></h4>
            </div>
            <form id="variableForm" action="#" method="post">

                <input name="addVariable_caseId" id="addVariable_caseId" type="hidden">
                <input name="addVariable_vId" id="addVariable_vId" type="hidden">

                <div class="modal-body">
                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">名 称：
                            </button>
                        </div>
                        <input name="add_vname" id="add_vname" type="text" class="form-control"
                               style="border-radius: 4px">
                        <label id="vnameInfo" style="display: block;color: red;font-size: 10px"></label>
                    </div>
                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">表达式：
                            </button>
                        </div>
                        <input name="add_vkey" id="add_vkey" type="text" class="form-control"
                               style="border-radius: 4px">
                        <label id="vkeyInfo" style="display: block;color: red;font-size: 10px"></label>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            onclick="clearFormData('variableForm')"
                            style="float: right;margin-left: 10px;margin-right: 20px">关闭
                    </button>

                    <button id="btn_variable_add" style="float: right;display: none" type="button"
                            class="btn btn-primary"
                            onclick="insertVariable($('#addVariable_caseId').val())">添加
                    </button>
                    <button id="btn_variable_modify" style="float:right" type="button" class="btn btn-primary"
                            onclick="updateVariable($('#addVariable_vId').val())">修改
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="panel-body" style="padding-bottom:0px;">

    <div>
        <ul class="breadcrumb">
            <li><a href="/task/taskManager"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> 任务管理 </a>
            </li>
            <li class="active" id="task_name"></li>
        </ul>
    </div>

    <div id="toolbar" class="btn-group">
        <button id="add" type="button" class="btn btn-success" onclick="showAddGroupModal()">
            <span class="glyphicon glyphicon-plus"></span>新增
        </button>
        <button id="add" type="button" class="btn btn-success" onclick="showAddGroupModal()">
            <span class="glyphicon glyphicon-plus"></span>配置任务变量
        </button>
        <button id="add" type="button" class="btn btn-success" onclick="showAddGroupModal()">
            <span class="glyphicon glyphicon-plus"></span>关联场景场景
        </button>
        <button id="add" type="button" class="btn btn-success" onclick="showAddGroupModal()">
            <span class="glyphicon glyphicon-plus"></span>新增
        </button>
        <button id="add" type="button" class="btn btn-success" onclick="showAddGroupModal()">
            <span class="glyphicon glyphicon-plus"></span>新增
        </button>
    </div>


    <table id="taskOverviewTable"></table>

</div>

<script type="application/javascript">
    var table_options_task_overview = {
        url: '/task/getGroupListFromTaskId',
        method: 'get',                      //请求方式（*）
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: false,                     //是否启用排序
        toolbar: '#toolbar',                 //工具按钮使用日期
        sortOrder: "asc",                   //排序方式
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        showColumns: false,                  //是否显示所有的列
        search: true,
        height: 700,
        pageNumber: 1, //初始化加载第一页，默认第一页
        pageSize: 10,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
        clickToSelect: true,                // 是否启用点击选中行
//        showExport: true,                     //是否显示导出
        showRefresh: true,
//        exportDataType: "all",              //basic', 'all', 'selected'.
//        silent: true,
        queryParams: queryGroupListParams, //参数
        columns: [
            {
                field: 'groupId',
                title: 'ID',
                align: "center",
                width: "20",
            },
            {
                field: 'groupName',
                title: '场景名称',
                align: "center",
                width: "100",
            }, {
                field: 'description',
                title: '描述',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if (row.description == null) {
                        return " ";
                    } else {
                        return row.description;
                    }
                }

            }, {
                field: '操作',
                title: '操作',
                align: "center",
                width: "90",
                formatter: function (value, row, index) {
                    var group_modify = '<div class="btn-group" style="margin-right: 6px"> <button type="button" class="btn btn-info" onclick="showGroupModifyModal(\'' + row.groupId + '\')">修改</button></div>';

                    var group_del = '<div class="btn-group" style="margin-right: 6px"> <button type="button" class="btn btn-info" onclick="deleteGroup(\'' + row.groupId + '\')">删除</button></div>';

                    var group_manager = '<div class="btn-group"> <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">更多 <span class="caret"></span> </button>' +
                        '<ul class="dropdown-menu dropdown-menu-right" style="min-width:100px;text-align:center">';

                    // var a1 = '<li><a href="javascript:void(0);" onclick="showGroupModifyModal(\'' + row.groupId + '\' )">修改分组</a></li>';
                    // var a2 = '<li><a href="javascript:void(0);" onclick="deleteGroup( \'' + row.groupId + '\' )">删除分组</a></li>';
                    var a3 = '<li><a href="javascript:void(0);" onclick="managerCaseListByGroup(\'' + row.groupId + '\' , \'' + row.groupName + '\')">配置用例</a></li>';
                    var a4 = '<li><a href="javascript:void(0);" onclick="setGroupVariable( \'' + row.groupId + '\' )">定义场景变量</a></li>';

                    var group_manager_end = '</ul></div>';

                    return group_modify + group_del + group_manager + a3 + a4 + group_manager_end;
                }
            }],

    };

    var table_options_task_overview_getCasesFromGroup = {
        url: '/group/getCaseListByGroup',
        method: 'get',                      //请求方式（*）
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: false,                     //是否启用排序
        clickToSelect: true,
        toolbar: '#toolbar2', //工具按钮使用日期
        search: true,
        searchOnEnterKey: false,
        trimOnSearch: true,
        sortOrder: "asc",                   //排序方式
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        showColumns: false,                  //是否显示所有的列
        height: 530,
        pageNumber: 1,                       //初始化加载第一页，默认第一页
        pageSize: 10,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
//        clickToSelect: true,                // 是否启用点击选中行
//        showExport: true,                     //是否显示导出
//        showRefresh: true,
//        exportDataType: "all",              //basic', 'all', 'selected'.
//        silent: true,
        queryParams: queryParamsGetCasesByGroupID, //参数
        columns: [{
            field: 'aId',
            title: 'ID',
            align: "center",
            width: "20",
        },
            {
                field: '请求地址',
                title: '请求URL',
                align: "center",
                width: "100",

                //                title="Popover title"
//            data-container="body" data-toggle="popover" data-placement="left"
//            data-content="左侧的 Popover 中的一些内容">
                formatter: function (value, row, index) {
                    if (row.type == 0) {
                        if (row.httpURL == null) {
                            return "-"
                        }
                        if (row.httpURL.length > 70) {
                            var text = row.httpURL.substr(0, 70) + "...";
                            return '<a href="javascript:void(0);" onclick="window.open(\'/main/modifyCase?isModify=false&id=' + row.aId + '\')">' + '<span data-toggle="tooltip" title="' + row.httpURL + '"> ' + text + ' </span>' + '</a>'

                        } else {
                            return '<a href="javascript:void(0);" onclick="window.open(\'/main/modifyCase?isModify=false&id=' + row.aId + '\')">' + row.httpURL + '</a>';
                        }
                    } else {
                        if (row.rpcService == null && row.rpcMethod == null) {
                            return "-";
                        } else {
                            if ((row.rpcService.length + row.rpcMethod.length) > 70) {
                                var name = row.rpcService + " -> " + row.rpcMethod;
                                var text = name.substr(0, 70) + "...";
                                return '<a href="javascript:void(0);" onclick="window.open(\'/main/modifyCase?isModify=false&id=' + row.aId + '\')">' + '<span data-toggle="tooltip" title="' + name + '"> ' + text + ' </span>' + '</a>'
                                // return '<span data-toggle="tooltip" title="' + name + '"> ' + text + ' </span>';
                            }
                            return '<a href="javascript:void(0);" onclick="window.open(\'/main/modifyCase?isModify=false&id=' + row.aId + '\')">' + row.rpcService + " -> " + row.rpcMethod + '</a>';
                            // return row.rpcService + " -> " + row.rpcMethod;
                        }
                    }

                }
            },
            // {
            //     field: 'rpcService',
            //     title: 'turbo接口服务',
            //     align: "center",
            //     width: "130",
            //     formatter: function (value, row, index) {
            //         if (row.rpcService == null && row.rpcMethod == null) {
            //             return "-";
            //         } else {
            //             if ((row.rpcService.length + row.rpcMethod.length) > 25) {
            //                 var name = row.rpcService + " -> " + row.rpcMethod;
            //                 var text = name.substr(0, 25) + "...";
            //                 return '<span data-toggle="tooltip" title="' + name + '"> ' + text + ' </span>';
            //             }
            //             return row.rpcService + " -> " + row.rpcMethod;
            //         }
            //     }
            // },
            {
                field: 'description',
                title: '描述',
                align: "center",
                width: "130",
                formatter: function (value, row, index) {
                    if (row.description.length > 18) {
                        var text = row.description.substr(0, 18) + "...";
                        return '<span data-toggle="tooltip" title="' + row.description + '"> ' + text + ' </span>';
                    } else {
                        return row.description;
                    }
                }
            }, {
                field: '',
                title: '优先级',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    var priority_begin = null;
                    if ($('#isSelected').val() == 0 || $('#isSelected').val() == null
                        || $('#isSelected').val() == undefined || $('#isSelected').val() == "") {
                        priority_begin = '<div class="btn-group" style="padding-left: 2px">' +
                            ' <select id="selectedPriority" disabled ><option value ="0" selected>0</option></select></div>';
                        return priority_begin;
                    } else {
                        if (row.priority == 0) {
                            return '<div class="btn-group" style="padding-left: 2px">' +
                                ' <select id="selectedPriority" onchange="gradeChange(\'' + row.gcId + '\',this )">' +
                                '<option value ="0" selected>0</option><option value ="1">1</option><option value="2">2</option>' +
                                ' <option value ="3">3</option></select></div>';
                        } else if (row.priority == 1) {
                            return '<div class="btn-group" style="padding-left: 2px">' +
                                ' <select id="selectedPriority" onchange="gradeChange(\'' + row.gcId + '\',this)">' +
                                '<option value ="0">0</option><option value ="1" selected>1</option><option value="2">2</option>' +
                                ' <option value ="3">3</option></select></div>';
                        }
                        else if (row.priority == 2) {
                            return '<div class="btn-group" style="padding-left: 2px">' +
                                ' <select id="selectedPriority" onchange="gradeChange(\'' + row.gcId + '\' ,this)">' +
                                '<option value ="0">0</option><option value ="1" >1</option>' +
                                '<option value="2" selected>2</option><option value ="3">3</option></select></div>';
                        }
                        else if (row.priority == 3) {
                            return '<div class="btn-group" style="padding-left: 2px">' +
                                ' <select id="selectedPriority" onchange="gradeChange(\'' + row.gcId + '\' ,this)">' +
                                '<option value ="0">0</option><option value ="1" >1</option>' +
                                '<option value="2" >2</option><option value ="3" selected>3</option></select></div>';
                        } else {
                            return '<div class="btn-group" style="padding-left: 2px">' +
                                ' <select id="selectedPriority" onchange="gradeChange(\'' + row.gcId + '\' ,this)">' +
                                '<option value ="0">0</option><option value ="1" >1</option>' +
                                '<option value="2" >2</option><option value ="3">3</option>' +
                                '<option value ="' + row.priority + '" selected>' + row.priority + '</option></select></div>';
                        }
                    }

                }
            }
            , {
                field: '操作',
                title: '操作',
                align: "center",
                width: "90",
                formatter: function (value, row, index) {

                    var debugCase = '<div class="btn-group" style="padding-left: 2px">' +
                        ' <button id="addGroupButton" type="button" class=" btn-success btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  onclick="window.open(\'/main/executeCase?id=' + row.aId + '\')">调试 </button></div>';
                    var group_begin = '<div class="btn-group">' +
                        ' <button id="addGroupButton" type="button"  class=" btn-default btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  onclick="addGroupCase(\'' + row.aId + '\' , this )">添加 </button></div>';
                    var group_delete = '<div class="btn-group">' +
                        ' <button type="button" class="btn-default btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="deleteGroupCase(\'' + row.aId + '\' )">删除 </button></div>';
                    // var group_move = '<div class="btn-group">' +
                    //     ' <button type="button" class="btn-default btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="float: left" onclick="moveGroupCase(\'' + row.aId + '\' )">迁移 </button></div>';

                    var btn_group_begin = '<div class="btn-group">' +
                        ' <button class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">更多<span class="caret"></span></button>' +
                        '<ul class="dropdown-menu dropdown-menu-right" style="min-width:100px;text-align:center">';
                    var btn_group_1 = '<li><a href="javascript:void(0);" onclick="window.open(\'/main/executeCase?id=' + row.aId + '\')">调试</a></li>';
                    var btn_group_2 = '<li><a href="javascript:void(0);" onclick="moveGroupCase(\'' + row.aId + '\' )">迁移</a></li>';
                    var btn_group_3 = '<li><a href="javascript:void(0);" onclick="managerVariableListByGroup (\'' + row.aId + '\')">变量</a></li>';
                    var btn_group_end = '</ul></div>';

                    if ($('#isSelected').val() == 1) {
                        return group_delete + btn_group_begin + btn_group_1 + btn_group_2 + btn_group_3 + btn_group_end;
                    } else {
                        return group_begin + debugCase;
                    }
                }
            }
        ],
        onSearch: function (text) {
            $('#taskOverviewTableAddCase').bootstrapTable(table_options_task_overview_getCasesFromGroup);
//            alert(text);
//                $('#taskOverviewTable').val(data.beginTime+' 至 '+data.endTime);
        },
        onPostBody: function () {
//            $("[data-toggle='popover']").popover();
            $("[data-toggle='tooltip']").tooltip();
        }

    };

    var table_options_overview_getVariableListFromGroup = {
        url: '/variable/getCaseVariableListFromGroupId',
        method: 'post',                      //请求方式（*）
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: false,                     //是否启用排序
        clickToSelect: true,
        toolbar: '#variableToolbar', //工具按钮使用日期
        search: false,
        searchOnEnterKey: false,
        trimOnSearch: true,
        sortOrder: "asc",                   //排序方式
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        showColumns: false,                  //是否显示所有的列
        height: 530,
        pageNumber: 1,                       //初始化加载第一页，默认第一页
        pageSize: 10,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
//        clickToSelect: true,                // 是否启用点击选中行
//        showExport: true,                     //是否显示导出
//        showRefresh: true,
//        exportDataType: "all",              //basic', 'all', 'selected'.
//        silent: true,
        queryParams: queryParamsGetVariableListGroupID, //参数
        columns: [{
            field: 'vId',
            title: 'ID',
            align: "center",
            width: "20",
        },
            {
                field: 'v_name',
                title: '变量名',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if (row.v_name == null) {
                        return "-"
                    }
                    if (row.v_name.length > 30) {
                        var text = row.v_name.substr(0, 30) + "...";
                        return '<span data-toggle="tooltip" title="' + row.v_name + '"> ' + text + ' </span>';
                    } else {
                        return row.v_name;
                    }
                }
            },
            {
                field: 'v_value',
                title: '表达式',
                align: "center",
                width: "130",
                formatter: function (value, row, index) {
                    if (row.v_value.length > 25) {
                        var text = row.v_value.substr(0, 25) + "...";
                        return '<span data-toggle="tooltip" title="' + row.v_value + '"> ' + text + ' </span>';
                    } else {
                        return row.v_value;
                    }
                }
            },
            {
                field: '操作',
                title: '操作',
                align: "center",
                width: "90",
                formatter: function (value, row, index) {

                    // var group_begin = '<div class="btn-group">' +
                    //     ' <button id="addGroupButton" type="button"  class=" btn-default btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  onclick="addGroupCase(\'' + row.aId + '\' , this )">添加 </button></div>';
                    //
                    var group_move = '<div class="btn-group">' +
                        ' <button type="button" class="btn-default btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="deleteGroupCaseVariable(\'' + row.vId + '\' )">移除 </button></div>';

                    var group_modify = '<div class="btn-group">' +
                        ' <button type="button" class="btn-info btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="showUpdatevariableModal(\'' + row.vId + '\' )">修改 </button></div>';


                    var btn_group_begin = '<div class="btn-group">' +
                        ' <button class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">更多<span class="caret"></span></button>' +
                        '<ul class="dropdown-menu dropdown-menu-right" style="min-width:100px;text-align:center">';

                    var btn_addToGroup = '<li><a href="javascript:void(0);" onclick="insertGroupVariable(\'' + row.vId + '\' )">添加</a></li>';
                    var btn_modify = '<li><a href="javascript:void(0);" onclick="showUpdatevariableModal(\'' + row.vId + '\' )">修改</a></li>';
                    var btn_deleter = '<li><a href="javascript:void(0);" onclick="delVariable(\'' + row.vId + '\' )">删除</a></li>';
                    var btn_group_end = '</ul></div>';

                    if ($('#v_isSelected').val() == 1) {
                        return group_move + group_modify;
                    } else {
                        return btn_group_begin + btn_addToGroup + btn_modify + btn_deleter + btn_group_end;
                    }
                }
            }
        ],
        onSearch: function (text) {
            $('#OverviewTableCaseVariableList').bootstrapTable(table_options_overview_getVariableListFromGroup);
        },
        onPostBody: function () {
//            $("[data-toggle='popover']").popover();
            $("[data-toggle='tooltip']").tooltip();
        }

    };

    function managerCaseListByGroup(gId, gname) {
        $('#taskOverviewTableAddCase').bootstrapTable('destroy');
        $('#myModalLabelAddCase').text("");
        $('#group_id').val(gId);
        $('#isSelected').val(1);
        $('#addCaseByGroup').modal('show');
        $('#myModalLabelAddCase').text(gname);
        $('#myTab li:eq(0) a').tab('show');
        $('#taskOverviewTableAddCase').bootstrapTable(table_options_task_overview_getCasesFromGroup);
    }

    /**
     * 打开变量管理列表
     */
    function managerVariableListByGroup(cId) {
        $('#OverviewTableCaseVariableList').bootstrapTable('destroy');
        $('#addVariableButton').css("display", "none")
        $('#v_isSelected').val(1);
        $('#v_caseId').val(cId);
        $('#v_groupId').val($('#group_id').val());
        $('#CaseVeriableListModal').modal('show');
        $('#variableTab li:eq(0) a').tab('show');
        $('#OverviewTableCaseVariableList').bootstrapTable(table_options_overview_getVariableListFromGroup);
    }

    /**
     * 删除记录
     */
    function deleteGroup(id) {
        function del() {
            if (confirm("真的要删除该条记录吗?")) {
                return true;
            } else {
                return false;
            }
        }

        if (del()) {
            $.ajax({
                type: "get",
                url: "/group/deletegroup",
                dataType: "json",
                data: {
                    id: id
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("删除成功！");
                        $('#taskOverviewTable').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        result;
                    }
                },
                error: function () {
                    alert("删除异常");

                }
            });
        } else {
            return;
        }
    }


    /**
     * 删除群组用例
     */
    function deleteGroupCase(aId) {
        function del() {
            if (confirm("真的要删除该条记录吗?")) {
                return true;
            } else {
                return false;
            }
        }

        if (del()) {
            $.ajax({
                type: "get",
                url: "/group/deleteGroupCase",
                dataType: "json",
                data: {
                    groupId: $('#group_id').val(),
                    caseId: aId
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert(result.message);
                        $('#taskOverviewTableAddCase').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        result;
                    }
                },
                error: function () {
                    alert(result.message);

                }
            });
        } else {
            return;
        }
    }

    /**
     * 删除群组用例
     */
    function deleteGroupCaseVariable(vId) {
        function del() {
            if (confirm("真的要移除该条记录吗?")) {
                return true;
            } else {
                return false;
            }
        }

        if (del()) {
            $.ajax({
                type: "get",
                url: "/variable/delGroupCaseVariable",
//                dataType: "json",
                data: {
                    gId: $('#group_id').val(),
                    vId: vId
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert(result.message);
                        $('#OverviewTableCaseVariableList').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        result;
                    }
                },
                error: function () {
                    alert(result.message);

                }
            });
        } else {
            return;
        }
    }

    /**
     * 显示分组窗口
     */
    function showAddGroupModal() {
        $('#myModalLabel').text("添加用例场景");
        $('#btn_modify').css("display", "none");
        $('#btn_add').css('display', 'block');
        $('#groupModal').modal('show');
    }

    /**
     * 显示修改窗口
     */
    function showGroupModifyModal(id) {
        $('#myModalLabel').text("修改用例场景");
        $("#btn_add").css("display", "none");
        $('#btn_modify').css('display', 'block');
        $.ajax({
            type: "get",
            url: "/group/getGroupByID",
            dataType: "json",
            data: {
                id: id
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    clearFormData("groupForm");
                    $('#groupId').val(result.data.groupId);
                    $('#groupName').val(result.data.groupName);
                    $('#description').val(result.data.description);
                    $('#groupModal').modal('show');
                    return;
                } else {
                    alert(result.message);
                    result;
                }
            },
            error: function () {
                alert("修改异常！");
            }
        });

    }

    //    请求参数
    function queryGroupListParams(params) {
        return {
            limit: params.limit,
            offset: params.offset,
            taskId: ${taskId},
            isSelected: 1,
        };
    }

    function queryParamsGetCasesByGroupID(params) {
        return {
            limit: params.limit,
            offset: params.offset,
            query_gId: $('#group_id').val(),
            isSelected: $('#isSelected').val(),
            searchText: params.search,
        };
    }

    function queryParamsGetVariableListGroupID(params) {
        return {
            limit: params.limit,
            offset: params.offset,
            groupId: $('#v_groupId').val(),
            isSelected: $('#v_isSelected').val(),
            caseId: $('#v_caseId').val(),
            searchText: params.search,
        };
    }

    //初始化table
    $('#taskOverviewTable').bootstrapTable(table_options_task_overview);


    function selectedAddCaseList(gId) {
        $('#isSelected').val(1);
        $('#group_id').val(gId);
        $('#taskOverviewTableAddCase').bootstrapTable('destroy');
        $('#taskOverviewTableAddCase').bootstrapTable(table_options_task_overview_getCasesFromGroup);
    }

    function selectedNoAddCaseList(gId) {
        $('#isSelected').val(0);
        $('#group_id').val(gId);
        $('#taskOverviewTableAddCase').bootstrapTable('destroy');
        $('#taskOverviewTableAddCase').bootstrapTable(table_options_task_overview_getCasesFromGroup);
    }

    function selectedAddVariableListFrom(gId, cId) {
        $('#addVariableButton').css("display", "none")
        $('#v_isSelected').val(1);
        $('#v_groupId').val(gId);
        $('#v_caseId').val(cId);
        $('#OverviewTableCaseVariableList').bootstrapTable('destroy');
        $('#OverviewTableCaseVariableList').bootstrapTable(table_options_overview_getVariableListFromGroup);
    }

    function selectedNotAddVariableListFrom(gId, cId) {
        $('#addVariableButton').css("display", "block")
        $('#v_isSelected').val(0);
        $('#v_groupId').val(gId);
        $('#v_caseId').val(cId);
        $('#OverviewTableCaseVariableList').bootstrapTable('destroy');
        $('#OverviewTableCaseVariableList').bootstrapTable(table_options_overview_getVariableListFromGroup);
    }

    function addGroupCase(aId, obj) {
//        alert(obj.getAttribute("id"));
        obj.style.display = "none";
        $.ajax({
            type: "GET",
            url: "/group/addGroupCase",
            dataType: "json",
            data: {
                groupId: $('#group_id').val(),
                caseId: aId
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    alert(result.message);

//                    $('#taskOverviewTable').bootstrapTable('refresh');
                    return;
                } else {
//                    obj.disabled=false
                    obj.style.display = "block";
                    alert(result.message);
                    return;
                }
            },
            error: function () {
                alert("添加异常！");
            }
        });

    }

    /**
     * 添加分组
     */
    function insertGroup() {
        if (check()) {
            $.ajax({
                type: "POST",
                url: "/group/addGroup",
                dataType: "json",
                data: $("#groupForm").serialize(),
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("添加成功！");
                        $('#groupModal').modal('hide');
                        clearFormData("groupForm");
                        $('#taskOverviewTable').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        return;
                    }
                },
                error: function () {
                    alert("添加异常！");
                }
            });
        }

    }

    /**
     * 更新分组信息
     */
    function updateGroup() {
        if (check()) {
            $.ajax({
                type: "POST",
                url: "/group/updateGroup",
                dataType: "json",
                data: $("#groupForm").serialize(),
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("修改成功！");
                        $('#groupModal').modal('hide');
                        clearFormData("groupForm");
                        $('#taskOverviewTable').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        return;
                    }
                },
                error: function () {
                    alert("添加异常！");

                }
            });

        }

    }


    /**
     * 检查输入内容是否正确
     * @returns {boolean}
     */
    function check() {
        if ($("#groupName").val() == "" || $("#groupName").val().length == 0) {
            $("#nameInfo").text("场景名称不能为空！");
            $("#nameInfo").css("display", "block");
            return false;
        }
        else if ($("#description").val() == "" || $("#description").val().length == 0) {
            $("#nameInfo").css("display", "none");
            $("#desInfo").text("描述不能为空！");
            $("#desInfo").css("display", "block");
            return false;
        } else {
            $("#groupForm").find("label").css("display", "none");
            return true;
        }
    }


    /**
     * 设置点击事件
     */
    $(function () {
        var i = 1;
        $('#btn_reset').click(function () {
//            $('#query_tuId').val('');
            $('#query_gName').val('');
            $('#query_description').val('');
            $('#taskOverviewTable').bootstrapTable('refresh');
        });
        $('#btn_query').click(function () {
//            var queryId = Number($('#query_tuId').val());
//            if (isNaN(queryId)) {
//                alert("ID 输入必须为数字！");
//                return
//            }
            $('#taskOverviewTable').bootstrapTable('destroy');
            $('#taskOverviewTable').bootstrapTable(table_options_task_overview);
        });
    });

    function moveGroupCase(aId) {
        $('#moveCaseId').val(aId);
        $('#moveCaseByGroup').modal('show');
        $('#toGroupId').val("");
    }

    function closeMoveGroup() {
        $('#moveCaseByGroup').modal('hide');
    }

    function MoveGroup() {
        if ($('#toGroupId').val() == '' || $('#toGroupId').val() == null) {
            alert("场景id不能为空！");
        } else {
            $.ajax({
                type: "get",
                url: "/group/isExist",
                dataType: "json",
                data: {
                    groupId: $('#toGroupId').val(),
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code != 200) {
                        alert("场景不存在");
                    }
                },
                error: function () {
                    alert("添加异常！");
                }
            });
        }
        doMoveGroupCase();
    }

    function doMoveGroupCase() {
        ajax1 = $.ajax({
            type: "GET",
            url: "/group/moveGroupCase",
            dataType: "json",
            data: {
                fromGroupId: $('#group_id').val(),
                caseId: $('#moveCaseId').val(),
                toGroupId: $('#toGroupId').val(),
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 1) {
                    alert("迁移成功！");
                    return;
                } else {
                    alert(result.message);
                    return;
                }
            },
            error: function () {
                alert("添加异常22！");
            }
        });
        $.when(ajax1).done(function () {
            freshTable();
        });
    }

    function freshTable() {
        $('#taskOverviewTableAddCase').bootstrapTable('destroy');
        $('#taskOverviewTableAddCase').bootstrapTable(table_options_task_overview_getCasesFromGroup);
    }

    function gradeChange(gcId, obj) {
//        var objS = document.getElementById("selectedPriority");
        var grade = obj.options[obj.selectedIndex].value;

        ajax1 = $.ajax({
            type: "GET",
            url: "/group/setGroupCasePriority",
            dataType: "json",
            data: {
                id: gcId,
                priority: grade,
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    alert("设置成功！");
                    return;
                } else {
                    alert(result.message);
                    return;
                }
            },
            error: function () {
                alert("添加异常22！");
            }
        });

    }


    /**
     * 插入群组的变量
     * @param vId
     */
    function insertGroupVariable(vId) {

        $.ajax({
            type: "POST",
            url: "/variable/addGroupCaseVariable",
            dataType: "json",
            data: {
                groupId: $('#v_groupId').val(),
                vId: vId
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    alert("添加成功！");
                    $('#OverviewTableCaseVariableList').bootstrapTable('refresh');
                    return;
                } else {
                    alert(result.message);
                    return;
                }
            },
            error: function () {
                alert("添加异常！");
            }
        });

    }


</script>


<script type="application/javascript" title="公共方法">

    function clearModalData(formID, tableId) {
        $("#" + formID)[0].reset();
        $("#" + formID).find("label").css("display", "none");
        $("#" + tableId).bootstrapTable('destroy');
    }

    /**
     * 清空表单内容
     * @param formID
     */
    function clearFormData(formID) {
        $("#" + formID)[0].reset();
        $("#" + formID).find("label").css("display", "none");
    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#task_name").text("${taskName}");

    });
</script>
</body>
</html>
