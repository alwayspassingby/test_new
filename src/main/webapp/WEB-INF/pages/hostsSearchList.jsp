
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <title>Hosts管理列表</title>

    <script src="/js/jquery.js"></script>

    <script src="/bootstrap3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/bootstrap3/css/bootstrap.min.css">

    <script src="/bootstrap-table/dist/bootstrap-table.js"></script>
    <link rel="stylesheet" href="/bootstrap-table/dist/bootstrap-table.css">
    <script src="/bootstrap-table/dist/bootstrap-table-zh-CN.js"></script>

</head>

<body>

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                <h4 class="modal-title" id="myModalLabel">添加hosts</h4>
            </div>
            <form id="insertHost" action="#" method="post">
                <div class="modal-body">
                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">地址：
                            </button>
                        </div>
                        <input name="ip" id="ip" type="text" class="form-control" style="border-radius: 4px">
                        <label id="ipInfo" style="display: block;color: red;font-size: 10px"></label>
                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">域名：
                            </button>
                        </div>
                        <input name="host" id="host" type="text" class="form-control" style="border-radius: 4px">
                        <label id="hostInfo" style="display: block;color: red;font-size: 10px"></label>

                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">描述：
                            </button>
                        </div>
                        <input name="description" id="description" type="text" class="form-control"
                               style="border-radius: 4px">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            onclick="clearFormData('insertHost')">关闭
                    </button>

                    <button id="btn_add" type="button" class="btn btn-primary" onclick="insertHosts()">添加</button>
                </div>
            </form>

        </div>
    </div>
</div>


<div class="modal fade" id="modifyModal" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                <h4 class="modal-title" id="myModalLabel">修改hosts</h4>
            </div>
            <form id="updateHost" action="#" method="post">
                <div class="modal-body">

                    <input name="hId" id="update_id" type="hidden">

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">地址：
                            </button>
                        </div>
                        <input name="ip" id="update_ip" type="text" class="form-control" style="border-radius: 4px">
                        <label id="label_ipInfo" style="display: block;color: red;font-size: 10px"></label>
                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">域名：
                            </button>
                        </div>
                        <input name="host" id="update_host" type="text" class="form-control" style="border-radius: 4px">
                        <label id="label_hostInfo" style="display: block;color: red;font-size: 10px"></label>

                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">描述：
                            </button>
                        </div>
                        <input name="description" id="update_description" type="text" class="form-control"
                               style="border-radius: 4px">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            onclick="clearFormData('modifyModal')">关闭
                    </button>

                    <button id="btn_modify" type="button" class="btn btn-primary" onclick="updateHost()">修改</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="panel-body" style="padding-bottom:0px;">

    <div class="panel panel-default">
        <div class="panel-heading">查询条件</div>
        <div class="panel-body">
            <form id="formSearch" class="form-horizontal">
                <div class="form-group" style="margin-top:15px">

                    <label class="control-label col-sm-1" style="width: 30px">ID</label>
                    <div class="col-sm-1">
                        <input type="text" class="form-control" id="queryId">
                    </div>

                    <label class="control-label col-sm-1" style="width: 90px">IP地址</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="queryIp">
                    </div>

                    <label class="control-label col-sm-1" style="width: 90px">域名</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="queryHost">
                    </div>

                    <div class="col-sm-3" style="text-align:left;">
                        <button type="button" style="margin-left:50px" id="btn_query" class="btn btn-primary">查询
                        </button>
                        <button type="button" style="margin-left:50px" id="btn_reset" class="btn btn-default">重置
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div id="toolbar" class="btn-group">
        <button id="add" type="button" class="btn btn-success" data-toggle="modal" data-target="#addModal">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>新增
        </button>
    </div>

    <table id="taskOverviewTable"></table>

</div>

<script type="application/javascript">
    var table_options_task_overview = {
        url: '/host/queryHostsList',
        method: 'get',                      //请求方式（*）
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: false,                     //是否启用排序
        toolbar: '#toolbar',                 //工具按钮使用日期
        sortOrder: "asc",                   //排序方式
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        showColumns: false,                  //是否显示所有的列
        height: 700,
        pageNumber: 1,                       //初始化加载第一页，默认第一页
        pageSize: 10,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
        clickToSelect: true,                // 是否启用点击选中行
//        showExport: true,                     //是否显示导出
        showRefresh: true,
//        exportDataType: "all",              //basic', 'all', 'selected'.
//        silent: true,
        queryParams: queryParams, //参数
        columns: [
            {
            field: 'hId',
            title: 'ID',
            align: "center",
            width: "20",
            },
            {
                field: 'ip',
                title: 'ip',
                align: "center",
                width: "100"
            },
            {
                field: 'host',
                title: '域名',
                align: "center",
                width: "150"
            }, {
                field: 'description',
                title: '描述',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if (row.description == null) {
                        return " "
                    } else {
                        return row.description;
                    }
                }

            }, {
                field: '操作',
                title: '操作',
                align: "center",
                width: "90",
                formatter: function (value, row, index) {
                    var group_begin = '<div class="btn-group"> <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">更多 <span class="caret"></span> </button>' +
                            '<ul class="dropdown-menu">';

                    var a1 = '<li><a href="javascript:void(0);" onclick="showHostModifyContent(\'' + row.hId + '\' )">修改</a></li>';
//                    <li class="divider"></li>
                    var a2 = '<li><a href="javascript:void(0);" onclick="deleteHost( \'' + row.hId + '\' )">删除</a></li>';
                    var group_end = '</ul></div>';
                    return group_begin + a1 + a2 + group_end;
                }
            }],
        onLoadSuccess: function (data) {
//                $('#taskOverviewTable').val(data.beginTime+' 至 '+data.endTime);
        }


    };


    function openModal(data) {
        data = html_decode(data)
        $('.modal-body').html("");
        var contens = data.split("&");
        var strs = "";
        for (i = 0; i < contens.length; i++) {
            if (contens[i] != null && contens[i] != undefined && contens[i] != '') {
                strs += "<p>" + contens[i] + "  & " + "</p>";
            }
        }
        $('.modal-body').append(strs);
        $('#myModal').modal();
    }


    /**
     * 删除记录
     */
    function deleteHost(hId) {
        function del() {
            if (confirm("真的要删除该条记录吗?")) {
                return true;
            } else {
                return false;
            }
        }

        if (del()) {
            $.ajax({
                type: "get",
                url: "/host/deleteHosts",
                dataType: "json",
                data: {
                    id: hId
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("删除成功！");
                        $('#taskOverviewTable').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        result;
                    }
                },
                error: function () {
                    alert("删除异常");

                }
            });
        } else {
            return
        }
    }
    //    请求参数
    function queryParams(params) {
        return {
            limit: params.limit,
            offset: params.offset,
            queryId: $('#queryId').val(),
            queryIp: $('#queryIp').val(),
            queryHost: $('#queryHost').val(),
        };
    }

    //初始化table
    $('#taskOverviewTable').bootstrapTable(table_options_task_overview);


    /**
     * 清空表单内容
     * @param formID
     */
    function clearFormData(formID) {
        $("#" + formID)[0].reset();
        $("#" + formID).find("label").css("display", "none");
    }

    /**
     * 添加hosts
     */
    function insertHosts() {
        if (check()) {
            var da = $("#insertHost").serialize();
            $.ajax({
                type: "POST",
                url: "/host/addHosts",
                dataType: "json",
                data: $("#insertHost").serialize(),
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("添加成功！");
                        $('#addModal').modal('hide');
                        clearFormData("insertHost");
                        $('#taskOverviewTable').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        result;
                    }
                },
                error: function () {
                    alert("添加异常！");

                }
            });

        }

    }
    /**
     * 更新host
     */
    function updateHost() {
        if (modifyCheck()) {
            $.ajax({
                type: "POST",
                url: "/host/updateHosts",
                dataType: "json",
                data: $("#updateHost").serialize(),
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("修改成功！");
                        $('#modifyModal').modal('hide');
                        clearFormData("updateHost");
                        $('#taskOverviewTable').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        result;
                    }
                },
                error: function () {
                    alert("添加异常！");

                }
            });

        }

    }

    function showHostModifyContent(id) {

        $.ajax({
            type: "get",
            url: "/host/getHostById",
            dataType: "json",
            data: {
                id: id
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    clearFormData("updateHost");
                    $('#update_id').val(result.data.hId);
                    $('#update_ip').val(result.data.ip);
                    $('#update_host').val(result.data.host);
                    $('#update_description').val(result.data.description);
                    $('#modifyModal').modal('show');
                    return;
                } else {
                    alert(result.message);
                    result;
                }
            },
            error: function () {
                alert("添加异常！");

            }
        });

    }

    /**
     * 检查输入内容是否正确
     * @returns {boolean}
     */
    function check() {
        var reg_ip = /^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$/;
        var reg_host = /^\S(\S+\.)+\S+$/;

        if ($("#ip").val() == "" || $("#ip").val().length == 0) {
            $("#ipInfo").text("IP地址不能为空！");
            $("#ipInfo").css("display", "block");
            return false;
        } else if (!(reg_ip.test($("#ip").val()))) {
            $("#ipInfo").text("IP地址格式不正确，例：127.0.0.1");
            $("#ipInfo").css("display", "block");
            return false
        }
        else if ($("#host").val() == "" || $("#host").val().length == 0) {
            $("#ipInfo").css("display", "none");
            $("#hostInfo").text("IP地址不能为空！");
            $("#hostInfo").css("display", "block");
            return false;
        } else if (!(reg_host.test($("#host").val()))) {
            $("#ipInfo").css("display", "none");
            $("#hostInfo").text("域名格式不正，例：mall.autohome.com.cn");
            $("#hostInfo").css("display", "block");
            return false
        } else {
            $("#insertHost").find("label").css("display", "none");
            return true;
        }
    }


    function modifyCheck() {
        var reg_ip = /^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$/;
        var reg_host = /^\S(\S+\.)+\S+$/;

        if ($("#update_ip").val() == "" || $("#update_ip").val().length == 0) {
            $("#label_ipInfo").text("IP地址不能为空！");
            $("#label_ipInfo").css("display", "block");
            return false;
        } else if (!(reg_ip.test($("#update_ip").val()))) {
            $("#label_ipInfo").text("IP地址格式不正确，例：127.0.0.1");
            $("#label_ipInfo").css("display", "block");
            return false
        }
        else if ($("#update_host").val() == "" || $("#update_host").val().length == 0) {
            $("#label_ipInfo").css("display", "none");
            $("#label_hostInfo").text("IP地址不能为空！");
            $("#label_hostInfo").css("display", "block");
            return false;
        } else if (!(reg_host.test($("#update_host").val()))) {
            $("#label_ipInfo").css("display", "none");
            $("#label_hostInfo").text("域名格式不正，例：mall.autohome.com.cn");
            $("#label_hostInfo").css("display", "block");
            return false
        } else {
            $("#updateHost").find("label").css("display", "none");
            return true;
        }
    }

    /**
     * 设置点击事件
     */
    $(function () {
        $('#btn_reset').click(function () {
            $('#queryId').val('');
            $('#queryIp').val('');
            $('#queryHost').val('');
            $('#taskOverviewTable').bootstrapTable('refresh');
        });
        $('#btn_query').click(function () {
            var queryId = Number($('#queryId').val());
            if (isNaN(queryId)) {
                alert("ID 输入必须为数字！");
                return
            }
            $('#taskOverviewTable').bootstrapTable('destroy');
            $('#taskOverviewTable').bootstrapTable(table_options_task_overview);
        });
    });

</script>


</body>
</html>
