
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>自动化平台</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>自动化平台</title>
    <link rel="stylesheet" href="/css/mall.css">
    <link rel="stylesheet" href="/css/mall_style.css">
    <script src="/js/jquery.js"></script>
</head>
<body style="background-color:#f2f9fd;">
<div class="header bg-main">
    <div class="logo">
        <%--<h1><img style="padding-left: 10px" src="/images/header-logo-mall.png"  height="40" alt="" />自动化平台管理中心</h1>--%>
        <h1>TES 自动化平台管理中心</h1>
    </div>
</div>
<div class="leftnav">
    <div class="leftnav-title"><strong><span class="icon-list"></span>菜单列表</strong></div>
    <ul style="display:block">
        <li id="nav" value="none" ><span style="padding-left: 20px" target="right" class="icon-caret-right"></span>用例管理</li>
        <%--<li><a href="/main/addHttpCase" style="font-size: 12px" target="right">添加Http接口</a></li>--%>
        <%--<li><a href="/main/addRpcCase" style="font-size: 12px" target="right">添加RPC接口</a></li>--%>
        <li><a href="/main/searchCase" style="font-size: 12px" target="right">测试用例管理</a></li>
    </ul>
    <ul style="display:block">
        <li id="nav-debug" value="none" ><span style="padding-left: 20px" target="right" class="icon-caret-right"></span>接口调试</li>
        <li><a href="/main/executeHttpCase" style="font-size: 12px" target="right">Http接口调试</a></li>
        <%--<li><a href="/main/executeRpcCase" style="font-size: 12px" target="right">RPC接口调试</a></li>--%>
    </ul>
    <ul style="display:block">
        <%--<li><a href="adv.html" style="padding-left: 20px" target="right"><span class="icon-caret-right"></span>任务执行管理</a></li>--%>
        <li id="nav-task" value="none"><span style="padding-left: 20px" target="right" class="icon-caret-right"></span>项目管理
        </li>
        <li><a href="../task/taskManager" style="font-size: 12px" target="right">测试任务管理</a></li>
        <li><a href="/group/groupManager" style="font-size: 12px" target="right">测试场景管理</a></li>
    </ul>
    <ul style="display:block">
        <%--<li><a href="adv.html" style="padding-left: 20px" target="right"><span class="icon-caret-right"></span>任务执行管理</a></li>--%>
        <li id="nav-task" value="none" ><span style="padding-left: 20px" target="right" class="icon-caret-right"></span>报告查询</li>
        <li><a href="../report/reportList" style="font-size: 12px" target="right">报告列表</a></li>
        <%--<li><a href="groupManagerNew" style="font-size: 12px" target="right">Group管理</a></li>--%>
    </ul>
    <%--<ul style="display:block">--%>
        <%--<li><span style="padding-left: 20px" target="right" class="icon-caret-right"></span>UI用例管理</li>--%>
        <%--&lt;%&ndash;<li><span style="padding-left: 20px" target="right" class="icon-caret-right"></span>UI用例管理</li>&ndash;%&gt;--%>
        <%--<li><a href="pass.html" style="font-size: 12px" target="right">UI用例管理</a></li>--%>
    <%--</ul>--%>
    <ul style="display:block">
        <li><a href="/host/hostList" style="padding-left: 20px" target="right"><span class="icon-caret-right"></span>host地址管理</a>
        </li>
    </ul>

    <%--<ul style="display:block">--%>
    <%--<li><a href="/tuser/userManager" style="padding-left: 20px" target="right"><span--%>
    <%--class="icon-caret-right"></span>测试账号管理</a>--%>
    <%--</li>--%>
    <%--</ul>--%>


    <ul style="display:block">
        <li><a href="/variable/variableManager" style="padding-left: 20px" target="right"><span
                class="icon-caret-right"></span>自定义变量管理</a>
        </li>
    </ul>

    <ul style="display:block">
        <li id="nav-points" value="none"><span style="padding-left: 20px" target="right"
                                               class="icon-caret-right"></span>会员积分通知查看
        </li>

        <li><a href="/event/memberEventList" style="font-size: 12px" target="right">测试环境查看</a></li>
        <li><a href="/event/memberEventListProduct" style="font-size: 12px" target="right">线上环境查看</a></li>
    </ul>


</div>
<script type="text/javascript">
    $(function(){
        $(".leftnav ul li ").click(function(){
            $("#a_leader_txt").text($(this).text());
            $(".leftnav ul li a").removeClass("on");
            $(this).addClass("on");
        })
    });
</script>
<ul class="bread">
    <li><a href="#" target="right" class="icon-home" onclick="window.location.reload();"> 首页</a></li>
    <li><div id="a_leader_txt"></div></li>


</ul>

<div class="admin">
    <iframe scrolling="auto" rameborder="0" src="../report/summary" name="right" width="100%" height="100%">
    </iframe>
</div>

</body>
</html>
