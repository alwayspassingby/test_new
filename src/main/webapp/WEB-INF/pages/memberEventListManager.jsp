<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <title>会员积分事件列表</title>

    <script src="/js/jquery.js"></script>

    <script src="/bootstrap3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/bootstrap3/css/bootstrap.min.css">

    <script src="/bootstrap-table/dist/bootstrap-table.js"></script>
    <link rel="stylesheet" href="/bootstrap-table/dist/bootstrap-table.css">
    <script src="/bootstrap-table/dist/bootstrap-table-zh-CN.js"></script>

    <link rel="stylesheet" href="/css/c-json.css">
    <script src="/js/c-json.js"></script>
</head>
<body>
<!-- 模态框（Modal） -->
<div class="modal fade" id="memberContentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="font-size: 15px">
                    查看内容信息
                </h4>
            </div>
            <div class="modal-body" id="modal-body-report" style="word-wrap: break-word;">
                <div id="show-div3" style="height:400px;width: 100%;overflow-y: auto"></div>
                <div id="show-div4" style="height:0px;margin-left:10px;width: 100%"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="text-align: center">关闭
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<div class="panel-body" style="padding-bottom:0px;">

    <div class="panel panel-default">
        <div class="panel-heading">查询条件</div>
        <div class="panel-body">
            <form id="formMemberSearch" class="form-horizontal">
                <div class="form-group" style="margin-top:15px">

                    <label class="control-label col-sm-1" style="width: 90px">事件类型</label>
                    <div class="col-sm-3">
                        <select id="source" class="form-control">
                            <option value="" selected>全部</option>
                            <option value="PointsOfferMatchProgressUpdated">PointsOfferMatchProgressUpdated</option>
                            <option value="PointsOffered">PointsOffered</option>
                        </select>
                    </div>

                    <div class="col-sm-3" style="text-align:left;">
                        <button type="button" style="margin-left:50px" id="btn_query" class="btn btn-primary"
                                onclick="selectMenberEvent()">查询
                        </button>
                        <button type="button" style="margin-left:10px" id="btn_reset" class="btn btn-default" onclick="function clearSelect() {
                            // $('#source').val('');
                            $('#formMemberSearch')[0].reset();
                        }
                        clearSelect()">重置
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <table id="memberEventOverviewTable"></table>

</div>


<script type="application/javascript">
    var table_options_member_event_overview = {
        url: '/event/queryMemberEventPointNotificationList',
        method: 'get',                      //请求方式（*）
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: false,                     //是否启用排序
        // toolbar: '#toolbar',                 //工具按钮使用日期
        sortOrder: "asc",                   //排序方式
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        showColumns: false,                  //是否显示所有的列
        height: 550,
        pageNumber: 1,                       //初始化加载第一页，默认第一页
        pageSize: 10,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
        clickToSelect: true,                // 是否启用点击选中行
//        showExport: true,                     //是否显示导出
//         showRefresh: true,
//        exportDataType: "all",              //basic', 'all', 'selected'.
//        silent: true,
        queryParams: queryParams, //参数
        columns: [{
            field: 'id',
            title: 'ID',
            align: "center",
            width: "20",
        },
            {
                field: 'source',
                title: '事件',
                align: "center",
                width: "100"
            },
            {
                field: 'content',
                title: '通知内容',
                align: "center",
                width: "350",
                formatter: function (value, row, index) {
                    if (row.content == null || row.content == "" || row.content.length == 0
                        || row.content == undefined) {
                        row.content = "-";
                    } else {
                        return row.content.substr(0, 60) + ' ... <a href="javascript:void(0);" onclick="openContentModal(\'' + html_encode(row.content) + '\');">查看</a>';

                    }
                }
            },
            {
                field: 'createTime',
                title: '接收时间',
                align: "center",
                width: "180",
                formatter: function (value, row, index) {
                    if (row.createTime == null || row.createTime == "" || row.createTime.length == 0
                        || row.createTime == undefined) {
                        row.createTime = "-";
                    } else {
                        var timestamp3 = row.createTime;
                        var newDate = new Date();
                        newDate.setTime(timestamp3);
                        return newDate.toLocaleString();
                    }
                }
            }],
        onLoadSuccess: function (data) {
//                $('#taskOverviewTable').val(data.beginTime+' 至 '+data.endTime);
        }


    };

    //初始化table
    $('#memberEventOverviewTable').bootstrapTable(table_options_member_event_overview);

    function queryParams(params) {
        return {
            limit: params.limit,
            offset: params.offset,
            source: $('#source').val(),
        };
    }

    function selectMenberEvent() {
        $('#memberEventOverviewTable').bootstrapTable('refresh', table_options_member_event_overview);

    }


    function html_encode(str) {
        var s = "";
        if (str == "" || str.length == 0) {
            return "";
        }
        s = str.replace(/&/g, "&amp;");
        s = s.replace(/</g, "&lt;");
        s = s.replace(/>/g, "&gt;");
        s = s.replace(/\\/g, "&xg;");
        s = s.replace(/ /g, "&nbsp;");
        s = s.replace(/\'/g, "&#aabb;");
        s = s.replace(/\"/g, "&#ccdd;");
        s = s.replace(/\n/g, " ");
        return s;
    }

    function html_decode(str) {
        var s = "";
        if (str.length == 0) return "";
        s = str.replace(/&amp;/g, " & ");   //2
        s = s.replace(/&lt;/g, "<");
        s = s.replace(/&gt;/g, "> ");
        s = s.replace(/&nbsp;/g, " ");
        s = s.replace(/&#aabb;/g, "\'");
        s = s.replace(/&#ccdd;/g, "\"");
        s = s.replace(/<br>/g, " ");
        return s;
    }


    /**
     * 打开内容窗口
     * @param contentInfo
     */
    function openContentModal(contentInfo) {
        $("#modal-body-report").val("");
        $("#show-div4").val("");
        $("#show-div3").empty();
        // $('#modal-body-report').html("");
        contentInfo = html_decode(contentInfo);

        try {
            $('#show-div4').css("display", "block");
            $('#show-div3').css('display', 'block');
            var dataJson = JSON.stringify(contentInfo);
            var data = JSON.parse(dataJson);
            if (data == null || data == undefined || data.length == 0) {
                $("#show-div4").val("");
            }
            else {
                $("#show-div4").val(data);
            }
            Process();
        } catch (e) {
            $('#show-div4').css("display", "none");
            $('#show-div3').css('display', 'none');
            $("#modal-body-report").val("");
            contentInfo = contentInfo.split("/n");
            if (contentInfo == null || contentInfo == undefined || contentInfo.length == 0) {
                return;
                var data = "";
                for (var i = 0; i < contentInfo.length; i++) {
                    if (contentInfo[i] != null && contentInfo[i] != undefined && contentInfo[i] != '') {
                        data += '<p class="text-primary">' + contentInfo[i] + '</p>';
                    }
                }
            }
            $('#modal-body-report').append(data);
        }
        $('#memberContentModal').modal("show");
    }
</script>


</body>
</html>
