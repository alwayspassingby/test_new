
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="renderer" content="webkit">
    <title>修改用例</title>
    <link rel="stylesheet" href="/bootstrap3/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/tes_front.css">
    <script src="/js/jquery.js"></script>
    <script src="/bootstrap3/js/bootstrap.min.js"></script>
    <script src="/js/tes_front_case.js"></script>

    <%--<script src="/js/pintuer.js"></script>--%>
</head>

<script type="application/javascript">

    $(document).ready(setData());
//     $(document).ready(function () {
//         setData();
//             $.ajax({
//                 type: "get",
//                 url: "/main/getGroup",
//                 dataType: "json",
//                 data: {
//                 },
//                 success: function (data) {
//                     var dataJson = JSON.stringify(data);
//                     var result = JSON.parse(dataJson);
//                     var size = result.length;
//                     var count = 0;
//                     var obj1=document.all.groupId;
//                     obj1.options[0]=new Option("请选择",'0');
//                     for ( var i = 0 ;i< size ;i++){
//                         obj1.options[i+1]=new Option(result[i].groupName,result[i].groupId);
//                     }
//                     setData();
//                 },
//                 error: function () {
//     //                    alert("~~~~~~~~~~~~~~~~~");
//                 }
//             });
//         });

    function setData() {
        var caseId = "${caseId}";
        var isModify = "${isModify}";
        if (caseId == null || caseId == undefined || caseId == '') {
            $("body").empty();
            alert("获取用例ID异常，id = " + caseId)

        } else {
            $.ajax({
                type: "get",
                url: "/main/getAPICaseByID",
                dataType: "json",
                data: {
                    id: caseId
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        if (result.data == null || result.data == undefined || result.data == ''){
                            $("body").empty();
                            alert("caseID 不存在！caseID:" + caseId);
                        }else {
                            var caseDataJson = "";
                            caseDataJson = eval(result.data);
                            initPageContentFromCaseData(caseDataJson);
                            if (isModify == null || isModify.length == 0 || isModify == "true") {
                                $('#update_button').css("display", "block");
                            } else {
                                $('#update_button').css("display", "none");
                            }
                            return;
                        }

                    } else {
                        $("body").empty();
                        alert(result.message);
                        result;
                    }

                },
                error: function () {
                    $(".form-group").remove();
                    alert("获取用例异常 ID = " + caseId);

                }
            });
        }
    }

</script>

<body style="overflow-x:hidden">


<div class="panel panel-default">
    <div class="panel-heading"><strong><span class="glyphicon glyphicon-pencil"></span> 修改用例信息</strong></div>
    <div class="panel-body">
        <form method="POST" class="form-horizontal" role="form" action="#" id="updateHttpCase">
            <%--<div class="form-group">--%>
            <%--<div class="label">--%>
            <%--<label>用例所属组：</label>--%>
            <%--</div>--%>
            <%--<div class="field">--%>
            <%--<select type="text" class="input w65" id="groupId" name="groupId"--%>
            <%--placeholder="请选择接口所属组：">--%>
            <%--<OPTION selected></OPTION>--%>
            <%--</select>--%>
            <%--<div class="input-help" style="color:red" >--%>
            <%--<li>*</li>--%>
            <%--</div>--%>
            <%--</div>--%>
            <%--</div>--%>


            <div class="form-group" style="margin-top: 20px">
                <label class="control-label col-lg-1">APICaseID:</label>
                <div class="col-lg-6 col-md-6">
                    <input class="form-control" id="caseId" name="caseId" type="text " value="0" disabled>
                </div>
            </div>

            <div class="form-group" id="URLGroup">
                <label class="control-label col-lg-1">URL:</label>
                <div class="col-lg-6 col-md-6">
                    <input class="form-control" id="httpURL" name="httpURL" type="text"
                           placeholder="请输入接口请求域名" value="">
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-6 col-md-6">
                    <input class="form-control" id="type" name="type" type="hidden" value="0">
                </div>
            </div>

            <input type="hidden" id="aId" name="aId" value="0">


            <div class="form-group" id="httpRequestTypeGroup">
                <label class="control-label col-lg-1">请求方式:</label>
                <div class="col-lg-6 col-md-6">
                    <select name="httpRequestType" id="httpRequestType" class="form-control">
                        <option value="">请选择请求方式</option>
                        <option value="0">GET</option>
                        <option value="1">POST</option>
                    </select>
                </div>
            </div>

            <div>
                <div class="form-group" id="HttpHeadersGroup">
                    <label class="control-label col-lg-1">HttpHeaders:</label>
                    <div class="col-lg-5 col-md-5">
                        <button class="btn btn-primary btn-mini" onclick="addInputs('httpHeaders')" type="button"><span
                                class="glyphicon glyphicon-plus"></span></button>
                        <span class="glyphicon glyphicon-question-sign"
                              style="padding-left: 20px;color: #2e6da4"></span>
                    </div>
                </div>
                <div class="form-group" id="httpHeaders">
                </div>
            </div>


            <div>
                <div class="form-group" id="CookiesGroup">
                    <label class="control-label col-lg-1">Cookies:</label>
                    <div class="col-lg-6 col-md-6">
                        <button class="btn btn-primary btn-mini" onclick="addInputs('httpCookies')" type="button"><span
                                class="glyphicon glyphicon-plus"></span></button>
                    </div>
                </div>
                <div class="form-group" id="httpCookies">
                </div>
            </div>


            <%--调用的服务名称  --%>
            <div class="form-group" id="rpcServiceGroup">
                <label class="control-label col-lg-1">RPC接口服务:</label>
                <div class="col-lg-6 col-md-6">
                    <input class="form-control" id="rpcService" name="rpcService" type="text">
                </div>
            </div>

            <%--调用的服务方法  --%>
            <div class="form-group" id="rpcMethodGroup">
                <label class="control-label col-lg-1">RPC接口方法:</label>
                <div class="col-lg-6 col-md-6">
                    <input class="form-control" id="rpcMethod" name="rpcMethod" type="text">
                </div>
            </div>

            <%--调用的服务方法版本号 --%>
            <div class="form-group" id="rpcVersionGroup">
                <label class="control-label col-lg-1">RPC接口版本:</label>
                <div class="col-lg-6 col-md-6">
                    <input class="form-control" id="rpcVersion" name="rpcVersion" type="text">
                </div>
            </div>

            <%--调用的服务接口的分组 --%>
            <div class="form-group" style="margin-top: 20px" id="rpcGroup_Group">
                <label class="control-label col-lg-1">RPC接口分组:</label>
                <div class="col-lg-6 col-md-6">
                    <input class="form-control" id="rpcGroup" name="rpcGroup" type="text">
                </div>
            </div>

            <div class="form-group" id="parameterTypeGroup">
                <label class="control-label col-lg-1">参数类型:</label>
                <div class="col-lg-6 col-md-6">
                    <select name="parameterType" id="parameterType" class="form-control"
                            onchange="selectParameterChange(this,'parameter')">
                        <option value="1" selected>form类型</option>
                        <option value="2">raw-json类型</option>
                        <option value="3">raw-text类型</option>
                    </select>
                </div>
            </div>

            <div>
                <%--请求参数--%>
                <div class="form-group" id="parameterGroup">
                    <label class="control-label col-lg-1">请求参数:</label>
                    <div class="col-lg-6 col-md-6">
                        <button id="addParameterInput" class="btn btn-primary btn-mini" onclick="addInputs('parameter')"
                                type="button"><span
                                class="glyphicon glyphicon-plus"></span></button>
                    </div>
                </div>

                <div class="form-group" id="parameter">

                </div>


                <%--请求说明 --%>
                <div class="form-group">
                    <label class="control-label col-lg-1">请求说明:</label>
                    <div class="col-lg-6 col-md-6">
                        <input type="text" class="form-control" id="description" name="description" value=""/>
                    </div>
                </div>

                <%--断言类型--%>
                <div class="form-group">
                    <label class="control-label col-lg-1">检查结果类型:</label>
                    <div class="col-lg-6 col-md-6">
                        <select type="text" class="form-control" id="expectedType" name="expectedType"
                                onchange="selectChange(this)">
                            <option value="">请选择结果检查类型：</option>
                            <option value="1">json内容断言</option>
                            <option value="3">文本内容断言</option>
                        </select>
                    </div>
                </div>

                <%--断言表达式--%>
                <div class="form-group">
                    <label class="control-label col-lg-1">表达式:</label>
                    <div class="col-lg-6 col-md-6">
                        <select type="text" class="form-control" id="expression" name="expression">
                            <option value="">请选择表达式：</option>
                            <option value="0">isNotNull</option>
                            <option value="1">isNull</option>
                            <option value="2">isEqualTo</option>
                            <option value="3">isNotEqualTo</option>
                            <option value="4">contains</option>
                            <option value="5">doesNotContain</option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-lg-1">检查点:</label>
                    <div class="col-lg-6 col-md-6">
                        <input type="text" class="form-control" id="expectedKey" name="expectedKey" value=""/>
                    </div>

                </div>

                <%--表达式的Value--%>
                <div class="form-group">
                    <label class="control-label col-lg-1">预期结果:</label>
                    <div class="col-lg-6 col-md-6">
                        <input type="text" class="form-control" id="expectedValue" name="expectedValue" value=""/>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-1 col-sm-10">
                        <button type="button" class="btn btn-primary" id="update_button"
                                onclick="updateTestCase('updateHttpCase')">更新
                        </button>
                        <%--<button type="button" class="btn btn-default" id="back_button" onclick="window.history.back();">返回</button>--%>
                    </div>

                </div>

        </form>
    </div>
</div>


</body>
</html>
