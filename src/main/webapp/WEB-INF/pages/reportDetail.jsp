
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <title>报告列表</title>

    <script src="/js/jquery.js"></script>
    <%--<script src="/js/highcharts.js"></script>--%>

    <script src="/bootstrap3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/bootstrap3/css/bootstrap.min.css">

    <script src="/bootstrap-table/dist/bootstrap-table.js"></script>
    <link rel="stylesheet" href="/bootstrap-table/dist/bootstrap-table.css">
    <link rel="stylesheet" href="/css/tes_front_report_plan.css">
    <script src="/bootstrap-table/dist/bootstrap-table-zh-CN.js"></script>

</head>
<body>
<!-- 模态框（Modal） -->
<div class="modal fade" id="myModalReport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="font-size: 15px">
                    查看用例消息
                </h4>
            </div>
            <div class="modal-body" id="modal-body-report" style="word-wrap: break-word;">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="text-align: center">关闭
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>


<div class="modal fade" id="feedback" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                <h4 class="modal-title" id="myModalLabel">反馈失败原因</h4>
            </div>
            <form id="insertHost" action="#" method="post">
                <input name="taskId" id="taskId" type="hidden">
                <input name="version" id="version" type="hidden">
                <input name="caseId" id="caseId" type="hidden">
                <input name="feedbackId" id="feedbackId" type="hidden">
                <div class="modal-body">
                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <textarea name="feedbackContent" id="feedbackContent" type="text" style="margin: 0px; width: 564px; height: 150px;"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            onclick="clearFormData('feedback')">关闭
                    </button>

                    <button id="btn_add" type="button" class="btn btn-primary" onclick="addFeedback()">确定</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="panel-body" style="padding-left:15%;padding-bottom:20px;">

    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="progress blue">
		                        <span class="progress-left">
		                            <span class="progress-bar"></span>
		                        </span>
                    <span class="progress-right">
		                            <span class="progress-bar"></span>
		                        </span>
                    <div class="progress-value" id="total">-</div>
                </div>
                <div style="font-size: 20px;color: #049dff;padding-top:20px;text-align:center">总共</div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="progress green">
                            <span class="progress-left">
                                <span class="progress-bar"></span>
                            </span>
                    <span class="progress-right">
                                <span class="progress-bar"></span>
                            </span>
                    <div class="progress-value" id="success">-</div>
                </div>
                <div style="font-size: 20px;color: #1abc9c;padding-top:20px;text-align:center">成功</div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="progress pink">
		                        <span class="progress-left">
		                            <span class="progress-bar"></span>
		                        </span>
                    <span class="progress-right">
		                            <span class="progress-bar"></span>
		                        </span>
                    <div class="progress-value" id="fial">-</div>
                </div>
                <div style="font-size: 20px;color: #ed687c;padding-top:20px;text-align:center">失败</div>
            </div>

        </div>
    </div>

</div>

    <div class="panel panel-default">
        <div class="panel-heading">查询条件</div>
        <div class="panel-body">
            <form id="formSearch" class="form-horizontal">
                <div class="form-group" style="margin-top:15px">

                    <label class="control-label col-sm-1" style="width: 90px">断言结果</label>
                    <div class="col-sm-2">
                        <select id="isSuccess" class="form-control">
                            <option value="">全部</option>
                            <option value="0">成功</option>
                            <option value="1">失败</option>
                        </select>
                    </div>

                    <div class="col-sm-3" style="text-align:left;">
                        <button type="button" style="margin-left:50px" id="btn_query" class="btn btn-primary" onclick="function selectDetail() {
                            $('#reportDetailOverviewTable').bootstrapTable('refresh',table_options_report_detail_overview);
                        }
                        selectDetail()">查询
                        </button>
                        <button type="button" style="margin-left:50px" id="btn_reset" class="btn btn-default" onclick="function clearIsSussess() {
                            $('#isSuccess').val('');
                        }
                        clearIsSussess()">重置
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <table id="reportDetailOverviewTable"></table>
        <%--<input type="hidden" id="taskId" name="type" value=""/>--%>

</div>


<script type="application/javascript">
    function getNum() {
        $.ajax({
            type: "Get",
            url: "/report/getChartData",
            dataType: "json",
            data: {
                taskId:${taskId},
                version:${version}
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                var successNum = 0;
                var failNum = 0;
                if (data != null) {
                    successNum = result.successNum;
                    failNum = result.failNum;

                    $("#total").text(successNum+failNum);
                    $("#success").text(successNum);
                    $("#fial").text(failNum);
                } else {
                    $("#total").text(0);
                    $("#success").text(0);
                    $("#fial").text(0);;
                }
            },
            error: function () {
                alert("异常！");

            }
        });
    }
    $(document).ready(function() {
        getNum();

    });

    var table_options_report_detail_overview = {
        url: '/report/queryReportDetail',
        method: 'get',                      //请求方式（*）
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: false,                     //是否启用排序
        toolbar: '#toolbar',                 //工具按钮使用日期
        sortOrder: "asc",                   //排序方式
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        showColumns: false,                  //是否显示所有的列
        height: 1400,
        pageNumber: 1,                       //初始化加载第一页，默认第一页
        pageSize: 20,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
        clickToSelect: true,                // 是否启用点击选中行
//        showExport: true,                     //是否显示导出
        showRefresh: true,
//        exportDataType: "all",              //basic', 'all', 'selected'.
//        silent: true,
        queryParams: queryParams, //参数
        columns: [{
            field: 'id',
            title: 'ID',
            align: "center",
            width: "20",
        },
//            {
//            field: 'taskId',
//            title: '任务ID',
//            align: "center",
//            width: "20",
//        },
//            {
//                field: 'version',
//                title: '执行版本号',
//                align: "center",
//                width: "100"
//            },
            {
                field: 'caseId',
                title: '用例ID',
                align: "center",
                width: "100",
                formatter:function (value,row,index) {
                    if(row.caseId == null || row.caseId == "" || row.caseId.length == 0
                            || row.caseId == undefined){
                        row.caseId = "-";
                    }else {
                        return row.caseId;
                    }
                }
            },
            {
                field: 'caseInfo',
                title: '用例信息',
                align: "center",
                width: "350",
                formatter: function (value, row, index) {
                    var s = row.caseInfo;
                    if(row.caseInfo == null || row.caseInfo == "" || row.caseInfo.length == 0
                            || row.caseInfo == undefined){
                        row.caseInfo = "-";
                    }else {
                        return row.caseInfo.substr(0, 60) + ' ... <a href="javascript:void(0);" onclick="openModalReport(\'' + html_encode(row.caseInfo) + '\');">查看</a>';

                    }
                }
            },
            {
                field: 'caseResponse',
                title: '用例响应结果',
                align: "center",
                width: "150",
                formatter: function (value, row, index) {
                    if(row.caseResponse == null || row.caseResponse == "" || row.caseResponse.length == 0
                            || row.caseResponse == undefined){
                        row.caseResponse = "-";
                    }else {
                        return html_encode_new(row.caseResponse).substr(0, 60) + ' ... <a href="javascript:void(0);" onclick="openModalReport(\'' + html_encode(row.caseResponse) + '\');">查看</a>';

                    }
                }
            },
            {
                field: 'isSuccess',
                title: '断言',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if(row.isSuccess == null
                            || row.isSuccess == undefined){
                        row.isSuccess = "-";
                    }else {
                        if(row.isSuccess == 0){
                            return "成功";
                        }else if(row.isSuccess == 1){
                            return "失败";
                        }

                    }
                }
            },
            {
                field: 'failInfo',
                title: '接口失败原因',
                align: "center",
                width: "150",
                formatter: function (value, row, index) {
                    if(row.failInfo == null || row.failInfo == ''){
                        return "-";
                    }else {
                        if(row.failInfo.length < 60){
                            return row.failInfo;
                        }else {
                            return row.failInfo.substr(0, 60) + ' ... <a href="javascript:void(0);" onclick="openModalReport(\'' + html_encode(row.failInfo) + '\');">查看</a>';
                        }
                    }
                }
            },
            {
                field: 'failCause',
                title: '反馈失败原因',
                align: "center",
                width: "150",
                formatter: function (value, row, index) {
                    if(row.failCause == null || row.failCause == "" || row.failCause.length == 0
                            || row.failCause == undefined){
                        row.failCause = "-";
                    }else {
                        return row.failCause;
                    }
                }
            },
            {
                field: 'executeStartTime',
                title: '执行开始时间',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if(row.executeStartTime == null || row.executeStartTime == "" || row.executeStartTime.length == 0
                            || row.executeStartTime == undefined){
                        row.executeStartTime = "-";
                        return row.executeStartTime;
                    }
                    else {
                        var timestamp3 = row.executeStartTime;
                        var newDate = new Date();
                        newDate.setTime(timestamp3);
                        return newDate.toLocaleString();
                    }
                }
            },
            {
                field: 'executeEndTime',
                title: '执行开始时间',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if(row.executeEndTime == null || row.executeEndTime == "" || row.executeEndTime.length == 0
                            || row.executeEndTime == undefined){
                        row.executeEndTime = "-";
                        return row.executeEndTime;
                    }
                    else {
                        var timestamp3 = row.executeEndTime;
                        var newDate = new Date();
                        newDate.setTime(timestamp3);
                        return newDate.toLocaleString();
                    }
                }
            },
            {
                field: '操作',
                title: '操作',
                align: "center",
                width: "90",
                formatter: function (value, row, index) {
                    var run = '<div class="btn-group"><li style="display:inline;margin-right: 6px" class="btn btn-info dropdown-toggle" ><a style="color: #fff" href="javascript:void(0);" onclick="feedback( \'' + row.id + '\' )">反馈<span class="glyphicon glyphicon-triangle-right"></a></li></div>';
                    var debug = '<div class="btn-group"><li style="display:inline;margin-right: 6px" class="btn btn-info dropdown-toggle" ><a style="color: #fff" href="/main/executeCase?id=' + row.caseId + '">调试<span class="glyphicon glyphicon-triangle-right"></a></li></div>';

                    if(row.isSuccess == 0){
                        return "-";
                    }else if(row.isSuccess == 1){
                        return run + debug;
                    }
                }
            }],
        onLoadSuccess: function (data) {
//                $('#taskOverviewTable').val(data.beginTime+' 至 '+data.endTime);
        }


    };


    //初始化table
    $('#reportDetailOverviewTable').bootstrapTable(table_options_report_detail_overview);
    function feedback(id) {
        $('#feedbackId').val(id);
        $('#feedback').modal();
    }
    function queryParams(params) {
        return {
            limit: params.limit,
            offset: params.offset,
            taskId: ${taskId},
            version:${version},
//            taskPerson: $('#taskPerson').val(),
            isSuccess: $('#isSuccess').val(),
        };
    }

    function openModalReport(caseInfo) {
        caseInfo = html_decode(caseInfo)
        $('#modal-body-report').html("");
        if(caseInfo == null || caseInfo == undefined || caseInfo == ""){
            return;
        }
        var infos = caseInfo.split("/n");
        if(infos == null || infos == undefined || infos.length == 0){
            return;
        }
        var data = "";
        for (var i = 0; i < infos.length; i++) {
            if (infos[i] != null && infos[i] != undefined && infos[i] != '') {
                data += '<p class="text-primary">' + infos[i]  + '</p>';
            }
        }
        $('#modal-body-report').append(data);
        $('#myModalReport').modal("show");
    }
    function html_decode(str) {
        var s = "";
        if (str.length == 0) return "";
        s = str.replace(/&amp;/g, " & ");   //2
        s = s.replace(/&lt;/g, "<");
        s = s.replace(/&gt;/g, "> ");
        s = s.replace(/&nbsp;/g, " ");
        s = s.replace(/&#aabb;/g, "\'");
        s = s.replace(/&#ccdd;/g, "\"");
        s = s.replace(/<br>/g, " ");
        return s;
    }


    function html_encode(str) {

        var s = "";
        if (str == "" || str.length == 0) {
            return "";
        }
        s = str.replace(/title/g, " ");
        s = s.replace(/html/g, " ");
        s = s.replace(/head/g, " ");
        s = s.replace(/&/g, "&amp;");
        s = s.replace(/</g, "&lt;");
        s = s.replace(/>/g, "&gt;");
        s = s.replace(/ /g, "&nbsp;");
        s = s.replace(/\'/g, "&#aabb;");
        s = s.replace(/\"/g, "&#ccdd;");
        s = s.replace(/\n/g, " /n ");

        return s;
    }

     function html_encode_new(str) {

         var s = "";
         if (str == "" || str.length == 0) {
             return "";
         }
         s = str.replace(/title/g, " ");
         s = s.replace(/html/g, " ");
         s = s.replace(/head/g, " ");

         return s;
     }

    function addFeedback() {
        var feedbackId =$('#feedbackId').val();
        $.ajax({
            type: "Get",
            url: "/report/addFeedback",
            dataType: "json",
            data: {
                feedbackId:feedbackId,
                feedbackContent:$('#feedbackContent').val()
            },
            success: function (data) {
//                var dataJson = JSON.stringify(data);
//                var result = JSON.parse(dataJson);
                if (data == 1) {
                    alert("反馈成功！");
                    $('#feedback').hide();
                    $('#reportDetailOverviewTable').bootstrapTable('refresh',table_options_report_detail_overview);
                    return;
                } else {
                    alert("反馈异常");
                    result;
                }
            },
            error: function () {
                alert("添加异常！");

            }
        });
    }


</script>

</body>
</html>
