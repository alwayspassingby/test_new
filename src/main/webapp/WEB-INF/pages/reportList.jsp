
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <title>报告列表</title>

    <script src="/js/jquery.js"></script>

    <script src="/bootstrap3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/bootstrap3/css/bootstrap.min.css">

    <script src="/bootstrap-table/dist/bootstrap-table.js"></script>
    <link rel="stylesheet" href="/bootstrap-table/dist/bootstrap-table.css">
    <script src="/bootstrap-table/dist/bootstrap-table-zh-CN.js"></script>

</head>
<body>


<div class="panel-body" style="padding-bottom:0px;">

    <div class="panel panel-default">
        <div class="panel-heading">查询条件</div>
        <div class="panel-body">
            <form id="formSearch" class="form-horizontal">
                <div class="form-group" style="margin-top:15px">

                    <label class="control-label col-sm-1" style="width: 90px">任务ID</label>
                    <div class="col-sm-1">
                        <input type="text" class="form-control" id="taskId">
                    </div>

                    <label class="control-label col-sm-1" style="width: 90px">执行状态</label>
                    <div class="col-sm-2">
                        <select id="status" class="form-control">
                            <option value="">全部</option>
                            <option value="0">未执行</option>
                            <option value="1">执行中</option>
                            <option value="2">执行完成</option>
                        </select>
                    </div>

                    <label class="control-label col-sm-1" style="width: 90px">负责人</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="taskPerson">
                    </div>

                    <div class="col-sm-3" style="text-align:left;">
                        <button type="button" style="margin-left:50px" id="btn_query" class="btn btn-primary" onclick="selectReport()">查询
                        </button>
                        <button type="button" style="margin-left:10px" id="btn_reset" class="btn btn-default" onclick="function clearSelect() {
                            $('#taskId').val('');
                            $('#status').val('');
                            $('#taskPerson').val('');
                        }
                        clearSelect()">重置
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>



    <table id="reportOverviewTable"></table>

</div>


<script type="application/javascript">
    var table_options_report_overview = {
        url: '/report/queryReportList',
        method: 'get',                      //请求方式（*）
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: false,                     //是否启用排序
        toolbar: '#toolbar',                 //工具按钮使用日期
        sortOrder: "asc",                   //排序方式
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        showColumns: false,                  //是否显示所有的列
        height: 700,
        pageNumber: 1,                       //初始化加载第一页，默认第一页
        pageSize: 10,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
        clickToSelect: true,                // 是否启用点击选中行
//        showExport: true,                     //是否显示导出
        showRefresh: true,
//        exportDataType: "all",              //basic', 'all', 'selected'.
//        silent: true,
        queryParams: queryParams, //参数
        columns: [{
            field: 'taskId',
            title: '任务ID',
            align: "center",
            width: "20",
        },
            {
                field: 'version',
                title: '执行版本号',
                align: "center",
                width: "100"
            },
            {
                field: 'excuteTime',
                title: '执行时间',
                align: "center",
                width: "180",
                formatter:function (value,row,index) {
                    if(row.excuteTime == null || row.excuteTime == "" || row.excuteTime.length == 0
                            || row.excuteTime == undefined){
                        row.excuteTime = "-";
                    }else {
                        var timestamp3 = row.excuteTime;
                        var newDate = new Date();
                        newDate.setTime(timestamp3);
                        return newDate.toLocaleString();
                    }
                }
            },
            {
                field: 'totalCase',
                title: '任务用例数',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if(row.totalCase == null || row.totalCase == "" || row.totalCase.length == 0
                            || row.totalCase == undefined){
                        row.totalCase = "-";
                    }else {
                        return row.totalCase;
                    }
                }
            },
            {
                field: 'successCase',
                title: '成功用例数',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if(row.successCase == null || row.successCase == "" || row.successCase.length == 0
                            || row.successCase == undefined){
                        row.successCase = "-";
                    }else {
                        return row.successCase;
                    }
                }
            },
            {
                field: 'failCase',
                title: '失败用例数',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if(row.failCase == null || row.failCase == "" || row.failCase.length == 0
                            || row.failCase == undefined){
                        row.failCase = "-";
                    }else {
                        return row.failCase;
                    }
                }
            },
            {
                field: 'status',
                title: '执行状态',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if (row.status == 0){
                        return "未执行";
                    }else if(row.status == 1){
                        return "执行中";
                    }else if(row.status == 2){
                        return "执行完成";
                    }else {
                        return "--";
                    }
                }
            },
            {
                field: 'taskPerson',
                title: '负责人',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if(row.taskPerson == null || row.taskPerson == "" || row.taskPerson.length == 0
                            || row.taskPerson == undefined){
                        row.taskPerson = "-";
                    }
                    if (row.taskPerson.length > 30) {
                        return row.taskPerson.substr(0, 30)+"...";
                    } else {
                        return row.taskPerson
                    }
                }
            },
            {
                field: '操作',
                title: '操作',
                align: "center",
                width: "90",
                formatter: function (value, row, index) {
                    var run = '<div class="btn-group"><li style="display:inline;margin-right: 6px" class="btn btn-info dropdown-toggle" ><a style="color: #fff" href="javascript:void(0);" onclick="reportDetail( \'' + row.taskId + '\' , \'' + row.version + '\' )">报告详情<span class="glyphicon glyphicon-triangle-right"></a></li></div>';
//                    var group_begin = '<div class="btn-group"> <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">更多 <span class="caret"></span> </button>' +
//                            '<ul class="dropdown-menu">';
//                    var group_end = '</ul></div>';
                    return run;
//                    return run + group_begin + a1 + a2 + a3 + a4 + a5 +group_end;
                }
            }],
        onLoadSuccess: function (data) {
//                $('#taskOverviewTable').val(data.beginTime+' 至 '+data.endTime);
        }


    };

    //初始化table
    $('#reportOverviewTable').bootstrapTable(table_options_report_overview);
    function reportDetail(taskId,version) {
        window.location.href="/report/reportDetail?taskId="+taskId+"&version="+version;

    }
    function queryParams(params) {
        return {
            limit: params.limit,
            offset: params.offset,
            taskId: $('#taskId').val(),
            taskPerson: $('#taskPerson').val(),
            status: $('#status').val(),
        };
    }

    function selectReport() {
        $('#reportOverviewTable').bootstrapTable('refresh',table_options_report_overview);

    }


</script>


</body>
</html>
