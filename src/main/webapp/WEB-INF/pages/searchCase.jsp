
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <%--<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>--%>
    <meta name="viewport" content="width=device-width"/>
    <title>查询接口用例</title>

    <script src="/js/jquery.js"></script>
    <script src="/bootstrap3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/bootstrap3/css/bootstrap.min.css">

    <script src="/bootstrap-table/dist/bootstrap-table.js"></script>
    <link rel="stylesheet" href="/bootstrap-table/dist/bootstrap-table.css">
    <script src="/bootstrap-table/dist/bootstrap-table-zh-CN.js"></script>
    <script src="/js/tes_front_case.js"></script>
    <script src="/js/tes_front_variable.js"></script>

    <script type="application/javascript">
        var _caseId = 0;
    </script>

        <style>
            .W250 .th-inner {
                width: 250px !important;
            }

            .W200 .th-inner {
                width: 100px !important;
            }

            .popover {
                word-break: break-all;
            }

            .tooltip-inner {
                word-break: break-all;
                max-width: 300px;
                font-size: 12px;
                /*color: #151515;*/
                /*background-color: #7B7B7B;*/
            }
        </style>

</head>

<body style="overflow-x:hidden">

<div class="modal fade" id="modalParameter" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="font-size: 15px">
                    查看用例参数
                </h4>
            </div>
            <div class="modal-body" id="modal-body-parameter" style="word-wrap: break-word;">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="text-align: center">关闭
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<div class="modal fade" id="CaseVeriableListModal" tabindex="-1" role="dialog" data-backdrop="false"
     data-keyboard="false"
     aria-labelledby="CaseVeriableListModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 1000px">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">查看用例的变量列表</h4>
            </div>

            <div id="toolbar2" class="btn-group" style="display: block">

                <button id="addCaseVeriable" class="btn btn-success btn-mini" onclick="showAddVariableModal()"
                        type="button"><span
                        class="glyphicon glyphicon-plus"></span></button>
            </div>

            <div class="panel-body" style="padding-bottom:0px;">
                <table id="OverviewTableCaseVariableList"></table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"
                        onclick="clearFormData('CaseVeriableListModal')">关闭
                </button>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="variableModal" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false"
     aria-labelledby="variableModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                <h4 class="modal-title" id="variableModalLabel"></h4>
            </div>
            <form id="variableForm" action="#" method="post">

                <input name="addVariable_caseId" id="addVariable_caseId" type="hidden">
                <input name="addVariable_vId" id="addVariable_vId" type="hidden">

                <div class="modal-body">
                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">名 称：
                            </button>
                        </div>
                        <input name="add_vname" id="add_vname" type="text" class="form-control"
                               style="border-radius: 4px">
                        <label id="vnameInfo" style="display: block;color: red;font-size: 10px"></label>
                    </div>
                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">表达式：
                            </button>
                        </div>
                        <input name="add_vkey" id="add_vkey" type="text" class="form-control"
                               style="border-radius: 4px">
                        <label id="vkeyInfo" style="display: block;color: red;font-size: 10px"></label>
                    </div>

                    <%--<div class="input-group" style="margin-right: 50px;margin-bottom: 20px">--%>
                    <%--<div class="input-group-btn">--%>
                    <%--<button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"--%>
                    <%--style="border: none">描述：--%>
                    <%--</button>--%>
                    <%--</div>--%>
                    <%--<input name="add_description" id="add_description" type="text" class="form-control"--%>
                    <%--style="border-radius: 4px">--%>
                    <%--</div>--%>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            onclick="clearFormData('variableForm')"
                            style="float: right;margin-left: 10px;margin-right: 20px">关闭
                    </button>

                    <button id="btn_variable_add" style="float: right;display: none" type="button"
                            class="btn btn-primary"
                            onclick="insertVariable(_caseId)">添加
                    </button>
                    <button id="btn_variable_modify" style="float:right" type="button" class="btn btn-primary"
                            onclick="updateVariable()">修改
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>


<div class="panel-body" style="padding-bottom:0px;">

    <div class="panel panel-default">
        <div class="panel-heading">查询条件</div>
        <div class="panel-body">
            <form id="GreoupSearch" class="form-horizontal">
                <div class="form-group" style="margin-top:15px">

                    <label class="control-label col-sm-1" style="width: 100px">ID: </label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="caseId">
                    </div>

                    <label class="control-label col-sm-1" style="width: 100px">类型: </label>
                    <div class="col-sm-2">
                        <select type="text" class="form-control" id="typeId">
                            <option value="2">全部</option>
                            <option value="0">http</option>
                            <option value="1">dubbo</option>
                        </select>
                    </div>

                    <label class="control-label col-sm-1" style="width: 100px">用例名称: </label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="descId">
                    </div>

                    <label class="control-label col-sm-1" style="width: 100px">url: </label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="urlId">
                    </div>
                </div>

                <%--<div class="form-group" style="margin-top:15px">--%>

                <%--<label class="control-label col-sm-1" style="width: 100px">RPC服务: </label>--%>
                <%--<div class="col-sm-2">--%>
                <%--<input type="text" class="form-control" id="serviceId">--%>
                <%--</div>--%>

                <%--<label class="control-label col-sm-1" style="width: 100px">RPC方法: </label>--%>
                <%--<div class="col-sm-2">--%>
                <%--<input type="text" class="form-control" id="rpcMethodId">--%>
                <%--</div>--%>

                <%--<label class="control-label col-sm-1" style="width: 100px">url: </label>--%>
                <%--<div class="col-sm-2">--%>
                <%--<input type="text" class="form-control" id="urlId">--%>
                <%--</div>--%>

                <%--<div class="col-sm-3 col-sm-offset-1">--%>
                <%--<button type="button" id="search" class="btn btn-primary">查询--%>
                <%--</button>--%>
                <%--<button type="button" style="margin-left:50px" id="reset" class="btn btn-default">重置--%>
                <%--</button>--%>
                <%--</div>--%>

                <%--</div>--%>

                <div class="form-group" style="margin-top:15px">
                    <div class="col-sm-3 col-sm-offset-1">
                        <button type="button" id="search" class="btn btn-primary">查询
                        </button>
                        <button type="button" style="margin-left:50px" id="reset" class="btn btn-default">重置
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="addCasetoolbar" class="btn-group">
        <button id="add" type="button" class="btn btn-success" onclick="function openAddCasePage(){
            window.location.href = '/main/executeCase';
        }openAddCasePage()">
            <span class="glyphicon glyphicon-plus"></span>新增
        </button>
    </div>
    <table id="taskOverviewTable"></table>

</div>


<script type="application/javascript">

    var table_options_task_overview = {
        url: '/main/queryAPICaseList',
        method: 'get',                      //请求方式（*）
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: false,                     //是否启用排序
        toolbar: '#addCasetoolbar',
        sortOrder: "asc",                   //排序方式
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
//        showColumns: true,
        pageNumber: 1,                       //初始化加载第一页，默认第一页
        pageSize: 10,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
        clickToSelect: true,
        height: 660,
//        showExport: true,                     //是否显示导出
//         showRefresh: true,
//        exportDataType: "all",              //basic', 'all', 'selected'.
        silent: true,
        queryParams: queryParams, //参数
        columns: [{
            field: 'aId',
            title: 'ID',
            align: "center",
            width: "20",
        }, {
            // class: 'W150',
            field: '',
            title: '用例名称',
            align: "left",
            width: "420",
            formatter: function (value, row, index) {
                if (row.description == null || row.description == "" || row.description.length == 0
                    || row.description == undefined) {
                    row.description = " ";
                }
                if (row.description.length > 50) {
                    var text = row.description.substr(0, 50) + "..";
                    return '<span data-toggle="tooltip" title="' + row.description + '"> ' + text + ' </span>';
                } else {
                    return row.description;
                }
            }
        }, {
                field: '',
                title: '服务地址',
            align: "left",
            width: "200",
                formatter: function (value, row, index) {
                    var name = null;
                    var url = "";
                    if (row.type == 1) {
                        if (row.rpcService == null && row.rpcMethod == null) {
                            return "-"
                        } else {
//                        return row.rpcService + " -> " + row.rpcMethod;
                            name = row.rpcService + " -> " + row.rpcMethod;
                            if (name.length > 80) {
                                var text = name.substr(0, 80) + "..";
                                return '<a href="/main/modifyCase?isModify=false&id=' + row.aId + '" >' + '<span data-toggle="tooltip" title="' + name + '"> ' + text + ' </span>' + '</a>'
                            } else {
                                return name;
                            }
                        }
                    } else {
                        name = row.httpURL;
                        if (name == null) {
                            return "-"
                        } else {
                            if (name.length > 80) {
                                var text = name.substr(0, 80) + "...";
//                        return '<span data-container="body" data-toggle="popover" data-placement="bottom"  data-content="' + row.httpURL + '"> ' + text +' </span>';;
//                                 return '<span data-toggle="tooltip" title="' + name + '"> ' + text +' </span>';
                                return '<a href="/main/modifyCase?isModify=false&id=' + row.aId + '" >' + '<span data-toggle="tooltip" title="' + name + '"> ' + text + ' </span>' + '</a>'
                            } else {
                                return '<a href="/main/modifyCase?isModify=false&id=' + row.aId + '" >' + name + '</a>'
                                // return name;
//                        return '<span data-toggle="tooltip" title="' + row.httpURL + '> \'' + row.httpURL + '\' </span>';
                            }
                        }
                    }

                }

            },
            {
                field: 'type',
                title: '类型',
                align: "center",
                width: "30",
                formatter: function (value, row, index) {
                    if (row.type == 0) {
                        return "http"
                    } else if (row.type == 1) {
                        return "rpc"
                    } else {
                        return "-"
                    }
                }
            }, {
                field: 'requestParameter',
                title: '请求参数',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if(row.requestParameter == null || row.requestParameter == "" || row.requestParameter.length == 0
                    || row.requestParameter == undefined){
                        row.requestParameter = " ";
                    }
                    return "<a href='javascript:void(0);' onclick='openModal(\"" + html_encode(row.requestParameter) + "\");'>查看用例参数</a>";
                }
            }, {
                field: '操作',
                title: '操作',
                align: "center",
                width: "300",
                formatter: function (value, row, index) {

                    var mUrl = "/main/modifyCase?id=" + row.aId;
                    var eUrl = "/main/executeCase?id=" + row.aId;

                    var case_modify = '<div class="btn-group" style="margin-right: 6px"> <button type="button" class="btn btn-info" onclick="openURL(\'' + mUrl + '\')">修改</button></div>';

                    var case_del = '<div class="btn-group" style="margin-right: 6px"> <button type="button" class="btn btn-danger" onclick="deleteAPICase( \'' + row.aId + '\' )">删除</button></div>';

                    var case_execute = '<div class="btn-group" style="margin-right: 6px"> <button type="button" class="btn btn-primary" onclick="openURL( \'' + eUrl + '\' )">调试</button></div>';

                    var var_list = '<div class="btn-group" style="margin-right: 6px"> <button type="button" class="btn btn-success" onclick="openCaseVariableModal(\'' + row.aId + '\' )">设置变量</button></div>';

                    // var group_manager = '<div class="btn-group"> <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">更多 <span class="caret"></span> </button>' +
                    //     '<ul class="dropdown-menu dropdown-menu-right" style="min-width:100px;text-align:center">';
                    //
                    // // var a1 = '<li><a href="javascript:void(0);" onclick="showGroupModifyModal(\'' + row.groupId + '\' )">修改分组</a></li>';
                    // // var a2 = '<li><a href="javascript:void(0);" onclick="deleteGroup( \'' + row.groupId + '\' )">删除分组</a></li>';
                    // var a3 = '<li><a href="javascript:void(0);" onclick="managerCaseListByGroup(\'' + row.groupId + '\' , \'' + row.groupName + '\')">配置用例</a></li>';
                    // var a4 = '<li><a href="javascript:void(0);" onclick="setGroupVariable( \'' + row.groupId + '\' )">定义场景变量</a></li>';
                    //
                    // var group_manager_end = '</ul></div>';
                    //
                    // var cv_begin = '<div class="btn-group"> <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">更多 <span class="caret"></span> </button>' +
                    //     '<ul class="dropdown-menu"  style="min-width:100px;text-align:center">';
                    // var cv_modify= '<li><a href="/main/modifyCase?id='+ row.aId + '" >修改</a></li>';
                    // var cv_exc= '<li><a href="/main/executeCase?id='+ row.aId + '" >调试</a></li>';
                    // var cv_del= '<li><a href="javascript:void(0);"  onclick="deleteAPICase( \'' + row.aId + '\' );">删除</a></li>';
                    // var cv_var = '<li><a href="javascript:void(0);" onclick="openCaseVariableModal(\'' + row.aId + '\' )">变量列表</a></li>';
                    // var cv_end = '</ul></div>';

                    // return case_modify + case_del + case_execute +  cv_begin + cv_modify + cv_exc + cv_del + cv_var + cv_end;
                    return case_modify + case_del + case_execute + var_list;
                }
            }],
        onLoadSuccess: function (data) {
//                $('#taskOverviewTable').val(data.beginTime+' 至 '+data.endTime);
        }


    };

    function openURL(path) {
        window.location.href = path;

    }

    function openModal(data) {
        str = html_decode(data);
        var obj = JSON.parse(str);
        $('#modal-body-parameter').html("");
        if (obj == null || obj == undefined || obj == ""){
            return;
        }
        var strs = "";
        for (var i = 0; i < obj.length; i++) {
            if (obj[i] != null && obj[i] != undefined && obj[i] != '') {
                strs += '<p class="text-primary">';
                if(obj[i].name != null && obj[i].name != undefined && obj[i].name != ''){
                    strs += obj[i].name + ' : '  + obj[i].value+ '</p>'
                }else {
                    strs +=  obj[i].value+ '</p>'
                }
            }
        }
        $('#modal-body-parameter').append(strs);
        $('#modalParameter').modal("show");
    }

    //    查看详情
    function openeDetails(data,parameter) {
        $('.modal-body').html("");
        var datas = "";
        datas = html_decode(data);
        var obj = JSON.parse(datas);
        dataParameter = html_decode(parameter);

        var details = "";
        details += "<div style='word-wrap: break-word;padding: 30px'> "
        if (obj.type == 1) {

            details += "<p'>" + "  RPC 接口服务 :    &nbsp;&nbsp;&nbsp;" + isEmpty(obj.rpcService) + "</p>";
            details += "<p'>" + "  RPC 接口版本 :    &nbsp;&nbsp;&nbsp;" + isEmpty(obj.rpcVersion) + "</p>";
            details += "<p'>" + "  RPC 接口分组 :    &nbsp;&nbsp;&nbsp;" + isEmpty(obj.rpcGroup) + "</p>";
            details += "<p'>" + "  RPC 方法名称 :    &nbsp;&nbsp;&nbsp;" + isEmpty(obj.rpcMethod) + "</p>";
            details += "<p'>" + "  RPC 请求参数 :    &nbsp;&nbsp;&nbsp;" + isEmpty(obj.dataParameter) + "</p>";
            details += "<p'>" + "  RPC 方法说明 :    &nbsp;&nbsp;&nbsp;" + isEmpty(obj.description) + "</p>";


        } else if (obj.type == 0) {
            details += "<p>" + "  请求URL  :    &nbsp;&nbsp;&nbsp;" + isEmpty(obj.httpURL) + "</p>";
            details += "<p>" + "  请求方式 :    &nbsp;&nbsp;&nbsp;" + isEmpty(obj.httpRequestType) + "</p>";
            details += "<p>" + "  参数类型 :    &nbsp;&nbsp;&nbsp;" + isEmpty(obj.parameterType) + "</p>";
            details += "<p>" + "  请求参数 :    &nbsp;&nbsp;&nbsp;" + isEmpty(obj.dataParameter) + "</p>";
            details += "<p>" + "  Content-Type :    &nbsp;&nbsp;&nbsp;" + isEmpty(obj.httpContentType) + "</p>";
            details += "<p>" + "  Cookies :    &nbsp;&nbsp;&nbsp;" + isEmpty(obj.httpCookies) + "</p>";



        } else {
            alert("数据异常！");
            return;
        }

        if (obj.expectedType == 1) {
            details += "<p >" + "  断言的类型 :    &nbsp;&nbsp;&nbsp;" + "json" + "</p>";

        } else {
            details += "<p >" + "  断言的类型 :    &nbsp;&nbsp;&nbsp;" + "code" + "</p>";
        }

        details += "<p>" + "  断言的字段 :    &nbsp;&nbsp;&nbsp;" + isEmpty(obj.expectedKey) + "</p>";
        details += "<p>" + "  断言的类型 :    &nbsp;&nbsp;&nbsp;" + isEmpty(expressionType(obj.expression)) + "</p>";
        details += "<p>" + "  断言的值 :     &nbsp;&nbsp;&nbsp;" + isEmpty(obj.expectedValue) + "</p>";
        details += "<p>" + "  全局变量名字 :   &nbsp;&nbsp;&nbsp;" + isEmpty(obj.staticName) + "</p>";
        details += "<p>" + "  表达式 :  &nbsp;&nbsp;&nbsp;" + isEmpty(obj.staticKey) + "</p>";

        details += "</div>"
        $('.modal-body').append(details);
        $('#myModal').modal();
    }

    function deleteAPICase(caseId) {
        function del(){
            if(confirm("真的要删除该条记录吗?")){
                return true;
            }else{
                return false;
            }
        }
        if(del()){
            $.ajax({
                type:"get",
                url : "/main/deleteCase",
                dataType:"json",
                data:{
                    id:caseId
                },
                success:function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if(result.code == 0){
                        alert("删除成功！");
                        $('#taskOverviewTable').bootstrapTable('refresh');
                        return;
                    }else {
                        alert(result.message);
                        result;
                    }
                },
                error:function () {
                    alert("删除异常");

                }
            });
        }else {
            return
        }
    }

    function expressionType(type) {
        if (type == 0) {
            return "isNotNull (不为空)"
        } else if (type == 1) {
            return "isNull (为空)"
        } else if (type == 2) {
            return "isEqualTo (相等)"
        } else if (type == 3) {
            return "isNotEqualTo (不相等)"
        } else if (type == 4) {
            return "isNotEqualTo (包含)"
        } else if (type == 5) {
            return "isNotEqualTo (不包含)"
        }else {
            return null
        }

    }


    function isEmpty(str) {
        var s = str;
        if (s != null && s != undefined && s != '') {
            return s;
        } else {
            return "空";
        }

    }


//    请求参数
    function queryParams(params) {
        return {
            limit: params.limit,
            offset: params.offset,
            caseId:$('#caseId').val(),
            urlId:$('#urlId').val(),
            serviceId:$('#serviceId').val(),
            typeId:$('#typeId').val(),
            rpcGroup:$('#rpcGroup').val(),
            descriptionId:$('#descId').val(),


//            startTime: $('#startTime').val(),
//            endTime: $('#endTime').val(),
//            commentUsername: $('#commentUsername').val(),
//            commentId: $('#commentId').val(),
//            topicId: $('#topicId').val(),
//            commentType: $('#commentType').val(),
//            type: $('#type').val(),
//            commentStatus: $('#commentStatus').val()
        };
    }

    /**
     * html encode
     * @param str
     * @returns {string}
     */
    function html_encode(str) {
        var s = "";
        if (str == "" || str.length == 0) {
            return "";
        }
        s = str.replace(/&/g, "&amp;");
        s = s.replace(/</g, "&lt;");
        s = s.replace(/>/g, "&gt;");
        s = s.replace(/\\/g, "&xg;");
        s = s.replace(/ /g, "&nbsp;");
        s = s.replace(/\'/g, "&#aabb;");
        s = s.replace(/\"/g, "&#ccdd;");
        s = s.replace(/\n/g, " ");
        return s;
    }

    /**
     * html decode
     * @param str
     * @returns {string}
     */
    function html_decode(str) {
        var s = "";
        if (str.length == 0) return "";
        s = str.replace(/&amp;/g, "&");   //2
        s = s.replace(/&lt;/g, "<");
        s = s.replace(/&gt;/g, ">");
        s = s.replace(/&xg;/g, '\\');
        s = s.replace(/&nbsp;/g, " ");
        s = s.replace(/&#aabb;/g, "\'");
        s = s.replace(/&#ccdd;/g, "\"");
        s = s.replace(/<br>/g, " ");
        return s;
    }

    /**
     * 打开变量列表静态框
     * @param cId
     */
    function openCaseVariableModal(cId) {
        _caseId = cId;
        $('#OverviewTableCaseVariableList').bootstrapTable('destroy');
        $('#CaseVeriableListModal').modal('show');
        $('#OverviewTableCaseVariableList').bootstrapTable(table_options_search_overview_addCaseVeriable);
    }




    //初始化table
    $('#taskOverviewTable').bootstrapTable(table_options_task_overview);

</script>

<script>
    $(function () {
        $('#reset').click(function() {
            $('#caseId').val("");
            $('#urlId').val("");
            $('#serviceId').val("");
            $('#typeId').val("");
            $('#rpcGroup').val("");
            $('#descId').val("");
            $('#taskOverviewTable').bootstrapTable('refresh', { url: '/main/queryAPICase' });
        });
        $('#search').click(function () {
//            var urlId = $('#urlId').val();
//            var serviceId = $('#serviceId').val();
//            var typeId = $('#typeId').val();
            $('#taskOverviewTable').bootstrapTable('destroy');
            $('#taskOverviewTable').bootstrapTable(table_options_task_overview);
//            $('#taskOverviewTable').bootstrapTable('refresh', { url: '/main/queryAPICase?urlId=' + urlId + '&serviceId=' +serviceId  + '&typeId=' +typeId});
        });
    });

    $(document).keyup(function(event){
        if(event.keyCode ==13){
            $("#search").trigger("click");
        }
    });

</script>
</body>
</html>
