
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <title>任务执行概述</title>

    <script src="/js/jquery.js"></script>
    <script src="/js/highcharts.js"></script>

    <script src="/bootstrap3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/bootstrap3/css/bootstrap.min.css">

    <script src="/bootstrap-table/dist/bootstrap-table.js"></script>
    <link rel="stylesheet" href="/bootstrap-table/dist/bootstrap-table.css">
    <script src="/bootstrap-table/dist/bootstrap-table-zh-CN.js"></script>

</head>
<body>

<div class="container" style="margin-left: 0px;">
    <%--<h2>任务执行概述</h2>--%>
    <table class="table table-striped">
        <caption>任务执行统计</caption>
        <thead>
        <tr>
            <th></th>
            <th>总任务数</th>
            <th>失败任务数</th>
            <th>总用例数</th>
            <th>成功用例数</th>
            <th>失败用例数</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td id="">今日执行</td>
            <td><span id="todayTotalTask"></span></td>
            <td><span id="todayFailTask" style="color:#F00"></span></td>
            <td><span id="todayTotalCase"></span></td>
            <td><span id="todaySuccessCase"></span></td>
            <td><span id="todayFailCase"style="color:#F00"></span></td>
        </tr>
        <tr>
            <td>昨日执行</td>
            <td><span id="yesterdayTotalTask"></span></td>
            <td><span id="yesterdayFailTask"style="color:#F00"></span></td>
            <td><span id="yesterdayTotalCase"></span></td>
            <td><span id="yesterdaySuccessCase"></span></td>
            <td><span id="yesterdayFailCase"style="color:#F00"></span></td>
        </tr>
        <tr>
            <td>过去一周</td>
            <td><span id="weekTotalTask"></span></td>
            <td><span id="weekFailTask"style="color:#F00"></span></td>
            <td><span id="weekTotalCase"></span></td>
            <td><span id="weekSuccessCase"></span></td>
            <td><span id="weekFailCase"style="color:#F00"></span></td>
        </tr>
        </tbody>
    </table>
</div>


<script type="application/javascript">
    $(document).ready(function () {
            $.ajax({
                type: "get",
                url: "/report/summaryData",
                dataType: "json",
                data: {
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (true) {
                        for(var i=0;i<result.length;i++){
                            if(result[i].type == 1){
                                $("#todayTotalTask").html(result[i].totalTask);
                                $("#todayFailTask").html(result[i].failTask);
                                $("#todayTotalCase").html(result[i].totalCase);
                                $("#todaySuccessCase").html(result[i].successCase);
                                $("#todayFailCase").html(result[i].failCase);
                            }
                            if(result[i].type == 2){
                                $("#yesterdayTotalTask").html(result[i].totalTask);
                                $("#yesterdayFailTask").html(result[i].failTask);
                                $("#yesterdayTotalCase").html(result[i].totalCase);
                                $("#yesterdaySuccessCase").html(result[i].successCase);
                                $("#yesterdayFailCase").html(result[i].failCase);
                            }
                            if(result[i].type == 3){
                                $("#weekTotalTask").html(result[i].totalTask);
                                $("#weekFailTask").html(result[i].failTask);
                                $("#weekTotalCase").html(result[i].totalCase);
                                $("#weekSuccessCase").html(result[i].successCase);
                                $("#weekFailCase").html(result[i].failCase);
                            }
                        }
                        return;
                    } else {
                        alert("获取概要数据异常");
                        result;
                    }
                },
                error: function () {

                }
            });

    });
</script>

</body>
</html>
