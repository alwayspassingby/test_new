
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <title>Task列表</title>

    <script src="/js/jquery.js"></script>

    <script src="/bootstrap3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/bootstrap3/css/bootstrap.min.css">

    <script src="/bootstrap-table/dist/bootstrap-table.js"></script>
    <link rel="stylesheet" href="/bootstrap-table/dist/bootstrap-table.css">
    <script src="/bootstrap-table/dist/bootstrap-table-zh-CN.js"></script>
    <style type="text/css">
        .popover{word-break:break-all;}
        .tooltip-inner {
            word-break:break-all;
            max-width: 300px;
            font-size: 12px;
            /*color: #151515;*/
            /*background-color: #7B7B7B;*/
        }
    </style>

</head>

<body>

<div class="modal fade" id="configTestlink" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false"
     aria-labelledby="configTestlinkLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                <h4 class="modal-title" id="configTestlinkLabel">添加Task</h4>
            </div>
            <form id="configTestlinkForm" action="#" method="post">
                <div class="modal-body">

                    <input name="taskId" id="config_task_Id" type="hidden">

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">测试项目：
                            </button>
                        </div>
                        <input name="testlink_project" id="testlink_project" type="text" class="form-control"
                               style="border-radius: 4px">
                        <label id="testlink_project_info" style="display: block;color: red;font-size: 10px"></label>
                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">测试计划：
                            </button>
                        </div>
                        <input name="testlink_testplan" id="testlink_testplan" type="text" class="form-control"
                               style="border-radius: 4px">
                        <label id="testlink_testplan_info" style="display: block;color: red;font-size: 10px"></label>

                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">测试平台：
                            </button>
                        </div>
                        <input name="testlink_platform" id="testlink_platform" type="text" class="form-control"
                               style="border-radius: 4px">
                        <label id="testlink_platform_info" style="display: block;color: red;font-size: 10px"></label>
                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">测试版本：
                            </button>
                        </div>
                        <input name="testlink_build" id="testlink_build" type="text" class="form-control"
                               style="border-radius: 4px">
                        <label id="testlink_build_info" style="display: block;color: red;font-size: 10px"></label>
                    </div>

                </div>
                <div class="modal-footer">
                    <button id="btn_config_testlink" style="float: right;" type="button" class="btn btn-primary"
                            onclick="updateTask()">配置
                    </button>
                    <button type="button" style="float: right;" class="btn btn-default" data-dismiss="modal"
                            onclick="clearFormData('insertHost')">关闭
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>


<div class="modal fade" id="addModal" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                <h4 class="modal-title" id="myModalLabelAdd">添加Task</h4>
            </div>
            <form id="insertTask" action="#" method="post">
                <div class="modal-body">

                    <input name="taskId" id="update_id" type="hidden">

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">任务名称：
                            </button>
                        </div>
                        <input name="taskName" id="taskNameAdd" type="text" class="form-control"
                               style="border-radius: 4px">
                        <label id="ipInfo" style="display: block;color: red;font-size: 10px"></label>
                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">任务描述：
                            </button>
                        </div>
                        <input name="description" id="taskDescAdd" type="text" class="form-control"
                               style="border-radius: 4px">
                        <label id="hostInfo" style="display: block;color: red;font-size: 10px"></label>

                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">发送类型：
                            </button>
                        </div>
                        <select name="sendType" id="sendType" class="form-control"style="border-radius: 4px">
                            <option value="9" selected>请选择消息发送类型</option>
                            <option value="0">不发钉钉消息</option>
                            <option value="1">只失败发钉钉</option>
                            <option value="2">只成功发钉钉</option>
                            <option value="3">成功失败都发</option>
                        </select>
                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">报告发送给
                            </button>
                        </div>
                        <input name="reporter" id="reporter" type="text" class="form-control"
                               style="border-radius: 4px" placeholder="请输入邮箱!">
                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">负责人：&nbsp&nbsp&nbsp&nbsp
                            </button>
                        </div>
                        <input name="taskPerson" id="person" type="text" class="form-control"
                               style="border-radius: 4px" placeholder="请输入邮箱!">
                    </div>

                </div>
                <div class="modal-footer">
                    <button id="btn_addTask" style="float: right;" type="button" class="btn btn-primary"
                            onclick="insertTasks()">添加
                    </button>
                    <button id="btn_modifyTask" style="float: right;" type="button" class="btn btn-primary"
                            onclick="updateTask()">确定
                    </button>
                    <button type="button" style="float: right;" class="btn btn-default" data-dismiss="modal"
                            onclick="clearFormData('insertHost')">关闭
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>


<div class="modal fade" id="modifyModal" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                <h4 class="modal-title" id="myModalLabelModify">修改任务</h4>
            </div>
            <form id="updateHost" action="#" method="post">
                <div class="modal-body">

                    <%--<input name="taskId" id="update_id" type="hidden">--%>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">任务名称：
                            </button>
                        </div>
                        <input name="taskName" id="update_taskName" type="text" class="form-control"
                               style="border-radius: 4px">
                        <label id="label_ipInfo" style="display: block;color: red;font-size: 10px"></label>
                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">任务描述：
                            </button>
                        </div>
                        <input name="description" id="update_description" type="text" class="form-control"
                               style="border-radius: 4px">
                        <label id="label_hostInfo" style="display: block;color: red;font-size: 10px"></label>

                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">报告发送给
                            </button>
                        </div>
                        <input name="reporter" id="update_reporter" type="text" class="form-control"
                               style="border-radius: 4px">
                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">负责人：&nbsp&nbsp&nbsp&nbsp
                            </button>
                        </div>
                        <input name="update_person" id="update_person" type="text" class="form-control"
                               style="border-radius: 4px">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            onclick="clearFormData('modifyModal')">关闭
                    </button>

                    <button id="btn_modify" type="button" class="btn btn-primary" onclick="updateTask()">保存</button>
                </div>
            </form>

        </div>
    </div>
</div>


<div class="modal fade" id="modifyModaAddGroup" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 1000px">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabelAddTask">添加用例组</h4>
            </div>

            <div id="toolbar2" class="btn-group" style="display: block">
                <ul id="myTab" class="nav nav-tabs">
                    <%--<input class="btn btn-default" type="button" value="添加" onclick="BtchAdd()">--%>
                    <li class="active">
                        <a href="#" data-toggle="tab" onclick="selectedList($('#task_id').val())">
                            已添加过的组
                        </a>
                    </li>
                    <li><a href="#" data-toggle="tab" onclick="noSelectedList($('#task_id').val())">未添加的组</a></li>
                </ul>
            </div>

            <form id="addGroup" action="#" method="post">
                <div class="modal-body">
                    <input name="taskId" id="task_id" type="hidden">
                    <input name="isSelected" id="isSelected" type="hidden">
                    <%--<input name="isSelected" id="isSelected" type="hidden">--%>
                    <table id="taskOverviewTableAddGroup"></table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            onclick="clearFormData('modifyModaAddGroup')">关闭
                    </button>

                    <%--<button id="btn_modify" type="button" class="btn btn-primary" onclick="updateTask()">保存</button>--%>
                </div>
            </form>

        </div>
    </div>
</div>


<div class="modal fade" id="ModalSetCrontab" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 500px">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalSetCrontab">设置执行时间</h4>
            </div>
            <form id="setCrontab" action="#" method="post">
                <input name="taskId" id="task_id2" type="hidden">
                <div class="modal-body">
                    <div class="form-group">

                        <div class="radio" style="margin-left: 20px">
                            <input type="radio" class="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                            不定时执行
                        </div>

                        <%--<div class="input-group input-group-sm" style="margin-right: 50px;margin-bottom: 20px">--%>
                            <%--&lt;%&ndash;<div class="input-group-btn">&ndash;%&gt;--%>
                                <%--&lt;%&ndash;<button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"&ndash;%&gt;--%>
                                        <%--&lt;%&ndash;style="border: none;float: left"> 不定时执行&ndash;%&gt;--%>
                                <%--&lt;%&ndash;</button>&ndash;%&gt;--%>
                            <%--&lt;%&ndash;</div>&ndash;%&gt;--%>
                            <%--&lt;%&ndash;<input name="options1" id="options1" type="text"&ndash;%&gt;--%>
                                   <%--&lt;%&ndash;class="form-control"&ndash;%&gt;--%>
                                   <%--&lt;%&ndash;style="border-radius: 4px;width: 100px;float: left">&ndash;%&gt;--%>
                        <%--</div>--%>

                        <div class="radio" style="margin-left: 20px">
                            <input type="radio" class="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                            定时执行
                        </div>

                        <div class="input-group input-group-sm">
                            <div class="input-group-btn">
                                <label style="font-size: 14px;padding-left: 10px;">设置定时任务表达式 （秒 分 小时 日 月份 星期 年份）</label>
                            </div>
                        </div>


                        <div class="input-group input-group-sm" style="margin-right: 50px;margin-bottom: 20px">
                            <div class="input-group-btn">
                                <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                        style="border: none">表达式 :
                                </button>
                            </div>
                            <input name="inputMinute2" id="inputCrontab" type="text" class="form-control"
                                   style="border-radius: 4px;width: 300px">
                        </div>

                        <div class="input-group input-group-sm">
                            <div class="input-group-btn">
                                <label style="font-size: 14px;padding-left: 10px;">请跳转到此链接，校验表达式是否正确：</label>
                                <a href="http://cron.qqe2.com/" style="font-size: 14px" target="_blank">http://cron.qqe2.com/</a>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            onclick="clearFormData1('setCrontab')">关闭
                    </button>

                    <button id="btn_set" type="button" class="btn btn-primary" onclick="getCrontab()">保存</button>
                </div>

            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="setHostModal" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 1000px">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabelAddHosts">设置任务host</h4>
            </div>

            <div id="toolbarHost" class="btn-group" style="display: block">
                <ul id="myTabHost" class="nav nav-tabs">
                    <li class="active">
                        <a href="#" data-toggle="tab" onclick="selectedSetHost($('#taskId_h').val())">
                            已添加的Host
                        </a>
                    </li>
                    <li><a href="#" data-toggle="tab" onclick="selectedNoSetHost($('#taskId_h').val())">未添加的Host</a>
                    </li>
                </ul>
            </div>

            <form id="setHostForm" action="#" hidden>
                <input name="taskId_h" id="taskId_h" type="hidden">
                <input name="isSelected_h" id="isSelectedHost" type="hidden">
            </form>
            <div class="panel-body" style="padding-bottom:0px;">
                <table id="taskOverviewTableSetHost"></table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"
                        onclick="clearModalData('setHostForm')">关闭
                </button>

                <%--<button id="btn_modify" type="button" class="btn btn-primary" onclick="updateTask()">保存</button>--%>
            </div>

        </div>
    </div>
</div>


<%--task添加管理测试用户窗口--%>
<div class="modal fade" id="setTUserModal" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 1000px">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabelAddTUser">设置任务的测试用户</h4>
            </div>

            <div id="toolbarTUser" class="btn-group" style="display: block">
                <ul id="myTabTUser" class="nav nav-tabs">
                    <li class="active">
                        <a href="#" data-toggle="tab" onclick="selectedSetTUser($('#taskId_tu').val())">
                            已添加的用户列表
                        </a>
                    </li>
                    <li><a href="#" data-toggle="tab" onclick="selectedNoSetTUser($('#taskId_tu').val())">未添加的用户列表</a>
                    </li>
                </ul>
            </div>

            <form id="setTUserForm" action="#" hidden>
                <input name="taskId_tu" id="taskId_tu" type="hidden">
                <input name="isSelected_tu" id="isSelectedTUser" type="hidden">
            </form>
            <div class="panel-body" style="padding-bottom:0px;">
                <table id="taskOverviewTableSetTUser"></table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"
                        onclick="clearModalData('setTUserForm')">关闭
                </button>

                <%--<button id="btn_modify" type="button" class="btn btn-primary" onclick="updateTask()">保存</button>--%>
            </div>

        </div>
    </div>
</div>

<%--任务变量--%>
<div class="modal fade" id="TaskVariableListModal" tabindex="-1" role="dialog" data-backdrop="false"
     data-keyboard="false"
     aria-labelledby="TaskVariableListModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 1000px">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">全局变量列表</h4>
            </div>

            <div id="variableToolbar" class="btn-group" style="display: block">

                <ul id="variableTab" class="nav nav-tabs">
                    <li class="active">
                        <a href="#" data-toggle="tab"
                           onclick="selectedAddVariableList($('#v_taskId').val())">
                            已添加
                        </a>
                    </li>
                    <li><a href="#" data-toggle="tab"
                           onclick="selectedNotAddVariableList($('#v_taskId').val())">未添加</a>
                    </li>
                </ul>

                <div id="addTaskVariableButton" style="padding-top: 10px">
                    <button id="addTaskVeriable" class="btn btn-success btn-sm"
                            onclick="showAddTaskVariableModalByTastId($('#v_taskId').val())" type="button"><span
                            class="glyphicon glyphicon-plus"></span></button>
                </div>

            </div>

            <form id="TaskVariableList" action="#" hidden>
                <input name="v_taskId" id="v_taskId" type="hidden">
                <input name="v_isSelected" id="v_isSelected" type="hidden">
            </form>

            <div class="panel-body" style="padding-bottom:0px;">
                <table id="OverviewTableTaskVariableList"></table>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"
                        onclick="clearModalData('TaskVariableList','OverviewTableTaskVariableList')">关闭
                </button>
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="taskVariableModal" tabindex="-1" role="dialog" data-backdrop="false"
     data-keyboard="false" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                <h4 class="modal-title" id="taskVariableModalLabel"></h4>
            </div>
            <form id="taskVariableForm" action="#" method="post">

                <input name="vId" id="vId" type="hidden">
                <input name="v_type" id="v_type" value="2" type="hidden">
                <input name="v_tId" id="v_tId" type="hidden">

                <div class="modal-body">
                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">名称：
                            </button>
                        </div>
                        <input name="v_name" id="v_name" type="text" class="form-control" style="border-radius: 4px">
                        <label id="name_Info" style="display: block;color: red;font-size: 10px"></label>
                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">value：
                            </button>
                        </div>
                        <input name="v_value" id="v_value" type="text" class="form-control" style="border-radius: 4px">
                        <label id="value_Info" style="display: block;color: red;font-size: 10px"></label>

                    </div>

                <%--<div class="input-group" style="margin-right: 50px;margin-bottom: 20px">--%>
                        <%--<div class="input-group-btn">--%>
                            <%--<button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"--%>
                                    <%--style="border: none">类型：--%>
                            <%--</button>--%>
                        <%--</div>--%>
                        <%--<select type="text" class="form-control" name="type" id="type" style="border-radius: 4px">--%>
                            <%--&lt;%&ndash;<option value="0" selected>全局变量</option>&ndash;%&gt;--%>
                            <%--&lt;%&ndash;<option value="2">任务变量</option>&ndash;%&gt;--%>
                        <%--</select>--%>
                        <%--<label id="type_Info" style="display: block;color: red;font-size: 10px"></label>--%>

                    <%--</div>--%>


                    <%--<div class="input-group" style="margin-right: 50px;margin-bottom: 20px">--%>
                        <%--<div class="input-group-btn">--%>
                            <%--<button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"--%>
                                    <%--style="border: none">描述：--%>
                            <%--</button>--%>
                        <%--</div>--%>
                        <%--<input name="description" id="description" type="text" class="form-control"--%>
                               <%--style="border-radius: 4px">--%>
                    <%--</div>--%>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            onclick="clearFormData('variableForm')"
                            style="float: right;margin-left: 10px;margin-right: 20px">关闭
                    </button>

                    <button id="v_but_add" style="float: right;display: none" type="button" class="btn btn-primary"
                            onclick="addTaskVariable()">添加
                    </button>
                    <button id="v_but_modify" style="float:right" type="button" class="btn btn-primary"
                            onclick="updateTaskVariable()">修改
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="panel-body" style="padding-bottom:0px;">

    <div class="panel panel-default">
        <div class="panel-heading">查询条件</div>
        <div class="panel-body">
            <form id="formSearch" class="form-horizontal">
                <div class="form-group" style="margin-top:15px">

                    <label class="control-label col-sm-1" style="width: 30px">ID</label>
                    <div class="col-sm-1">
                        <input type="text" class="form-control" id="taskId">
                    </div>

                    <label class="control-label col-sm-1" style="width: 90px">任务名称</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="taskName" onkeyup="this.value=this.value.replace(/\s+/g,'')">
                    </div>

                    <label class="control-label col-sm-1" style="width: 90px">任务描述</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="taskDesc"  onkeyup="this.value=this.value.replace(/\s+/g,'')">
                    </div>

                    <div class="col-sm-3" style="text-align:left;">
                        <button type="button" style="margin-left:50px" id="btn_query" class="btn btn-primary">查询
                        </button>
                        <button type="button" style="margin-left:50px" id="btn_reset" class="btn btn-default">重置
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div id="toolbar" class="btn-group">
        <button id="add" type="button" class="btn btn-success" onclick="showAddTaskModal()">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>新增
        </button>
    </div>


    <table id="taskOverviewTable"></table>

</div>

<script type="application/javascript">
    var table_options_task_overview = {
        url: '/task/queryTaskList',
        method: 'get',                      //请求方式（*）
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: false,                     //是否启用排序
        toolbar: '#toolbar',                 //工具按钮使用日期
        sortOrder: "asc",                   //排序方式
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        showColumns: false,                  //是否显示所有的列
        height: 700,
        pageNumber: 1,                       //初始化加载第一页，默认第一页
        pageSize: 10,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
        clickToSelect: true,                // 是否启用点击选中行
//        showExport: true,                     //是否显示导出
        showRefresh: true,
//        exportDataType: "all",              //basic', 'all', 'selected'.
//        silent: true,
        queryParams: queryParams, //参数
        columns: [{
            field: 'taskId',
            title: 'ID',
            align: "center",
            width: "20",
        },
            {
                field: 'taskName',
                title: '任务名称',
                align: "center",
                width: "100"
            },
            {
                field: 'description',
                title: '任务描述',
                align: "center",
                width: "150"
            },
            {
                field: 'taskPerson',
                title: '负责人',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if (row.taskPerson == null || row.taskPerson == "" || row.taskPerson.length == 0
                            || row.taskPerson == undefined) {
                        row.taskPerson = "-";
                    }
                    if (row.taskPerson.length > 30) {
                        return row.taskPerson.substr(0, 30) + "...";
                    } else {
                        return row.taskPerson
                    }
                }
            },
            {
                field: '操作',
                title: '操作',
                align: "center",
                width: "90",
                formatter: function (value, row, index) {
                    var run = '<div class="btn-group"><li style="display:inline;margin-right: 6px" class="btn btn-info dropdown-toggle" ><a style="color: #fff" href="javascript:void(0);" onclick="runTask( \'' + row.taskId + '\' )">执行<span class="glyphicon glyphicon-triangle-right"></a></li></div>';
                    var group_begin = '<div class="btn-group"> <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">更多 <span class="caret"></span> </button>' +
                            '<ul class="dropdown-menu">';

                    var a1 = '<li><a href="javascript:void(0);" onclick="showHostModifyContent(\'' + row.taskId + '\' )">修改任务</a></li>';
//                    <li class="divider"></li>
                    var a2 = '<li><a href="javascript:void(0);" onclick="deleteTask( \'' + row.taskId + '\' )">删除任务</a></li>';
                    var a3 = '<li><a href="javascript:void(0);" onclick="managerGroupByTask( \'' + row.taskId + '\' )">配置用例场景</a></li>';
                    var a4 = '<li><a href="javascript:void(0);" onclick="setCrontab( \'' + row.taskId + '\' )">设置定时</a></li>';
                    var a5 = '<li><a href="javascript:void(0);" onclick="setHost( \'' + row.taskId + '\' )">设置Host</a></li>';
//                    var a6 = '<li><a href="javascript:void(0);" onclick="setTUser( \'' + row.taskId + '\' )">设置用户</a></li>';
                    var a7 = '<li><a href="javascript:void(0);" onclick="setTaskVariable( \'' + row.taskId + '\' )">自定义变量</a></li>';
                    var a8 = '<li><a href="javascript:void(0);" onclick="openTestLinkConfig( \'' + row.taskId + '\' )">testlink管理配置</a></li>';
                    var group_end = '</ul></div>';
//                    return run + group_begin + a1 + a2 + a3 + a4 + a5 + a6 + a7 + group_end;
                    return run + group_begin + a1 + a2 + a3 + a4 + a5 + a7 + a8 + group_end;
                }
            }],
        onLoadSuccess: function (data) {
//                $('#taskOverviewTable').val(data.beginTime+' 至 '+data.endTime);
        }


    };


    var table_options_task_overview_addgroup = {
        url: '/task/getGroupListFromTaskId',
        method: 'get',                      //请求方式（*）
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
//        pagination: true,                   //是否显示分页（*）
        sortable: false,                     //是否启用排序
        clickToSelect: true,
        toolbar: '#toolbar2',                 //工具按钮使用日期
        sortOrder: "asc",                   //排序方式
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        showColumns: false,                  //是否显示所有的列
        height: 530,
        pageNumber: 1,                       //初始化加载第一页，默认第一页
        pageSize: 10,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
//        clickToSelect: true,                // 是否启用点击选中行
//        showExport: true,                     //是否显示导出
        showRefresh: true,
//        exportDataType: "all",              //basic', 'all', 'selected'.
//        silent: true,
        queryParams: queryParamsAddGroup, //参数
        columns: [{
            field: 'groupId',
            title: 'ID',
            align: "center",
            width: "20",
        },
            {
                field: 'groupName',
                title: '组名称',
                align: "center",
                width: "100"
            },
            {
                field: 'description',
                title: '组描述',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
//                    if(row.requestParameter == null || row.requestParameter == "" || row.requestParameter.length == 0
//                            || row.requestParameter == undefined){
//                        row.requestParameter = " ";
//                    }
                    if (row.description.length > 30) {
                        return row.description.substr(0, 30) + "...";
                    } else {
                        return row.description
                    }
                }
            }
            , {
                field: '操作',
                title: '操作',
                align: "center",
                width: "90",
                formatter: function (value, row, index) {
                    var group_begin = '<div class="btn-group">' +
                            ' <button id="addGroupButton" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  onclick="addGroupByTask(\'' + row.groupId + '\' )">添加 </button></div>';
                    var group_delete = '<div class="btn-group">' +
                            ' <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="deleteGroupByTask(\'' + row.groupId + '\' )">删除 </button></div>';
                    if ($('#isSelected').val() == 1) {
                        return group_delete;
                    } else {
                        return group_begin;
                    }
                }
            }
        ],
        onLoadSuccess: function (data) {
//                $('#taskOverviewTable').val(data.beginTime+' 至 '+data.endTime);
        }


    };

//    获取task下的host列表
    var table_options_task_overview_setHost = {
        url: '/task/getHostsListByTask',
        method: 'get',                      //请求方式（*）
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: false,                     //是否启用排序
        clickToSelect: true,
        toolbar: '#toolbarHost', //工具按钮使用日期
        search: true,
        searchOnEnterKey:false,
        trimOnSearch:true,
        sortOrder: "asc",                   //排序方式
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        showColumns: false,                  //是否显示所有的列
        height: 530,
        pageNumber: 1,                       //初始化加载第一页，默认第一页
        pageSize: 10,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
//        clickToSelect: true,                // 是否启用点击选中行
//        showExport: true,                     //是否显示导出
//        showRefresh: true,
//        exportDataType: "all",              //basic', 'all', 'selected'.
//        silent: true,

        queryParams: queryParamsSetHosts, //参数
        columns: [{
            field: 'hId',
            title: 'ID',
            align: "center",
            width: "20",
        },
            {
                field: 'ip',
                title: 'ip',
                align: "center",
                width: "130",
            },
            {
                field: 'host',
                title: '域名',
                align: "center",
                width: "130",
                formatter: function (value, row, index) {
                        if ((row.host.length)> 25) {
                            var text =row.host.substr(0, 30) + "...";
                            return '<span data-toggle="tooltip" title="' + row.host + '"> ' + text +' </span>';
                        } else {
                            return row.host;
                        }
                }
            }, {
                field: 'description',
                title: '描述',
                align: "center",
                width: "130",
                formatter: function (value, row, index) {
                    if (row.description.length > 25) {
                        var text = row.description.substr(0, 25) + "...";
                        return '<span data-toggle="tooltip" title="' + row.description + '"> ' + text + ' </span>';
                    } else {
                        return row.description;
                    }
                }
            }
            , {
                field: '操作',
                title: '操作',
                align: "center",
                width: "90",
                formatter: function (value, row, index) {
                    var group_begin = '<div class="btn-group">' +
                            ' <button id="addSetHostButton" type="button"  class=" btn-default btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  onclick="addTaskHost(\'' + row.hId + '\' , this )">添加 </button></div>';
                    var group_delete = '<div class="btn-group">' +
                            ' <button type="deleteHostButton" class="btn-default btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="deleteSetHost(\'' + row.hId + '\' )">删除 </button></div>';
                    if ($('#isSelectedHost').val() == 1) {
                        return group_delete ;
                    } else {
                        return group_begin;
                    }
                }
            }
        ],
        onSearch: function (text) {
            $('#taskOverviewTableSetHost').bootstrapTable(table_options_task_overview_setHost);
//            alert(text);
//                $('#taskOverviewTable').val(data.beginTime+' 至 '+data.endTime);
        },
        onPostBody: function () {
//            $("[data-toggle='popover']").popover();
            $("[data-toggle='tooltip']").tooltip();
        }

    };

//    获取task下的User列表
    var table_options_task_overview_setTUsers = {
        url: '/task/getTUserListByTask',
        method: 'get',                      //请求方式（*）
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: false,                     //是否启用排序
        clickToSelect: true,
        toolbar: '#toolbarTUser', //工具按钮使用日期
        search: true,
        searchOnEnterKey:false,
        trimOnSearch:true,
        sortOrder: "asc",                   //排序方式
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        showColumns: false,                  //是否显示所有的列
        height: 530,
        pageNumber: 1,                       //初始化加载第一页，默认第一页
        pageSize: 10,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
//        clickToSelect: true,                // 是否启用点击选中行
//        showExport: true,                     //是否显示导出
//        showRefresh: true,
//        exportDataType: "all",              //basic', 'all', 'selected'.
//        silent: true,

        queryParams: queryParamsSetTUsers, //参数
        columns: [{
            field: 'tuId',
            title: 'ID',
            align: "center",
            width: "20",
        },
            {
                field: 'tname',
                title: '用户名',
                align: "center",
                width: "130",
            },
             {
                field: 'description',
                title: '描述',
                align: "center",
                width: "130",
                formatter: function (value, row, index) {
                    if (row.description.length > 25) {
                        var text = row.description.substr(0, 25) + "...";
                        return '<span data-toggle="tooltip" title="' + row.description + '"> ' + text + ' </span>';
                    } else {
                        return row.description;
                    }
                }
            }
            , {
                field: '操作',
                title: '操作',
                align: "center",
                width: "90",
                formatter: function (value, row, index) {
                    var group_begin = '<div class="btn-group">' +
                            ' <button id="addSetTUserButton" type="button"  class=" btn-default btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  onclick="addTaskTUser(\'' + row.tuId + '\' , this )">添加 </button></div>';
                    var group_delete = '<div class="btn-group">' +
                            ' <button type="deleteTUserButton" class="btn-default btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="deleteTaskTUser(\'' + row.tuId + '\' )">删除 </button></div>';
                    if ($('#isSelectedTUser').val() == 1) {
                        return group_delete ;
                    } else {
                        return group_begin;
                    }
                }
            }
        ],
        onSearch: function (text) {
            $('#taskOverviewTableSetTUser').bootstrapTable(table_options_task_overview_setTUsers);
//            alert(text);
//                $('#taskOverviewTable').val(data.beginTime+' 至 '+data.endTime);
        },
        onPostBody: function () {
//            $("[data-toggle='popover']").popover();
            $("[data-toggle='tooltip']").tooltip();
        }

    };


    var table_options_overview_getVariableListFromTask = {
        url: '/variable/getTaskVariableList',
        method: 'post',                      //请求方式（*）
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: false,                     //是否启用排序
        clickToSelect: true,
        toolbar: '#variableToolbar', //工具按钮使用日期
        search: false,
        searchOnEnterKey: false,
        trimOnSearch: true,
        sortOrder: "asc",                   //排序方式
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        showColumns: false,                  //是否显示所有的列
        height: 530,
        pageNumber: 1,                       //初始化加载第一页，默认第一页
        pageSize: 10,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
//        clickToSelect: true,                // 是否启用点击选中行
//        showExport: true,                     //是否显示导出
//        showRefresh: true,
//        exportDataType: "all",              //basic', 'all', 'selected'.
//        silent: true,
        queryParams: queryParamsGetVariableListTask, //参数
        columns: [{
            field: 'vId',
            title: 'ID',
            align: "center",
            width: "20",
        },
            {
                field: 'v_name',
                title: '变量名',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if (row.v_name == null) {
                        return "-"
                    }
                    if (row.v_name.length > 30) {
                        var text = row.v_name.substr(0, 30) + "...";
                        return '<span data-toggle="tooltip" title="' + row.v_name + '"> ' + text + ' </span>';
                    } else {
                        return row.v_name;
                    }
                }
            },
            {
                field: 'v_value',
                title: '表达式',
                align: "center",
                width: "130",
                formatter: function (value, row, index) {
                    if (row.v_value.length > 25) {
                        var text = row.v_value.substr(0, 25) + "...";
                        return '<span data-toggle="tooltip" title="' + row.v_value + '"> ' + text + ' </span>';
                    } else {
                        return row.v_value;
                    }
                }
            },
            {
                field: '操作',
                title: '操作',
                align: "center",
                width: "90",
                formatter: function (value, row, index) {

                    var group_move = '<div class="btn-group">' +
                            ' <button type="button" class="btn-default btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="moveTaskVariable(\'' + row.vId + '\' )">移除 </button></div>';

                    var group_modify = '<div class="btn-group">' +
                            ' <button type="button" class="btn-info btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="showUpdateTaskVariableModal(\'' + row.vId + '\' )">修改 </button></div>';


                    var btn_group_begin = '<div class="btn-group">' +
                            ' <button class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">更多<span class="caret"></span></button>' +
                            '<ul class="dropdown-menu dropdown-menu-right" style="min-width:100px;text-align:center">';

                    var btn_add = '<li><a href="javascript:void(0);" onclick="addToTaskVariableList(\'' + row.vId + '\' )">添加</a></li>';
                    var btn_modify = '<li><a href="javascript:void(0);" onclick="showUpdateTaskVariableModal(\'' + row.vId + '\' )">修改</a></li>';
                    var btn_deleter = '<li><a href="javascript:void(0);" onclick="delVariable(\'' + row.vId + '\' )">删除</a></li>';
                    var btn_group_end = '</ul></div>';

                    if ($('#v_isSelected').val() == 1) {
                        if (row.type == 2) {
                            return group_move + group_modify;
                        } else {
                            return group_move;
                        }
                    } else {
                        if (row.type == 2) {
                            return btn_group_begin + btn_add + btn_modify + btn_deleter + btn_group_end;
                        } else {
                            return btn_group_begin + btn_add + btn_group_end;
                        }
                    }
                }
            }
        ],
//        onSearch: function (text) {
//            $('#OverviewTableCaseVariableList').bootstrapTable(table_options_overview_getVariableListFromGroup);
//        },
        onPostBody: function () {
//            $("[data-toggle='popover']").popover();
            $("[data-toggle='tooltip']").tooltip();
        }

    };


    function openModal(data) {
        data = html_decode(data)
        $('.modal-body').html("");
        var contens = data.split("&");
        var strs = "";
        for (i = 0; i < contens.length; i++) {
            if (contens[i] != null && contens[i] != undefined && contens[i] != '') {
                strs += "<p>" + contens[i] + "  & " + "</p>";
            }
        }
        $('.modal-body').append(strs);
        $('#myModal').modal();
    }


    /*
     * 添加task的组
     */

    function managerGroupByTask(tId) {
        $('#taskOverviewTableAddGroup').bootstrapTable('destroy');
        $('#task_id').val(tId);
        $('#isSelected').val(1);
        $('#modifyModaAddGroup').modal('show');
        $('#myTab li:eq(0) a').tab('show');
        $('#taskOverviewTableAddGroup').bootstrapTable(table_options_task_overview_addgroup);
    }


    /**
     * 删除记录
     */
    function deleteTask(hId) {
        function del() {
            if (confirm("真的要删除该条记录吗?")) {
                return true;
            } else {
                return false;
            }
        }

        if (del()) {
            $.ajax({
                type: "get",
                url: "/task/deleteTasks",
                dataType: "json",
                data: {
                    id: hId
                },
                success: function (data) {
//                    var dataJson = JSON.stringify(data);
//                    var result = JSON.parse(dataJson);
//                    if (result.code == 0) {
                    if (data != 0) {
                        alert("删除成功！");
                        $('#taskOverviewTable').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        result;
                    }
                },
                error: function () {
                    alert("删除异常");

                }
            });
        } else {
            return
        }
    }

    //    请求参数
    function queryParamsAddGroup(params) {
        return {
            limit: params.limit,
            offset: params.offset,
            taskId: $('#task_id').val(),
            isSelected: $('#isSelected').val(),
//            taskName: $('#taskName').val(),
//            description: $('#taskDesc').val(),
        };
    }

    //    请求参数
    function queryParams(params) {
        return {
            limit: params.limit,
            offset: params.offset,
            taskId: $('#taskId').val(),
            taskName: $('#taskName').val(),
            description: $('#taskDesc').val(),
        };
    }

    //初始化table
    $('#taskOverviewTable').bootstrapTable(table_options_task_overview);


    /**
     * 清空表单内容
     * @param formID
     */
    function clearFormData(formID) {
        $("#" + formID)[0].reset();
        $("#" + formID).find("label").css("display", "none");
    }

    function clearFormData1(formID) {
        $('#ModalSetCrontab').hide();
    }

    /**
     * 添加hosts
     */
    function insertTasks() {
        if (modifyCheck()) {
            var da = $("#insertTask").serialize();
            $.ajax({
                type: "POST",
                url: "/task/addTasks",
                dataType: "json",
                data: $("#insertTask").serialize(),
                success: function (data) {
//                    var dataJson = JSON.stringify(data);
//                    var result = JSON.parse(dataJson);
//                    if (result.code == 0) {
                    if (data != 0) {
                        alert("添加成功！");
                        $('#addModal').modal('hide');
                        clearFormData("insertTask");
                        $('#taskOverviewTable').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        result;
                    }
                },
                error: function () {
                    alert("添加异常！");

                }
            });
        }

    }
    /**
     * 更新host
     */
    function updateTask() {
        if (modifyCheck()) {
            var da = $("#insertHost").serialize();
            $.ajax({
                type: "POST",
                url: "/task/updateTask",
                dataType: "json",
                data: $("#insertTask").serialize(),
                success: function (data) {
//                    var dataJson = JSON.stringify(data);
//                    var result = JSON.parse(dataJson);
//                    if (result.code == 0) {
                    if (data != 0) {
                        alert("修改成功！");
                        $('#addModal').modal('hide');
                        clearFormData("updateHost");
                        $('#taskOverviewTable').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        result;
                    }
                },
                error: function () {
                    alert("添加异常！");

                }
            });

        }

    }

    function addGroupByTask(groupId) {
//        alert($('#task_id').val());
        document.getElementById("addGroupButton").disabled = true;

        $.ajax({
            type: "get",
            url: "/task/addGroupByTask",
            dataType: "json",
            data: {
                groupId: groupId,
                taskId: $('#task_id').val()
            },
            success: function (data) {
                if (data != 0) {
                    alert("添加成功");
                    noSelectedList($('#task_id').val());
                    return;
                } else {
                    alert("添加异常");
                    result;
                }
            },
            error: function () {
                alert("添加异常！");

            }
        });


    }

    function deleteGroupByTask(groupId) {

//        alert($('#task_id').val());

        $.ajax({
            type: "get",
            url: "/task/deleteGroupByTask",
            dataType: "json",
            data: {
                groupId: groupId,
                taskId: $('#task_id').val()
            },
            success: function (data) {
                if (data != 0) {
                    alert("删除成功");
                    selectedList($('#task_id').val());
                    return;
                } else {
                    alert("删除异常");
                    result;
                }
            },
            error: function () {
                alert("添加异常！");

            }
        });


    }

    /**
     * 检查输入内容是否正确
     * @returns {boolean}
     */
    function check() {
        var reg_ip = /^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$/;
        var reg_host = /^\S(\S+\.)+\S+$/;

        if ($("#taskNameAdd").val() == "") {
            alert("任务名称不能为空！");
            return false;
        } else if ($("#taskDescAdd").val() == "") {
            alert("任务描述不能为空！");
            return false;
        } else if ($("#reporter").val() == "") {
            alert("任务描述不能为空！");
            return false;
        }
// else if (!(reg_host.test($("#host").val()))) {
//            $("#ipInfo").css("display", "none");
//            $("#hostInfo").text("域名格式不正，例：mall.autohome.com.cn");
//            $("#hostInfo").css("display", "block");
//            return false
//        }
        else {
//            $("#insertTask").find("label").css("display", "none");
            return true;
        }
    }


    function modifyCheck() {
        var reg_ip = /^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$/;
        var reg_host = /^\S(\S+\.)+\S+$/;

        if ($("#taskNameAdd").val() == "" || $("#taskNameAdd").val().length == 0) {
            alert("任务名称不能为空！");
            return false;
        }
        else if ($("#taskDescAdd").val() == "" || $("#taskDescAdd").val().length == 0) {
            alert("描述不能为空！");
            return false;
        } else if ($("#reporter").val() == "" || $("#reporter").val().length == 0) {
            alert("‘发送报告给’不能为空！");
            return false;
        } else if ($("#sendType").val() == "" || $("#sendType").val().length == 0) {
            alert("请选择消息发送类型！");
            return false;
        }else if ($("#person").val() == "" || $("#person").val().length == 0) {
            alert("负责人不能为空！");
            return false;
        }
        else {
            $("#insertTask").find("label").css("display", "none");
            return true;
        }
    }

    /**
     * 设置点击事件
     */
    $(function () {
        $('#btn_reset').click(function () {
            $('#taskId').val('');
            $('#taskName').val('');
            $('#taskDesc').val('');
            $('#taskOverviewTable').bootstrapTable('refresh');
        });
        $('#btn_query').click(function () {
            var queryId = Number($('#taskId').val());
            if (isNaN(queryId)) {
                alert("ID 输入必须为数字！");
                return
            }
            $('#taskOverviewTable').bootstrapTable('destroy');
            $('#taskOverviewTable').bootstrapTable(table_options_task_overview);
        });
    });

//    task已经添加的hosts设置
    function selectedSetHost(taskId) {
        $('#taskId_h').val(taskId);
        $('#isSelectedHost').val(1);
        $('#taskOverviewTableSetHost').bootstrapTable('destroy');
        $('#taskOverviewTableSetHost').bootstrapTable(table_options_task_overview_setHost);
    }

    //    task未添加的hosts设置
    function selectedNoSetHost(taskId) {
        $('#taskId_h').val(taskId);
        $('#isSelectedHost').val(0);
        $('#taskOverviewTableSetHost').bootstrapTable('destroy');
        $('#taskOverviewTableSetHost').bootstrapTable(table_options_task_overview_setHost);
    }


//    task已经添加的测试用户设置
    function selectedSetTUser(taskId) {
        $('#taskId_tu').val(taskId);
        $('#isSelectedTUser').val(1);
        $('#taskOverviewTableSetTUser').bootstrapTable('destroy');
        $('#taskOverviewTableSetTUser').bootstrapTable(table_options_task_overview_setTUsers);
    }

    //    task未添加的测试用户设置
    function selectedNoSetTUser(taskId) {
        $('#taskId_tu').val(taskId);
        $('#isSelectedTUser').val(0);
        $('#taskOverviewTableSetTUser').bootstrapTable('destroy');
        $('#taskOverviewTableSetTUser').bootstrapTable(table_options_task_overview_setTUsers);
    }

    function selectedList(taskId) {
        $('#isSelected').val(1);
        $('#task_id').val(taskId);
        $('#taskOverviewTableAddGroup').bootstrapTable('destroy');
        $('#taskOverviewTableAddGroup').bootstrapTable(table_options_task_overview_addgroup);
    }

    function noSelectedList(taskId) {
        $('#isSelected').val(0);
        $('#task_id').val(taskId);
        $('#taskOverviewTableAddGroup').bootstrapTable('destroy');
        $('#taskOverviewTableAddGroup').bootstrapTable(table_options_task_overview_addgroup);
    }

    function runTask(taskId) {
        var reqUrl = "http://test-admin.dd01.fun/task/runTask";
        $.ajax({
            type: "Get",
            url: reqUrl,
            dataType: "json",
            data: {
                taskId: taskId
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
//                if (data != 0) {
                    alert("任务已提交，等待执行！");
                    return;
                } else {
                    alert(result.message);
                    result;
                }
            },
            error: function () {
                alert("天啊噜，执行任务异常了，咋办，赶紧找保安哥哥看看！");

            }
        });
    }


    function openTestLinkConfig(taskId) {
        getTestlinkConfigByTaskId(taskId);
        $('#task_id2').val(taskId);
        $('#ModalSetCrontab').modal('show');
    }

    function setCrontab(taskId) {
        getExistingCrontab(taskId);
        $('#task_id2').val(taskId);
        $('#ModalSetCrontab').modal('show');
    }

    function getCrontab() {
        var crontabTime = null;
        var selected = $("input[name='optionsRadios']:checked").val();
//        if (selected == "option1") {
//            var optionsRadios1Hour = $('#optionsRadios1Hour').val();
//            var optionsRadios1Minute = $('#optionsRadios1Minute').val();
//
//            if (optionsRadios1Hour == null || optionsRadios1Hour == "" || optionsRadios1Hour.length == 0
//                    || optionsRadios1Hour == undefined){
//                alert("请求设置小时时间！");
//                return;
//            }
//            if (optionsRadios1Minute == null || optionsRadios1Minute == "" || optionsRadios1Minute.length == 0
//                    || optionsRadios1Minute == undefined){
//                optionsRadios1Minute = 0;
//            }
//            crontabTime = "0 " + $('#optionsRadios1Minute').val() + " " + $('#optionsRadios1Hour').val() + " 1/1 * * *";
//        }
//        if (selected == "option2") {
//            crontabTime = "0 " + "0 " + "0/" + $('#inputHour2').val() + " * * ? *";
//        }
        if (selected == "option1") {
//            crontabTime = "0 " + "0/" + $('#inputMinute2').val() + " *" + " * * ? *";
            crontabTime = "0";
        } else if(selected == "option2"){
            if( !crontabCheck()){
                alert("表达式不合法！");
                return;
            }else {
//                alert("表达式合法");
                crontabTime = $('#inputCrontab').val();
            }
//            return;
        }else {
            alert("请选择定时任务类型！");
            return;
        }
        var taskId = $('#task_id2').val();

        $.ajax({
            type: "get",
            url: "/task/setCrontab",
            dataType: "json",
            data: {
                taskId: taskId,
                crontabTime: crontabTime
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    alert(result.message);
                    $('#ModalSetCrontab').modal('hide');
                    return;
                } else {
                    alert(result.message);
                    return;
                }
            },
            error: function () {
                alert("添加异常！");
                return;
            }
        });
    }

    function showHostModifyContent(taskId) {
        $.ajax({
            type: "get",
            url: "/task/queryTaskList",
            dataType: "json",
            data: {
                taskId: taskId
            },
            success: function (data) {
                if (data != 0) {
                    $('#addModal').modal('show');
                    $('#myModalLabelAdd').text("修改任务");
                    $('#taskNameAdd').val(data.rows[0].taskName);
                    $('#taskDescAdd').val(data.rows[0].description);
                    $('#reporter').val(data.rows[0].reporter);
                    $('#person').val(data.rows[0].taskPerson);
                    $('#sendType').val(data.rows[0].sendType);
                    $('#btn_modifyTask').css("display", "block");
                    $('#btn_addTask').css('display', 'none');
                    $('#update_id').val(data.rows[0].taskId);
                    return;
                } else {
                    alert("修改任务异常");
                    result;
                }
            },
            error: function () {
                alert("添加异常！");

            }
        });

    }


    function showAddTaskModal() {
        $('#myModalLabelAdd').text("添加任务");
        $('#btn_modifyTask').css("display", "none");
        $('#btn_addTask').css('display', 'block');
        $('#taskNameAdd').val("");
        $('#taskDescAdd').val("");
        $('#reporter').val("");
        $('#person').val("");
//        $('#sendType').val("请选择消息发送类型");
        $('#addModal').modal('show');
        document.getElementById("sendType")[9].selected=true;
    }
    function showModifyTaskModal(taskId) {
        $('#myModalLabelModify').text("修改任务");
        $('#btn_modifyTask').css("display", "block");
        $('#btn_addTask').css('display', 'none');
        $('#modifyModal').modal('show');
    }

    function setHost(taskId) {
        $('#taskOverviewTableSetHost').bootstrapTable('destroy');
        $('#taskId_h').val(taskId);
        $('#isSelectedHost').val(1);
        $('#setHostModal').modal('show');
        $('#myTabHost li:eq(0) a').tab('show');
        $('#taskOverviewTableSetHost').bootstrapTable(table_options_task_overview_setHost);
    }

//    设置测试用户
    function setTUser(taskId) {
        $('#taskOverviewTableSetTUser').bootstrapTable('destroy');
        $('#taskId_tu').val(taskId);
        $('#isSelectedTUser').val(1);
        $('#setTUserModal').modal('show');
        $('#mytabtuser li:eq(0) a').tab('show');
        $('#taskOverviewTableSetTUser').bootstrapTable(table_options_task_overview_setTUsers);
    }

    function setTaskVariable(taskId) {
        $('#addTaskVariableButton').css("display", "none")
        $('#OverviewTableTaskVariableList').bootstrapTable('destroy');
        $('#v_taskId').val(taskId);
        $('#v_isSelected').val(1);
        $('#TaskVariableListModal').modal('show');
        $('#variableTab li:eq(0) a').tab('show');
        $('#OverviewTableTaskVariableList').bootstrapTable(table_options_overview_getVariableListFromTask);
    }

    function selectedAddVariableList(tId) {
        $('#addTaskVariableButton').css("display", "none")
        $('#v_isSelected').val(1);
        $('#v_taskId').val(tId);
        $('#OverviewTableTaskVariableList').bootstrapTable('destroy');
        $('#OverviewTableTaskVariableList').bootstrapTable(table_options_overview_getVariableListFromTask);
    }

    function queryParamsGetVariableListTask(params) {
        return {
            limit: params.limit,
            offset: params.offset,
            isSelected: $('#v_isSelected').val(),
            taskId: $('#v_taskId').val(),
            searchText: params.search,
        };
    }

    function selectedNotAddVariableList(tId) {
        $('#addTaskVariableButton').css("display", "block")
        $('#v_isSelected').val(0);
        $('#v_taskId').val(tId);
        $('#OverviewTableTaskVariableList').bootstrapTable('destroy');
        $('#OverviewTableTaskVariableList').bootstrapTable(table_options_overview_getVariableListFromTask);
    }



    function clearModalData(formID) {
        $("#" + formID)[0].reset();
        $("#" + formID).find("label").css("display", "none");
        $('#taskOverviewTableSetHost').bootstrapTable('destroy');
    }

    /**
     * 删除task关联的变量
     */
    function moveTaskVariable(vId) {
        function del() {
            if (confirm("真的要移除该条记录吗?")) {
                return true;
            } else {
                return false;
            }
        }

        if (del()) {
            $.ajax({
                type: "get",
                url: "/variable/moveTaskVariable",
//                dataType: "json",
                data: {
                    taskId: $('#v_taskId').val(),
                    vId: vId
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert(result.message);
                        $('#OverviewTableTaskVariableList').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        result;
                    }
                },
                error: function () {
                    alert(result.message);

                }
            });
        } else {
            return;
        }
    }

    /**
     * 添加 变量到列表
     * @param vId
     */
    function addToTaskVariableList(vId) {

        $.ajax({
            type: "POST",
            url: "/variable/addToTaskVariableList",
            dataType: "json",
            data: {
                taskId: $('#v_taskId').val(),
                vId: vId
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    alert("添加成功！");
                    $('#OverviewTableTaskVariableList').bootstrapTable('refresh');
                    return;
                } else {
                    alert(result.message);
                    return;
                }
            },
            error: function () {
                alert("添加异常！");
            }
        });

    }

    function addTaskHost(hostId,obj) {
        obj.style.display = "none";
        $.ajax({
            type: "GET",
            url: "/task/addSetHost",
            dataType: "json",
            data: {
                task_id: $('#taskId_h').val(),
                host_id: hostId
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    alert(result.message);

//                    $('#taskOverviewTable').bootstrapTable('refresh');
                    return;
                } else {
//                    obj.disabled=false
                    obj.style.display = "block";
                    alert(result.message);
                    return;
                }
            },
            error: function () {
                obj.style.display = "block";
                alert("添加异常！");
            }
        });
    }

    function deleteSetHost(hostId) {
        function del() {
            if (confirm("真的要删除该条记录吗?")) {
                return true;
            } else {
                return false;
            }
        }
        if (del()) {
            $.ajax({
                type: "get",
                url: "/task/deleteSetHost",
                dataType: "json",
                data: {
                    task_id: $('#taskId_h').val(),
                    host_id: hostId
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert(result.message);
                        $('#taskOverviewTableSetHost').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        result;
                    }
                },
                error: function () {
                    alert(result.message);

                }
            });
        } else {
            return;
        }
    }


    function addTaskTUser(tuId,obj) {
        obj.style.display = "none";
        $.ajax({
            type: "GET",
            url: "/task/addTaskTUser",
            dataType: "json",
            data: {
                taskId: $('#taskId_tu').val(),
                tuId: tuId
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    alert(result.message);

//                    $('#taskOverviewTable').bootstrapTable('refresh');
                    return;
                } else {
//                    obj.disabled=false
                    obj.style.display = "block";
                    alert(result.message);
                    return;
                }
            },
            error: function () {
                obj.style.display = "block";
                alert("添加异常！");
            }
        });
    }

    function deleteTaskTUser(tuId) {
        function del() {
            if (confirm("真的要删除该条记录吗?")) {
                return true;
            } else {
                return false;
            }
        }
        if (del()) {
            $.ajax({
                type: "get",
                url: "/task/deleteTaskTUser",
                dataType: "json",
                data: {
                    taskId: $('#taskId_tu').val(),
                    tuId: tuId
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert(result.message);
                        $('#taskOverviewTableSetTUser').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        result;
                    }
                },
                error: function () {
                    alert(result.message);

                }
            });
        } else {
            return;
        }
    }


    function queryParamsSetHosts(params) {
        return {
            limit: params.limit,
            offset: params.offset,
            taskId: $('#taskId_h').val(),
            isSelected: $('#isSelectedHost').val(),
            searchText: params.search,
        };
    }


    function queryParamsSetTUsers(params) {
        return {
            limit: params.limit,
            offset: params.offset,
            taskId: $('#taskId_tu').val(),
            isSelected: $('#isSelectedTUser').val(),
            searchText: params.search,
        };
    }




    $(document).ready(function() {
        $('input[type=radio][name=optionsRadios]').change(function() {
            if (this.value == 'option1') {
//                $('#inputCrontab').css("disabled", "true");
                $('#inputCrontab').attr("disabled",true);
            }
            else if (this.value == 'option2') {
//                $('#inputCrontab').css("disabled", "none");
                $('#inputCrontab').attr("disabled",false);
            }
        });
    });

</script>


<script type="application/javascript" title="定时任务脚本">



    function CronExpressionValidator() {
    }
    /**
     * 定时任务字符串 检查
     **/
    function crontabCheck() {
        value = $('#inputCrontab').val();
        // split and test length
        var expressionArray = value.split(" ");
        var len = expressionArray.length;

        if ((len != 6) && (len != 7)) {
            return false;
        }

        // check only one question mark
        var match = value.match(/\?/g);
        if (match != null && match.length > 1) {
            return false;
        }

        // check only one question mark
        var dayOfTheMonthWildcard = "";

        // if appropriate length test parts
        // [0] Seconds 0-59 , - * /
        if (CronExpressionValidator.isNotWildCard(expressionArray[0], /[\*]/gi)) {
            if (!CronExpressionValidator.segmentValidator("([0-9\\\\,-\\/])", expressionArray[0], [0, 59], "seconds")) {
                return false;
            }
        }

        // [1] Minutes 0-59 , - * /
        if (CronExpressionValidator.isNotWildCard(expressionArray[1], /[\*]/gi)) {
            if (!CronExpressionValidator.segmentValidator("([0-9\\\\,-\\/])", expressionArray[1], [0, 59], "minutes")) {
                return false;
            }
        }

        // [2] Hours 0-23 , - * /
        if (CronExpressionValidator.isNotWildCard(expressionArray[2], /[\*]/gi)) {
            if (!CronExpressionValidator.segmentValidator("([0-9\\\\,-\\/])", expressionArray[2], [0, 23], "hours")) {
                return false;
            }
        }

        // [3] Day of month 1-31 , - * ? / L W C
        if (CronExpressionValidator.isNotWildCard(expressionArray[3], /[\*\?]/gi)) {
            if (!CronExpressionValidator.segmentValidator("([0-9LWC\\\\,-\\/])", expressionArray[3], [1, 31], "days of the month")) {
                return false;
            }
        } else {
            dayOfTheMonthWildcard = expressionArray[3];
        }

        // [4] Month 1-12 or JAN-DEC , - * /
        if (CronExpressionValidator.isNotWildCard(expressionArray[4], /[\*]/gi)) {
            expressionArray[4] = CronExpressionValidator.convertMonthsToInteger(expressionArray[4]);
            if (!CronExpressionValidator.segmentValidator("([0-9\\\\,-\\/])", expressionArray[4], [1, 12], "months")) {
                return false;
            }
        }

        // [5] Day of week 1-7 or SUN-SAT , - * ? / L C #
        if (CronExpressionValidator.isNotWildCard(expressionArray[5], /[\*\?]/gi)) {
            expressionArray[5] = CronExpressionValidator.convertDaysToInteger(expressionArray[5]);
            if (!CronExpressionValidator.segmentValidator("([0-9LC#\\\\,-\\/])", expressionArray[5], [1, 7], "days of the week")) {
                return false;
            }
        } else {
            if (dayOfTheMonthWildcard == String(expressionArray[5])) {
                return false;
            }
        }

        // [6] Year empty or 1970-2099 , - * /
        if (len == 7) {
            if (CronExpressionValidator.isNotWildCard(expressionArray[6], /[\*]/gi)) {
                if (!CronExpressionValidator.segmentValidator("([0-9\\\\,-\\/])", expressionArray[6], [1970, 2099], "years")) {
                    return false;
                }
            }
        }
        return true;
    }

    // ----------------------------------
    // isNotWildcard 静态方法;
    // ----------------------------------
    CronExpressionValidator.isNotWildCard = function(value, expression) {
        var match = value.match(expression);
        return (match == null || match.length == 0) ? true : false;
    }

    // ----------------------------------
    // convertDaysToInteger 静态方法;
    // ----------------------------------
    CronExpressionValidator.convertDaysToInteger = function(value) {
        var v = value;
        v = v.replace(/SUN/gi, "1");
        v = v.replace(/MON/gi, "2");
        v = v.replace(/TUE/gi, "3");
        v = v.replace(/WED/gi, "4");
        v = v.replace(/THU/gi, "5");
        v = v.replace(/FRI/gi, "6");
        v = v.replace(/SAT/gi, "7");
        return v;
    }

    // ----------------------------------
    // convertMonthsToInteger 静态方法;
    // ----------------------------------
    CronExpressionValidator.convertMonthsToInteger = function(value) {
        var v = value;
        v = v.replace(/JAN/gi, "1");
        v = v.replace(/FEB/gi, "2");
        v = v.replace(/MAR/gi, "3");
        v = v.replace(/APR/gi, "4");
        v = v.replace(/MAY/gi, "5");
        v = v.replace(/JUN/gi, "6");
        v = v.replace(/JUL/gi, "7");
        v = v.replace(/AUG/gi, "8");
        v = v.replace(/SEP/gi, "9");
        v = v.replace(/OCT/gi, "10");
        v = v.replace(/NOV/gi, "11");
        v = v.replace(/DEC/gi, "12");
        return v;
    }

    // ----------------------------------
    // segmentValidator 静态方法;
    // ----------------------------------
    CronExpressionValidator.segmentValidator = function(expression, value, range, segmentName) {
        var v = value;
        var numbers = new Array();

        // first, check for any improper segments
        var reg = new RegExp(expression, "gi");
        if (!reg.test(v)) {
            return false;
        }

        // check duplicate types
        // check only one L
        var dupMatch = value.match(/L/gi);
        if (dupMatch != null && dupMatch.length > 1) {
            return false;
        }

        // look through the segments
        // break up segments on ','
        // check for special cases L,W,C,/,#,-
        var split = v.split(",");
        var i = -1;
        var l = split.length;
        var match;

        while (++i < l) {
            // set vars
            var checkSegment = split[i];
            var n;
            var pattern = /(\w*)/;
            match = pattern.exec(checkSegment);

            // if just number
            pattern = /(\w*)\-?\d+(\w*)/;
            match = pattern.exec(checkSegment);

            if (match
                    && match[0] == checkSegment
                    && checkSegment.indexOf("L") == -1
                    && checkSegment.indexOf("l") == -1
                    && checkSegment.indexOf("C") == -1
                    && checkSegment.indexOf("c") == -1
                    && checkSegment.indexOf("W") == -1
                    && checkSegment.indexOf("w") == -1
                    && checkSegment.indexOf("/") == -1
                    && (checkSegment.indexOf("-") == -1 || checkSegment
                            .indexOf("-") == 0) && checkSegment.indexOf("#") == -1) {
                n = match[0];

                if (n && !(isNaN(n)))
                    numbers.push(n);
                else if (match[0] == "0")
                    numbers.push(n);
                continue;
            }
            // includes L, C, or w
            pattern = /(\w*)L|C|W(\w*)/i;
            match = pattern.exec(checkSegment);

            if (match
                    && match[0] != ""
                    && (checkSegment.indexOf("L") > -1
                    || checkSegment.indexOf("l") > -1
                    || checkSegment.indexOf("C") > -1
                    || checkSegment.indexOf("c") > -1
                    || checkSegment.indexOf("W") > -1 || checkSegment
                            .indexOf("w") > -1)) {

                // check just l or L
                if (checkSegment == "L" || checkSegment == "l")
                    continue;
                pattern = /(\w*)\d+(l|c|w)?(\w*)/i;
                match = pattern.exec(checkSegment);

                // if something before or after
                if (!match || match[0] != checkSegment) {
                    continue;
                }

                // get the number
                var numCheck = match[0];
                numCheck = numCheck.replace(/(l|c|w)/ig, "");

                n = Number(numCheck);

                if (n && !(isNaN(n)))
                    numbers.push(n);
                else if (match[0] == "0")
                    numbers.push(n);
                continue;
            }

            var numberSplit;

            // includes /
            if (checkSegment.indexOf("/") > -1) {
                // take first #
                numberSplit = checkSegment.split("/");

                if (numberSplit.length != 2) {
                    continue;
                } else {
                    n = numberSplit[0];

                    if (n && !(isNaN(n)))
                        numbers.push(n);
                    else if (numberSplit[0] == "0")
                        numbers.push(n);
                    continue;
                }
            }

            // includes #
            if (checkSegment.indexOf("#") > -1) {
                // take first #
                numberSplit = checkSegment.split("#");

                if (numberSplit.length != 2) {
                    continue;
                } else {
                    n = numberSplit[0];

                    if (n && !(isNaN(n)))
                        numbers.push(n);
                    else if (numberSplit[0] == "0")
                        numbers.push(n);
                    continue;
                }
            }

            // includes -
            if (checkSegment.indexOf("-") > 0) {
                // take both #
                numberSplit = checkSegment.split("-");

                if (numberSplit.length != 2) {
                    continue;
                } else if (Number(numberSplit[0]) > Number(numberSplit[1])) {
                    continue;
                } else {
                    n = numberSplit[0];

                    if (n && !(isNaN(n)))
                        numbers.push(n);
                    else if (numberSplit[0] == "0")
                        numbers.push(n);
                    n = numberSplit[1];

                    if (n && !(isNaN(n)))
                        numbers.push(n);
                    else if (numberSplit[1] == "0")
                        numbers.push(n);
                    continue;
                }
            }

        }
        // lastly, check that all the found numbers are in range
        i = -1;
        l = numbers.length;

        if (l == 0)
            return false;

        while (++i < l) {
            // alert(numbers[i]);
            if (numbers[i] < range[0] || numbers[i] > range[1]) {
                return false;
            }
        }
        return true;
    }


    function getTestlinkConfigByTaskId(taskId) {
        $.ajax({
            type: "get",
            url: "/task/getExistingCrontab",
            dataType: "json",
            data: {
                taskId: taskId
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.crontab != null) {
                    if (result.crontab == "0") {
                        document.getElementById('optionsRadios1').checked = "checked";
                        $('#inputCrontab').val("");
                    } else {
                        document.getElementById('optionsRadios2').checked = "checked";
                        $('#inputCrontab').val(result.crontab);
                    }
                    return;
                } else {
                    document.getElementById('optionsRadios1').checked = "checked";
                    $('#inputCrontab').val("");
//                    alert(result.message);
                    result;
                }
            },
            error: function () {
                alert(result.message);

            }
        });
    }

    /**
     * 获取存在的定时任务
     * @param taskId
     */
    function getExistingCrontab(taskId) {
        $.ajax({
            type: "get",
            url: "/task/getExistingCrontab",
            dataType: "json",
            data: {
                taskId: taskId
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.crontab != null) {
                    if(result.crontab == "0"){
                        document.getElementById('optionsRadios1').checked = "checked";
                        $('#inputCrontab').val("");
                    }else {
                        document.getElementById('optionsRadios2').checked = "checked";
                        $('#inputCrontab').val(result.crontab);
                    }
                    return;
                } else {
                    document.getElementById('optionsRadios1').checked = "checked";
                    $('#inputCrontab').val("");
//                    alert(result.message);
                    result;
                }
            },
            error: function () {
                alert(result.message);

            }
        });
    }
</script>

<script type="application/javascript">


//    变量检查
    function variableCheck() {
        if ($("#v_name").val() == "" || $("#v_name").val().length == 0) {
            $("#name_Info").text("变量名称不能为空！");
            $("#name_Info").css("display", "block");
            return false;
        }
        else if ($("#v_value").val() == "" || $("#v_value").val().length == 0) {
            $("#name_Info").css("display", "none");
            $("#value_Info").text("变量的值不能为空！");
            $("#value_Info").css("display", "block");
            return false;
        }  else {
            $("#variableForm").find("label").css("display", "none");
            return true;
        }
    }

    /**
     * 显示添加任务窗口
     **/
    function showAddTaskVariableModalByTastId(tId) {
        $('#v_tId').val(tId)
        $('#taskVariableModalLabel').text("设置任务变量");
        $('#v_but_modify').css("display", "none");
        $('#v_but_add').css('display', 'block');
        $('#taskVariableModal').modal('show');
    }

    /**
     * 添加任务变量
     */
    function addTaskVariable() {
        if (variableCheck()) {
            $.ajax({
                type: "POST",
                url: "/variable/addTaskVariable",
                dataType: "json",
                data: {
                    type : 2,
                    taskId : $("#v_tId").val(),
                    v_name : $("#v_name").val(),
                    v_value : $("#v_value").val()
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("添加成功！");
                        $('#taskVariableModal').modal('hide');
                        clearFormData("taskVariableForm");
                        $('#OverviewTableTaskVariableList').bootstrapTable('refresh');
                        return;
                    } else if (result.code == 2010202) {
                        alert(result.message);
                        return;
                    } else {
                        alert(result.message);
                        return;
                    }
                },
                error: function () {
                    alert("添加异常！");
                }
            });
        }

    }


    /**
     * 显示修改窗口
     */
    function showUpdateTaskVariableModal(vId) {
        $('#vId').val(vId);
        $('#taskVariableModalLabel').text("修改变量变量");
        $("#v_but_add").css("display", "none");
        $('#v_but_modify').css('display', 'block');
        $.ajax({
            type: "get",
            url: "/variable/getGlobalVariableById",
            dataType: "json",
            data: {
                vId: vId
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    clearFormData("taskVariableForm");
                    $('#vId').val(result.data.vId);
                    $('#v_name').val(result.data.v_name);
                    $('#v_value').val(result.data.v_value);
                    $('#v_type').val(result.data.type);
                    $('#v_tId').val($("#v_taskId").val());
                    $('#taskVariableModal').modal('show');

                    return;
                } else {
                    alert(result.message);
                    result;
                }
            },
            error: function () {
                alert("修改异常！");
            }
        });

    }


/**
 *
 * 添加任务变量内容
 */
function updateTaskVariable() {
        if (variableCheck()) {
            $.ajax({
                type: "POST",
                url: "/variable/updateTaskVariable",
                dataType: "json",
                data: {
                    taskId : $("#v_tId").val(),
                    v_name : $("#v_name").val(),
                    v_value : $("#v_value").val(),
                    type : $("#v_type").val(),
                    vId : $("#vId").val()
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("修改成功！");
                        $('#taskVariableModal').modal('hide');
                        clearFormData("taskVariableForm");
                        $('#OverviewTableTaskVariableList').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        return;
                    }
                },
                error: function () {
                    alert("添加异常！");

                }
            });

        }

    }

</script>

</body>
</html>
