
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <title>测试用户管理</title>

    <script src="/js/jquery.js"></script>

    <script src="/bootstrap3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/bootstrap3/css/bootstrap.min.css">

    <script src="/bootstrap-table/dist/bootstrap-table.js"></script>
    <link rel="stylesheet" href="/bootstrap-table/dist/bootstrap-table.css">
    <script src="/bootstrap-table/dist/bootstrap-table-zh-CN.js"></script>

</head>

<body>

<div class="modal fade" id="userModal" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form id="userForm" action="#" method="post">

                <input name="tuId" id="tuId" type="hidden">

                <div class="modal-body">
                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">账号：
                            </button>
                        </div>
                        <input name="tname" id="tname" type="text" class="form-control" style="border-radius: 4px">
                        <label id="nameInfo" style="display: block;color: red;font-size: 10px"></label>
                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">密码：
                            </button>
                        </div>
                        <input name="tpwd" id="tpwd" type="text" class="form-control" style="border-radius: 4px">
                        <label id="pwdInfo" style="display: block;color: red;font-size: 10px"></label>

                    </div>

                    <%--类型--%>
                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">类型：
                            </button>
                        </div>
                        <select class="form-control" name="type" id="type" style="border-radius: 4px">
                            <%--<option value="0" >请选择登陆端的类型</option>--%>
                            <%--<option value="1" >Android端登陆</option>--%>
                            <%--<option value="2" >IOS端登陆</option>--%>
                            <%--<option value="3" >Web端登陆</option>--%>
                        </select>
                        <label id="typeInfo" style="display: block;color: red;font-size: 10px"></label>
                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">描述：
                            </button>
                        </div>
                        <input name="description" id="description" type="text" class="form-control"
                               style="border-radius: 4px">
                    </div>

                    <button type="button" style="border: none;outline:none;" data-toggle="collapse" data-target="#collapse">
                        自定义参数(Web端不需要填写，移动端不填将使用默认参数)<span class="caret"></span></button>
                    <hr style="border:1px;border-top:1px solid #ccc; margin-top: 0px;" />


                    <div id="collapse" class="panel-collapse collapse">

                        <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                            <div class="input-group-btn">
                                <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                        style="border: none;width: 90px">URL：
                                </button>
                            </div>
                            <input name="loginUrl" id="loginUrl" type="text" class="form-control" style="border-radius: 4px">
                        </div>

                        <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                            <div class="input-group-btn">
                                <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                        style="border: none;width: 90px">eid：
                                </button>
                            </div>
                            <input name="eid" id="eid" type="text" class="form-control" style="border-radius: 4px">
                        </div>

                        <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                            <div class="input-group-btn">
                                <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                        style="border: none;width: 90px">deviceType：
                                </button>
                            </div>
                            <input name="deviceType" id="deviceType" type="text" class="form-control" style="border-radius: 4px">
                        </div>

                        <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                            <div class="input-group-btn">
                                <button type="button" disabled="disabled" class="btn btn-default col-sm-2"
                                        style="border: none;width: 90px">deviceId：
                                </button>
                            </div>
                            <input name="deviceId" id="deviceId" type="text" class="form-control" style="border-radius: 4px">
                        </div>

                        <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                            <div class="input-group-btn">
                                <button type="button" disabled="disabled" class="btn btn-default"
                                        style="border: none;width: 90px">ua：
                                </button>
                            </div>
                            <input name="ua" id="ua" type="text" class="form-control" style="border-radius: 4px">
                        </div>

                    </div>
                    <%--<div class="input-group" id="validCodeGroup"--%>
                         <%--style="margin-right: 50px;margin-bottom: 20px;display: none">--%>
                        <%--<div class="input-group-btn">--%>
                            <%--<button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"--%>
                                    <%--style="border: none;float: left">验证码：--%>
                            <%--</button>--%>
                            <%--<input name="validCode" id="validCode" type="text" class="form-control"--%>
                                   <%--style="border-radius: 4px;width: 18% ; float: left">--%>
                            <%--<img id="baseImg" width="80" height="30"--%>
                                 <%--style="margin-left: 15px;margin-top: 4px;float: left"--%>
                                 <%--src="" onclick="getImgLoginCode()"/>--%>
                            <%--<div id="codeInfo"--%>
                                 <%--style="display: block;float: left;color: red;margin-top: 10px;margin-left: 5px;font-size: 10px">--%>
                                <%--点击图片刷新--%>
                            <%--</div>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            onclick="clearFormData('userForm')"
                            style="float: right;margin-left: 10px;margin-right: 20px">关闭
                    </button>

                    <button id="btn_add" style="float: right;display: none" type="button" class="btn btn-primary"
                            onclick="insertUser()">添加
                    </button>
                    <button id="btn_modify" style="float:right" type="button" class="btn btn-primary"
                            onclick="updateUser()">修改
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="panel-body" style="padding-bottom:0px;">

    <div class="panel panel-default">
        <div class="panel-heading">查询条件</div>
        <div class="panel-body">
            <form id="mallTestUserSearch" class="form-horizontal">
                <div class="form-group" style="margin-top:15px">

                    <%--<label class="control-label col-sm-1" style="width: 80px">用户ID</label>--%>
                    <%--<div class="col-sm-1">--%>
                    <%--<input type="text" class="form-control" id="query_tuId">--%>
                    <%--</div>--%>

                    <label class="control-label col-sm-1" style="width: 90px">用户名</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="query_tuName">
                    </div>

                    <label class="control-label col-sm-1" style="width: 90px">描述</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="query_description">
                    </div>

                    <div class="col-sm-3" style="text-align:left;">
                        <button type="button" style="margin-left:50px" id="btn_query" class="btn btn-primary">查询
                        </button>
                        <button type="button" style="margin-left:50px" id="btn_reset" class="btn btn-default">重置
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div id="toolbar" class="btn-group">
        <button id="add" type="button" class="btn btn-success" onclick="showAddUserModal()">
            <span class="glyphicon glyphicon-plus"></span>新增
        </button>
    </div>

    <table id="taskOverviewTable"></table>

</div>

<script type="application/javascript">
    var table_options_task_overview = {
        url: '/tuser/queryTUserList',
        method: 'get',                      //请求方式（*）
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: false,                     //是否启用排序
        toolbar: '#toolbar',                 //工具按钮使用日期
        sortOrder: "asc",                   //排序方式
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        showColumns: false,                  //是否显示所有的列
        height: 700,
        pageNumber: 1,                       //初始化加载第一页，默认第一页
        pageSize: 10,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
        clickToSelect: true,                // 是否启用点击选中行
//        showExport: true,                     //是否显示导出
        showRefresh: true,
//        exportDataType: "all",              //basic', 'all', 'selected'.
//        silent: true,
        queryParams: queryParams, //参数
        columns: [
            {
                field: 'tuId',
                title: 'ID',
                align: "center",
                width: "20",
            },
            {
                field: 'tname',
                title: '用户名',
                align: "center",
                width: "100",
            },
            {
                field: 'type',
                title: '登陆类型',
                align: "center",
                width: "150",
                formatter: function (value, row, index) {
                    if (row.type == 1) {
                        return "Android端";
                    } else if (row.type == 2) {
                        return "IOS端";
                    } else if (row.type == 3) {
                        return "Web端登陆";
                    } else {
                        return "异常账号";
                    }
                }
            }, {
                field: 'description',
                title: '描述',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if (row.description == null) {
                        return " ";
                    } else {
                        return row.description;
                    }
                }

            }, {
                field: '操作',
                title: '操作',
                align: "center",
                width: "90",
                formatter: function (value, row, index) {
                    var group_begin = '<div class="btn-group"> <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">更多 <span class="caret"></span> </button>' +
                            '<ul class="dropdown-menu">';

                    var a1 = '<li><a href="javascript:void(0);" onclick="showUserModifyModal(\'' + row.tuId + '\' )">修改</a></li>';
//                    <li class="divider"></li>
                    var a2 = '<li><a href="javascript:void(0);" onclick="deleteUser( \'' + row.tuId + '\' )">删除</a></li>';
                    var group_end = '</ul></div>';

                    return group_begin + a1 + a2 + group_end;
//                    if (row.type == 1) {
//                        return group_begin + a1 + a2 + group_end;
//                    } else {
//                        return group_begin + a2 + group_end;
//                    }

                }
            }],

    };


    /**
     * 删除记录
     */
    function deleteUser(tuId) {
        function del() {
            if (confirm("真的要删除该条记录吗?")) {
                return true;
            } else {
                return false;
            }
        }

        if (del()) {
            $.ajax({
                type: "get",
                url: "/tuser/deleteUser",
                dataType: "json",
                data: {
                    tuId: tuId
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("删除成功！");
                        $('#taskOverviewTable').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        result;
                    }
                },
                error: function () {
                    alert("删除异常");

                }
            });
        } else {
            return;
        }
    }


    /**
     * 添加用户窗口
     */
    function showAddUserModal() {
        $('#myModalLabel').text("添加测试账号");
        var el = document.getElementById("type");
        el.options[0] = new Option("请选择登陆设备类型", '0',true);

        for(var i = 1;i<= 3;i++){
            if ( i == 1) {
                el.options[i] = new Option("Android端", '1');
            } else if (i == 2) {
                el.options[i] = new Option("IOS端", '2');
            } else if (i == 3) {
                el.options[i] = new Option("Web端", '3');
            }
        }

        $('#btn_modify').css("display", "none");
        $('#btn_add').css('display', 'block');
        $('#collapse').collapse('hide');

        $('#userModal').modal('show');
    }

    /**
     * 显示修改窗口
     */
    function showUserModifyModal(id) {
        $('#myModalLabel').text("修改账号");
        $("#btn_add").css("display", "none");
        $('#btn_modify').css('display', 'block');
        $.ajax({
            type: "get",
            url: "/tuser/getUserById",
            dataType: "json",
            data: {
                id: id
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    clearFormData("userForm");
                    $('#collapse').collapse('show');
                    $('#tuId').val(result.data.tuId);
                    $('#tname').val(result.data.tname);
                    $('#tpwd').val(result.data.tpwd);
                    $('#description').val(result.data.description);
                    var el = document.getElementById("type");
                    if (result.data.type >3 || result.data.type <1){
                        el.options[0] = new Option("类型异常", '0');
                    }else {
                        $('#type').empty();
                        if (result.data.type == 1) {
                            el.options[0] = new Option("Android端", '1');
                        }else if (result.data.type == 2){
                            el.options[0] = new Option("IOS端", '2');
                        }else {
                            el.options[0] = new Option("Web端", '3');
                        }
//                        $('#type').val(result.data.type);
                    }
                    $('#loginUrl').val(result.data.loginUrl);
                    $('#eid').val(result.data.eid);
                    $('#deviceType').val(result.data.deviceType);
                    $('#deviceId').val(result.data.deviceId);
                    $('#ua').val(result.data.ua);

                    $('#collapse').collapse('show');
                    $('#userModal').modal('show');
                    return;
                } else {
                    alert(result.message);
                    result;
                }
            },
            error: function () {
                alert("添加异常！");
            }
        });

    }

    //    请求参数
    function queryParams(params) {
        return {
            limit: params.limit,
            offset: params.offset,
//            query_tuId: $('#query_tuId').val(),
            query_tuName: $('#query_tuName').val(),
            query_description: $('#query_description').val(),
            query_type: 1,
        };
    }

    //初始化table
    $('#taskOverviewTable').bootstrapTable(table_options_task_overview);


    /**
     * 清空表单内容
     * @param formID
     */
    function clearFormData(formID) {
        $("#" + formID)[0].reset();
        $("#" + formID).find("label").css("display", "none");
//        $('#baseImg').attr('src', 'data:image/gif;base64,');
//        $("#validCodeGroup").css("display", "none");


    }

    /**
     * 添加账号
     */
    function insertUser() {
        if (check()) {
            $.ajax({
                type: "POST",
                url: "/tuser/addTUser",
                dataType: "json",
                data: $("#userForm").serialize(),
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("添加成功！");
                        $('#userModal').modal('hide');
                        clearFormData("userForm");
                        $("#validCodeGroup").css("display", "none");
                        $('#taskOverviewTable').bootstrapTable('refresh');
                        return;
                    } else if (result.code == 2010202) {
                        alert(result.message);
                        $('#validCodeGroup').css("display", "block");
                        getImgLoginCode();
                        return;
                    } else {
                        alert(result.message);
                        return;
//                        $('#validCodeGroup').css("display", "block");
//                        getImgLoginCode();
                    }
                },
                error: function () {
                    alert("添加异常！");
                }
            });
        }

    }

    /**
     * 更新host
     */
    function updateUser() {
        if (check()) {
            $.ajax({
                type: "POST",
                url: "/tuser/updateUser",
                dataType: "json",
                data: $("#userForm").serialize(),
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("修改成功！");
                        $('#userModal').modal('hide');
                        clearFormData("userForm");
                        $('#taskOverviewTable').bootstrapTable('refresh');
                        return;
                    } else if (result.code == 2010202) {
                        alert(result.message);
                        $('#validCodeGroup').css("display", "block");
                        getImgLoginCode();
                        return;
                    } else {
                        alert(result.message);
                        return;
//                        $('#validCodeGroup').css("display", "block");
//                        getImgLoginCode();
                    }
                },
                error: function () {
                    alert("添加异常！");

                }
            });

        }

    }


    /**
     * 检查输入内容是否正确
     * @returns {boolean}
     */
    function check() {
        if ($("#tname").val() == "" || $("#tname").val().length == 0) {
            $("#nameInfo").text("用户名不能为空！");
            $("#nameInfo").css("display", "block");
            return false;
        }
        else if ($("#tpwd").val() == "" || $("#tpwd").val().length == 0) {
            $("#nameInfo").css("display", "none");
            $("#pwdInfo").text("用户密码不能为空！");
            $("#pwdInfo").css("display", "block");
            return false;
        } else if ($("#type").val() == "" || $("#type").val().length == 0) {
            $("#nameInfo").css("display", "none");
            $("#pwdInfo").css("display", "none");
            $("#typeInfo").text("用户类型没有选择！");
            $("#typeInfo").css("display", "block");
            return false;
        } else {
            $("#userForm").find("label").css("display", "none");
            return true;
        }
    }


//    /**
//     * 获取验证码
//     */
//    function getImgLoginCode() {
//
//        $.ajax({
//            type: "GET",
//            url: "/tuser/getImgLoginCode",
//            dataType: "json",
//            success: function (data) {
//                var dataJson = JSON.stringify(data);
//                var result = JSON.parse(dataJson);
//                if (result.code == 0) {
//                    $('#baseImg').attr('src', 'data:image/gif;base64,' + result.data);
//                    return;
//                } else {
//                    alert(result.message);
//                    result;
//                }
//            },
//            error: function () {
//                alert("添加异常！");
//            }
//        });
//    }

    /**
     * 设置点击事件
     */
    $(function () {
        var i = 1;
        $('#btn_reset').click(function () {
//            $('#query_tuId').val('');
            $('#query_tuName').val('');
            $('#query_description').val('');
            $('#taskOverviewTable').bootstrapTable('refresh');
        });
        $('#btn_query').click(function () {
//            var queryId = Number($('#query_tuId').val());
//            if (isNaN(queryId)) {
//                alert("ID 输入必须为数字！");
//                return
//            }
            $('#taskOverviewTable').bootstrapTable('destroy');
            $('#taskOverviewTable').bootstrapTable(table_options_task_overview);
        });
    });

</script>


</body>
</html>
