
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <title>自定义变量管理</title>

    <script src="/js/jquery.js"></script>

    <script src="/bootstrap3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/bootstrap3/css/bootstrap.min.css">

    <script src="/bootstrap-table/dist/bootstrap-table.js"></script>
    <link rel="stylesheet" href="/bootstrap-table/dist/bootstrap-table.css">
    <script src="/bootstrap-table/dist/bootstrap-table-zh-CN.js"></script>

    <style type="text/css">
        .popover {
            word-break: break-all;
        }

        .tooltip-inner {
            word-break: break-all;
            max-width: 300px;
            font-size: 12px;
            /*color: #151515;*/
            /*background-color: #7B7B7B;*/
        }
    </style>
</head>

<body>

<!-- 模态框（Modal） -->
<div class="modal fade" id="VariableValueModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" style="font-size: 15px">
                    查看内容信息
                </h4>
            </div>
            <div class="modal-body" id="modal-body-variable-value" style="word-wrap: break-word;">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="text-align: center">关闭
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<div class="modal fade" id="variableModal" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                <h4 class="modal-title" id="variableModalLabel"></h4>
            </div>
            <form id="variableForm" action="#" method="post">

                <input name="vId" id="vId" type="hidden">

                <div class="modal-body">
                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">名称：
                            </button>
                        </div>
                        <input name="v_name" id="v_name" type="text" class="form-control" style="border-radius: 4px">
                        <label id="name_Info" style="display: block;color: red;font-size: 10px"></label>
                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">value：
                            </button>
                        </div>
                        <input name="v_value" id="v_value" type="text" class="form-control" style="border-radius: 4px">
                        <label id="value_Info" style="display: block;color: red;font-size: 10px"></label>

                    </div>

                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">类型：
                            </button>
                        </div>
                        <select type="text" class="form-control" name="type" id="type" style="border-radius: 4px">
                            <%--<option value="0" selected>全局变量</option>--%>
                            <%--<option value="2">任务变量</option>--%>
                        </select>
                        <label id="type_Info" style="display: block;color: red;font-size: 10px"></label>

                    </div>


                    <div class="input-group" style="margin-right: 50px;margin-bottom: 20px">
                        <div class="input-group-btn">
                            <button type="button" disabled="disabled" class="btn btn-default dropdown-toggle"
                                    style="border: none">描述：
                            </button>
                        </div>
                        <input name="description" id="description" type="text" class="form-control"
                               style="border-radius: 4px">
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"
                            onclick="clearFormData('variableForm')"
                            style="float: right;margin-left: 10px;margin-right: 20px">关闭
                    </button>

                    <button id="btn_add" style="float: right;display: none" type="button" class="btn btn-primary"
                            onclick="insertGlobalVariable()">添加
                    </button>
                    <button id="btn_modify" style="float:right" type="button" class="btn btn-primary"
                            onclick="updateGlobalVariable()">修改
                    </button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="panel-body" style="padding-bottom:0px;">

    <div class="panel panel-default">
        <div class="panel-heading">查询条件</div>
        <div class="panel-body">
            <form id="globalVariableSearch" class="form-horizontal">
                <div class="form-group" style="margin-top:15px">

                    <label class="control-label col-sm-1" style="width: 80px">ID</label>
                    <div class="col-sm-1">
                        <input type="text" class="form-control" id="query_id">
                    </div>

                    <label class="control-label col-sm-1" style="width: 90px">变量名称</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="query_name">
                    </div>

                    <label class="control-label col-sm-1" style="width: 90px">描述</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="query_description">
                    </div>

                    <label class="control-label col-sm-1" style="width: 90px">描述</label>
                    <div class="col-sm-2">
                        <select type="text" class="form-control" id="query_type">
                            <option value="0" selected>全局变量</option>
                            <option value="2">任务变量</option>
                        </select>
                    </div>

                </div>
                <div style="text-align:left; padding-top: 10px">
                    <button type="button" style="margin-left:50px" id="btn_query" class="btn btn-primary">查询
                    </button>
                    <button type="button" style="margin-left:50px" id="btn_reset" class="btn btn-default">重置
                    </button>
                </div>
            </form>
        </div>
    </div>


    <div id="toolbar" class="btn-group">
        <button id="add" type="button" class="btn btn-success" onclick="showAddVariableModal()">
            <span class="glyphicon glyphicon-plus"></span>新增
        </button>
    </div>

    <table id="taskOverviewTable"></table>

</div>

<script type="application/javascript">
    var table_options_variable_overview = {
        url: '/variable/getGlobalVariableList',
        method: 'post',                      //请求方式（*）
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: false,                     //是否启用排序
        toolbar: '#toolbar',                 //工具按钮使用日期
        sortOrder: "asc",                   //排序方式
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        showColumns: false,                  //是否显示所有的列
        height: 700,
        pageNumber: 1,                       //初始化加载第一页，默认第一页
        pageSize: 10,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
        clickToSelect: true,                // 是否启用点击选中行
//        showExport: true,                     //是否显示导出
        showRefresh: true,
//        exportDataType: "all",              //basic', 'all', 'selected'.
//        silent: true,
        queryParams: queryParams, //参数
        columns: [
            {
                field: 'vId',
                title: 'ID',
                align: "center",
                width: "20",
            },
            {
                field: 'v_name',
                title: '变量名称',
                align: "center",
                width: "200",
            },
            {
                field: 'v_value',
                title: '设置的值',
                align: "center",
                width: "400",
                formatter: function (value, row, index) {
                    if (row.v_value == null || row.v_value == "" || row.v_value.length == 0
                        || row.v_value == undefined) {
                        return "-";
                    } else {
                        if (row.v_value.length > 60) {
                            return '<span data-toggle="tooltip" title="' + row.v_value + '"> ' + row.v_value.substr(0, 60) + ' </span> '
                                + '  ... <a href="javascript:void(0);" onclick="openContentModal(\'' + html_encode(row.v_value) + '\');">查看</a>';
                        } else {
                            return row.v_value;
                        }

                    }
                }
            },
            {
                field: 'type',
                title: '变量类型',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if (row.type == 0) {
                        return "系统变量";
                    } else if (row.type == 1) {
                        return "群组变量";
                    } else if (row.type == 2) {
                        return "任务变量";
                    } else {
                        return "未知类型";
                    }
                }
            }, {
                field: 'description',
                title: '描述',
                align: "center",
                width: "100",
                formatter: function (value, row, index) {
                    if (row.description == null) {
                        return " ";
                    } else {
                        return row.description;
                    }
                }

            }, {
                field: '操作',
                title: '操作',
                align: "center",
                width: "90",
                formatter: function (value, row, index) {
                    if (row.type == 0) {
                        var group_begin = '<div class="btn-group"> <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">更多 <span class="caret"></span> </button>' +
                                '<ul class="dropdown-menu" style="min-width:100px;text-align:center">';
                        var a1 = '<li><a href="javascript:void(0);" onclick="showVariableModifyModal(\'' + row.vId + '\' )">修改</a></li>';
                        var a2 = '<li><a href="javascript:void(0);" onclick="delGlobalVariable( \'' + row.vId + '\' )">删除</a></li>';
                        var group_end = '</ul></div>';
                        return group_begin + a1 + a2 + group_end;
                    } else if (row.type == 2) {
                        var button = '<div class="btn-group"> ' +
                                '<button type="button" class="btn btn-info" onclick="showVariableModifyModal(\'' + row.vId + '\' )" >查看引用</button></div>';
                        return button
                    }
                }
            }],
        onPostBody: function () {
//            $("[data-toggle='popover']").popover();
            $("[data-toggle='tooltip']").tooltip();
        }

    };

    /**
     * 打开内容窗口
     * @param contentInfo
     */
    function openContentModal(contentInfo) {

        $('#modal-body-variable-value').html("");
        contentInfo = html_decode(contentInfo);
        contentInfo = contentInfo.split("/n");
        if (contentInfo == null || contentInfo == undefined || contentInfo == "") {
            return;
        }
        var data = "";
        for (var i = 0; i < contentInfo.length; i++) {
            if (contentInfo[i] != null && contentInfo[i] != undefined && contentInfo[i] != '') {
                data += '<p class="text-primary">' + contentInfo[i] + '</p>';
            }
        }
        $('#modal-body-variable-value').append(data);
        $('#VariableValueModal').modal("show");
    }

    /**
     * 删除记录
     */
    function delGlobalVariable(vId) {
        function del() {
            if (confirm("真的要删除该条记录吗?")) {
                return true;
            } else {
                return false;
            }
        }

        if (del()) {
            $.ajax({
                type: "get",
                url: "/variable/delGlobalVariableById",
                dataType: "json",
                data: {
                    vId: vId
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("删除成功！");
                        $('#taskOverviewTable').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        result;
                    }
                },
                error: function () {
                    alert("删除异常");

                }
            });
        } else {
            return;
        }
    }


    /**
     * 添加用户窗口
     */
    function showAddVariableModal() {
        $('#variableModalLabel').text("添加全局变量");
        var el = document.getElementById("type");
        el.options[0] = new Option("全局变量", '0', true);

//        for(var i = 1;i<= 3;i++){
//            if ( i == 1) {
//                el.options[i] = new Option("Android端", '1');
//            } else if (i == 2) {
//                el.options[i] = new Option("IOS端", '2');
//            } else if (i == 3) {
//                el.options[i] = new Option("Web端", '3');
//            }
//        }

        $('#btn_modify').css("display", "none");
        $('#btn_add').css('display', 'block');
        $('#variableModal').modal('show');
    }

    /**
     * 显示修改窗口
     */
    function showVariableModifyModal(id) {
        $('#variableModalLabel').text("修改变量信息");
        $("#btn_add").css("display", "none");
        $('#btn_modify').css('display', 'block');
        $.ajax({
            type: "get",
            url: "/variable/getGlobalVariableById",
            dataType: "json",
            data: {
                vId: id
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    clearFormData("variableForm");
                    $('#vId').val(result.data.vId);
                    $('#v_name').val(result.data.v_name);
                    $('#v_value').val(result.data.v_value);
                    $('#description').val(result.data.description);
                    var el = document.getElementById("type");
                        $('#type').empty();
                    if (result.data.type == 0) {
                        el.options[0] = new Option("系统变量", '0');
                    } else if (result.data.type == 1) {
                        el.options[0] = new Option("群组变量", '1');
                        }else if (result.data.type == 2){
                        el.options[0] = new Option("任务变量", '2');
                    } else {
                        el.options[0] = new Option("类型异常", '999');
                        }
                    $('#variableModal').modal('show');
                    return;
                } else {
                    alert(result.message);
                    result;
                }
            },
            error: function () {
                alert("添加异常！");
            }
        });

    }

    //    请求参数
    function queryParams(params) {
        return {
            limit: params.limit,
            offset: params.offset,
            vId: $('#query_id').val(),
            v_name: $('#query_name').val(),
            description: $('#query_description').val(),
            variable_type: $('#query_type').val(),
        };
    }

    //初始化table
    $('#taskOverviewTable').bootstrapTable(table_options_variable_overview);


    /**
     * 清空表单内容
     * @param formID
     */
    function clearFormData(formID) {
        $("#" + formID)[0].reset();
        $("#" + formID).find("label").css("display", "none");
//        $('#baseImg').attr('src', 'data:image/gif;base64,');
//        $("#validCodeGroup").css("display", "none");


    }

    /**
     * 添加账号
     */
    function insertGlobalVariable() {
        if (check()) {
            $.ajax({
                type: "POST",
                url: "/variable/addGlobalVariable",
                dataType: "json",
                data: $("#variableForm").serialize(),
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("添加成功！");
                        $('#variableModal').modal('hide');
                        clearFormData("variableForm");
                        $("#validCodeGroup").css("display", "none");
                        $('#taskOverviewTable').bootstrapTable('refresh');
                        return;
                    } else if (result.code == 2010202) {
                        alert(result.message);
                        $('#validCodeGroup').css("display", "block");
                        getImgLoginCode();
                        return;
                    } else {
                        alert(result.message);
                        return;
                    }
                },
                error: function () {
                    alert("添加异常！");
                }
            });
        }

    }

    /**
     * 更新host
     */
    function updateGlobalVariable() {
        if (check()) {
            $.ajax({
                type: "POST",
                url: "/variable/updateGlobalVariable",
                dataType: "json",
                data: $("#variableForm").serialize(),
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("修改成功！");
                        $('#variableModal').modal('hide');
                        clearFormData("variableForm");
                        $('#taskOverviewTable').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        return;
                    }
                },
                error: function () {
                    alert("添加异常！");

                }
            });

        }

    }


    /**
     * 检查输入内容是否正确
     * @returns {boolean}
     */
    function check() {
        if ($("#v_name").val() == "" || $("#v_name").val().length == 0) {
            $("#name_Info").text("变量名称不能为空！");
            $("#name_Info").css("display", "block");
            return false;
        }
        else if ($("#v_value").val() == "" || $("#v_value").val().length == 0) {
            $("#name_Info").css("display", "none");
            $("#value_Info").text("变量的值不能为空！");
            $("#value_Info").css("display", "block");
            return false;
        } else if ($("#type").val() == "" || $("#type").val().length == 0) {
            $("#name_Info").css("display", "none");
            $("#value_Info").css("display", "none");
            $("#type_Info").text("变量类型没有选择！");
            $("#type_Info").css("display", "block");
            return false;
        } else {
            $("#variableForm").find("label").css("display", "none");
            return true;
        }
    }


//    /**
//     * 获取验证码
//     */
//    function getImgLoginCode() {
//
//        $.ajax({
//            type: "GET",
//            url: "/tuser/getImgLoginCode",
//            dataType: "json",
//            success: function (data) {
//                var dataJson = JSON.stringify(data);
//                var result = JSON.parse(dataJson);
//                if (result.code == 0) {
//                    $('#baseImg').attr('src', 'data:image/gif;base64,' + result.data);
//                    return;
//                } else {
//                    alert(result.message);
//                    result;
//                }
//            },
//            error: function () {
//                alert("添加异常！");
//            }
//        });
//    }

    /**
     * 设置点击事件
     */
    $(function () {
        $('#btn_reset').click(function () {
            document.getElementById("globalVariableSearch").reset();
            $('#taskOverviewTable').bootstrapTable('refresh');
        });
        $('#btn_query').click(function () {
            $('#taskOverviewTable').bootstrapTable('destroy');
            $('#taskOverviewTable').bootstrapTable(table_options_variable_overview);
        });
    });

    /**
     * html encode
     * @param str
     * @returns {string}
     */
    function html_encode(str) {
        var s = "";
        if (str == "" || str.length == 0) {
            return "";
        }
        s = str.replace(/&/g, "&amp;");
        s = s.replace(/</g, "&lt;");
        s = s.replace(/>/g, "&gt;");
        s = s.replace(/\\/g, "&xg;");
        s = s.replace(/ /g, "&nbsp;");
        s = s.replace(/\'/g, "&#aabb;");
        s = s.replace(/\"/g, "&#ccdd;");
        s = s.replace(/\n/g, " ");
        return s;
    }

    /**
     * html decode
     * @param str
     * @returns {string}
     */
    function html_decode(str) {
        var s = "";
        if (str.length == 0) return "";
        s = str.replace(/&amp;/g, "&");   //2
        s = s.replace(/&lt;/g, "<");
        s = s.replace(/&gt;/g, ">");
        s = s.replace(/&xg;/g, '\\');
        s = s.replace(/&nbsp;/g, " ");
        s = s.replace(/&#aabb;/g, "\'");
        s = s.replace(/&#ccdd;/g, "\"");
        s = s.replace(/<br>/g, " ");
        return s;
    }

</script>


</body>
</html>
