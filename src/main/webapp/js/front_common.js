﻿function clearModalData(formID, tableId) {
    $("#" + formID)[0].reset();
    $("#" + formID).find("label").css("display", "none");
    $("#" + tableId).bootstrapTable('destroy');
}

/**
 * 清空表单内容
 * @param formID
 */
function clearFormData(formID) {
    $("#" + formID)[0].reset();
    $("#" + formID).find("label").css("display", "none");
}