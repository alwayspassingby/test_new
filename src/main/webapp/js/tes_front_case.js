﻿/**
 * 更新用户
 * @param formId
 */
function updateTestCase(formId) {
    var type = $("#type").val();
    if (type == 0) {
        insertTestCase(true, formId);
    } else if (type == 1) {
        insertTestCase(false, formId);
    } else {
        alert("用例类型不正确！")
    }

}

/**
 * 添加用户
 * @param isAddHttpCase 为true 代表添加http 用例 , false为添加rpc用例
 * @param formId
 */
function insertTestCase(isAddHttpCase, formId) {
    if (check(isAddHttpCase)) {
        var datalist = $("#" + formId).serializeArray();
        if (isAddHttpCase) {
            // 获取headers的值
            var obj_h = new Object();
            obj_h.name = "httpHeaders";
            obj_h.value = get_InputOrTextarea_Value("httpHeaders", true);
            datalist.push(obj_h);

            // 获取请求参数的值
            var obj_p = new Object();
            obj_p.name = "requestParameter";
            var select = $("#parameterType").val();
            if (select != 2 && select != 3) {
                obj_p.value = get_InputOrTextarea_Value("parameter", true);
            } else {
                obj_p.value = get_InputOrTextarea_Value("parameter", false);
            }
            datalist.push(obj_p);

            // 获取httpCookies的值
            var obj_c = new Object();
            obj_c.name = "httpCookies";
            obj_c.value = get_InputOrTextarea_Value("httpCookies", true);
            datalist.push(obj_c);
        } else {

            // 获取请求参数的值
            var obj_p = new Object();
            obj_p.name = "requestParameter";
            obj_p.value = get_InputOrTextarea_Value("parameter", true);
            datalist.push(obj_p);
        }
        $.ajax({
            type: "POST",
            url: "/main/addCase",
            dataType: "json",
            data: $.param(datalist),
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    alert(data.message);
                    var aId = $("#aId").val();
                    if (aId == null || aId == '' || aId == undefined) {
                        document.getElementById(formId).reset();
                        document.getElementById('httpHeaders').innerHTML = '';
                        document.getElementById('httpCookies').innerHTML = '';
                        document.getElementById('parameter').innerHTML = '';

                    }
                    return;
                } else {
                    alert(data.message);
                    return;
                }
            },
            error: function () {
                alert("添加用例异常，请联系管理员！");

            }
        });


    }
}


// 如：{Name:'摘取天上星',position:'IT技术'}
// ps:注意将同名的放在一个数组里
function getFormJson(form) {
    var o = {};
    var a = form;
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
}


/**
 * 动态添加 div ,div标签包含input
 * @param divId div
 */
function addInputs(divId) {
    var count = new Date().getTime() + Math.round(Math.random() * 100);
    var id = divId + "_" + count;
// <div class="col-lg-6 col-md-6 add-div" >
//         <textarea class="form-control" id="" name="httpCookies" rows="5"></textarea>
//         </div>

    var inputDiv = '<div class="input-group col-lg-6 add-div" id="' + id + '">' +
        '<input type="text" class="col-lg-4 add-input" placeholder="请输入参数的Key" name="" value=""/>' +
        '<input type="text" class="col-lg-7 add-input" placeholder="请求输入参数的value"  name="" value=""/>' +
        '<button class="btn btn-danger btn-mini del-btn"  onclick="delInput(\'' + id + '\')" type="button">删除</button>' +
        '</div>';
    $("#" + divId).append(inputDiv);
}

/**
 *  添加文本域
 * @param divId
 */
function addTextarea(divId) {
    var count = new Date().getTime() + Math.round(Math.random() * 100);
    var id = divId + "_" + count;
// <div class="col-lg-6 col-md-6 add-div" >
//         <textarea class="form-control" id="" name="httpCookies" rows="5"></textarea>
//         </div>

    var inputDiv = '<div class="col-lg-6 col-md-6 add-div" id="' + id + '">' +
        '<textarea class="form-control" placeholder="请求输入参数的内容" rows="5"></textarea>' +
        '</div>';
    $("#" + divId).append(inputDiv);
}

/**
 * 添加输入框并且赋值
 * @param divId
 * @param data
 */
function addInputsAndinitValue(divId, data) {
    if (data == null || data == undefined || data == '') {
        return;
    }
    var result = JSON.parse(data);
    if (result.length == 0) {
        return;
    }

    for (var i = 0; i < result.length; i++) {
        var id = divId + "_" + i;
        var key = result[i].name;
        var value = result[i].value;
        var inputDiv = '<div class="input-group col-lg-6 add-div" id="' + id + '">' +
            '<input type="text" class="col-lg-4 add-input" placeholder="请输入参数的Key" name="" value="' + key + '"/>' +
            '<input type="text" class="col-lg-7 add-input" placeholder="请求输入参数的value"  name="" value="' + value + '"/>' +
            '<button class="btn btn-danger btn-mini del-btn"  onclick="delInput(\'' + id + '\')" type="button">删除</button>' +
            '</div>';
        $("#" + divId).append(inputDiv);
    }
}

/**
 * 添加文本域框并且赋值
 * @param divId
 * @param data
 */
function addTextareaAndinitValue(divId, data) {
    if (data == null || data == undefined || data == '') {
        return;
    }
    var result = JSON.parse(data);
    var count = new Date().getTime() + Math.round(Math.random() * 100);
    var id = divId + "_" + count;
    if (result.length == 0) {
        var inputDiv = '<div class="col-lg-6 col-md-6 add-div" id="' + id + '">' +
            '<textarea class="form-control" placeholder="请求输入参数的内容" rows="5"></textarea>' +
            '</div>';
        $("#" + divId).append(inputDiv);
        return
    } else {
        var value = result[0].value;
        var inputDiv = '<div class="col-lg-6 col-md-6 add-div" id="' + id + '">' +
            '<textarea class="form-control" rows="5" value="">' + value + '</textarea>' +
            '</div>';
        $("#" + divId).append(inputDiv);
        return
    }

}

/**
 * 根据ID删除指定的 元素
 * @param divId
 */
function delInput(divId) {
    $("#" + divId).remove();
}

/**
 * 用例检查
 * @returns {boolean}
 */
function check(isHttp) {

    // if ($("#groupId").val() == ""  || $("#groupId").val() == 0) {
    //     alert("请选择用例所属组！");
    //     return false;
    // }
    if (isHttp) {
        if (!check_input("httpCookies")) {
            return false;
        }

        if ($("#httpURL").val() == "") {
            alert("请输入接口URL！");
            return false;
        }
        if ($("#httpRequestType").val() == "") {
            alert("请选择请求方式！");
            return false;
        }

        if (!check_input("httpHeaders")) {
            return false;
        }

        if (!check_input("httpCookies")) {
            return false;
        }

        if ($("#parameterType").val() == "") {
            alert("请选择参数类型！");
            return false;
        }

        if ($("#parameterType").val() != 2 && $("#parameterType").val() != 3) {
            if (!check_input("parameter")) {
                return false;
            }
        }
    } else {
        // if ($("#rpcGroup").val() == "") {
        //     alert("请输入RPC接口分组！");
        //     return false;
        // }
        if ($("#rpcService").val() == "") {
            alert("请输入接口rpcService ！");
            return false;
        }
        if ($("#rpcMethod").val() == "") {
            alert("请输入 rpcMethod ！");
            return false;
        }

        if (!check_input("parameter")) {
            return false;
        }
    }

    if ($("#description").val() == "") {
        alert("请输入用例说明！");
        return false;
    }

    var type = $("#expectedType").val();

    if (type == "") {
        alert("请选择检查结果类型！");
        return false;
    }

    if (type == 0 || type == 3) {
        if ($("#expression").val() == "") {
            alert("请选择表达式！");
            return false;
        }

        if ($("#expectedValue").val() == "") {
            alert("请输入期望结果 ！");
            return false;
        }
    } else if (type == 2) {

    } else {
        if ($("#expression").val() == "") {
            alert("请选择表达式！");
            return false;
        }

        if ($("#expectedKey").val() == "") {
            alert("请输入检查点 ！");
            return false;
        }

        if ($("#expectedValue").val() == "") {
            alert("请输入期望结果 ！");
            return false;
        }
    }
    return true;

}


/**
 * 选择改变
 * @param obj
 */
function selectChange(obj) {
    var grade = obj.options[obj.selectedIndex].value;
    if (grade == 0 || grade == 3) {
        document.getElementById("expression").disabled = false;
        document.getElementById("expectedKey").disabled = true;
        document.getElementById("expectedValue").disabled = false; //选择框
    } else if (grade == 2) {
        document.getElementById("expression").disabled = true;
        document.getElementById("expectedKey").disabled = true;
        document.getElementById("expectedValue").disabled = true; //选择框
    }
    // 否则，grade ==1 || grade == null || grade.length == 0 || grade == undefined
    else {
        document.getElementById("expression").disabled = false;
        document.getElementById("expectedKey").disabled = false;
        document.getElementById("expectedValue").disabled = false;
    }
}


function setAuthPanelShow(obj) {
    var grade = obj.options[obj.selectedIndex].value;
    if (grade == 0) {
        $('#authCollapse').collapse('hide');
        document.getElementById('auth_false').checked = "checked";
    } else {
        document.getElementById('auth_false').checked = "checked";
        $('#authCollapse').collapse('show');
    }

}

function selectParameterChange(obj, divId) {
    var select = obj.options[obj.selectedIndex].value;

    // raw-json类型 和 raw-text类型
    if (select == 2 || select == 3) {
        // $("#addParameterInput").css("display", "none");
        $("#addParameterInput").attr('disabled', true);
        $("#" + divId).empty();
        addTextarea(divId);

    }
    //如果还不是就是form类型
    else {
        // $("#addParameterInput").css("display", "block");
        $("#addParameterInput").attr('disabled', false);
        $("#" + divId).empty();
    }
}

/**
 * 检查输入框是否为空
 * @param divId
 * @returns {boolean}
 */
function check_input(divId) {
    // 判断参数列表
    var select = $("#parameterType").val();
    // if (select == 1) {
    try {
        var nodes = $("#" + divId + " > div");
        if (nodes.length != 0) {
            for (var i = 0; i < nodes.length; i++) {
                var n = nodes[i].getElementsByTagName("input");
                var key = n[0].value;
                var value = n[1].value;
                if (key == "" || key == null || key == undefined) {
                    n[0].style.borderColor = "#ff0000";
                    alert("参数名称不能为空!");
                    return false;
                }
                n[0].style.borderColor = "";
            }
            return true;
        }
        return true;
    } catch (e) {
        alert(e.message);
        return false;
    }
    // }else {
    //     return true;
    // }

}

/**
 * 获取动态生成的input列表
 * @param divId
 * @param isInput true 为input，false 为textarea
 * @returns {*}
 */
function get_InputOrTextarea_Value(divId, isInput) {
    // 判断参数列表
    var objArrary = new Array();
    try {
        var nodes = $("#" + divId + " > div");
        if (nodes.length == 0) {
            return null;
        }
        for (var i = 0; i < nodes.length; i++) {
            var obj = new Object();
            if (isInput) {
                var inputs = nodes[i].getElementsByTagName("input");
                var key = inputs[0].value;
                var value = inputs[1].value;
                if (key == "" || key == null || key == undefined) {
                    key = "";
                }
                if (value == "" || value == null || value == undefined) {
                    value = "";
                }
                obj.name = key;
                obj.value = value;
            } else {
                var textarea = nodes[i].getElementsByTagName("textarea");
                var value = textarea[0].value;
                if (value == "" || value == null || value == undefined) {
                    value = "";
                }
                obj.name = "";
                obj.value = value;
            }
            objArrary.push(obj)
        }
        return JSON.stringify(objArrary);
    } catch (e) {
        alert(e.message);
        return null;
    }
}


/**
 * 初始化修改页中的数据
 * @param data
 */
function initPageContentFromCaseData(data) {
    if (data == null || data == undefined || data == '') {
        alert("获取用例异常，id = " + "${caseId}")
    } else {
        if (data.type == 0 || data.type == 1) {
            $("#caseId").val(data.aId);
            if (data.type == 0) {
                $("#rpcServiceGroup").remove();
                $("#rpcMethodGroup").remove();
                $("#rpcVersionGroup").remove();
                $("#rpcGroup_Group").remove();
//                   设置http 用例 url
                $("#httpURL").val(data.httpURL);

                $("#type").val("0");

//                   设置http请求类型
                var httpRequestType = data.httpRequestType;
                $("#httpRequestType option").each(function () {
                    if ($(this).val() == httpRequestType) {
                        $(this).attr("selected", true);
                        return;
                    }
                });

                // 设置参数类型
                var parameterType = data.parameterType;
                $("#parameterType option").each(function () {
                    if ($(this).val() == parameterType) {
                        $(this).attr("selected", true);
                        return;
                    }
                });

                //设置请求头
                addInputsAndinitValue("httpHeaders", data.httpHeaders);

                //设置cookies
                addInputsAndinitValue("httpCookies", data.httpCookies)

                // 设置参数
                if (parameterType == 1) {
                    addInputsAndinitValue("parameter", data.requestParameter);
                } else {
                    $("#addParameterInput").attr('disabled', true);
                    addTextareaAndinitValue("parameter", data.requestParameter)
                }
                $("#expectedType").append("<option value='0'>Http状态码断言</option>");

            }
            //否则就turbo接口
            else {
                $("#type").val("1");

                $("#URLGroup").remove();

                $("#typeGroup").remove();

                $("#httpRequestTypeGroup").remove();

                $("#HttpHeadersGroup").remove();

                $("#CookiesGroup").remove();

                $("#httpCookies").remove();

                $("#parameterTypeGroup").remove();


                // 设置rpc接口版本号
                $("#rpcService").val(data.rpcService);

                // 设置rpc方法
                $("#rpcMethod").val(data.rpcMethod);

                //设置rpc 接口版本号
                $("#rpcVersion").val(data.rpcVersion);

                // 设置rpc接口分组
                $("#rpcGroup").val(data.rpcGroup);

                // 设置参数
                addInputsAndinitValue("parameter", data.requestParameter);

                $("#expectedType").append("<option value='2'>调通即成功</option>");
            }

            $("#aId").val(data.aId);

//               设置描述
            $("#description").val(data.description);

//                设置断言类型
            var expectedType = data.expectedType;
            $("#expectedType option").each(function () {
                if ($(this).val() == expectedType) {
                    $(this).attr("selected", true);
                    return;
                }
            });

            // 设置检查点
            $("#expectedKey").val(data.expectedKey);
            //设置断言的字段
            $("#expectedValue").val(data.expectedValue);
            //设置表达式
            var expression = data.expression;
            $("#expression option").each(function () {
                if ($(this).val() == expression) {
                    $(this).attr("selected", true);
                    return;
                }
            });

            if (expectedType == 2) {
                if (data.type == 1) {
                    $("#expression").attr('disabled', true);
                    $("#expectedKey").attr('disabled', true);
                    $("#expectedValue").attr('disabled', true);
                }
            } else {
                if (expectedType == 0 || expectedType == 3) {
                    $("#expectedKey").attr('disabled', true);
                }
            }
        }

    }
}


/**
 * 清空表单内容
 * @param formID
 */
function clearFormData(formID) {
    $("#" + formID)[0].reset();
    $("#" + formID).find("label").css("display", "none");
}


