﻿var table_options_search_overview_addCaseVeriable = {
    url: '/variable/getCaseVariableList',
    method: 'post',                      //请求方式（*）
    striped: true,                      //是否显示行间隔色
    cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
//        pagination: true,                   //是否显示分页（*）
    sortable: false,                     //是否启用排序
    clickToSelect: true,
    toolbar: '#toolbar2',                 //工具按钮使用日期
    sortOrder: "asc",                   //排序方式
    sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
    showColumns: false,                  //是否显示所有的列
    height: 530,
    pageNumber: 1,                       //初始化加载第一页，默认第一页
    pageSize: 10,                       //每页的记录行数（*）
    pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
//        clickToSelect: true,                // 是否启用点击选中行
//        showExport: true,                     //是否显示导出
    showRefresh: true,
//        exportDataType: "all",              //basic', 'all', 'selected'.
//        silent: true,
    queryParams: queryParamsAddCaseVariable, //参数
    columns: [{
        field: 'vId',
        title: 'ID',
        align: "center",
        width: "20",
    }, {
        field: 'caseId',
        title: '用例ID',
        align: "center",
        width: "20",
    },
        {
            field: 'v_name',
            title: '变量名',
            align: "center",
            width: "100"
        },
        {
            field: 'v_value',
            title: '表达式',
            align: "center",
            width: "100",
            formatter: function (value, row, index) {
                if (row.v_value.length > 30) {
                    return row.v_value.substr(0, 30) + "...";
                } else {
                    return row.v_value
                }
            }
        }
        , {
            field: '操作',
            title: '操作',
            align: "center",
            width: "90",
            formatter: function (value, row, index) {
                var var_modify = '<div class="btn-group">' +
                    ' <button id="addGroupButton" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  onclick="showUpdatevariableModal(\'' + row.vId + '\' )">修改</button></div>';
                var var_delete = '<div class="btn-group">' +
                    ' <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="delVariable(\'' + row.vId + '\' )">删除 </button></div>';
                return var_modify + var_delete
            }
        }
    ],
    onLoadSuccess: function (data) {
//                $('#taskOverviewTable').val(data.beginTime+' 至 '+data.endTime);
    }


};

// 变量窗口参数处理
function queryParamsAddCaseVariable(params) {
    return {
        limit: params.limit,
        offset: params.offset,
        caseId: _caseId,
    };
}

/**
 * 更新变量
 */
function updateVariable() {
    function upConf() {
        if (confirm("修改可能会影响到目前使用该记录的用例，真的要修改吗?")) {
            return true;
        } else {
            return false;
        }
    }

    if (upConf()) {
        $("#addVariable_caseId").val(_caseId);
        if (formCheck()) {
            $.ajax({
                type: "POST",
                url: "/variable/modifyCaseVariable",
                dataType: "json",
                data: {
                    caseId: $("#addVariable_caseId").val(),
                    vId: $("#addVariable_vId").val(),
                    v_name: $("#add_vname").val(),
                    v_value: $("#add_vkey").val()
                    // description: $("#add_description").val()
                },
                success: function (data) {
                    var dataJson = JSON.stringify(data);
                    var result = JSON.parse(dataJson);
                    if (result.code == 0) {
                        alert("修改成功！");
                        $('#variableModal').modal('hide');
                        clearFormData("variableForm");
                        $('#OverviewTableCaseVariableList').bootstrapTable('refresh');
                        return;
                    } else {
                        alert(result.message);
                        return;
                    }
                },
                error: function () {
                    alert("添加异常！");
                }
            });
        }
    }

}


/**
 * 显示修改窗口
 */
function showUpdatevariableModal(vId) {
    $('#addVariable_vId').val(vId);
    $('#variableModalLabel').text("修改用例变量");
    $("#btn_add").css("display", "none");
    $('#btn_modify').css('display', 'block');
    $.ajax({
        type: "get",
        url: "/variable/getCaseVariableById",
        dataType: "json",
        data: {
            vId: vId
        },
        success: function (data) {
            var dataJson = JSON.stringify(data);
            var result = JSON.parse(dataJson);
            if (result.code == 0) {
                clearFormData("variableForm");
                _caseId = result.data.caseId
                $('#addVariable_vId').val(result.data.vId);
                $('#add_vname').val(result.data.v_name);
                $('#add_vkey').val(result.data.v_value);
                // $('#add_description').val(result.data.description);
                $('#variableModal').modal('show');

                return;
            } else {
                alert(result.message);
                result;
            }
        },
        error: function () {
            alert("修改异常！");
        }
    });

}


/**
 * 添加变量
 */
function insertVariable(cId) {
    $("#addVariable_caseId").val(cId);
    if (formCheck()) {
        $.ajax({
            type: "POST",
            url: "/variable/addCaseVariable",
            dataType: "json",
            data: {
                caseId: $("#addVariable_caseId").val(),
                v_name: $("#add_vname").val(),
                v_value: $("#add_vkey").val()
                // description: $("#add_description").val()
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    alert("添加成功！");
                    $('#variableModal').modal('hide');
                    clearFormData("variableForm");
                    $('#OverviewTableCaseVariableList').bootstrapTable('refresh');
                    return;
                } else {
                    alert(result.message);
                    return;
                }
            },
            error: function () {
                alert("添加异常！");
            }
        });
    }
}


/**
 * 删除变量
 **/
function delVariable(vId) {

    function upConf() {
        if (confirm("删除可能会影响到目前使用该记录的用例，真的要删除吗??")) {
            return true;
        } else {
            return false;
        }
    }

    if (upConf()) {
        $.ajax({
            type: "get",
            url: "/variable/delCaseVariableById",
            dataType: "json",
            data: {
                vId: vId
            },
            success: function (data) {
                var dataJson = JSON.stringify(data);
                var result = JSON.parse(dataJson);
                if (result.code == 0) {
                    alert("删除成功！");
                    $('#OverviewTableCaseVariableList').bootstrapTable('refresh');
                    return;
                } else {
                    alert(result.message);
                    return;
                }
            },
            error: function () {
                alert("添加异常！");
            }
        });
    }
}


/**
 * 检查输入内容是否正确
 * @returns {boolean}
 */
function formCheck() {
    if ($("#add_vname").val() == "" || $("#add_vname").val().length == 0) {
        $("#vnameInfo").text("名称不能为空！");
        $("#vnameInfo").css("display", "block");
        return false;
    }
    else if ($("#add_vkey").val() == "" || $("#add_vkey").val().length == 0) {
        $("#vnameInfo").css("display", "none");
        $("#vkeyInfo").text("表达式不能为空！");
        $("#vkeyInfo").css("display", "block");
        return false;
    } else {
        $("#variableForm").find("label").css("display", "none");
        return true;
    }
}


// 显示添加变量窗口
function showAddVariableModal() {
    $('#variableModalLabel').text("添加用例变量");
    $('#btn_variable_modify').css("display", "none");
    $('#btn_variable_add').css('display', 'block');
    $('#variableModal').modal('show');
}

// 显示添加变量窗口
function showAddVariableModalByCaseId(cId) {
    $('#addVariable_caseId').val(cId)
    $('#variableModalLabel').text("添加用例变量");
    $('#btn_variable_modify').css("display", "none");
    $('#btn_variable_add').css('display', 'block');
    $('#variableModal').modal('show');
}